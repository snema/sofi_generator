import re
import sys
import base_object
from os import walk


class Analysis(base_object.Base):
    name = ""


class LocatedVariables(Analysis):
    """ LOCATED_VARIABLES.h contain varibale pointers for plugins(space from 1.-> *)
    and dinamic addressing(space 0. only )
    Example:
    __LOCATED_VAR(UDINT,__QD0_1,Q,D,0,1)    //%QD0.1  enhancement dinamic address space
    __LOCATED_VAR(UINT,__IW0_1,I,W,0,1)     //%IW0.1  enhancement dinamic address space
    __LOCATED_VAR(WORD,__MW1_0_0_0,M,W,1,0,0,0) //another module for example modbus
    """
    name = "LOCATED_VARIABLES.h"
    size_type = {"X": 1, "B": 1, "W": 2, "D": 4, "L": 8}
    self_space_number = 0  # currently all modbus readble regs are located in 0. space
    self_locations = []
    masters_locations = []  # for modbus master
    locations = {}
    MAX_ADDRESS = 0xffff
    max_address_use = 0

    def handle_location(self):
        path_beremiz = self.path_project_src + '/' + self.name
        try:
            # IEC2C compiler generate a list of located variables :
            # LOCATED_VARIABLES.h
            location_file = open(path_beremiz, 'r', encoding="utf-8")
            # each line of LOCATED_VARIABLES.h declares a located variable
            num_lines_beremiz = sum(1 for line in open(path_beremiz, encoding="utf-8"))
            lines = location_file.readlines()
            location_file.close()
            # This regular expression parses the lines genereated by IEC2C
            LOCATED_MODEL = re.compile(
                r"__LOCATED_VAR\((?P<IEC_TYPE>[A-Z]*),(?P<NAME>[_A-Za-z0-9]*),"
                "(?P<DIR>[QMI])(?:,(?P<SIZE>[XBWDL]))?,(?P<LOC>[,0-9]*)\)")
            for current_line in range(num_lines_beremiz):
                # If line match RE,
                result = LOCATED_MODEL.match(lines[current_line])
                if result:
                    # Get the resulting dict
                    resdict = result.groupdict()
                    # rewrite string for variadic location as a tuple of
                    # integers
                    resdict['LOC'] = list(map(int, resdict['LOC'].split(',')))
                    # set located size to 'X' if not given
                    if not resdict['SIZE']:
                        resdict['SIZE'] = 'X'
                    resdict['LOC'].append(self.get_size(resdict['SIZE']))
                    # finally store into located variable list
                    if resdict['LOC'][0] == self.self_space_number:
                        if str(resdict['LOC'][0]) not in self.locations.keys():
                            self.locations[str(resdict['LOC'][0])] = []
                            self.locations[str(resdict['LOC'][0])].append(resdict)
                        else:
                            self.locations[str(resdict['LOC'][0])].append(resdict)
                        if self.check_loc_address_space(resdict):
                            self.self_locations.append(resdict)
                        else:
                            self.print_error('find intersection in global vars')
                    else:
                        self.masters_locations.append(resdict)
                        loc = str(resdict['LOC'][0]) + "_" + str(resdict['LOC'][1]) + "_" + str(resdict['LOC'][2])
                        if loc not in self.locations.keys():
                            self.locations[loc] = []
                            self.locations[loc].append(resdict)
                        else:
                            self.locations[loc].append(resdict)
            self.print_progress("local var use {}".format(self.max_address_use))
            self.print_progress(self.self_locations)
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_beremiz + '\n')

    def get_size(self, iec_size):
        return self.size_type[iec_size]

    def check_loc_address_space(self, local_var):
        """checks all LOCATED VARIABLE about intersection"""
        size = local_var['LOC'][2]
        start_address = local_var['LOC'][1] * size
        max_address = start_address + size
        if len(self.self_locations) == 0:
            self.max_address_use = max_address
        else:
            for i in range(len(self.self_locations)):
                size_temp = self.self_locations[i]['LOC'][2]
                start_address_temp = self.self_locations[i]['LOC'][1] * size_temp
                if (((start_address >= start_address_temp) &
                     (start_address < (start_address_temp + size_temp))) |
                        (((start_address + size) >= start_address_temp) &
                         ((start_address + size) < (start_address_temp + size_temp)))):
                    self.print_error("!!!LOCATED VARIABLE ERROR {}".format(local_var))
                    self.print_error("with {}".format(self.self_locations[i]))
                    return False
                if max_address <= self.MAX_ADDRESS:
                    if max_address > self.max_address_use:
                        self.max_address_use = max_address
                else:
                    self.print_error("!!!LOCATED VARIABLE ERROR MAX_ADDRESS!!!")
                    return False
        return True


class MB(Analysis):
    """ check MB_%number%.h and if exist in current build directory, will get info
    from """
    name = "MB_"
    names = []  # then will be name of found headers
    nodes_num = 0
    nodes = []  # list of nodes (dictionary)
    routes = []  # list of routes
    areas = []  # list of areas
    requests = []  # list of requests (dictionary)
    rs232_name = "RS232"
    rs485_1_name = "RS485-1"
    rs485_2_name = "RS485-2"
    routes_num = 0
    MAX_AREA_NUMBER = 30
    areas_num = 0

    def route_and_area_check(self, max_self_address_mdb, max_self_address_mdb_array):
        for i in range(len(self.names)):
            path_mb = self.path_project_src + '/' + self.names[i]
            num_lines_beremiz = sum(1 for line in open(path_mb, encoding="utf-8"))
            file_beremiz = open(path_mb, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            file_beremiz.close()
            number_of_route_nodes_model = re.compile(
                r"#define\s+TOTAL_ROUT_NODE_COUNT\s+(?P<NUMS>[0-9]+)")
            number_of_area = re.compile(
                r"#define\s+TOTAL_MODBUS_AREA_COUNT\s+(?P<NUMS>[0-9]+)")
            routes_number = 0
            total_areas_number = 0
            for current_line in range(num_lines_beremiz):
                if routes_number == 0:
                    # If line match RE,
                    result = number_of_route_nodes_model.match(lines[current_line])
                    if result:
                        # Get the resulting dict
                        result_dict = result.groupdict()
                        routes_number = int(result_dict['NUMS'])
                elif re.search("\s*static\s+route_node_t\s+route_nodes\s*\[\s*TOTAL_ROUT_NODE_COUNT\s*\]\s*=\s*\{",
                               lines[current_line]):
                    for line in range(current_line, num_lines_beremiz):
                        route_nodes_model = re.compile(
                            r"\s*\{\s*(?P<CHANNEL_FROM>[0-9A-Za-z_]+),\s*(?P<CHANNEL_TO>[0-9A-Za-z_]+),"
                            r"\s*(?P<ADDRESS>[0-9]+)\s*,\s*(?P<RES>[0-9]+)\s*\},*")
                        result = route_nodes_model.match(lines[line])
                        if result:
                            temp_route = {"CHANNEL_FROM": result["CHANNEL_FROM"],
                                          "CHANNEL_TO": result["CHANNEL_TO"],
                                          "ADDRESS": result["ADDRESS"],
                                          "RES": result["RES"],
                                          "ID": self.routes_num}
                            for j in range(self.routes_num):
                                if (int(result["ADDRESS"]) == int(self.routes[j]["ADDRESS"])) \
                                        and (result["CHANNEL_FROM"] == self.routes[j]["CHANNEL_FROM"]):
                                    self.print_error("have two same address and channel from in route")
                            self.routes.append(temp_route)
                            self.routes_num += 1
                        if re.search("\s*\};", lines[line]):
                            break
                if total_areas_number == 0:
                    # If line match RE,
                    result = number_of_area.match(lines[current_line])
                    if result:
                        # Get the resulting dict
                        result_dict = result.groupdict()
                        total_areas_number = int(result_dict['NUMS'])
                        if total_areas_number > self.MAX_AREA_NUMBER:
                            self.print_error("area increase max number {} > {}".format
                                             (total_areas_number, self.MAX_AREA_NUMBER))

                elif re.search("\s*static\s+area_node_t\s+area_nodes\s*"
                               "\[\s*TOTAL_MODBUS_AREA_COUNT\s*\]\s*=\s*\{", lines[current_line]):
                    '''static area_node_t	area_nodes[TOTAL_MODBUS_AREA_COUNT] = {'''
                    for line in range(current_line, num_lines_beremiz):
                        '''{"1_0_1", COILS_01, 1, 0}'''
                        area_model = re.compile(r"\s*\{\s*\"(?P<LOCATION>[0-9_]+)\",\s*(?P<FUNC_TYPE>[0-9A-Za-z_]+),"
                                                r"\s*(?P<REGS_NUMBER>[0-9]+),\s*(?P<ADDRESS>[0-9]+)\s*,\s*NULL\s*\},*")
                        result = area_model.match(lines[line])
                        if result:
                            temp_area = {"LOCATION": result["LOCATION"],
                                         "FUNC_TYPE": result["FUNC_TYPE"],
                                         "REGS_NUMBER": result["REGS_NUMBER"],
                                         "ADDRESS": result["ADDRESS"],
                                         "ID": self.areas_num}
                            if self.check_address_space_for_area(self.areas, temp_area,
                                                                 max_self_address_mdb, max_self_address_mdb_array):
                                self.areas.append(temp_area)
                                self.areas_num += 1
                                if self.areas_num > self.MAX_AREA_NUMBER:
                                    self.print_error("area increase max number {} > {}".format(self.areas_num,
                                                                                               self.MAX_AREA_NUMBER))
                        if re.search("\s*\};", lines[line]):
                            break
        return self.routes_num

    def node_check(self):
        for i in range(len(self.names)):
            path_mb = self.path_project_src + '/' + self.names[i]
            num_lines_beremiz = sum(1 for line in open(path_mb, encoding="utf-8"))
            file_beremiz = open(path_mb, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            file_beremiz.close()
            number_of_rtuclient_nodes_model = re.compile(
                r"#define\s+NUMBER_OF_RTUCLIENT_NODES\s(?P<NUMS>[0-9]+)")
            have_nodes = 0
            for current_line in range(num_lines_beremiz):
                if have_nodes == 0:
                    # If line match RE,
                    result = number_of_rtuclient_nodes_model.match(lines[current_line])
                    if result:
                        # Get the resulting dict
                        result_dict = result.groupdict()
                        have_nodes = int(result_dict['NUMS'])
                elif re.search("\s*static\s+client_node_t\s+client_nodes\s*"
                               "\[\s*NUMBER_OF_CLIENT_NODES\s*\]\s*=\s*\{", lines[current_line]):
                    for line in range(current_line, num_lines_beremiz):
                        client_nodes_model = re.compile(
                            r"\s*\{\s*\"(?P<NODE_NAME>[0-9\.]+)\",\s*\{\s*naf_rtu\s*\,\s*\{\s*\.rtu\s*=\s*"
                            r"\{\s*\"(?P<CHANNEL_NAME>[0-9A-Za-z_]+)\"\s*,\s*(?P<BAUD_RATE>[0-9A-Za-z_]+)\s*/\*baud\*/,"
                            r"\s*(?P<PARITY>[0-9]+)\s*/\*parity\*/,\s*(?P<DATA_BITS>[0-9]+)\s*/\*data\s+bits\*/,"
                            r"\s*(?P<STOP_BIT>[0-9]+),\s*(?P<IGNORE_ECHO>[0-9]+)\s*/\*\s*ignore\s+echo\s*\*/\}\}\},"
                            r"\s*(?P<MB_ND>[\-0-9]+)\s*/\*\s*mb_nd\s*\*/,\s*(?P<INIT_STATE>[0-9]+)\s*"
                            r"/\*\s*init_state\s*\*/,\s*(?P<COMMUNICATION_PERIOD>[0-9]+)\s*/\*\s*communication\s+"
                            r"period\s*\*/\},*")
                        result = client_nodes_model.match(lines[line])
                        if result:
                            if self.find_same_node_by_channel(result["CHANNEL_NAME"]):
                                self.print_error("found two nodes with same channel")
                                return -1
                            else:
                                temp_node = {"NODE_NAME": result["NODE_NAME"],
                                             "CHANNEL_NAME": result["CHANNEL_NAME"],
                                             "BAUD_RATE": result["BAUD_RATE"],
                                             "PARITY": result["PARITY"],
                                             "DATA_BITS": result["DATA_BITS"],
                                             "STOP_BIT": result["STOP_BIT"],
                                             "IGNORE_ECHO": result["IGNORE_ECHO"],
                                             "MB_ND": result["MB_ND"],
                                             "PERIOD": result["COMMUNICATION_PERIOD"],
                                             "ID": self.nodes_num}
                                if temp_node["PARITY"] == "1" or \
                                        temp_node["PARITY"] == "2":
                                    if temp_node["DATA_BITS"] == "9":
                                        self.print_error("in modbus master {} excess data bits \n"
                                                         "if use bit parity max data bits 8".
                                                         format(temp_node["NODE_NAME"]))
                                    elif temp_node["DATA_BITS"] == "8":
                                        temp_node["DATA_BITS"] = "9"
                                    elif temp_node["DATA_BITS"] == "7":
                                        temp_node["DATA_BITS"] = "8"
                                self.nodes.append(temp_node)
                                self.nodes_num += 1
                        if re.search("\s*\};", lines[line]):
                            break
        return 0

    def request_check(self, locations):
        for i in range(len(self.names)):
            path_mb = self.path_project_src + '/' + self.names[i]
            num_lines_beremiz = sum(1 for line in open(path_mb, encoding="utf-8"))
            file_beremiz = open(path_mb, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            file_beremiz.close()
            number_of_rtuclient_reqts = re.compile(
                r"#define\s+NUMBER_OF_RTUCLIENT_REQTS\s(?P<NUMS>[0-9]+)")
            have_nodes = 0
            for current_line in range(num_lines_beremiz):
                if have_nodes == 0:
                    # If line match RE,
                    result = number_of_rtuclient_reqts.match(lines[current_line])
                    if result:
                        # Get the resulting dict
                        result_dict = result.groupdict()
                        have_nodes = int(result_dict['NUMS'])
                elif re.search("\s*static\s+client_request_t\s+client_requests\s*"
                               "\[\s*NUMBER_OF_CLIENT_REQTS\s*\]\s*=\s*\{", lines[current_line]):
                    current_line += 1
                    for line in range(current_line, num_lines_beremiz):
                        request_model_1 = re.compile(
                            r"\s*\{\"(?P<LOCATION>[0-9_]+)\",\s*(?P<NODE_ID>[0-9]+),\s*(?P<SLAVE_ADDRESS>[0-9]+),"
                            r"\s*(?P<REQ_TYPE>[_a-zA-Z0-9]+),\s*(?P<FUNCTION>[0-9]+),\s*(?P<START_ADDRESS>[0-9]+)\s*,\s*(?P<REG_NUM>[0-9]+),")
                        result = request_model_1.match(lines[line])
                        if result:
                            if self.check_address_space(locations, result["LOCATION"], result["REG_NUM"]):
                                node_id = self.find_id_by_location(result["LOCATION"])
                                if node_id >= 0:
                                    temp_request = {"LOCATION": result["LOCATION"],
                                                    "NODE_ID": node_id,
                                                    "SLAVE_ADDRESS": result["SLAVE_ADDRESS"],
                                                    "REQ_TYPE": result["REQ_TYPE"],
                                                    "FUNCTION": result["FUNCTION"],
                                                    "START_ADDRESS": result["START_ADDRESS"],
                                                    "REG_NUM": result["REG_NUM"],
                                                    }
                                    if temp_request["FUNCTION"] in ["5", "6"]:
                                        temp_request["REG_NUM"] = "1"
                                    line += 1
                                    request_model_2 = re.compile(r"\s*(?P<RETRIES>[_a-zA-Z0-9]+),"
                                                                 r"\s*(?P<ERROR_CODE>[0-9_]+)\s*/\*\s*error_code\s*\*/,"
                                                                 r"\s*(?P<PREV_CODE>[0-9]+)\s*/\*\s*prev_code\s*\*/,"
                                                                 r"\s*\{(?P<SEC_TIME_OUT>[0-9]+),"
                                                                 r"\s*(?P<NANO_SEC_TIME_OUT>[0-9]+)\}\s*/\*\s*timeout\s*\*/,")
                                    result = request_model_2.match(lines[line])
                                    if result:
                                        temp_request["RETRIES"] = result["RETRIES"]
                                        temp_request["ERROR_CODE"] = result["ERROR_CODE"]
                                        temp_request["PREV_CODE"] = result["PREV_CODE"]
                                        temp_request["SEC_TIME_OUT"] = result["SEC_TIME_OUT"]
                                        temp_request["NANO_SEC_TIME_OUT"] = result["NANO_SEC_TIME_OUT"]
                                        line += 1
                                        request_model_3 = re.compile(r"\{(?P<PLC_BUFFER>[0-9,]+)\},"
                                                                     r"\s*\{(?P<COM_BUFFER>[0-9,]+)\}\},*")
                                        result = request_model_3.match(lines[line])
                                        if result:
                                            time_out_check = self.check_time_out(temp_request)
                                            if time_out_check > 0:
                                                temp_request["PLC_BUFFER"] = result["PLC_BUFFER"]
                                                temp_request["COM_BUFFER"] = result["COM_BUFFER"]
                                                if (temp_request["FUNCTION"] in ["3", "4", "6", "16"]) and \
                                                        int(temp_request["FUNCTION"]) > 120:
                                                    self.print_error("mdb request error location -{}:"
                                                                     "max regs number is 120 for this function".
                                                                     format(temp_request["LOCATION"]))
                                                else:
                                                    self.requests.append(temp_request)
                                            else:
                                                self.print_error("mdb request error location -{}:"
                                                                 "request time_out more then period ".
                                                                 format(temp_request["LOCATION"]))
                                                return -1
                                else:
                                    self.print_error("don't find node {} of request".format(node_id))
                                    return -1
                            else:
                                self.print_error("Modbus address increase")
                                return -1
                        if re.search("\s*\};", lines[line]):
                            break
        return len(self.requests)

    @staticmethod
    def check_address_space(locations, current_location, regs_number):
        if current_location in locations.keys():
            for i in range(len(locations[current_location])):
                number = locations[current_location][i]['LOC'][3]
                size = locations[current_location][i]['LOC'][4]
                if (number * size + size) > (int(regs_number) * 2):
                    return 0
        return 1

    def check_address_space_for_area(self, areas, area, max_self_address_mdb, max_self_address_mdb_array):
        res = 1
        regs = int(area["REGS_NUMBER"])
        address = int(area["ADDRESS"])
        if address<0:
            self.print_progress("modbus not used for {} because address les then zero ".format(area["LOCATION"]))
            res = 0
        if (address + regs) > 65535:
            self.print_error("modbus area space excess id - {} function - {}".format(area["ID"], area["FUNC_TYPE"]))
            res = 0
        if res:
            for area_temp in areas:
                if area["FUNC_TYPE"] == area_temp["FUNC_TYPE"]:
                    regs_temp = int(area_temp["REGS_NUMBER"])
                    address_temp = int(area_temp["ADDRESS"])
                    if ((address >= address_temp) and (address < (address_temp + regs_temp))) or \
                            ((address + regs > address_temp) and ((address + regs) <= (address_temp + regs_temp))):
                        self.print_error("modbus area intersection between id - {} function - {} and id - {}"
                                         .format(area["ID"], area["FUNC_TYPE"], area_temp["ID"],
                                                 area_temp["FUNC_TYPE"]))
                        res = 0
        # check intersections between areas and self modbus area
        if res:
            if area["FUNC_TYPE"] in ["HOLDING_REGISTERS_03", "INPUT_REGISTERS_04"]:
                if max_self_address_mdb:
                    if (((address + regs) >= self.BASE_SELF_MDB_ADDRESS) and
                        ((address + regs) < (self.BASE_SELF_MDB_ADDRESS + max_self_address_mdb))) or \
                            ((address >= self.BASE_SELF_MDB_ADDRESS) and
                             (address < (self.BASE_SELF_MDB_ADDRESS + max_self_address_mdb))):
                        self.print_error("modbus area address intersections between {} "
                                         "and self user address space 40000 + {}".format(area["ID"],
                                                                                         max_self_address_mdb))
                        res = 0
                if res and max_self_address_mdb_array:
                    if (((address + regs) >= self.BASE_SELF_MDB_ADDRESS_ARRAY) and
                        ((address + regs) < (self.BASE_SELF_MDB_ADDRESS_ARRAY + max_self_address_mdb_array))) or \
                            ((address >= self.BASE_SELF_MDB_ADDRESS_ARRAY) and
                             (address < (self.BASE_SELF_MDB_ADDRESS_ARRAY + max_self_address_mdb_array))):
                        self.print_error("modbus area address intersections between {} "
                                         "and self array user address space 30000 + {}".
                                         format(area["ID"], max_self_address_mdb_array))
                        res = 0
        return res

    def find_same_node_by_channel(self, channel):
        """req location look like 1_0_1,node location look like 1.0 """
        for i in range(len(self.nodes)):
            if self.nodes[i]["CHANNEL_NAME"] == channel:
                return 1
        return 0

    def find_id_by_location(self, req_location):
        """req location look like 1_0_1,node location look like 1.0 """
        for i in range(len(self.nodes)):
            loc_list_node = list(map(int, self.nodes[i]["NODE_NAME"].split('.')))
            loc_list_req = list(map(int, req_location.split('_')))
            if loc_list_req[0] == loc_list_node[0] and loc_list_req[1] == loc_list_node[1]:
                return i
        return -1

    def find_mb_headers(self):
        """find MB_{number}.h files in dir"""
        number = 0
        for (dirpath, dirnames, filenames) in walk(self.path_project_src):
            for i in range(len(filenames)):
                if re.search("MB_\d+\.h", filenames[i]):
                    self.names.append(filenames[i])
                    number += 1
                    self.print_progress(filenames[i])
        return number

    def check_time_out(self, request):
        """control all request, timeout should be less then invocation time"""
        result = -1
        timeout = int(request["SEC_TIME_OUT"]) * 1000
        timeout += int(request["NANO_SEC_TIME_OUT"]) // 1000000
        node = self.find_id_by_location(request["LOCATION"])
        if node >= 0:
            period = int(self.nodes[node]["PERIOD"])
            if period >= timeout:
                result = 1
        return result

    def add_area(self, location, function_type, regs_number, address,
                 max_self_address_mdb, max_self_address_mdb_array):
        temp_area = {"LOCATION": location,
                     "FUNC_TYPE": function_type,
                     "REGS_NUMBER": regs_number,
                     "ADDRESS": address,
                     "ID": self.areas_num}
        if self.check_address_space_for_area(self.areas, temp_area,
                                             max_self_address_mdb, max_self_address_mdb_array):
            self.areas.append(temp_area)
            self.areas_num += 1
            if self.areas_num > self.MAX_AREA_NUMBER:
                self.print_error("area increase max number {} > {}".format(self.areas_num,
                                                                           self.MAX_AREA_NUMBER))


class Config(Analysis):
    """ config.h contain extern variable declaration
    Example:
        __DECLARE_GLOBAL_PROTOTYPE(WORD,REQ5)
        __DECLARE_GLOBAL_PROTOTYPE(__ARRAY_OF_WORD_6,REQ6)
    """
    name = "config.h"
    array_config_list = []
    array_prototype = {"name": "", "type": "", "size": 1}

    def handle_array_prototype(self):
        path_beremiz = self.path_project_src + '/' + self.name
        try:
            config = open(path_beremiz, 'r', encoding="utf-8")
            num_lines_beremiz = sum(1 for line in open(path_beremiz, encoding="utf-8"))
            lines = config.readlines()
            config.close()
            array_prototype = re.compile(r"\s*__DECLARE_GLOBAL_PROTOTYPE\s*\(\s*__ARRAY_OF_"
                                         r"(?P<TYPE>[0-9A-Za-z_]+)_(?P<SIZE>[0-9]+)\s*,\s*(?P<NAME>[0-9A-Za-z_]+)\s*\)\s*")
            for current_line in range(num_lines_beremiz):
                prototype = array_prototype.match(lines[current_line])
                if prototype:
                    array = dict(self.array_prototype)
                    array["name"] = prototype["NAME"]
                    array["type"] = prototype["TYPE"]
                    array["size"] = prototype["SIZE"]
                    self.array_config_list.append(array)
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_beremiz + '\n')
