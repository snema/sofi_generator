import re
import sys
import base_object


class Analysis(base_object.Base):
    name = ""


class Plc(Analysis):
    """we have to find  config variable and they description"""
    name = "plc.xml"

    var_template = {"name": "", "description": "", "retain": 0, "constant": 0,
                    "address": "", "init_value": 0, "type": "", "size": 1, "dimension_lower": 0, "dimension_upper": 0, "array": 0,
                    "byte_size": 0, "start_byte": 0}
    config_variables_list = []
    max_address = 0
    MAX_ADDRESS = 10000
    company_name = "Unnamed"
    product_name = "Unnamed"
    version = 1
    creation_time = "2000"
    project_name = "Unnamed"
    modification_time = "2000"

    def find_vars_description(self):
        path_beremiz = self.path_project_xml + '/' + self.name
        try:
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            self.num_lines = sum(1 for line in open(path_beremiz, encoding="utf-8"))
            lines = file_beremiz.readlines()
            current_line = 0
            template_var_name = re.compile(r"\s*\<\s*variable\s+name\s*\=\s*\"(?P<NAME>[0-9A-Za-z_]*)\"\s*(address\=\")?(?P<ADDRESS>[0-9A-Za-z_\%\.]*)\"*\>")
            template_var_desc = re.compile(r"\s*\<\s*xhtml\:p\>\s*\<\!\s*\[CDATA\[(?P<DESCRIPTION>[\s\w\d\-\(\)\[\]\.\,\:\/\+\%\<\>\{\}\#a-zA-zа-яА-яёЁ]*)\]\]\>\<\/xhtml\:p\>")
            global_vars_template = re.compile(r"\s*\<globalVars\s*(?P<OPTION>[\d\w\=\"]*)\>")
            type_templ = re.compile(r"\s*\<\s*(?P<TYPE>[\d\w]*)\/\>")
            init_value_templ = re.compile(r"\s*\<\s*simpleValue\s+value\=\"(?P<VALUE>[0-9\.]*)\"\/\>")
            array_dimension_templ = re.compile(r"\s*\<\s*dimension\s+lower\s*\=\s*\"(?P<LOWER>[0-9\.]*)\"\s+upper\s*\=\s*\"(?P<UPPER>[0-9\.]*)\"\/\>")
            variable_num = 0
            while current_line < len(lines):
                #wait config vars section
                var_header = global_vars_template.match(lines[current_line])
                if var_header:
                    self.print_progress("handling config VARIABLES")
                    while current_line < self.num_lines:
                        current_line += 1
                        var_name = template_var_name.match(lines[current_line])
                        if re.search('\s*\<\s*\/globalVars\s*\>',lines[current_line]):
                            break
                        elif var_name:
                            #pass only global vars
                            var_dict = dict(self.var_template)
                            var_dict["name"] = var_name["NAME"].upper()#save name
                            var_dict["address"] = var_name["ADDRESS"]#address if exict
                            self.print_progress("begin to find description")
                            if var_header["OPTION"] == "retain=\"true\"":
                                var_dict["retain"] = 1 #check retain option
                            elif var_header["OPTION"] == "constant=\"true\"":
                                var_dict["constant"] = 1 #check constant option -> read only
                            while current_line < self.num_lines:
                                current_line += 1
                                var_desc = template_var_desc.match(lines[current_line])
                                if re.search('\s*\<\s*\/variable\s*\>',lines[current_line]):
                                    break
                                elif re.search('\s*\<\s*type\s*\>',lines[current_line]):
                                    while current_line < self.num_lines:
                                        current_line += 1
                                        if re.search('\s*\<\s*\/type\s*\>', lines[current_line]):
                                            break
                                        elif re.search('\s*\<\s*array\s*\>', lines[current_line]):
                                            '''find array options'''
                                            var_dict["array"] = 1
                                            while current_line < self.num_lines:
                                                current_line += 1
                                                array_dimension = array_dimension_templ.match(lines[current_line])
                                                if re.search('\s*\<\s*\/array\s*\>', lines[current_line]):
                                                    break
                                                elif array_dimension:
                                                    var_dict["dimension_lower"] = int(array_dimension["LOWER"])
                                                    var_dict["dimension_upper"] = int(array_dimension["UPPER"])
                                                    var_dict["size"] = (var_dict["dimension_upper"] - var_dict["dimension_lower"])+1
                                                elif re.search('\s*\<\s*baseType\s*\>', lines[current_line]):
                                                    current_line += 1
                                                    '''find type'''
                                                    var_type = type_templ.match(lines[current_line])
                                                    if var_type:
                                                        var_dict["type"] = var_type["TYPE"]
                                                        var_dict["byte_size"] = self.iec_type_size(var_dict["type"]) * \
                                                                                var_dict["size"]
                                                        var_dict["start_byte"] = self.iec_type_size(var_dict["type"]) * \
                                                                                var_dict["dimension_lower"]
                                        else:
                                            var_type = type_templ.match(lines[current_line])
                                            if var_type:
                                                var_dict["type"] = var_type["TYPE"]
                                                var_dict["byte_size"] = self.iec_type_size(var_dict["type"])


                                elif re.search('\<initialValue\>',lines[current_line]):
                                    current_line += 1
                                    if re.search('\<\/initialValue\>', lines[current_line]):
                                        break
                                    elif re.search('\<arrayValue\>',lines[current_line]):
                                        init_array = []
                                        while current_line < self.num_lines:
                                            current_line += 1
                                            if re.search('\s*\<\s*\/arrayValue\s*\>', lines[current_line]):
                                                var_dict["init_value"] = init_array
                                                break
                                            elif re.search('\s*\<\s*value\s*\>', lines[current_line]):
                                                while current_line < self.num_lines:
                                                    current_line += 1
                                                    if re.search('\s*\<\s*\/value\s*\>', lines[current_line]):
                                                        break
                                                    var_init_value = init_value_templ.match(lines[current_line])
                                                    if var_init_value:
                                                        init_array.append(var_init_value["VALUE"])
                                    else:
                                        var_init_value = init_value_templ.match(lines[current_line])
                                        if var_init_value:
                                            var_dict["init_value"] = var_init_value["VALUE"]
                                elif var_desc:
                                    #we found vars description
                                    self.print_progress("found description")
                                    var_dict["description"] = var_desc["DESCRIPTION"]
                            self.check_array_variable(var_dict)
                            self.config_variables_list.append(var_dict)
                            variable_num += 1
                current_line += 1
            file_beremiz.close()
            #self.print_error(self.config_variables_list)
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_beremiz + '\n')

    def check_array_variable(self, var):
        """checks all array about intersection"""
        if var["array"]:
            size = var['byte_size']
            start_address = var['start_byte']
            max_address = start_address + size
            if max_address > self.max_address:
                self.max_address = max_address
            for i in range(len(self.config_variables_list)):
                if self.config_variables_list[i]["array"]:
                    size_temp = self.config_variables_list[i]['byte_size']
                    start_address_temp = self.config_variables_list[i]['start_byte']
                    if (((start_address >= start_address_temp) &
                            (start_address < (start_address_temp + size_temp))) |
                        (((start_address + size) >= start_address_temp) &
                         ((start_address + size) < (start_address_temp + size_temp)))):
                        self.print_error("!!!ARRAY ADDRESS ERROR conflict {} {} {} {} {}".format(var, start_address,start_address_temp,size,size_temp))
                        self.print_error("with {}".format(self.config_variables_list[i]))
                        return False
                    if max_address <= self.MAX_ADDRESS:
                        if max_address > self.max_address:
                            self.max_address = max_address
                    else:
                        self.print_error("!!!ARRAY ADDERS to much (>10000)!!!")
                        return False
            return True

    def find_project_description(self):
        """find project descriptions: companyName,projectName,version"""
        path_beremiz = self.path_project_xml + '/' + self.name
        try:
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            current_line = 0
            while current_line < len(lines):
                template_file_header = re.compile(r"\s*\<fileHeader\s+companyName=\"(?P<COMPANY_NAME>[\w\W]+)\"\s+productName=\"(?P<PRODUCT_NAME>[\w\W]*)\"\s+productVersion=\"(?P<VERSION>[\w\W]+)\s+creationDateTime=\"(?P<CREATION_TIME>[\w\W]+)\"\/\>")
                template_content_header = re.compile(r"\s+\<contentHeader\s+name=\"(?P<PROJECT_NAME>[\w\W]+)\"\s+modificationDateTime=\"(?P<MODIFICATION_TIME>[\s\:\-\w]+)\"[\w\W]*\>")
                file_header = template_file_header.match(lines[current_line])
                if file_header:
                    self.company_name = file_header['COMPANY_NAME']
                    self.product_name = file_header['PRODUCT_NAME']
                    self.version = re.findall(r'\d+', file_header['VERSION'])
                    self.creation_time = file_header['CREATION_TIME']
                else:
                    file_header = template_content_header.match(lines[current_line])
                    if file_header:
                        self.project_name = file_header['PROJECT_NAME']
                        self.modification_time = file_header['MODIFICATION_TIME']
                        template_time = re.compile(r"(?P<YEAR>[\d]+)-(?P<MONTH>[\d]+)-(?P<DAY>[\d]+)\w+(?P<HOUR>[\d]+):(?P<MIN>[\d]+):(?P<SEC>[\d]+)\>")
                        time_parser = template_time.match(self.modification_time)
                        if time_parser:
                            self.print_progress("modification time recognized")
                        else:
                            self.print_progress("modification time not recognized")
                current_line += 1
            file_beremiz.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_beremiz + '\n')
        return
