import re
import sys
import base_object


class Analysis(base_object.Base):
    name = ""


class Plc(Analysis):
    name = "plc.st"
    init_global_variable = []
    resource_list = []
    num_lines = 0

    def find_resource(self):
        path_beremiz = self.path_project_src + '/' + self.name
        try:
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            self.num_lines = sum(1 for line in open(path_beremiz, encoding="utf-8"))
            lines = file_beremiz.readlines()
            current_line = 0
            while current_line < len(lines):
                if re.search('CONFIGURATION\s+config', lines[current_line]):
                    self.print_progress("handing config init function")
                    while current_line < self.num_lines:
                        current_line += 1
                        if re.search('END_CONFIGURATION', lines[current_line]):
                            self.print_progress("end handing config init function")
                            break
                        elif re.search('^\s*VAR_GLOBAL', lines[current_line]):
                            current_line = self.handing_global_variable_space(lines, current_line)
                        elif re.search('^\s*RESOURCE\s*\w+\s+\w+\s+\w+', lines[current_line]):
                            current_line = self.handing_resource_space(lines, current_line)
                current_line += 1
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_beremiz + '\n')

    def handing_global_variable_space(self, lines, current_line):
        """return line last"""
        self.print_progress("handing GLOBAL VAR space")
        const = 0
        if re.search('^\s*VAR_GLOBAL\s+CONSTANT', lines[current_line]):
            const = 1
        while current_line < self.num_lines:
            current_line += 1
            if re.search('^\s*END_VAR', lines[current_line]):
                break
            elif re.search('^\s*\w+\s*\:\s*\w+\s*\:\=\s*\d+\s*\;', lines[current_line]):
                variable = re.match('^\s*(\w+)\s*\:\s*(\w+)\s*\:\=\s*(\d+)\s*\;', lines[current_line])
                variable_dict = {"name": variable.group(1),
                                 "type": variable.group(2),
                                 "value": variable.group(3),
                                 "const": const}
                self.print_progress(variable_dict)
                self.init_global_variable.append(variable_dict)

        return current_line

    def handing_resource_space(self, lines, current_line):
        """return line last"""

        resource = re.match('^\s*RESOURCE\s*(\w+)\s+(\w+)\s+(\w+)', lines[current_line])
        resource_dict = {"name": resource.group(1),
                         "state": resource.group(2),
                         "type": resource.group(3),
                         "program_num": 0,
                         }
        self.print_progress("handing resource " + resource_dict["name"])
        current_line += 1
        task_list = []
        program_list = []
        while current_line < self.num_lines:
            if re.search('^\s*TASK\s+(\w+)\s*\(\s*'
                         'INTERVAL\s*\:\=\s*([\w\d\#\.]+)\s*\,\s*'
                         'PRIORITY\s*\:\=\s*(\d)\s*\)\;', lines[current_line]):

                task = re.match('^\s*TASK\s+(\w+)\s*\(\s*'
                                'INTERVAL\s*\:\=\s*([\w\d\#\.]+)\s*\,\s*'
                                'PRIORITY\s*\:\=\s*(\d)\s*\)\;',
                                lines[current_line])
                task_dict = {"name": task.group(1),
                             "interval": task.group(2),
                             "priority": task.group(3)}
                self.print_progress('find task' + task_dict.__str__())
                task_list.append(task_dict)
            if re.search('^\s*PROGRAM\s+(\w+)\s+(\w+)\s+(\w+)\s*\:\s*(\w+)\s*\;',
                         lines[current_line]):
                program = re.match('^\s*PROGRAM\s+(\w+)\s+(\w+)\s+(\w+)\s*\:\s*(\w+)\s*\;',
                                   lines[current_line])
                program_dict = {"instance": program.group(1),
                                "program_name": program.group(4)}
                self.print_progress('find program' + program_dict.__str__())
                program_list.append(program_dict)
            if re.search('^\s*END_RESOURCE', lines[current_line]):
                break
            current_line += 1
        resource_dict["task"] = task_list
        resource_dict["program"] = program_list
        self.resource_list.append(resource_dict)
        return current_line
