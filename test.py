import generator

project_path = "C:/Users/shoma/Desktop/BeremizExamples/modbus/build/"
lib_path = "C:/_work/sofi/free_rtos/lib/"
gcc_path = "C:/Beremiz/gcc_6_3_1/bin/"
cmake_path = "C:/Beremiz/cmake-3-13-1-win32-x86/bin/"


def main():
    generator.make(project_path, lib_path, gcc_path, cmake_path)


if __name__ == "__main__":
    main()
