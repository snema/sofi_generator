#include "os_service.h"
u32 put_preinit_data(void){
    u8 * flash;
    u8 * ram;
    flash =(u8*) &_sidata;	
    for (ram = (u8*)&_sdata;(u32)ram<(u32)(&_edata);ram++) {
        *ram = *flash;
        flash++;
    }
    return ((u32)(&_edata) - (u32)(&_sdata));
}
u32 clear_bss_data(void){
    u8 * ram;
    for (ram = (u8*)&_sbss;(u32)ram<(u32)(&_ebss);ram++) {
        *ram = 0;
    }
    return ((u32)(&_ebss) - (u32)(&_sbss));
}

