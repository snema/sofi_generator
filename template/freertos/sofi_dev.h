 
/*add includes below */
#include "type_def.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
int get_value_by_address(u32 address,u64* value,regs_flag_t flag);
int set_value_by_address(u32 address,u64 value,regs_flag_t flag);
/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
