 
/*add includes below */
#include "type_def.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
extern uint32_t _sidata;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _sbss;
extern uint32_t _ebss;
u32 put_preinit_data(void);
u32 clear_bss_data(void);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
