// Updater check
#include "regs.h"
#include "sofi_dev.h"
#include "sofi_beremiz.h"
#include "link_functions.h"
#include "math.h"
#include "string.h"
#include "os_service.h"
#include "modbus_master.h"
#define TASKS_NUM 0
#define PWM_RUN 256
const task_description_t task_description[TASKS_NUM] = {
    /*generator_comment:additional_vars !!!DON'T TOUCH TO THE COMMENT!!!*/

};
#ifndef BITY
#define BITY(z, x) (z >> x)
#endif

#ifndef BITZ
#define BITZ(y, x) (y << x)
#endif

extern link_functions_t *p_link_functions;
void READ_PARAM_UDINT_init__(READ_PARAM_UDINT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENABLE, 0, retain)
  __INIT_VAR(data__->ADDRESS, 0, retain)
  __INIT_VAR(data__->CHECK, 0, retain)
  __INIT_VAR(data__->VALUE, 0, retain)
}

// Code part
void READ_PARAM_UDINT_body__(READ_PARAM_UDINT *data)
{
  // Control execution
  __SET_VAR(data->, CHECK, , 0);
  if (!__GET_VAR(data->EN))
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  if (__GET_VAR(data->ENABLE, ))
  {
    int res;
    u64 value;
    regs_flag_t flag = U32_REGS_FLAG;
    res = get_value_by_address((u32)__GET_VAR(data->ADDRESS, ), &value, flag);
    if (res != 0)
    {
      __SET_VAR(data->, CHECK, , 0);
      __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    }
    else
    {
      __SET_VAR(data->, VALUE, , (IEC_DINT)value);
      __SET_VAR(data->, CHECK, , 1);
    }
  }
  else
  {
    __SET_VAR(data->, VALUE, , 0);
    __SET_VAR(data->, CHECK, , 0);
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_PARAM_body__()
void SET_PARAM_UDINT_init__(SET_PARAM_UDINT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENABLE, 0, retain)
  __INIT_VAR(data__->ADDRESS, 0, retain)
  __INIT_VAR(data__->VALUE, 0, retain)
  __INIT_VAR(data__->CHECK, 0, retain)
}

// Code part
void SET_PARAM_UDINT_body__(SET_PARAM_UDINT *data)
{
  // Control execution
  __SET_VAR(data->, CHECK, , 0);
  if (!__GET_VAR(data->EN))
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  if (__GET_VAR(data->ENABLE, ))
  {
    int res;
    regs_flag_t flag = U32_REGS_FLAG;
    res = set_value_by_address((u32)__GET_VAR(data->ADDRESS, ), (u64)__GET_VAR(data->VALUE, ), flag);
    if (res != 0)
    {
      __SET_VAR(data->, CHECK, , 0);
      __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    }
    else
    {
      __SET_VAR(data->, CHECK, , 1);
    }
  }
  else
  {
    __SET_VAR(data->, CHECK, , 0);
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // SET_PARAM_body__()
void READ_PARAM_UINT_init__(READ_PARAM_UINT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENABLE, 0, retain)
  __INIT_VAR(data__->ADDRESS, 0, retain)
  __INIT_VAR(data__->CHECK, 0, retain)
  __INIT_VAR(data__->VALUE, 0, retain)
}

void READ_PARAM_UINT_body__(READ_PARAM_UINT *data)
{
  // Control execution
  __SET_VAR(data->, CHECK, , 0);
  if (!__GET_VAR(data->EN))
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  if (__GET_VAR(data->ENABLE, ))
  {
    int res;
    u64 value;
    regs_flag_t flag = U16_REGS_FLAG;
    res = get_value_by_address((u32)__GET_VAR(data->ADDRESS, ), &value, flag);
    if (res != 0)
    {
      __SET_VAR(data->, CHECK, , 0);
      __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    }
    else
    {
      __SET_VAR(data->, VALUE, , (IEC_INT)value);
      __SET_VAR(data->, CHECK, , 1);
    }
  }
  else
  {
    __SET_VAR(data->, VALUE, , 0);
    __SET_VAR(data->, CHECK, , 0);
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_PARAM_body__()
void SET_PARAM_UINT_init__(SET_PARAM_UINT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENABLE, 0, retain)
  __INIT_VAR(data__->ADDRESS, 0, retain)
  __INIT_VAR(data__->VALUE, 0, retain)
  __INIT_VAR(data__->CHECK, 0, retain)
}

// Code part
void SET_PARAM_UINT_body__(SET_PARAM_UINT *data)
{
  // Control execution
  __SET_VAR(data->, CHECK, , 0);
  if (!__GET_VAR(data->EN))
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  if (__GET_VAR(data->ENABLE, ))
  {
    int res;
    regs_flag_t flag = U16_REGS_FLAG;
    res = set_value_by_address((u32)__GET_VAR(data->ADDRESS, ), (u64)__GET_VAR(data->VALUE, ), flag);
    if (res != 0)
    {
      __SET_VAR(data->, CHECK, , 0);
      __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    }
    else
    {
      __SET_VAR(data->, CHECK, , 1);
    }
  }
  else
  {
    __SET_VAR(data->, CHECK, , 0);
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // SET_PARAM_body__()
void READ_PARAM_USINT_init__(READ_PARAM_USINT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENABLE, 0, retain)
  __INIT_VAR(data__->ADDRESS, 0, retain)
  __INIT_VAR(data__->CHECK, 0, retain)
  __INIT_VAR(data__->VALUE, 0, retain)
}

void READ_PARAM_USINT_body__(READ_PARAM_USINT *data)
{
  // Control execution
  __SET_VAR(data->, CHECK, , 0);
  if (!__GET_VAR(data->EN))
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  if (__GET_VAR(data->ENABLE, ))
  {
    int res;
    u64 value;
    regs_flag_t flag = U8_REGS_FLAG;
    res = get_value_by_address((u32)__GET_VAR(data->ADDRESS, ), &value, flag);
    if (res != 0)
    {
      __SET_VAR(data->, CHECK, , 0);
      __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    }
    else
    {
      __SET_VAR(data->, VALUE, , (IEC_SINT)value);
      __SET_VAR(data->, CHECK, , 1);
    }
  }
  else
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    __SET_VAR(data->, VALUE, , 0);
    __SET_VAR(data->, CHECK, , 0);
  }
  return;
} // READ_PARAM_body__()
void SET_PARAM_USINT_init__(SET_PARAM_USINT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENABLE, 0, retain)
  __INIT_VAR(data__->ADDRESS, 0, retain)
  __INIT_VAR(data__->VALUE, 0, retain)
  __INIT_VAR(data__->CHECK, 0, retain)
}

// Code part
void SET_PARAM_USINT_body__(SET_PARAM_USINT *data)
{
  // Control execution
  __SET_VAR(data->, CHECK, , 0);
  if (!__GET_VAR(data->EN))
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  if (__GET_VAR(data->ENABLE, ))
  {
    int res;
    regs_flag_t flag = U8_REGS_FLAG;
    res = set_value_by_address((u32)__GET_VAR(data->ADDRESS, ), (u64)__GET_VAR(data->VALUE, ), flag);
    if (res != 0)
    {
      __SET_VAR(data->, CHECK, , 0);
      __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
    }
    else
    {
      __SET_VAR(data->, CHECK, , 1);
    }
  }
  else
  {
    __SET_VAR(data->, CHECK, , 0);
    __SET_VAR(data->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // SET_PARAM_body__()

void READ_DI_init__(READ_DI *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DI_OUT, 0, retain)
}

// Code part
void READ_DI_body__(READ_DI *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char di_name[] = "di_state";
  regs_template_t regs_template;
  regs_template.name = di_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, DI_OUT, , reg.value.op_u32);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_DI_body__()

void READ_DI_CNT_init__(READ_DI_CNT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DI_NUMBER, 0, retain)
  __INIT_VAR(data__->DI_CNT_VALUE, 0, retain)
}
// Code part
void READ_DI_CNT_body__(READ_DI_CNT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  char cnt_name[] = "di_cnt";
  regs_template_t regs_template;
  regs_template.name = cnt_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER, );
    address += shift;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, DI_CNT_VALUE, , (u64)reg.value.op_u64);
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_DI_CNT_body__()

void READ_DI_FREQ_init__(READ_DI_FREQ *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DI_NUMBER, 0, retain)
  __INIT_VAR(data__->DI_FREQ_VALUE, 0, retain)
}
// Code part
void READ_DI_FREQ_body__(READ_DI_FREQ *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  char freq_name[] = "di_freq";
  regs_template_t regs_template;
  regs_template.name = freq_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_f = 0.0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER, );
    address += shift;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, DI_FREQ_VALUE, , reg.value.op_f);
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // DI_FREQ_body__()

void READ_AI_STATE_init__(READ_AI_STATE *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->AI_STATE_VALUE, 0, retain)
}

// Code part
void READ_AI_STATE_body__(READ_AI_STATE *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char ai_state_name[] = "ai_state";
  regs_template_t regs_template;
  regs_template.name = ai_state_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, AI_STATE_VALUE, , reg.value.op_u16);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }

  return;
} // READ_AI_STATE_body__()

void READ_AI_init__(READ_AI *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->AI_NUMBER, 0, retain)
  __INIT_VAR(data__->AI_VALUE, 0, retain)
}

// Code part
void READ_AI_body__(READ_AI *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    __SET_VAR(data__->, AI_VALUE, , (u16)0);
  }
  // Initialise TEMP variables

  char ai_name[] = "ai_unit";
  regs_template_t regs_template;
  regs_template.name = ai_name;
  if (__GET_VAR(data__->AI_NUMBER, ) < 8)
  {
    if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
    {
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->AI_NUMBER, );
      address += shift;
      if (p_link_functions->regs_get((u16)address, &reg) == 0)
      {
        __SET_VAR(data__->, AI_VALUE, , (u16)reg.value.op_u32);
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_AI_body__()

void READ_AI_REAL_init__(READ_AI_REAL *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->AI_NUMBER, 0, retain)
  __INIT_VAR(data__->AI_VALUE, 0.0f, retain)
}

// Code part
void READ_AI_REAL_body__(READ_AI_REAL *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    __SET_VAR(data__->, AI_VALUE, , 0.0f);
  }
  // Initialise TEMP variables
  char ai_name[] = "ai_physical";
  regs_template_t regs_template;
  regs_template.name = ai_name;
  if (__GET_VAR(data__->AI_NUMBER, ) < 8)
  {
    if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
    {
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->AI_NUMBER, );
      address += shift;
      if (p_link_functions->regs_get((u16)address, &reg) == 0)
      {
        __SET_VAR(data__->, AI_VALUE, , reg.value.op_f);
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_AI_REAL_body__()

void READ_DO_SC_init__(READ_DO_SC *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DO_SC_FLAG, 0, retain)
  __INIT_VAR(data__->DO_SC_EN, 0, retain)
}

// Code part
void READ_DO_SC_body__(READ_DO_SC *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char do_name[] = "do_sc_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, DO_SC_FLAG, , (reg.value.op_u8 >> 4));
      __SET_VAR(data__->, DO_SC_EN, , (reg.value.op_u8 & 0x0F));
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_DO_SC_body__()

void READ_DO_init__(READ_DO *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DO_OUT, 0, retain)
}

// Code part
void READ_DO_body__(READ_DO *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char do_name[] = "do_state";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, DO_OUT, , reg.value.op_u8);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_DO_body__()

void READ_RESET_init__(READ_RESET *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->RESET_NUM, 0, retain)
  __INIT_VAR(data__->LAST_RESET, 0, retain)
}

// Code part
void READ_RESET_body__(READ_RESET *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char num_name[] = "reset_num";
  char last_name[] = "last_reset";
  regs_template_t regs_template;
  regs_template.name = num_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, RESET_NUM, , reg.value.op_u16);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
    regs_template.name = last_name;
    if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
    {
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if (p_link_functions->regs_get(address, &reg) == 0)
      {
        __SET_VAR(data__->, LAST_RESET, , reg.value.op_u16);
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_RESET_body__()

void READ_PWR_init__(READ_PWR *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->V_PWR, 0, retain)
  __INIT_VAR(data__->V_BAT, 0, retain)
}

// Code part
void READ_PWR_body__(READ_PWR *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char bat_name[] = "v_bat";
  char external_pwr_name[] = "v_pwr";
  regs_template_t regs_template;
  regs_template.name = bat_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, V_BAT, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  regs_template.name = external_pwr_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, V_PWR, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_PWR_body__()

void READ_INTERNAL_TEMP_init__(READ_INTERNAL_TEMP *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->INTERNAL_TEMP_OUT, 0, retain)
}

// Code part
void READ_INTERNAL_TEMP_body__(READ_INTERNAL_TEMP *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char int_temp_name[] = "internal_temp";
  regs_template_t regs_template;
  regs_template.name = int_temp_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, INTERNAL_TEMP_OUT, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_INTERNAL_TEMP_body__()

void READ_TEMP_init__(READ_TEMP *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->MCU_TEMP, 0, retain)
  __INIT_VAR(data__->ADC_TEMP, 0, retain)
}

// Code part
void READ_TEMP_body__(READ_TEMP *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char internal_name[] = "internal_temp";
  char external_name[] = "external_temp";
  regs_template_t regs_template;
  regs_template.name = internal_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, MCU_TEMP, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  regs_template.name = external_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, ADC_TEMP, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_PWR_body__()

void READ_SYS_TICK_COUNTER_init__(READ_SYS_TICK_COUNTER *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->SYS_TICK_COUNTER_VALUE, 0, retain)
}
// Code part
void READ_SYS_TICK_COUNTER_body__(READ_SYS_TICK_COUNTER *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char tick_name[] = "sys_tick_counter";
  regs_template_t regs_template;
  regs_template.name = tick_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      __SET_VAR(data__->, SYS_TICK_COUNTER_VALUE, , reg.value.op_u64);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_SYS_TICK_COUNTER_body__()

void WRITE_MDB_ADDRESS_init__(WRITE_MDB_ADDRESS *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->MDB_ADDR, 0, retain)
}

// Code part
void WRITE_MDB_ADDRESS_body__(WRITE_MDB_ADDRESS *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char mdb_name[] = "mdb_addr";
  regs_template_t regs_template;
  regs_template.name = mdb_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u16 = (u16)__GET_VAR(data__->MDB_ADDR, );
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_MDB_ADDRESS_body__()

void WRITE_UART_SETS_init__(WRITE_UART_SETS *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->MESO_UART, 0, retain)
  __INIT_VAR(data__->SET_RS_485_2, 0, retain)
  __INIT_VAR(data__->SET_RS_232, 0, retain)
  __INIT_VAR(data__->SET_RS_485_1, 0, retain)
  __INIT_VAR(data__->SET_RS_485_IMMO, 0, retain)
  __INIT_VAR(data__->SET_HART, 0, retain)
}

// Code part
void WRITE_UART_SETS_body__(WRITE_UART_SETS *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  const char name_uart1[] = "uart1_sets";
  const char name_uart2[] = "uart2_sets";
  const char name_uart3[] = "uart3_sets";
  const char name_uart5[] = "uart5_sets";
  const char name_uart6[] = "uart6_sets";
  const char name_uart7[] = "uart7_sets";
  const char *name;
  u16 uart_settings = 0;
  for (u8 i = 0; i < 6; i++)
  {
    regs_template_t regs_template;
    switch (i)
    {
    case (0):
      regs_template.name = name_uart1;
      uart_settings = (u16)__GET_VAR(data__->MESO_UART, );
      break;
    case (1):
      regs_template.name = name_uart2;
      uart_settings = (u16)__GET_VAR(data__->SET_RS_485_2, );
      break;
    case (2):
      regs_template.name = name_uart3;
      uart_settings = (u16)__GET_VAR(data__->SET_RS_232, );
      break;
    case (3):
      regs_template.name = name_uart5;
      uart_settings = (u16)__GET_VAR(data__->SET_RS_485_1, );
      break;
    case (4):
      regs_template.name = name_uart6;
      uart_settings = (u16)__GET_VAR(data__->SET_RS_485_IMMO, );
      break;
    case (5):
      regs_template.name = name_uart7;
      uart_settings = (u16)__GET_VAR(data__->SET_HART, );
      break;
    default:
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      break;
    }
    if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
    {
      regs_access_t reg;
      reg.value.op_u16 = uart_settings;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if (p_link_functions->regs_set(address, reg) == 0)
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
        break;
      }
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      break;
    }
  }
  return;
} // WRITE_UART_SETS_body__()

void WRITE_CH_TIMEOUT_init__(WRITE_CH_TIMEOUT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->CH_NUMBER, 0, retain)
  __INIT_VAR(data__->CHANNEL_TIMEOUT, 0, retain)
}
// Code part
void WRITE_CH_TIMEOUT_body__(WRITE_CH_TIMEOUT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char ch_timeout_name[] = "channels_timeout";
  regs_template_t regs_template;
  regs_template.name = ch_timeout_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.flag = regs_template.type;
    u8 address = regs_template.guid & GUID_ADDRESS_MASK;
    u8 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->CH_NUMBER, );
    address += shift;
    reg.value.op_u32 = __GET_VAR(data__->CHANNEL_TIMEOUT, );
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_CH_TIMEOUT_body__()

void WRITE_DI_NOISE_FLTR_10US_init__(WRITE_DI_NOISE_FLTR_10US *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DI_NUMBER, 0, retain)
  __INIT_VAR(data__->DI_NOISE_FLTR_VALUE_10US, 0, retain)
}
// Code part
void WRITE_DI_NOISE_FLTR_10US_body__(WRITE_DI_NOISE_FLTR_10US *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char noise_name[] = "di_noise_fltr_us";
  regs_template_t regs_template;
  regs_template.name = noise_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER, );
    address += shift;
    reg.value.op_u16 = __GET_VAR(data__->DI_NOISE_FLTR_VALUE_10US, );
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // DI_PULSELESS_body__()

void WRITE_DI_PULSELESS_init__(WRITE_DI_PULSELESS *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DI_NUMBER, 0, retain)
  __INIT_VAR(data__->DI_PULSELESS_VALUE, 0, retain)
}
// Code part
void WRITE_DI_PULSELESS_body__(WRITE_DI_PULSELESS *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char pulseless_name[] = "di_pulseless";
  regs_template_t regs_template;
  regs_template.name = pulseless_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER, );
    address += shift;
    reg.value.op_u32 = __GET_VAR(data__->DI_PULSELESS_VALUE, );
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_DI_PULSELESS_body__()

void WRITE_DI_MODE_init__(WRITE_DI_MODE *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DI_NUMBER, 0, retain)
  __INIT_VAR(data__->DI_MODE_VALUE, 0, retain)
}
// Code part
void WRITE_DI_MODE_body__(WRITE_DI_MODE *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char mode_name[] = "di_mode";
  regs_template_t regs_template;
  regs_template.name = mode_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DI_NUMBER, );
    address += shift;
    reg.value.op_u16 = __GET_VAR(data__->DI_MODE_VALUE, );
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // DI_MODE_body__()

void WRITE_DO_init__(WRITE_DO *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DO_VALUE, 0, retain)
  __INIT_VAR(data__->DO_MASK, 0, retain)
}

// Code part
void WRITE_DO_body__(WRITE_DO *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_name[] = "do_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u16 = (u16)__GET_VAR(data__->DO_VALUE, ) | (((u16)__GET_VAR(data__->DO_MASK, )) << 4);
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_DO_body__()

void WRITE_DO_SC_init__(WRITE_DO_SC *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DO_SC_FLAG, 0, retain)
  __INIT_VAR(data__->DO_SC_EN, 0, retain)
}

// Code part
void WRITE_DO_SC_body__(WRITE_DO_SC *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_sc_name[] = "do_sc_ctrl";
  regs_template_t regs_template;
  regs_template.name = do_sc_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u16 = (u16)__GET_VAR(data__->DO_SC_EN, ) | (((u16)__GET_VAR(data__->DO_SC_FLAG, )) << 4);
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_DO_SC_body__()

void WRITE_DO_PWM_FREQ_init__(WRITE_DO_PWM_FREQ *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DO_PWM_FREQ, 0, retain)
}

// Code part
void WRITE_DO_PWM_FREQ_body__(WRITE_DO_PWM_FREQ *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char do_freq_name[] = "do_pwm_freq";
  regs_template_t regs_template;
  regs_template.name = do_freq_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u16 = (u16)__GET_VAR(data__->DO_PWM_FREQ, );
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_DO_PWM_FREQ_body__()

void WRITE_DO_PWM_CTRL_init__(WRITE_DO_PWM_CTRL *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DO_NUMBER, 0, retain)
  __INIT_VAR(data__->DO_PWM_DUTY, 0, retain)
  __INIT_VAR(data__->DO_PWM_RUN, 0, retain)
}
// Code part
void WRITE_DO_PWM_CTRL_body__(WRITE_DO_PWM_CTRL *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char ctrl_name[] = "do_pwm_ctrl";
  regs_template_t regs_template;
  regs_template.name = ctrl_name;
  u8 pwm_run = __GET_VAR(data__->DO_PWM_RUN, );
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.flag = regs_template.type;
    u8 address = regs_template.guid & GUID_ADDRESS_MASK;
    u8 shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->DO_NUMBER, );
    address += shift;
    switch (pwm_run)
    {
    case 0:
      reg.value.op_u16 = (u16)__GET_VAR(data__->DO_PWM_DUTY, );
      if (p_link_functions->regs_set(address, reg) == 0)
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
      break;
    case 1:
      reg.value.op_u16 = (u16)(__GET_VAR(data__->DO_PWM_DUTY, ) | PWM_RUN);
      if (p_link_functions->regs_set(address, reg) == 0)
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
      break;
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_DO_PWM_CTRL_body__()

void STRUCT_REAL_TIME_init__(STRUCT_REAL_TIME *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->HOUR_TIME, 0, retain)
  __INIT_VAR(data__->MINUTE_TIME, 0, retain)
  __INIT_VAR(data__->SEC_TIME, 0, retain)
  __INIT_VAR(data__->SUB_SEC_TIME, 0, retain)
  __INIT_VAR(data__->WEEK_DAY_TIME, 0, retain)
  __INIT_VAR(data__->MONTH_TIME, 0, retain)
  __INIT_VAR(data__->DATE_TIME, 0, retain)
  __INIT_VAR(data__->YEAR_TIME, 0, retain)
  __INIT_VAR(data__->YEAR_DAY_TIME, 0, retain)
}

// Code part
void STRUCT_REAL_TIME_body__(STRUCT_REAL_TIME *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char time_name[] = "time_hms";
  regs_template_t regs_template;
  regs_template.name = time_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    sofi_time_r time_temp;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get_buffer(address, (u8 *)(void *)&time_temp, sizeof(sofi_time_r)) == 0)
    {
      __SET_VAR(data__->, HOUR_TIME, , time_temp.hour);
      __SET_VAR(data__->, MINUTE_TIME, , time_temp.min);
      __SET_VAR(data__->, SEC_TIME, , time_temp.sec);
      __SET_VAR(data__->, SUB_SEC_TIME, , time_temp.sub_sec);
      __SET_VAR(data__->, WEEK_DAY_TIME, , time_temp.week_day);
      __SET_VAR(data__->, MONTH_TIME, , time_temp.month);
      __SET_VAR(data__->, DATE_TIME, , time_temp.date);
      __SET_VAR(data__->, YEAR_TIME, , time_temp.year);
      __SET_VAR(data__->, YEAR_DAY_TIME, , time_temp.year_day);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // STRUCT_REAL_TIME_body__()

void UNIX_TIME_init__(UNIX_TIME *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->UNIX_TIME_WRITE, 0, retain)
  __INIT_VAR(data__->UNIX_TIME_READ, 0, retain)
  __INIT_VAR(data__->UNIX_TIME_WRITED, 0, retain)
}

// Code part
void UNIX_TIME_body__(UNIX_TIME *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  u32 write, writed;
  write = __GET_VAR(data__->UNIX_TIME_WRITE, );
  writed = __GET_VAR(data__->UNIX_TIME_WRITED, );
  char name[] = "unix_time_sec";
  regs_template_t regs_template;
  regs_template.name = name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    s32 unix_time_read;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (write != writed)
    {
      regs_access_t reg;
      __SET_VAR(data__->, UNIX_TIME_WRITED, , write);
      reg.value.op_s32 = (s32)write; // data__->UNIX_TIME_WRITE
      reg.flag = regs_template.type;
      if (p_link_functions->regs_set((u16)address, reg) == 0)
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
    }
    if (p_link_functions->regs_get_buffer(address, (u8 *)(void *)&unix_time_read, sizeof(s32)) == 0)
    {
      __SET_VAR(data__->, UNIX_TIME_READ, , unix_time_read);
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }

  return;
} // UNIX_TIME_body__()

void WRITE_STRUCT_TIME_init__(WRITE_STRUCT_TIME *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->SEC_IN_MIN, 0, retain)
  __INIT_VAR(data__->MINUTE_IN_HOUR, 0, retain)
  __INIT_VAR(data__->HOUR_IN_DAY, 0, retain)
  __INIT_VAR(data__->DATE_IN_MONTH, 0, retain)
  __INIT_VAR(data__->MONTH_IN_YEAR, 0, retain)
  __INIT_VAR(data__->YEAR_SINCE_2000, 0, retain)
  __INIT_VAR(data__->SEC_IN_MIN_WRITED, 0, retain)
  __INIT_VAR(data__->MINUTE_IN_HOUR_WRITED, 0, retain)
  __INIT_VAR(data__->HOUR_IN_DAY_WRITED, 0, retain)
  __INIT_VAR(data__->DATE_IN_MONTH_WRITED, 0, retain)
  __INIT_VAR(data__->MONTH_IN_YEAR_WRITED, 0, retain)
  __INIT_VAR(data__->YEAR_SINCE_2000_WRITED, 0, retain)
}

// Code part
void WRITE_STRUCT_TIME_body__(WRITE_STRUCT_TIME *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  u32 write, writed;
  if ((__GET_VAR(data__->SEC_IN_MIN, ) != __GET_VAR(data__->SEC_IN_MIN_WRITED, )) ||
      (__GET_VAR(data__->MINUTE_IN_HOUR, ) != __GET_VAR(data__->MINUTE_IN_HOUR_WRITED, )) ||
      (__GET_VAR(data__->HOUR_IN_DAY, ) != __GET_VAR(data__->HOUR_IN_DAY_WRITED, )) ||
      (__GET_VAR(data__->DATE_IN_MONTH, ) != __GET_VAR(data__->DATE_IN_MONTH_WRITED, )) ||
      (__GET_VAR(data__->MONTH_IN_YEAR, ) != __GET_VAR(data__->MONTH_IN_YEAR_WRITED, )) ||
      (__GET_VAR(data__->YEAR_SINCE_2000, ) != __GET_VAR(data__->YEAR_SINCE_2000_WRITED, )))
  {
    sofi_time_r sofi_time;
    sofi_time.sub_sec = 0;
    sofi_time.sec = __GET_VAR(data__->SEC_IN_MIN, );
    sofi_time.min = __GET_VAR(data__->MINUTE_IN_HOUR, );
    sofi_time.hour = __GET_VAR(data__->HOUR_IN_DAY, );
    sofi_time.week_day = 1;
    sofi_time.date = __GET_VAR(data__->DATE_IN_MONTH, );
    sofi_time.month = __GET_VAR(data__->MONTH_IN_YEAR, );
    sofi_time.year = __GET_VAR(data__->YEAR_SINCE_2000, );
    sofi_time.year_day = 0;
    __SET_VAR(data__->, SEC_IN_MIN_WRITED, , __GET_VAR(data__->SEC_IN_MIN, ));
    __SET_VAR(data__->, MINUTE_IN_HOUR_WRITED, , __GET_VAR(data__->MINUTE_IN_HOUR, ));
    __SET_VAR(data__->, HOUR_IN_DAY_WRITED, , __GET_VAR(data__->HOUR_IN_DAY, ));
    __SET_VAR(data__->, DATE_IN_MONTH_WRITED, , __GET_VAR(data__->DATE_IN_MONTH, ));
    __SET_VAR(data__->, MONTH_IN_YEAR_WRITED, , __GET_VAR(data__->MONTH_IN_YEAR, ));
    __SET_VAR(data__->, YEAR_SINCE_2000_WRITED, , __GET_VAR(data__->YEAR_SINCE_2000, ));
    if ((sofi_time.month > 0) && (sofi_time.month <= 12) && (sofi_time.hour <= 23) && (sofi_time.min <= 59) && (sofi_time.sec <= 59) && (sofi_time.year <= 99))
    {
      char time_name[] = "time_hms";
      regs_template_t regs_template;
      regs_template.name = time_name;
      if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
      {
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        if (p_link_functions->regs_set_buffer(address, (u8 *)(void *)&sofi_time, sizeof(sofi_time_r)) != 0)
        {
          __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
        }
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  return;
} // WRITE_STRUCT_TIME_body__()

void PARSING_UINT_init__(PARSING_UINT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN_VAL, 0, retain)
  __INIT_VAR(data__->BIT_1, 0, retain)
  __INIT_VAR(data__->BIT_2, 0, retain)
  __INIT_VAR(data__->BIT_3, 0, retain)
  __INIT_VAR(data__->BIT_4, 0, retain)
  __INIT_VAR(data__->BIT_5, 0, retain)
  __INIT_VAR(data__->BIT_6, 0, retain)
  __INIT_VAR(data__->BIT_7, 0, retain)
  __INIT_VAR(data__->BIT_8, 0, retain)
  __INIT_VAR(data__->BIT_9, 0, retain)
  __INIT_VAR(data__->BIT_10, 0, retain)
  __INIT_VAR(data__->BIT_11, 0, retain)
  __INIT_VAR(data__->BIT_12, 0, retain)
  __INIT_VAR(data__->BIT_13, 0, retain)
  __INIT_VAR(data__->BIT_14, 0, retain)
  __INIT_VAR(data__->BIT_15, 0, retain)
  __INIT_VAR(data__->BIT_16, 0, retain)
}

// Code part
void PARSING_UINT_body__(PARSING_UINT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  if ((__GET_VAR(data__->IN_VAL, ) & 0x0001) == 1)
  {
    __SET_VAR(data__->, BIT_1, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_1, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0002), 1)) == 1)
  {
    __SET_VAR(data__->, BIT_2, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_2, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0004), 2)) == 1)
  {
    __SET_VAR(data__->, BIT_3, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_3, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0008), 3)) == 1)
  {
    __SET_VAR(data__->, BIT_4, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_4, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0010), 4)) == 1)
  {
    __SET_VAR(data__->, BIT_5, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_5, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0020), 5)) == 1)
  {
    __SET_VAR(data__->, BIT_6, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_6, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0040), 6)) == 1)
  {
    __SET_VAR(data__->, BIT_7, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_7, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0080), 7)) == 1)
  {
    __SET_VAR(data__->, BIT_8, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_8, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0100), 8)) == 1)
  {
    __SET_VAR(data__->, BIT_9, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_9, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0200), 9)) == 1)
  {
    __SET_VAR(data__->, BIT_10, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_10, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0400), 10)) == 1)
  {
    __SET_VAR(data__->, BIT_11, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_11, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x0800), 11)) == 1)
  {
    __SET_VAR(data__->, BIT_12, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_12, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x1000), 12)) == 1)
  {
    __SET_VAR(data__->, BIT_13, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_13, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x2000), 13)) == 1)
  {
    __SET_VAR(data__->, BIT_14, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_14, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x4000), 14)) == 1)
  {
    __SET_VAR(data__->, BIT_15, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_15, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x8000), 15)) == 1)
  {
    __SET_VAR(data__->, BIT_16, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_16, , FALSE);
  }
  return;
} // PARSING_UINT_body__()

void PARSING_USINT_init__(PARSING_USINT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN_VAL, 0, retain)
  __INIT_VAR(data__->BIT_1, 0, retain)
  __INIT_VAR(data__->BIT_2, 0, retain)
  __INIT_VAR(data__->BIT_3, 0, retain)
  __INIT_VAR(data__->BIT_4, 0, retain)
  __INIT_VAR(data__->BIT_5, 0, retain)
  __INIT_VAR(data__->BIT_6, 0, retain)
  __INIT_VAR(data__->BIT_7, 0, retain)
  __INIT_VAR(data__->BIT_8, 0, retain)
}

// Code part
void PARSING_USINT_body__(PARSING_USINT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }

  if ((__GET_VAR(data__->IN_VAL, ) & 0x01) == 1)
  {
    __SET_VAR(data__->, BIT_1, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_1, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x02), 1)) == 1)
  {
    __SET_VAR(data__->, BIT_2, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_2, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x04), 2)) == 1)
  {
    __SET_VAR(data__->, BIT_3, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_3, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x08), 3)) == 1)
  {
    __SET_VAR(data__->, BIT_4, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_4, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x10), 4)) == 1)
  {
    __SET_VAR(data__->, BIT_5, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_5, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x20), 5)) == 1)
  {
    __SET_VAR(data__->, BIT_6, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_6, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x40), 6)) == 1)
  {
    __SET_VAR(data__->, BIT_7, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_7, , FALSE);
  }
  if ((BITY((__GET_VAR(data__->IN_VAL, ) & 0x80), 7)) == 1)
  {
    __SET_VAR(data__->, BIT_8, , TRUE);
  }
  else
  {
    __SET_VAR(data__->, BIT_8, , FALSE);
  }
  return;
} // PARSING_USINT_body__()

void SCALE_UINT_TO_FLOAT_init__(SCALE_UINT_TO_FLOAT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->XIN, 0, retain)
  __INIT_VAR(data__->X1, 0, retain)
  __INIT_VAR(data__->X2, 0, retain)
  __INIT_VAR(data__->Y1, 0, retain)
  __INIT_VAR(data__->Y2, 0, retain)
  __INIT_VAR(data__->Y_OUT, 0, retain)
}

// Code part
void SCALE_UINT_TO_FLOAT_body__(SCALE_UINT_TO_FLOAT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  float X_IN = (float)(__GET_VAR(data__->XIN, ));
  float X_1 = (float)(__GET_VAR(data__->X1, ));
  float X_2 = (float)(__GET_VAR(data__->X2, ));
  float Y_1 = __GET_VAR(data__->Y1, );
  float Y_2 = __GET_VAR(data__->Y2, );
  float value_shift = 0.0;
  float value_koef = 0.0;
  if (X_1 == X_2)
  {
    value_shift = (X_IN - 720.0) / (13720.0);
  }
  else
  {
    value_shift = (X_IN - X_1) / (X_2 - X_1);
  }
  if (Y_1 == Y_2)
  {
    value_koef = ((16.0) * (value_shift) + 4.0);
  }
  else
  {
    value_koef = ((Y_2 - Y_1) * (value_shift) + Y_1);
  }
  __SET_VAR(data__->, Y_OUT, , value_koef);
  return;
} // SCALE_UINT_TO_FLOAT_body__()

// Code part
uint32_t TASK_PERIOD(BOOL EN, BOOL *ENO, STRING task_name)
{
  // Control execution
  if (EN)
  {
    if (ENO != NULL)
    {
      *ENO = 1;
    }
    for (u8 i = 0; i < TASKS_NUM; i++)
    {
      if (memcmp(task_name.body, task_description[i].task_name, task_name.len) == 0)
      {
        return task_description[i].task_period_ms;
      }
    }
  }
  else
  {
    if (ENO != NULL)
    {
      *ENO = 0;
    }
  }
  return 0;
} // TASK_PERIOD()

// Code part
u8 WRITE_TO_CONSOLE(BOOL EN, BOOL *ENO, STRING msg)
{
  // Control execution
  if (EN)
  {
    if (ENO != NULL)
    {
      *ENO = 1;
    }
    p_link_functions->printf((const char *)&msg);
  }
  else
  {
    if (ENO != NULL)
    {
      *ENO = 0;
    }
  }
  return 0;
} // WRITE_TO_CONSOLE()

void DWORD_TO_FLOAT_init__(DWORD_TO_FLOAT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN_VAL, 0, retain)
  __INIT_VAR(data__->OUT_VAL, 0, retain)
}

// Code part
void DWORD_TO_FLOAT_body__(DWORD_TO_FLOAT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  float a_f;
  u32 in_dword = __GET_VAR(data__->IN_VAL, );
  memcpy(&a_f, &in_dword, sizeof(u32));
  __SET_VAR(data__->, OUT_VAL, , a_f);

  return;
} // DWORD_TO_FLOAT()

void WORDS_TO_FLOAT_init__(WORDS_TO_FLOAT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->WORDH, 0, retain)
  __INIT_VAR(data__->WORDL, 0, retain)
  __INIT_VAR(data__->OUT_VAL, 0, retain)
}

// Code part
void WORDS_TO_FLOAT_body__(WORDS_TO_FLOAT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  float a_f;
  u32 d_word = ((u16)__GET_VAR(data__->WORDL, ) | (((u16)__GET_VAR(data__->WORDH, )) << 16));
  memcpy(&a_f, &d_word, sizeof(u32));
  __SET_VAR(data__->, OUT_VAL, , a_f);

  return;
} // WORDS_TO_FLOAT()
#define COILS_01_TYPE 1
#define INPUT_DISCRETES_02_TYPE 2
#define HOLDING_REGISTERS_03_TYPE 3
#define INPUT_REGISTERS_04_TYPE 4

void WRITE_AREA_LOCATION_init__(WRITE_AREA_LOCATION *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->VALUE, 0, retain)
  __INIT_VAR(data__->LOCATION0, 0, retain)
  __INIT_VAR(data__->LOCATION1, 0, retain)
  __INIT_VAR(data__->LOCATION2, 0, retain)
  __INIT_VAR(data__->LOCATION_WRITED, 0, retain)
}

void WRITE_AREA_LOCATION_body__(WRITE_AREA_LOCATION *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u16 value_temp = __GET_VAR(data__->VALUE, );
  u16 location0 = __GET_VAR(data__->LOCATION0, );
  u16 location1 = __GET_VAR(data__->LOCATION1, );
  u16 location2 = __GET_VAR(data__->LOCATION2, );
  u8 area_command = 0;
  u8 *array_p = NULL;
  u16 i;
  for (i = 0; i < MODBUS_AREA_COUNT; i++)
  {
    if ((area_nodes[i].location0 == location0) &&
        (area_nodes[i].location1 == location1) &&
        (location2 < area_nodes[i].regs_number))
    {
      area_command = area_nodes[i].command;
      array_p = area_nodes[i].data;
      break;
    }
  }
  if (area_command != 0)
  {
    if (((area_command == HOLDING_REGISTERS_03_TYPE) || (area_command == INPUT_REGISTERS_04_TYPE) ||
         (area_command == COILS_01_TYPE) || (area_command == INPUT_DISCRETES_02_TYPE)) &&
        (array_p != NULL))
    {
      memcpy(&array_p[location2 * 2], &value_temp, 2);
      __SET_VAR(data__->, LOCATION_WRITED, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, LOCATION_WRITED, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, LOCATION_WRITED, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_AREA_LOCATION()

void READ_AREA_LOCATION_init__(READ_AREA_LOCATION *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->LOCATION0, 0, retain)
  __INIT_VAR(data__->LOCATION1, 0, retain)
  __INIT_VAR(data__->LOCATION2, 0, retain)
  __INIT_VAR(data__->VALUE, 0, retain)
  __INIT_VAR(data__->LOCATION_READED, 0, retain)
}

void READ_AREA_LOCATION_body__(READ_AREA_LOCATION *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u16 value_temp = 0;
  u16 location0 = __GET_VAR(data__->LOCATION0, );
  u16 location1 = __GET_VAR(data__->LOCATION1, );
  u16 location2 = __GET_VAR(data__->LOCATION2, );
  u8 area_command = 0;
  u8 *array_p = NULL;
  u16 i;
  for (i = 0; i < MODBUS_AREA_COUNT; i++)
  {
    if ((area_nodes[i].location0 == location0) &&
        (area_nodes[i].location1 == location1) &&
        (location2 < area_nodes[i].regs_number))
    {
      area_command = area_nodes[i].command;
      array_p = area_nodes[i].data;
      break;
    }
  }
  if (area_command != 0)
  {
    if (((area_command == HOLDING_REGISTERS_03_TYPE) || (area_command == INPUT_REGISTERS_04_TYPE) ||
         (area_command == COILS_01_TYPE) || (area_command == INPUT_DISCRETES_02_TYPE)) &&
        (array_p != NULL))
    {
      memcpy(&value_temp, &array_p[location2 * 2], 2);
      __SET_VAR(data__->, VALUE, , value_temp);
      __SET_VAR(data__->, LOCATION_READED, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, LOCATION_READED, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, LOCATION_READED, , __BOOL_LITERAL(FALSE));
  }
  return;
}
void WRITE_REQUEST_LOCATION_init__(WRITE_REQUEST_LOCATION *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->VALUE, 0, retain)
  __INIT_VAR(data__->LOCATION0, 0, retain)
  __INIT_VAR(data__->LOCATION1, 0, retain)
  __INIT_VAR(data__->LOCATION2, 0, retain)
  __INIT_VAR(data__->LOCATION3, 0, retain)
  __INIT_VAR(data__->LOCATION_WRITED, 0, retain)
}

void WRITE_REQUEST_LOCATION_body__(WRITE_REQUEST_LOCATION *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u16 value_temp = __GET_VAR(data__->VALUE, );
  u16 location0 = __GET_VAR(data__->LOCATION0, );
  u16 location1 = __GET_VAR(data__->LOCATION1, );
  u16 location2 = __GET_VAR(data__->LOCATION2, );
  u16 location3 = __GET_VAR(data__->LOCATION3, );
  u8 request_command = 0;
  u8 *array_p = NULL;
  u16 i;
  for (i = 0; i < MDB_REQTS_COUNT; i++)
  {
    if ((client_requests[i].location0 == location0) &&
        (client_requests[i].location1 == location1) &&
        (client_requests[i].location2 == location2) &&
        (location3 < client_requests[i].count))
    {
      request_command = client_requests[i].mb_function;
      array_p = (u8 *)client_requests[i].coms_buffer;
      break;
    }
  }
  if (request_command != 0)
  {
    if (((request_command == 3) || (request_command == 4) || (request_command == 6) || (request_command == 16) ||
         (request_command == 1) || (request_command == 2) || (request_command == 5) || (request_command == 15)) &&
        (array_p != NULL))
    {
      memcpy(&array_p[location3 * 2], &value_temp, 2);
      __SET_VAR(data__->, LOCATION_WRITED, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, LOCATION_WRITED, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, LOCATION_WRITED, , __BOOL_LITERAL(FALSE));
  }
  return;
} // WRITE_REQUEST_LOCATION()

void READ_REQUEST_LOCATION_init__(READ_REQUEST_LOCATION *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->LOCATION0, 0, retain)
  __INIT_VAR(data__->LOCATION1, 0, retain)
  __INIT_VAR(data__->LOCATION2, 0, retain)
  __INIT_VAR(data__->LOCATION3, 0, retain)
  __INIT_VAR(data__->VALUE, 0, retain)
  __INIT_VAR(data__->LOCATION_READED, 0, retain)
}

void READ_REQUEST_LOCATION_body__(READ_REQUEST_LOCATION *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u16 value_temp = 0;
  u16 location0 = __GET_VAR(data__->LOCATION0, );
  u16 location1 = __GET_VAR(data__->LOCATION1, );
  u16 location2 = __GET_VAR(data__->LOCATION2, );
  u16 location3 = __GET_VAR(data__->LOCATION3, );
  u8 request_command = 0;
  u8 *array_p = NULL;
  u16 i;
  for (i = 0; i < MDB_REQTS_COUNT; i++)
  {
    if ((client_requests[i].location0 == location0) &&
        (client_requests[i].location1 == location1) &&
        (client_requests[i].location2 == location2) &&
        (location3 < client_requests[i].count))
    {
      request_command = client_requests[i].mb_function;
      array_p = (u8 *)client_requests[i].coms_buffer;
      break;
    }
  }
  if (client_requests != 0)
  {
    if (((request_command == 3) || (request_command == 4) || (request_command == 6) || (request_command == 16) ||
         (request_command == 1) || (request_command == 2) || (request_command == 5) || (request_command == 15)) &&
        (array_p != NULL))
    {
      memcpy(&value_temp, &array_p[location3 * 2], 2);
      __SET_VAR(data__->, VALUE, , value_temp);
      __SET_VAR(data__->, LOCATION_READED, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, LOCATION_READED, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, LOCATION_READED, , __BOOL_LITERAL(FALSE));
  }
  return;
}

void RES_TEMP_SENSOR_init__(RES_TEMP_SENSOR *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->VLT_V, 0.0f, retain)
  __INIT_VAR(data__->CUR_MA, 0.0f, retain)
  __INIT_VAR(data__->SENSOR_TYPE, 0, retain)
  __INIT_VAR(data__->R_0, 0.0f, retain)
  __INIT_VAR(data__->R_WIRE, 0.0f, retain)
  __INIT_VAR(data__->R_SENSE, 0.0f, retain)
  __INIT_VAR(data__->T_SENSE, 0.0f, retain)
}

// Code part
void RES_TEMP_SENSOR_body__(RES_TEMP_SENSOR *data__)
{
  typedef struct
  {
    uint8_t metall;
    float a;
    float b;
    float d1;
    float d2;
    float d3;
    float d4;
  } sensor_param_t;
  typedef enum
  {
    CU = 0,
    PT = 1,
    NI = 2,
  } metall_t;
#define SENSOR_NMB 3
#define MAX_SAMPLE_NUM 1000
  sensor_param_t param_table[SENSOR_NMB] = {
      {.metall = PT, .a = 3.9083e-3, .b = -5.775e-7, .d1 = 255.819f, .d2 = 9.1455f, .d3 = -2.92363f, .d4 = 1.7909f}, // Pt, alpha = 0.00385
      {.metall = PT, .a = 3.9083e-3, .b = -5.775e-7, .d1 = 255.819f, .d2 = 9.1455f, .d3 = -2.92363f, .d4 = 1.7909f}, // Pt, alpha = 0.00391
      {.metall = CU, .a = 4.28e-3, .b = 0.0f, .d1 = 233.87f, .d2 = 7.937f, .d3 = -2.0062f, .d4 = -0.3953f},          // Cu, alpha = 0.00426
                                                                                                                     //{.metall = CU,  .a = 4.26e-3,   .b = 0.0f,      .d1 = 0.0f,     .d2 = 0.0f,     .d3 = 0.0f,         .d4 = 0.0f},        //Cu, W = 1.426
                                                                                                                     //{.metall = NI,  .a = 5.4963e-3, .b = 6.7556e-6, .d1 = 144.096f, .d2 = -25.502f, .d3 = 4.4876f,      .d4 = 0.0f},        //Ni,
  };
  // Resistance measure
  float vlt = __GET_VAR(data__->VLT_V, );
  float cur = __GET_VAR(data__->CUR_MA, );
  float r_wire = __GET_VAR(data__->R_WIRE, );
  float res = vlt / cur * 1000.0f - r_wire;
  __SET_VAR(data__->, R_SENSE, , res);
  // Temperature calc
  float tmpr = 0.0f;
  float r_0 = __GET_VAR(data__->R_0, );
  uint8_t sensor_type = __GET_VAR(data__->SENSOR_TYPE, );
  if (sensor_type >= SENSOR_NMB)
  {
    tmpr = NAN;
  }
  else
  {
    switch (param_table[sensor_type].metall)
    {
    case PT:
      if (res < r_0)
      {
        tmpr = param_table[sensor_type].d1 * (res / r_0 - 1.0f) +
               param_table[sensor_type].d2 * powf(res / r_0 - 1.0f, 2.0f) +
               param_table[sensor_type].d3 * powf(res / r_0 - 1.0f, 3.0f) +
               param_table[sensor_type].d4 * powf(res / r_0 - 1.0f, 4.0f);
      }
      else if (res > r_0)
      {
        tmpr = (sqrtf(powf(param_table[sensor_type].a, 2.0f) - 4.0f * param_table[sensor_type].b * (1.0f - res / r_0)) -
                param_table[sensor_type].a) /
               (2.0f * param_table[sensor_type].b);
      }
      break;
    case CU:
      if (res < r_0)
      {
        tmpr = param_table[sensor_type].d1 * (res / r_0 - 1.0f) +
               param_table[sensor_type].d2 * powf(res / r_0 - 1.0f, 2.0f) +
               param_table[sensor_type].d3 * powf(res / r_0 - 1.0f, 3.0f) +
               param_table[sensor_type].d4 * powf(res / r_0 - 1.0f, 4.0f);
      }
      else if (res > r_0)
      {
        tmpr = (res / r_0 - 1.0f) / param_table[sensor_type].a;
      }
      break;
    default:
      tmpr = NAN;
    }
  }
  __SET_VAR(data__->, T_SENSE, , tmpr);

  return;
} // RES_TEMP_SENSOR()

void ROUND_REAL_init__(ROUND_REAL *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN_REAL, 0.0f, retain)
  __INIT_VAR(data__->DECIMAL_POINT, 0, retain)
  __INIT_VAR(data__->OUT_REAL, 0.0f, retain)
}

// Code part
void ROUND_REAL_body__(ROUND_REAL *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  float inp_real = __GET_VAR(data__->IN_REAL, );
  u8 dec_point = __GET_VAR(data__->DECIMAL_POINT, );
  float out_real = 0.0f;

  switch (dec_point)
  {
  case 0:
    out_real = roundf(inp_real);
    break;
  case 1:
    out_real = roundf(inp_real * 10.0f) / 10.0f;
    break;
  case 2:
    out_real = roundf(inp_real * 100.0f) / 100.0f;
    break;
  case 3:
    out_real = roundf(inp_real * 1000.0f) / 1000.0f;
    break;
  case 4:
    out_real = roundf(inp_real * 10000.0f) / 10000.0f;
    break;
  default:
    out_real = roundf(inp_real);
    break;
  }
  __SET_VAR(data__->, OUT_REAL, , out_real);
  return;
}

void UTON_init__(UTON *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->PT, 0, retain)
  __INIT_VAR(data__->ET, 0, retain)
  __INIT_VAR(data__->Q, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->TT, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->NIN, __BOOL_LITERAL(FALSE), retain)
  READ_SYS_TICK_COUNTER_init__(&data__->R_SYS, retain);
  __INIT_VAR(data__->CMS, 0, retain)
  __INIT_VAR(data__->SMS, 0, retain)
}

// Code part
void UTON_body__(UTON *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  READ_SYS_TICK_COUNTER_body__(&data__->R_SYS);
  __SET_VAR(data__->, CMS, , __GET_VAR(data__->R_SYS.SYS_TICK_COUNTER_VALUE));
  if (__GET_VAR(data__->IN, ))
  {
    if (!(__GET_VAR(data__->NIN, )))
    {
      __SET_VAR(data__->, SMS, , __GET_VAR(data__->CMS, ));
    };
    __SET_VAR(data__->, NIN, , __GET_VAR(data__->IN, ));
    if (((__GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, )) >= __GET_VAR(data__->PT, )))
    {
      __SET_VAR(data__->, Q, , __BOOL_LITERAL(TRUE));
      __SET_VAR(data__->, TT, , __BOOL_LITERAL(FALSE));
    };
    if (((__GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, )) < __GET_VAR(data__->PT, )))
    {
      __SET_VAR(data__->, ET, , (__GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, )));
      __SET_VAR(data__->, Q, , __BOOL_LITERAL(FALSE));
      __SET_VAR(data__->, TT, , __BOOL_LITERAL(TRUE));
    };
  }
  else
  {
    __SET_VAR(data__->, Q, , __BOOL_LITERAL(FALSE));
    __SET_VAR(data__->, ET, , 0);
    __SET_VAR(data__->, SMS, , 0);
    __SET_VAR(data__->, NIN, , __BOOL_LITERAL(FALSE));
    __SET_VAR(data__->, TT, , __BOOL_LITERAL(FALSE));
  };

  return;
} // UTON_body__()

void UTOF_init__(UTOF *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->PT, 0, retain)
  __INIT_VAR(data__->ET, 0, retain)
  __INIT_VAR(data__->CMS, 0, retain)
  __INIT_VAR(data__->SMS, 0, retain)
  __INIT_VAR(data__->NIN, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->Q, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->TT, __BOOL_LITERAL(FALSE), retain)
  READ_SYS_TICK_COUNTER_init__(&data__->R_SYS, retain);
}
// Code part
void UTOF_body__(UTOF *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
  }
  READ_SYS_TICK_COUNTER_body__(&data__->R_SYS);
  __SET_VAR(data__->, CMS, , __GET_VAR(data__->R_SYS.SYS_TICK_COUNTER_VALUE, ));

  // appropriation to CMS sys_tick_counter value
  if (__GET_VAR(data__->IN, ))
  {
    __SET_VAR(data__->, ET, , 0);
    __SET_VAR(data__->, SMS, , 0);
    __SET_VAR(data__->, NIN, , __BOOL_LITERAL(TRUE));
    __SET_VAR(data__->, Q, , __BOOL_LITERAL(TRUE));
    __SET_VAR(data__->, TT, , __BOOL_LITERAL(FALSE));
  }
  else
  {
    if (__GET_VAR(data__->NIN, ))
    {
      __SET_VAR(data__->, SMS, , (u64)__GET_VAR(data__->CMS, ));
    }
    __SET_VAR(data__->, NIN, , __GET_VAR(data__->IN, ));
    if ((__GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, )) >= __GET_VAR(data__->PT, ))
    {
      __SET_VAR(data__->, Q, , __BOOL_LITERAL(FALSE));
      __SET_VAR(data__->, TT, , __BOOL_LITERAL(FALSE));
    }
    if ((__GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, )) < __GET_VAR(data__->PT, ))
    {
      __SET_VAR(data__->, ET, , __GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, ));
      __SET_VAR(data__->, Q, , __BOOL_LITERAL(TRUE));
      __SET_VAR(data__->, TT, , __BOOL_LITERAL(TRUE));
    }
  }

  return;
}

void WORD_ON_BOOL_init__(WORD_ON_BOOL *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->WORD_IN, 0, retain)
  __INIT_VAR(data__->B_OUT0, 0, retain)
  __INIT_VAR(data__->B_OUT1, 0, retain)
  __INIT_VAR(data__->B_OUT2, 0, retain)
  __INIT_VAR(data__->B_OUT3, 0, retain)
  __INIT_VAR(data__->B_OUT4, 0, retain)
  __INIT_VAR(data__->B_OUT5, 0, retain)
  __INIT_VAR(data__->B_OUT6, 0, retain)
  __INIT_VAR(data__->B_OUT7, 0, retain)
  __INIT_VAR(data__->B_OUT8, 0, retain)
  __INIT_VAR(data__->B_OUT9, 0, retain)
  __INIT_VAR(data__->B_OUT10, 0, retain)
  __INIT_VAR(data__->B_OUT11, 0, retain)
  __INIT_VAR(data__->B_OUT12, 0, retain)
  __INIT_VAR(data__->B_OUT13, 0, retain)
  __INIT_VAR(data__->B_OUT14, 0, retain)
  __INIT_VAR(data__->B_OUT15, 0, retain)
}

// Code part
void WORD_ON_BOOL_body__(WORD_ON_BOOL *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  if (__GET_VAR(data__->WORD_IN, ) & 0x0001)
  {
    __SET_VAR(data__->, B_OUT0, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT0, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0002), 1))
  {
    __SET_VAR(data__->, B_OUT1, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT1, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0004), 2))
  {
    __SET_VAR(data__->, B_OUT2, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT2, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0008), 3))
  {
    __SET_VAR(data__->, B_OUT3, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT3, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0010), 4))
  {
    __SET_VAR(data__->, B_OUT4, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT4, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0020), 5))
  {
    __SET_VAR(data__->, B_OUT5, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT5, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0040), 6))
  {
    __SET_VAR(data__->, B_OUT6, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT6, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0080), 7))
  {
    __SET_VAR(data__->, B_OUT7, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT7, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0100), 8))
  {
    __SET_VAR(data__->, B_OUT8, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT8, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0200), 9))
  {
    __SET_VAR(data__->, B_OUT9, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT9, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0400), 10))
  {
    __SET_VAR(data__->, B_OUT10, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT10, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x0800), 11))
  {
    __SET_VAR(data__->, B_OUT11, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT11, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x1000), 12))
  {
    __SET_VAR(data__->, B_OUT12, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT12, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x2000), 13))
  {
    __SET_VAR(data__->, B_OUT13, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT13, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x4000), 14))
  {
    __SET_VAR(data__->, B_OUT14, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT14, , __BOOL_LITERAL(FALSE));
  }
  if (BITY((__GET_VAR(data__->WORD_IN, ) & 0x8000), 15))
  {
    __SET_VAR(data__->, B_OUT15, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, B_OUT15, , __BOOL_LITERAL(FALSE));
  }
  return;
}

void BOOL_ON_WORD_init__(BOOL_ON_WORD *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->B_IN0, 0, retain)
  __INIT_VAR(data__->B_IN1, 0, retain)
  __INIT_VAR(data__->B_IN2, 0, retain)
  __INIT_VAR(data__->B_IN3, 0, retain)
  __INIT_VAR(data__->B_IN4, 0, retain)
  __INIT_VAR(data__->B_IN5, 0, retain)
  __INIT_VAR(data__->B_IN6, 0, retain)
  __INIT_VAR(data__->B_IN7, 0, retain)
  __INIT_VAR(data__->B_IN8, 0, retain)
  __INIT_VAR(data__->B_IN9, 0, retain)
  __INIT_VAR(data__->B_IN10, 0, retain)
  __INIT_VAR(data__->B_IN11, 0, retain)
  __INIT_VAR(data__->B_IN12, 0, retain)
  __INIT_VAR(data__->B_IN13, 0, retain)
  __INIT_VAR(data__->B_IN14, 0, retain)
  __INIT_VAR(data__->B_IN15, 0, retain)
  __INIT_VAR(data__->WORD_OUT, 0, retain)
}

// Code part
void BOOL_ON_WORD_body__(BOOL_ON_WORD *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u16 w_out = BITZ(__GET_VAR(data__->B_IN0, ), 0);
  w_out = (BITZ(__GET_VAR(data__->B_IN1, ), 1) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN2, ), 2) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN3, ), 3) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN4, ), 4) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN5, ), 5) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN6, ), 6) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN7, ), 7) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN8, ), 8) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN9, ), 9) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN10, ), 10) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN11, ), 11) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN12, ), 12) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN13, ), 13) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN14, ), 14) | w_out);
  w_out = (BITZ(__GET_VAR(data__->B_IN15, ), 15) | w_out);
  __SET_VAR(data__->, WORD_OUT, , w_out);
  return;
}

void SETBIT_init__(SETBIT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN, 0, retain)
  __INIT_VAR(data__->BIT_N, 0, retain)
  __INIT_VAR(data__->BIT_VALUE, 0, retain)
  __INIT_VAR(data__->OUT, 0, retain)
}

// Code part
void SETBIT_body__(SETBIT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u32 sdword = __GET_VAR(data__->IN, );
  u32 bitn = pow(2, __GET_VAR(data__->BIT_N, ));
  u8 bitvalue = __GET_VAR(data__->BIT_VALUE, );
  if (bitvalue == 1)
  {
    __SET_VAR(data__->, OUT, , sdword | bitn);
  }
  else
  {
    __SET_VAR(data__->, OUT, , (sdword ^ (sdword & bitn)));
  }

  return;
}

void GETBIT_init__(GETBIT *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN, 0, retain)
  __INIT_VAR(data__->BIT_N, 0, retain)
  __INIT_VAR(data__->OUT, 0, retain)
}

// Code part
void GETBIT_body__(GETBIT *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u32 sdword = __GET_VAR(data__->IN, );
  u32 bitn = pow(2, __GET_VAR(data__->BIT_N, ));
  if ((sdword & bitn) > 0)
  {
    __SET_VAR(data__->, OUT, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, OUT, , __BOOL_LITERAL(FALSE));
  }
  return;
}

void AI_HANDLER_init__(AI_HANDLER *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->AI_CH, 0, retain)
  __INIT_VAR(data__->VAL_FORCE, 0.0f, retain)
  __INIT_VAR(data__->EUMAX, 0.0f, retain)
  __INIT_VAR(data__->EUMIN, 0.0f, retain)
  __INIT_VAR(data__->SP_DB, 0.0f, retain)
  __INIT_VAR(data__->SP_K, 0.0f, retain)
  __INIT_VAR(data__->SP_HH, 0.0f, retain)
  __INIT_VAR(data__->SP_H, 0.0f, retain)
  __INIT_VAR(data__->SP_L, 0.0f, retain)
  __INIT_VAR(data__->SP_LL, 0.0f, retain)
  __INIT_VAR(data__->AI_TIME, 0, retain)
  __INIT_VAR(data__->AI_PV, 0.0f, retain)
  __INIT_VAR(data__->AI_SR, 0.0f, retain)
  __INIT_VAR(data__->EN_HH, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->EN_H, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->EN_L, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->EN_LL, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->EN_FORCE, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->EN_ARCH, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->AI_REG, 0, retain)
  UTON_init__(&data__->UT, retain);
  WORD_ON_BOOL_init__(&data__->WB, retain);
}

// Code part
void AI_HANDLER_body__(AI_HANDLER *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u16 ai_ch = __GET_VAR(data__->AI_CH, );
  float val_force = __GET_VAR(data__->VAL_FORCE, );
  float eumax = __GET_VAR(data__->EUMAX, );
  float eumin = __GET_VAR(data__->EUMIN, );
  float sp_db = __GET_VAR(data__->SP_DB, );
  float sp_k = __GET_VAR(data__->SP_K, );
  float sp_hh = __GET_VAR(data__->SP_HH, );
  float sp_h = __GET_VAR(data__->SP_H, );
  float sp_l = __GET_VAR(data__->SP_L, );
  float sp_ll = __GET_VAR(data__->SP_LL, );
  u16 ai_time = __GET_VAR(data__->AI_TIME, );
  float ai_pv = __GET_VAR(data__->AI_PV, );
  float ai_sr = __GET_VAR(data__->AI_SR, );

  __SET_VAR(data__->WB., WORD_IN, , __GET_VAR(data__->AI_REG, ));
  WORD_ON_BOOL_body__(&data__->WB);
  __SET_VAR(data__->, EN_HH, , __GET_VAR(data__->WB.B_OUT0));
  __SET_VAR(data__->, EN_H, , __GET_VAR(data__->WB.B_OUT1));
  __SET_VAR(data__->, EN_L, , __GET_VAR(data__->WB.B_OUT2));
  __SET_VAR(data__->, EN_LL, , __GET_VAR(data__->WB.B_OUT3));
  __SET_VAR(data__->, EN_FORCE, , __GET_VAR(data__->WB.B_OUT4));
  __SET_VAR(data__->, EN_ARCH, , __GET_VAR(data__->WB.B_OUT11));
  u8 en_hh = __GET_VAR(data__->EN_HH);
  u8 en_h = __GET_VAR(data__->EN_H);
  u8 en_l = __GET_VAR(data__->EN_L);
  u8 en_ll = __GET_VAR(data__->EN_LL);
  u8 en_force = __GET_VAR(data__->EN_FORCE);
  u8 en_arch = __GET_VAR(data__->EN_ARCH);
  float sr_real = 0.0f;
  float sr_last = 0.0f;
  u8 out_range = 0;
  u8 brk = 0;
  u8 momhh = 0;
  u8 momh = 0;
  u8 moml = 0;
  u8 momll = 0;
  u8 timer_dn = 0;
  u8 act_hh = 0;
  u8 act_h = 0;
  u8 act_l = 0;
  u8 act_ll = 0;

  char ai_name[] = "ai_physical";
  regs_template_t regs_template;
  regs_template.name = ai_name;
  if (ai_ch < 8)
  {
    if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
    {
      regs_access_t reg;
      reg.value.op_u64 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      u16 shift = p_link_functions->regs_size_in_byte(regs_template.type) * ai_ch;
      address += shift;
      if (p_link_functions->regs_get((u16)address, &reg) == 0)
      {
        sr_real = reg.value.op_f;
      }
    }
  }
  /* Фильтрация */
  if ((sp_k > 1.0f) || (sp_k <= 0.0f))
  {
    sp_k = 1.0f;
  }
  ai_sr = sp_k * (sr_real - sr_last) + sr_last;

  /* Вне диапазона */
  if (((ai_sr > 1.0f) && (ai_sr < 3.5f)) || (ai_sr > 20.5f))
  {
    out_range = 1;
  }
  else
  {
    out_range = 0;
  }

  /* Обрыв */
  if (ai_sr <= 1.0f)
  {
    brk = 1;
  }
  else
  {
    brk = 0;
  }

  /* Форсирование/Имитация и масштабирование в инженерные единицы */
  if (en_force)
  {
    ai_pv = val_force;
  }
  else
  {
    ai_pv = (eumax - eumin) * (ai_sr - 4.0f) / 16.0f + eumin;
  }

  /* Ограничение значения AI_PV в диапазоне EUMIN->EUMAX */
  if (eumax < eumin)
  {
    if (ai_pv < eumax)
    {
      ai_pv = eumax;
    }
    if (ai_pv > eumin)
    {
      ai_pv = eumin;
    }
  }
  else
  {

    if (ai_pv < eumin)
    {
      ai_pv = eumin;
    }
    if (ai_pv > eumax)
    {
      ai_pv = eumax;
    }
  }
  /* Установка мгновенных алармов */
  if ((!brk & !out_range) | en_force)
  {
    /*SP_HH*/
    if (en_hh)
    {
      if (ai_pv >= (sp_hh + sp_db / 2.0f))
      {
        momhh = 1;
      }
      else
      {
        if (ai_pv <= (sp_hh - sp_db / 2.0f))
        {
          momhh = 0;
        }
      }
    }
    else
    {
      momhh = 0;
    }
    /*SP_H*/
    if (en_h)
    {
      if (ai_pv >= (sp_h + sp_db / 2.0f))
      {
        momh = 1;
      }
      else
      {
        if (ai_pv <= (sp_h - sp_db / 2.0f))
        {
          momh = 0;
        }
      }
    }
    else
    {
      momh = 0;
    }
    /*SP_L*/
    if (en_l)
    {
      if (ai_pv <= (sp_l - sp_db / 2.0f))
      {
        moml = 1;
      }
      else
      {
        if (ai_pv >= (sp_l + sp_db / 2.0f))
        {
          moml = 0;
        }
      }
    }
    else
    {
      moml = 0;
    }
    /*SP_LL*/
    if (en_ll)
    {
      if (ai_pv <= (sp_ll - sp_db / 2.0f))
      {
        momll = 1;
      }
      else
      {
        if (ai_pv >= (sp_ll + sp_db / 2.0f))
        {
          momll = 0;
        }
      }
    }
    else
    {
      momll = 0;
    }
    __SET_VAR(data__->, UT.IN, , (momhh || momh || moml || momll));
    __SET_VAR(data__->, UT.PT, , (ai_time * 1000));
    timer_dn = __GET_VAR(data__->UT.Q, );
    UTON_body__(&data__->UT);
    /* Установка алармов */
    act_hh = timer_dn && momhh;
    act_h = timer_dn && momh;
    act_l = timer_dn && moml;
    act_ll = timer_dn && momll;
  }
  else
  {
    momhh = 0;
    momh = 0;
    moml = 0;
    momll = 0;
    act_hh = 0;
    act_h = 0;
    act_l = 0;
    act_ll = 0;
  }
  __SET_VAR(data__->, AI_PV, , ai_pv);
  __SET_VAR(data__->, AI_SR, , ai_sr);
  __SET_VAR(data__->, VAL_FORCE, , val_force);
  __SET_VAR(data__->, SP_DB, , sp_db);
  __SET_VAR(data__->, SP_K, , sp_k);
  __SET_VAR(data__->, SP_HH, , sp_hh);
  __SET_VAR(data__->, SP_H, , sp_h);
  __SET_VAR(data__->, SP_L, , sp_l);
  __SET_VAR(data__->, SP_LL, , sp_ll);
  __SET_VAR(data__->, AI_TIME, , moml);
  u16 w_out = BITZ(en_hh, 0);
  w_out = (BITZ(en_h, 1) | w_out);
  w_out = (BITZ(en_l, 2) | w_out);
  w_out = (BITZ(en_ll, 3) | w_out);
  w_out = (BITZ(en_force, 4) | w_out);
  w_out = (BITZ(brk, 5) | w_out);
  w_out = (BITZ(out_range, 6) | w_out);
  w_out = (BITZ(act_hh, 7) | w_out);
  w_out = (BITZ(act_h, 8) | w_out);
  w_out = (BITZ(act_l, 9) | w_out);
  w_out = (BITZ(act_ll, 10) | w_out);
  w_out = (BITZ(en_arch, 11) | w_out);
  __SET_VAR(data__->, AI_REG, , w_out);
  return;
}

void HART_CH_SEL_init__(HART_CH_SEL *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->HART_CH_MASK, 0, retain)
}

// Code part
void HART_CH_SEL_body__(HART_CH_SEL *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  // Initialise TEMP variables
  char hart_name[] = "hart_channel";
  regs_template_t regs_template;
  regs_template.name = hart_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u16 = (u16)__GET_VAR(data__->HART_CH_MASK, );
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_set(address, reg) == 0)
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // HART_CH_SEL

void FIRST_SCAN_init__(FIRST_SCAN *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->OUT, __BOOL_LITERAL(FALSE), retain)
}
void FIRST_SCAN_body__(FIRST_SCAN *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  __SET_VAR(data__->, OUT, , FIRST_CYCLE);
}

void PID_FB_init__(PID_FB *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->PV, 0.0f, retain)
  __INIT_VAR(data__->SP, 0.0f, retain)
  __INIT_VAR(data__->TR, 0.0f, retain)
  __INIT_VAR(data__->DIRECT, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->ON_OFF, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->HALT, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->RESET, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->KP, 0.0f, retain)
  __INIT_VAR(data__->KI, 0.0f, retain)
  __INIT_VAR(data__->KD, 0.0f, retain)
  __INIT_VAR(data__->SP_TUBE, 0.0f, retain)
  __INIT_VAR(data__->VSPMAX, 0.0f, retain)
  __INIT_VAR(data__->PV_MAX, 0.0f, retain)
  __INIT_VAR(data__->PV_MIN, 0.0f, retain)
  __INIT_VAR(data__->PV_NOISE, 0.0f, retain)
  __INIT_VAR(data__->PV_OVER, 0.0f, retain)
  __INIT_VAR(data__->IN_MODE, 0, retain)
  __INIT_VAR(data__->PID_MODE, 0, retain)
  __INIT_VAR(data__->OUT_MODE, 0, retain)
  __INIT_VAR(data__->STATUS, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->ALARM, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->Y, 0.0f, retain)
  __INIT_VAR(data__->ERR, 0.0f, retain)
  __INIT_VAR(data__->PV_RANGE_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->PV_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->SP_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->PID_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->TR_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->SP_TUBE_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->VSPMAX_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->Y_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->PV_OVER_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->MODE, 0, retain)
  __INIT_VAR(data__->PVMAX, 0.0f, retain)
  __INIT_VAR(data__->PVMIN, 0.0f, retain)
  __INIT_VAR(data__->PVRANGE, 0.0f, retain)
  __INIT_VAR(data__->ALARM_TR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->ALARM_PV, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->VSPMAX_ON, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->SP_TUBE_ON, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->SP_QUANT, 0.0f, retain)
  __INIT_VAR(data__->Y_QUANT, 0.0f, retain)
  __INIT_VAR(data__->YMAX, 0.0f, retain)
  __INIT_VAR(data__->YMIN, 0.0f, retain)
  __INIT_VAR(data__->YRANGE, 0.0f, retain)
  __INIT_VAR(data__->ERR1, 0.0f, retain)
  __INIT_VAR(data__->ERR2, 0.0f, retain)
  __INIT_VAR(data__->DERR1, 0.0f, retain)
  __INIT_VAR(data__->DERR2, 0.0f, retain)
  __INIT_VAR(data__->MODE_PREV, 0, retain)
}

// Code part
void PID_FB_body__(PID_FB *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables
  u8 mode = __GET_VAR(data__->MODE);
  u8 pvrange_error = __GET_VAR(data__->PV_RANGE_ERR);
  u8 pv_error = __GET_VAR(data__->PV_ERR);
  u8 sp_error = __GET_VAR(data__->SP_ERR);
  u8 pid_error = __GET_VAR(data__->PID_ERR);
  u8 tr_error = __GET_VAR(data__->TR_ERR);
  u8 vspmax_error = __GET_VAR(data__->VSPMAX_ERR);
  u8 sptube_error = __GET_VAR(data__->SP_TUBE_ERR);
  u8 y_error = __GET_VAR(data__->Y_ERR);
  u8 pv_over_error = __GET_VAR(data__->PV_OVER_ERR);
  float pv = __GET_VAR(data__->PV);
  float sp = __GET_VAR(data__->SP);
  float tr = __GET_VAR(data__->TR);
  u8 direct = __GET_VAR(data__->DIRECT);
  u8 on_off = __GET_VAR(data__->ON_OFF);
  u8 halt = __GET_VAR(data__->HALT);
  u8 reset = __GET_VAR(data__->RESET);
  float kp = __GET_VAR(data__->KP);
  float ki = __GET_VAR(data__->KI);
  float kd = __GET_VAR(data__->KD);
  float sptube = __GET_VAR(data__->SP_TUBE);
  float vspmax = __GET_VAR(data__->VSPMAX);
  float pv_max = __GET_VAR(data__->PV_MAX);
  float pv_min = __GET_VAR(data__->PV_MIN);
  float pv_noise = __GET_VAR(data__->PV_NOISE);
  float pv_over = __GET_VAR(data__->PV_OVER);
  u8 in_mode = __GET_VAR(data__->IN_MODE);
  u8 pid_mode = __GET_VAR(data__->PID_MODE);
  u8 out_mode = __GET_VAR(data__->OUT_MODE);
  u8 alarm = __GET_VAR(data__->ALARM);
  float y = __GET_VAR(data__->Y);
  float err = __GET_VAR(data__->ERR);
  float pvmax = __GET_VAR(data__->PVMAX);
  float pvmin = __GET_VAR(data__->PVMIN);
  float pvrange = __GET_VAR(data__->PVRANGE);
  u8 vspmax_on = __GET_VAR(data__->VSPMAX_ON);
  u8 sptube_on = __GET_VAR(data__->SP_TUBE_ON);
  float sp_quant = __GET_VAR(data__->SP_QUANT);
  float y_quant = __GET_VAR(data__->Y_QUANT);
  float ymin = __GET_VAR(data__->YMIN);
  float ymax = __GET_VAR(data__->YMAX);
  float yrange = __GET_VAR(data__->YRANGE);
  u8 alarm_tr = __GET_VAR(data__->ALARM_TR);
  u8 alarm_pv = __GET_VAR(data__->ALARM_PV);
  float err1 = __GET_VAR(data__->ERR1);
  float err2 = __GET_VAR(data__->ERR2);
  float derr1 = __GET_VAR(data__->DERR1);
  float derr2 = __GET_VAR(data__->DERR2);
  u8 mode_prev = __GET_VAR(data__->MODE_PREV);
  u8 status = __GET_VAR(data__->STATUS);

  if (status)
  {
    if (on_off)
    {
      mode = 1;
      if (direct)
      {
        mode = 2;
      }
      if (halt)
      {
        mode = 3;
      }
      if (reset)
      {
        mode = 4;
      }
      if (!(mode == mode_prev))
      {
        switch (mode)
        {
        case 1:
          switch (mode_prev)
          {
          case 0:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          case 2:
            mode_prev = mode;
            break;
          case 3:
            mode_prev = mode;
            break;
          case 4:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          }
          break;

        case 2:
          switch (mode_prev)
          {
          case 0:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          case 1:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          case 2:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          case 3:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          case 4:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          }
          break;

        case 3:
          switch (mode_prev)
          {
          case 0:
            mode = mode_prev;
            break;
          case 1:
            mode_prev = mode;
            break;
          case 2:
            mode = mode_prev;
            break;
          case 4:
            mode = mode_prev;
            break;
          }
          break;

        case 4:
          switch (mode_prev)
          {
          case 0:
            mode = mode_prev;
            break;
          case 1:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          case 2:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          case 3:
            mode_prev = mode;
            err1 = 0.0f;
            err2 = 0.0f;
            break;
          }
          break;
        }
      }
      switch (mode)
      {
      case 1:
        if ((pv < (pvmin - pv_noise) || (pvmax + pv_noise) < pv) && (!(sptube_on)))
        {
          pv_error = 1;
          alarm = 1;
          /*if(pv_quant >= pvmax){
            pv_quant = pvmax;
          }else{
            pv_quant = pvmin;
          }*/
        }
        sp_quant = sp;
        if ((sp_quant < pvmin) || (pvmax < sp_quant))
        {
          sp_error = 1;
          alarm = 1;
          if (sp_quant >= pvmax)
          {
            sp_quant = pvmax;
          }
          else
          {
            sp_quant = pvmin;
          }
        }
        err = sp_quant - pv;
        if (vspmax_on)
        {
          if (fabsf(err) > vspmax)
          {
            if (err > 0.0f)
            {
              err = vspmax;
            }
            else
            {
              err = -vspmax;
            }
          }
        }
        switch (pid_mode)
        {
        case 1:
          derr1 = err - err1;
          y_quant = y_quant + kp * derr1 + ki * err;
          err1 = err;
          break;
        case 2:
          derr1 = err - err1;
          derr2 = err - 2.0f * err1 + err2;
          y_quant = y_quant + kp * derr1 + kd * derr2;
          err2 = err1;
          err1 = err;
          break;
        case 3:
          derr1 = err - err1;
          derr2 = err - 2.0f * err1 + err2;
          y_quant = y_quant + kp * derr1 + ki * err + kd * derr2;
          err2 = err1;
          err1 = err;
          break;
        case 4:
          derr1 = err - err1;
          derr2 = err - 2.0f * err1 + err2;
          y_quant = y_quant + kp * derr1 + ki * err + kd * derr2;
          err2 = err1;
          err1 = err;
          if ((err < 0.0f) && (err < -sp_quant * pv_over / 100.0f))
          {
            pv_over_error = 1;
            alarm = 1;
          }
          break;
        }
        if ((y_quant < pvmin) || (pvmax < y_quant))
        {
          if (!(sptube_on))
          {
            y_error = 1;
            alarm = 1;
          }
          if (y_quant >= pvmax)
          {
            y_quant = pvmax;
          }
          else
          {
            y_quant = pvmin;
          }
        }
        y = (y_quant - pvmin) * yrange / pvrange + ymin;
        break;

      case 2:
        if ((pv < (pvmin - pv_noise) || (pvmax + pv_noise) < pv) && (!(sptube_on)))
        {
          pv_error = 1;
          if ((!(alarm)) || (alarm_tr))
          {
            alarm_pv = 1;
          }
        }
        else
        {
          pv_error = 0;
          if ((alarm_pv) && (!(alarm_tr)))
          {
            alarm = 0;
          }
          alarm_pv = 0;
        }
        err = sp - pv;
        y = tr;
        y_quant = (y - ymin) * pvrange / yrange + pvmin;
        if ((tr < ymin) || (ymax < tr))
        {
          tr_error = 1;
          if (!(alarm) || (alarm_pv))
          {
            alarm_tr = 1;
          }
        }
        else
        {
          tr_error = 0;
          if (alarm_tr && !(alarm_pv))
          {
            alarm = 0;
          }
          alarm_tr = 0;
        }
        if ((pv_error) || (tr_error))
        {
          alarm = 1;
        }
        break;

      case 3:
        err = sp - pv;
        break;

      case 4:
        if (halt)
        {
          err1 = 0.0f;
          err2 = 0.0f;
        }
        if (alarm)
        {
          status = 0;
        }
        break;
      }
    }
    else
    {
      y = 0.0f;
      y_quant = 0.0f;
      err = sp - pv;
      err1 = 0.0f;
      err2 = 0.0f;
      status = 0;
      mode = 0;
      mode_prev = 0;
    }
  }
  else
  {
    if (on_off || reset)
    {
      if (reset)
      {
        mode_prev = 4;
      }
      alarm = 0;
      sptube_on = 0;
      vspmax_on = 0;
      pvrange_error = 0;
      pv_error = 0;
      sp_error = 0;
      vspmax_error = 0;
      sptube_error = 0;
      pid_error = 0;
      pv_over_error = 0;
      tr_error = 0;
      y_error = 0;
      alarm_tr = 0;
      alarm_pv = 0;

      switch (in_mode)
      {
      case 1:
        if (pv_max - pv_min <= 0.0f)
        {
          pvrange_error = 1;
          alarm = 1;
          alarm_tr = 1;
        }
        else
        {
          pvmax = pv_max;
          pvmin = pv_min;
        }
        break;

      case 2:
        pvmax = 20.0f;
        pvmin = 4.0f;
        break;
      case 3:
        pvmax = 20.0f;
        pvmin = 0.0f;
        break;
      case 4:
        pvmax = 10.0f;
        pvmin = 0.0f;
        break;
      }
      if ((pv < (pvmin - pv_noise)) || ((pvmax + pv_noise) < pv))
      {
        pv_error = 1;
        alarm = 1;
      }
      if ((sp < pvmin) || (pvmax < sp))
      {
        sp_error = 1;
        alarm = 1;
      }
      if (sptube > 0.0f)
      {
        if (((sp + sptube / 2.0f) <= pvmax) && ((sp - sptube / 2.0f) >= pvmin))
        {
          pvmax = sp + sptube / 2.0f;
          pvmin = sp - sptube / 2.0f;
          sptube_on = 1;
        }
        else
        {
          sptube_error = 1;
          alarm = 1;
          alarm_tr = 1;
        }
      }
      else
      {
        if (sptube < 0.0f)
        {
          sptube_error = 1;
          alarm = 1;
          alarm_tr = 1;
        }
      }
      pvrange = pvmax - pvmin;
      if (vspmax > 0.0f)
      {
        if (vspmax > pvrange)
        {
          vspmax_error = 1;
          alarm = 1;
        }
        else
        {
          vspmax_on = 1;
        }
      }
      else
      {
        if (vspmax < 0.0f)
        {
          vspmax_error = 1;
          alarm = 1;
        }
      }
      if (kp == 0.0f)
      {
        pid_error = 1;
        alarm = 1;
        alarm_tr = 1;
      }
      switch (out_mode)
      {
      case 1:
        ymax = 100.0f;
        ymin = 0.0f;
        yrange = 100.0f;
        break;
      case 2:
        ymax = 20.0f;
        ymin = 4.0f;
        yrange = 16.0f;
        break;
      case 3:
        ymax = 20.0f;
        ymin = 0.0f;
        yrange = 20.0f;
        break;
      case 4:
        ymax = 10.0f;
        ymin = 0.0f;
        yrange = 10.0f;
        break;
      case 5:
        ymax = 100.0f;
        ymin = -100.0f;
        yrange = 200.0f;
        if (!sptube_on)
        {
          sptube_error = 1;
          alarm = 1;
          alarm_tr = 1;
        }
        break;
      }
      if ((tr < ymin) || (ymax < tr))
      {
        tr_error = 1;
        alarm = 1;
        alarm_tr = 1;
      }
      else
      {
        if ((sptube_on) && (out_mode == 5) && (!(tr == ymin)) && (!(tr == ymax)) && (!(tr == 0.0f)))
        {
          tr_error = 1;
          sptube_error = 1;
          alarm = 1;
          alarm_tr = 1;
        }
      }
      if (mode_prev == 4)
      {
        if (!(reset))
        {
          mode_prev = 0;
        }
      }
      else
      {
        if (alarm)
        {
          if ((direct) && (!(alarm_tr)))
          {
            status = 1;
          }
        }
        else
        {
          status = 1;
        }
      }
    }
    else
    {
      err = sp - pv;
    }
  }

  __SET_VAR(data__->, ALARM, , alarm);
  __SET_VAR(data__->, Y, , y);
  __SET_VAR(data__->, ERR, , err);
  __SET_VAR(data__->, Y_ERR, , y_error);
  __SET_VAR(data__->, PV_RANGE_ERR, , pvrange_error);
  __SET_VAR(data__->, PV_ERR, , pv_error);
  __SET_VAR(data__->, SP_ERR, , sp_error);
  __SET_VAR(data__->, PID_ERR, , pid_error);
  __SET_VAR(data__->, TR_ERR, , tr_error);
  __SET_VAR(data__->, SP_TUBE_ERR, , sptube_error);
  __SET_VAR(data__->, VSPMAX_ERR, , vspmax_error);
  __SET_VAR(data__->, PV_OVER_ERR, , pv_over_error);
  __SET_VAR(data__->, PVMAX, , pvmax);
  __SET_VAR(data__->, PVMIN, , pvmin);
  __SET_VAR(data__->, PVRANGE, , pvrange);
  __SET_VAR(data__->, VSPMAX_ON, , vspmax_on);
  __SET_VAR(data__->, SP_TUBE_ON, , sptube_on);
  __SET_VAR(data__->, SP_QUANT, , sp_quant);
  __SET_VAR(data__->, Y_QUANT, , y_quant);
  __SET_VAR(data__->, YMAX, , ymax);
  __SET_VAR(data__->, YMIN, , ymin);
  __SET_VAR(data__->, YRANGE, , yrange);
  __SET_VAR(data__->, ALARM_TR, , alarm_tr);
  __SET_VAR(data__->, ALARM_PV, , alarm_pv);
  __SET_VAR(data__->, ERR1, , err1);
  __SET_VAR(data__->, ERR2, , err2);
  __SET_VAR(data__->, DERR1, , derr1);
  __SET_VAR(data__->, DERR2, , derr2);
  __SET_VAR(data__->, MODE_PREV, , mode_prev);
  __SET_VAR(data__->, MODE, , mode);
  __SET_VAR(data__->, STATUS, , status);

  return;
} // PID_FB()

void SETBIT_WORD_init__(SETBIT_WORD *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN, 0, retain)
  __INIT_VAR(data__->BIT_N, 0, retain)
  __INIT_VAR(data__->BIT_VALUE, 0, retain)
  __INIT_VAR(data__->OUT, 0, retain)
}

// Code part
void SETBIT_WORD_body__(SETBIT_WORD *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u16 sword = __GET_VAR(data__->IN, );
  u16 bitn = pow(2, __GET_VAR(data__->BIT_N, ));
  u8 bitvalue = __GET_VAR(data__->BIT_VALUE, );
  if (bitvalue == 1)
  {
    __SET_VAR(data__->, OUT, , sword | bitn);
  }
  else
  {
    __SET_VAR(data__->, OUT, , (sword ^ (sword & bitn)));
  }

  return;
}

void GETBIT_WORD_init__(GETBIT_WORD *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN, 0, retain)
  __INIT_VAR(data__->BIT_N, 0, retain)
  __INIT_VAR(data__->OUT, 0, retain)
}

// Code part
void GETBIT_WORD_body__(GETBIT_WORD *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  u16 sword = __GET_VAR(data__->IN, );
  u16 bitn = pow(2, __GET_VAR(data__->BIT_N, ));
  if ((sword & bitn) > 0)
  {
    __SET_VAR(data__->, OUT, , __BOOL_LITERAL(TRUE));
  }
  else
  {
    __SET_VAR(data__->, OUT, , __BOOL_LITERAL(FALSE));
  }
  return;
}

void MODULE_STATE_init__(MODULE_STATE *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ADDR, 1, retain)
  __INIT_VAR(data__->CONN_STATE, __BOOL_LITERAL(FALSE), retain)
}
void MODULE_STATE_body__(MODULE_STATE *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise variables
  u8 addr = (__GET_VAR(data__->ADDR));
  if ((addr > 0) && (addr < 127))
  {
    u8 byte_nmb = addr / 8;
    u8 bit_nmb = addr % 8;
    u8 buff[16] = {0};
    char name[] = "can_modules_status";
    regs_template_t regs_template;
    regs_template.name = name;
    if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
    {
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if (p_link_functions->regs_get_buffer(address, &buff, 16) == 0)
      {
        if (buff[byte_nmb] & (1 << bit_nmb))
        {
          __SET_VAR(data__->, CONN_STATE, , __BOOL_LITERAL(TRUE));
        }
        else
        {
          __SET_VAR(data__->, CONN_STATE, , __BOOL_LITERAL(FALSE));
        }
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
}

void READ_HART_CH_init__(READ_HART_CH *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->CHANNEL, 0, retain)
  __INIT_VAR(data__->CURRENT, 0, retain)
  __INIT_VAR(data__->PV, 0, retain)
  __INIT_VAR(data__->SV, 0, retain)
  __INIT_VAR(data__->TV, 0, retain)
  __INIT_VAR(data__->FV, 0, retain)
}
void READ_HART_CH_body__(READ_HART_CH *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));

  regs_template_t regs_template;
  char cur_name[] = "hart_cur";
  char pv_name[] = "hart_pv";
  char sv_name[] = "hart_sv";
  char tv_name[] = "hart_tv";
  char fv_name[] = "hart_fv";
  u16 address = 0;
  regs_access_t reg;
  u16 shift = 0;

  // Current
  regs_template.name = cur_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    address = regs_template.guid & GUID_ADDRESS_MASK;

    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    address = regs_template.guid & GUID_ADDRESS_MASK;
    shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->CHANNEL, );
    address += shift;
    if (p_link_functions->regs_get((u16)address, &reg) == 0)
    {
      __SET_VAR(data__->, CURRENT, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  // PV
  regs_template.name = pv_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    address = regs_template.guid & GUID_ADDRESS_MASK;

    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    address = regs_template.guid & GUID_ADDRESS_MASK;
    shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->CHANNEL, );
    address += shift;
    if (p_link_functions->regs_get((u16)address, &reg) == 0)
    {
      __SET_VAR(data__->, PV, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  // SV
  regs_template.name = sv_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    address = regs_template.guid & GUID_ADDRESS_MASK;

    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    address = regs_template.guid & GUID_ADDRESS_MASK;
    shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->CHANNEL, );
    address += shift;
    if (p_link_functions->regs_get((u16)address, &reg) == 0)
    {
      __SET_VAR(data__->, SV, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  // TV
  regs_template.name = tv_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    address = regs_template.guid & GUID_ADDRESS_MASK;

    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    address = regs_template.guid & GUID_ADDRESS_MASK;
    shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->CHANNEL, );
    address += shift;
    if (p_link_functions->regs_get((u16)address, &reg) == 0)
    {
      __SET_VAR(data__->, TV, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  // FV
  regs_template.name = fv_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    address = regs_template.guid & GUID_ADDRESS_MASK;

    reg.value.op_u64 = 0;
    reg.flag = regs_template.type;
    address = regs_template.guid & GUID_ADDRESS_MASK;
    shift = p_link_functions->regs_size_in_byte(regs_template.type) * __GET_VAR(data__->CHANNEL, );
    address += shift;
    if (p_link_functions->regs_get((u16)address, &reg) == 0)
    {
      __SET_VAR(data__->, FV, , reg.value.op_f);
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
}

void SERVOCONTROL_FB_init__(SERVOCONTROL_FB *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ON_OFF, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->MAN_MODE, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->SAFE, 0.0f, retain)
  __INIT_VAR(data__->TRACK, 0.0f, retain)
  __INIT_VAR(data__->STATE, 0.0f, retain)
  __INIT_VAR(data__->TR_MAX, 0.0f, retain)
  __INIT_VAR(data__->TR_MIN, 0.0f, retain)
  __INIT_VAR(data__->STATUS, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->ALARM, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->DIRECT, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->TR, 0.0f, retain)
  __INIT_VAR(data__->TRANGE_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->SAFE_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->TRACK_ERR, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->STATE_ERR, __BOOL_LITERAL(FALSE), retain)
}

// Code part
void SERVOCONTROL_FB_body__(SERVOCONTROL_FB *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
  }

  u8 status = __GET_VAR(data__->STATUS);
  u8 on_off = __GET_VAR(data__->ON_OFF);
  u8 man_mode = __GET_VAR(data__->MAN_MODE);
  u8 direct = __GET_VAR(data__->DIRECT);
  float safe = __GET_VAR(data__->SAFE);
  float track = __GET_VAR(data__->TRACK);
  float state = __GET_VAR(data__->STATE);
  float tr_max = __GET_VAR(data__->TR_MAX);
  float tr_min = __GET_VAR(data__->TR_MIN);
  u8 alarm = __GET_VAR(data__->ALARM);
  float tr = __GET_VAR(data__->TR);
  u8 tr_error = __GET_VAR(data__->TRANGE_ERR);
  u8 safe_error = __GET_VAR(data__->SAFE_ERR);
  u8 state_error = __GET_VAR(data__->STATE_ERR);
  u8 track_error = __GET_VAR(data__->TRACK_ERR);

  if (status)
  {
    // Проверка настройки выхода TR
    if ((tr_max - tr_min) <= 0.0f)
    {
      tr_error = 1; // Ошибка настройки блока: Неверно заданы TR_min, TR_max
      status = 0;
    }
    // Проверка готовности входа SAFE
    if ((safe < tr_min) || (tr_max < safe))
    {
      safe_error = 1; // Ошибка настройки блока: SAFE задано неверно
      status = 0;
    }
    if (status)
    {
      // Проверка входа TRACK
      if ((track < tr_min) || (tr_max < track))
      {
        track_error = 1; // Аппаратная ошибка блока: TRACK вне диапазона
      }
      else
      {
        track_error = 0;
      }
      // Проверка входа STATE
      if ((state < tr_min || tr_max < state) && (!(state == 0.0f)))
      {
        state_error = 1; // Недопустимое значение на входе: STATE вне диапазона
      }
      else
      {
        state_error = 0;
      }
      if ((track_error) || (state_error))
      {
        alarm = 1;
        tr = safe;
      }
      else
      {
        alarm = 0;
        if (man_mode)
        {
          tr = track - state;
        }
        else
        {
          tr = safe;
        }
      }
      if (on_off)
      {
        if (!(direct))
        {
          direct = 1;
        }
      }
      else
      {
        if (direct)
        {
          direct = 0;
        }
      }
    }
    else
    {
      alarm = 1;
      direct = 0;
    }
  }
  else
  {
    if ((tr_max - tr_min) > 0.0f && safe >= tr_min && tr_max >= safe)
    {
      status = 1;
      alarm = 0;
      tr_error = 0;
      safe_error = 0;
      tr = safe;
      if (on_off)
      {
        direct = 1;
      }
      else
      {
        direct = 0;
      }
    }
  }
  __SET_VAR(data__->, STATUS, , status);
  __SET_VAR(data__->, ALARM, , alarm);
  __SET_VAR(data__->, DIRECT, , direct);
  __SET_VAR(data__->, TR, , tr);
  __SET_VAR(data__->, TRANGE_ERR, , tr_error);
  __SET_VAR(data__->, SAFE_ERR, , safe_error);
  __SET_VAR(data__->, TRACK_ERR, , track_error);
  __SET_VAR(data__->, STATE_ERR, , state_error);
  return;
} // SERVOCONTROL()

void FB_RATTLY_DI_init__(FB_RATTLY_DI *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->RUN, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->DI_IN, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->T_RTLY, 0, retain)
  __INIT_VAR(data__->CLK_RTLY, 0, retain)
  __INIT_VAR(data__->CLK_ARCH, 0, retain)
  __INIT_VAR(data__->DI_OUT, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->SAVE, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->ARCH_WORD, 0, retain)
  __INIT_VAR(data__->RTLY, __BOOL_LITERAL(FALSE), retain)
  FIRST_SCAN_init__(&data__->FS, retain);
  __INIT_VAR(data__->FS_YES, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->SAVE_NEW, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->OUT_OLD, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->T_ARCH, 0, retain)
  __INIT_VAR(data__->CNT_NO_RTLY, 0, retain)
  __INIT_VAR(data__->CNT_CICL_ARCH, 0, retain)
  __INIT_VAR(data__->WORD_MASK, 1, retain)
  __INIT_VAR(data__->WORD_ARCH, 0, retain)
  __INIT_VAR(data__->CNT_UP, 0, retain)
  __INIT_VAR(data__->CNT_DOWN, 0, retain)
  __INIT_VAR(data__->CNT_UP_DOWN, 0, retain)
  __INIT_VAR(data__->N, 0, retain)
  __INIT_VAR(data__->J, 0, retain)
}

// Code part
void FB_RATTLY_DI_body__(FB_RATTLY_DI *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  FIRST_SCAN_body__(&data__->FS);
  __SET_VAR(data__->, FS_YES, , __GET_VAR(data__->FS.OUT));
  if (__GET_VAR(data__->FS_YES, ))
  {
    if ((MOD__UDINT__UDINT__UDINT(
             (BOOL)__BOOL_LITERAL(TRUE),
             NULL,
             (UDINT)(__GET_VAR(data__->CLK_ARCH, ) * 2),
             (UDINT)16) == 0))
    {
      __SET_VAR(data__->, T_ARCH, , ((__GET_VAR(data__->CLK_ARCH, ) * 2) / 16));
      if ((MOD__UDINT__UDINT__UDINT(
               (BOOL)__BOOL_LITERAL(TRUE),
               NULL,
               (UDINT)__GET_VAR(data__->T_ARCH, ),
               (UDINT)__GET_VAR(data__->CLK_RTLY, )) == 0))
      {
        __SET_VAR(data__->, CNT_CICL_ARCH, , UDINT_TO_UINT((BOOL)__BOOL_LITERAL(TRUE), NULL, (UDINT)(__GET_VAR(data__->T_ARCH, ) / __GET_VAR(data__->CLK_RTLY, ))));
        __SET_VAR(data__->, N, , 1);
        __SET_VAR(data__->, J, , 0);
      }
      else
      {
        __SET_VAR(data__->, T_ARCH, , 0);
      };
    }
    else
    {
      __SET_VAR(data__->, T_ARCH, , 0);
    };
  };
  if (__GET_VAR(data__->RUN, ))
  {
    __SET_VAR(data__->, OUT_OLD, , __GET_VAR(data__->DI_OUT, ));
    if (((__GET_VAR(data__->T_RTLY, ) == 0) || (MOD__UDINT__UDINT__UDINT(
                                                    (BOOL)__BOOL_LITERAL(TRUE),
                                                    NULL,
                                                    (UDINT)__GET_VAR(data__->T_RTLY, ),
                                                    (UDINT)__GET_VAR(data__->CLK_RTLY, )) != 0)))
    {
      __SET_VAR(data__->, CNT_NO_RTLY, , 0);
      __SET_VAR(data__->, DI_OUT, , __GET_VAR(data__->DI_IN, ));
    }
    else
    {
      __SET_VAR(data__->, CNT_NO_RTLY, , (__GET_VAR(data__->T_RTLY, ) / __GET_VAR(data__->CLK_RTLY, )));
      if (((__GET_VAR(data__->CNT_UP, ) > __GET_VAR(data__->CNT_NO_RTLY, )) && __GET_VAR(data__->DI_IN, )))
      {
        __SET_VAR(data__->, DI_OUT, , __BOOL_LITERAL(TRUE));
        __SET_VAR(data__->, CNT_UP_DOWN, , 0);
        __SET_VAR(data__->, RTLY, , __BOOL_LITERAL(FALSE));
      }
      else if (((__GET_VAR(data__->CNT_DOWN, ) > __GET_VAR(data__->CNT_NO_RTLY, )) && !(__GET_VAR(data__->DI_IN, ))))
      {
        __SET_VAR(data__->, DI_OUT, , __BOOL_LITERAL(FALSE));
        __SET_VAR(data__->, CNT_UP_DOWN, , 0);
        __SET_VAR(data__->, RTLY, , __BOOL_LITERAL(FALSE));
      }
      else if (__GET_VAR(data__->DI_IN, ))
      {
        __SET_VAR(data__->, CNT_UP, , (__GET_VAR(data__->CNT_UP, ) + 1));
        __SET_VAR(data__->, CNT_DOWN, , 0);
        __SET_VAR(data__->, CNT_UP_DOWN, , (__GET_VAR(data__->CNT_UP_DOWN, ) + 1));
      }
      else if (!(__GET_VAR(data__->DI_IN, )))
      {
        __SET_VAR(data__->, CNT_DOWN, , (__GET_VAR(data__->CNT_DOWN, ) + 1));
        __SET_VAR(data__->, CNT_UP, , 0);
        __SET_VAR(data__->, CNT_UP_DOWN, , (__GET_VAR(data__->CNT_UP_DOWN, ) + 1));
      };
      if ((__GET_VAR(data__->CNT_UP_DOWN, ) > (__GET_VAR(data__->CNT_NO_RTLY, ) * 5)))
      {
        __SET_VAR(data__->, CNT_UP_DOWN, , 0);
      }
      else if ((__GET_VAR(data__->CNT_UP_DOWN, ) > (__GET_VAR(data__->CNT_NO_RTLY, ) * 2)))
      {
        __SET_VAR(data__->, RTLY, , __BOOL_LITERAL(TRUE));
      };
    };
    if ((__GET_VAR(data__->T_ARCH, ) > 0))
    {
      if (((__GET_VAR(data__->DI_OUT, ) != __GET_VAR(data__->OUT_OLD, )) && !(__GET_VAR(data__->SAVE_NEW, ))))
      {
        __SET_VAR(data__->, SAVE_NEW, , __BOOL_LITERAL(TRUE));
      };
      if ((__GET_VAR(data__->N, ) == 1))
      {
        if ((__GET_VAR(data__->J, ) <= 15))
        {
          if (__GET_VAR(data__->DI_OUT, ))
          {
            __SET_VAR(data__->, WORD_ARCH, , OR__WORD__WORD((BOOL)__BOOL_LITERAL(TRUE), NULL, (UINT)2, (WORD)__GET_VAR(data__->WORD_ARCH, ), (WORD)__GET_VAR(data__->WORD_MASK, )));
          };
          __SET_VAR(data__->, WORD_MASK, , SHL__WORD__WORD__SINT((BOOL)__BOOL_LITERAL(TRUE), NULL, (WORD)__GET_VAR(data__->WORD_MASK, ), (SINT)1));
          __SET_VAR(data__->, J, , (__GET_VAR(data__->J, ) + 1));
          if ((__GET_VAR(data__->J, ) == 16))
          {
            if (__GET_VAR(data__->SAVE_NEW, ))
            {
              __SET_VAR(data__->, ARCH_WORD, , __GET_VAR(data__->WORD_ARCH, ));
              __SET_VAR(data__->, SAVE, , __BOOL_LITERAL(TRUE));
              __SET_VAR(data__->, SAVE_NEW, , __BOOL_LITERAL(FALSE));
            }
            else
            {
              __SET_VAR(data__->, SAVE, , __BOOL_LITERAL(FALSE));
              if (__GET_VAR(data__->DI_OUT, ))
              {
                __SET_VAR(data__->, ARCH_WORD, , 65535);
              }
              else
              {
                __SET_VAR(data__->, ARCH_WORD, , 0);
              };
            };
            __SET_VAR(data__->, WORD_MASK, , 1);
            __SET_VAR(data__->, WORD_ARCH, , 0);
            __SET_VAR(data__->, J, , 0);
          };
        };
        if ((__GET_VAR(data__->CNT_CICL_ARCH, ) > 1))
        {
          __SET_VAR(data__->, N, , (__GET_VAR(data__->N, ) + 1));
        };
      }
      else if ((__GET_VAR(data__->N, ) == __GET_VAR(data__->CNT_CICL_ARCH, )))
      {
        __SET_VAR(data__->, N, , 1);
      }
      else if ((__GET_VAR(data__->N, ) < __GET_VAR(data__->CNT_CICL_ARCH, )))
      {
        __SET_VAR(data__->, N, , (__GET_VAR(data__->N, ) + 1));
      };
    };
  }
  else if (__GET_VAR(data__->SAVE, ))
  {
    __SET_VAR(data__->, SAVE, , __BOOL_LITERAL(FALSE));
  };

  goto __end;

__end:
  return;
} // FB_RATTLY_DI_body__()
void UTOFR_init__(UTOFR *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->IN, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->PT, 0, retain)
  __INIT_VAR(data__->RES, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->ET, 0, retain)
  __INIT_VAR(data__->CMS, 0, retain)
  __INIT_VAR(data__->SMS, 0, retain)
  __INIT_VAR(data__->NIN, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->Q, __BOOL_LITERAL(FALSE), retain)
  __INIT_VAR(data__->TT, __BOOL_LITERAL(FALSE), retain)
  READ_SYS_TICK_COUNTER_init__(&data__->R_SYS, retain);
}
// Code part
void UTOFR_body__(UTOFR *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
  }
  READ_SYS_TICK_COUNTER_body__(&data__->R_SYS);
  __SET_VAR(data__->, CMS, , __GET_VAR(data__->R_SYS.SYS_TICK_COUNTER_VALUE, ));

  // appropriation to CMS sys_tick_counter value
  if (__GET_VAR(data__->RES, ))
  {
    __SET_VAR(data__->, IN, , __BOOL_LITERAL(FALSE));
    __SET_VAR(data__->, TT, , __BOOL_LITERAL(FALSE));
    __SET_VAR(data__->, Q, , __BOOL_LITERAL(FALSE));
    __SET_VAR(data__->, SMS, , 0);
    __SET_VAR(data__->, CMS, , 0);
    __SET_VAR(data__->, ET, , 0);
  }
  else
  {
    if (__GET_VAR(data__->IN, ))
    {
      __SET_VAR(data__->, ET, , 0);
      __SET_VAR(data__->, SMS, , 0);
      __SET_VAR(data__->, NIN, , __BOOL_LITERAL(TRUE));
      __SET_VAR(data__->, Q, , __BOOL_LITERAL(TRUE));
      __SET_VAR(data__->, TT, , __BOOL_LITERAL(FALSE));
    }
    else
    {
      if (__GET_VAR(data__->NIN, ))
      {
        __SET_VAR(data__->, SMS, , (u64)__GET_VAR(data__->CMS, ));
      }
      __SET_VAR(data__->, NIN, , __GET_VAR(data__->IN, ));
      if ((__GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, )) >= __GET_VAR(data__->PT, ))
      {
        __SET_VAR(data__->, Q, , __BOOL_LITERAL(FALSE));
        __SET_VAR(data__->, TT, , __BOOL_LITERAL(FALSE));
      }
      if ((__GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, )) < __GET_VAR(data__->PT, ))
      {
        __SET_VAR(data__->, ET, , __GET_VAR(data__->CMS, ) - __GET_VAR(data__->SMS, ));
        __SET_VAR(data__->, Q, , __BOOL_LITERAL(TRUE));
        __SET_VAR(data__->, TT, , __BOOL_LITERAL(TRUE));
      }
    }
  }

  return;
}
void ERR_PARSE_init__(ERR_PARSE *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ERR_RESET, 0, retain)
  __INIT_VAR(data__->ISOL_PWR, 0, retain)
  __INIT_VAR(data__->DO_SC, 0, retain)
  __INIT_VAR(data__->DI_FREQ_OVERLOAD, 0, retain)
  __INIT_VAR(data__->ADC_CONNECTION, 0, retain)
  __INIT_VAR(data__->EXTERNAL_FLASH, 0, retain)
  __INIT_VAR(data__->TASK_INIT, 0, retain)
  __INIT_VAR(data__->CAN_SDO, 0, retain)
  __INIT_VAR(data__->LAN_CONNECTION, 0, retain)
  __INIT_VAR(data__->ARCHIEVE, 0, retain)
  __INIT_VAR(data__->LWIP_MEMORY, 0, retain)
  __INIT_VAR(data__->BATTERY_LOW, 0, retain)
   WORD_ON_BOOL_init__(&data__->WOB0, retain);
   WORD_ON_BOOL_init__(&data__->WOB1, retain);
}

// Code part
void ERR_PARSE_body__(ERR_PARSE *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  char err_0[] = "err_reg_0";
  char err_1[] = "err_reg_1";
  regs_template_t regs_template;
  regs_template.name = err_0;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u32 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      WORD_ON_BOOL_body__(&data__->WOB0);
      u16 error_0 = (u16)reg.value.op_u32;

      __GET_VAR(data__->WOB0.WORD_IN) = error_0;
      __SET_VAR(data__->, ISOL_PWR, , __GET_VAR(data__->WOB0.B_OUT0));
      __SET_VAR(data__->, DO_SC, , __GET_VAR(data__->WOB0.B_OUT1));
      __SET_VAR(data__->, DI_FREQ_OVERLOAD, , __GET_VAR(data__->WOB0.B_OUT2));
      __SET_VAR(data__->, ADC_CONNECTION, , __GET_VAR(data__->WOB0.B_OUT3));
      __SET_VAR(data__->, EXTERNAL_FLASH, , __GET_VAR(data__->WOB0.B_OUT4));
      __SET_VAR(data__->, TASK_INIT, , __GET_VAR(data__->WOB0.B_OUT5));
      __SET_VAR(data__->, CAN_SDO, , __GET_VAR(data__->WOB0.B_OUT6));
      __SET_VAR(data__->, LAN_CONNECTION, , __GET_VAR(data__->WOB0.B_OUT7));
      __SET_VAR(data__->, ARCHIEVE, , __GET_VAR(data__->WOB0.B_OUT8));
      __SET_VAR(data__->, LWIP_MEMORY, , __GET_VAR(data__->WOB0.B_OUT9));
      __SET_VAR(data__->, BATTERY_LOW, , __GET_VAR(data__->WOB0.B_OUT10));

    }
    
  }
  /*
  regs_template.name = err_1;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u32 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    {
      WORD_ON_BOOL_body__(&data__->WOB1);
      u16 error_1 = (u16)reg.value.op_u32;
      __GET_VAR(data__->WOB0.WORD_IN) = error_1;

    }
  }*/
  if (__GET_VAR(data__->ERR_RESET)){
    regs_template.name = err_0;
    if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
    {
      regs_access_t reg;
      reg.value.op_u32 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if (p_link_functions->regs_set(address, reg) == 0)
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
    regs_template.name = err_1;
    if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
    {
      regs_access_t reg;
      reg.value.op_u32 = 0;
      reg.flag = regs_template.type;
      u16 address = regs_template.guid & GUID_ADDRESS_MASK;
      if (p_link_functions->regs_set(address, reg) == 0)
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
      }
      else
      {
        __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
      }
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }

  return;

}
void READ_DI_CH_init__(READ_DI_CH *data__, BOOL retain)
{
  __INIT_VAR(data__->EN, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->ENO, __BOOL_LITERAL(TRUE), retain)
  __INIT_VAR(data__->DI_0, 0, retain)
  __INIT_VAR(data__->DI_1, 0, retain)
  __INIT_VAR(data__->DI_2, 0, retain)
  __INIT_VAR(data__->DI_3, 0, retain)
  __INIT_VAR(data__->DI_4, 0, retain)
  __INIT_VAR(data__->DI_5, 0, retain)
  __INIT_VAR(data__->DI_6, 0, retain)
  __INIT_VAR(data__->DI_7, 0, retain)
  __INIT_VAR(data__->DI_8, 0, retain)
  __INIT_VAR(data__->DI_9, 0, retain)
  __INIT_VAR(data__->DI_10, 0, retain)
  __INIT_VAR(data__->DI_11, 0, retain)
  __INIT_VAR(data__->DI_12, 0, retain)
  __INIT_VAR(data__->DI_13, 0, retain)
  __INIT_VAR(data__->DI_14, 0, retain)
  __INIT_VAR(data__->DI_15, 0, retain)
  WORD_ON_BOOL_init__(&data__->WOB, retain);
}

// Code part
void READ_DI_CH_body__(READ_DI_CH *data__)
{
  // Control execution
  if (!__GET_VAR(data__->EN))
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    return;
  }
  __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  // Initialise TEMP variables
  char di_name[] = "di_state";
  regs_template_t regs_template;
  regs_template.name = di_name;
  if (p_link_functions->regs_description_get_by_name(&regs_template) == 0)
  {
    regs_access_t reg;
    reg.value.op_u16 = 0;
    reg.flag = regs_template.type;
    u16 address = regs_template.guid & GUID_ADDRESS_MASK;
    if (p_link_functions->regs_get(address, &reg) == 0)
    { 
      WORD_ON_BOOL_body__(&data__->WOB);
      __GET_VAR(data__->WOB.WORD_IN) = reg.value.op_u16;
      __SET_VAR(data__->, DI_0, , __GET_VAR(data__->WOB.B_OUT0));
      __SET_VAR(data__->, DI_1, , __GET_VAR(data__->WOB.B_OUT1));
      __SET_VAR(data__->, DI_2, , __GET_VAR(data__->WOB.B_OUT2));
      __SET_VAR(data__->, DI_3, , __GET_VAR(data__->WOB.B_OUT3));
      __SET_VAR(data__->, DI_4, , __GET_VAR(data__->WOB.B_OUT4));
      __SET_VAR(data__->, DI_5, , __GET_VAR(data__->WOB.B_OUT5));
      __SET_VAR(data__->, DI_6, , __GET_VAR(data__->WOB.B_OUT6));
      __SET_VAR(data__->, DI_7, , __GET_VAR(data__->WOB.B_OUT7));
      __SET_VAR(data__->, DI_8, , __GET_VAR(data__->WOB.B_OUT8));
      __SET_VAR(data__->, DI_9, , __GET_VAR(data__->WOB.B_OUT9));
      __SET_VAR(data__->, DI_10, , __GET_VAR(data__->WOB.B_OUT10));
      __SET_VAR(data__->, DI_11, , __GET_VAR(data__->WOB.B_OUT11));
      __SET_VAR(data__->, DI_12, , __GET_VAR(data__->WOB.B_OUT12));
      __SET_VAR(data__->, DI_13, , __GET_VAR(data__->WOB.B_OUT13));
      __SET_VAR(data__->, DI_14, , __GET_VAR(data__->WOB.B_OUT14));
      __SET_VAR(data__->, DI_15, , __GET_VAR(data__->WOB.B_OUT15));
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(TRUE));
    }
    else
    {
      __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
    }
  }
  else
  {
    __SET_VAR(data__->, ENO, , __BOOL_LITERAL(FALSE));
  }
  return;
} // READ_DI_body__()


