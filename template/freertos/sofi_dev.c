#include "regs.h"
#include "sofi_dev.h" 
#include "link_functions.h"
#include "os_service.h"
extern link_functions_t * p_link_functions;
/*@brief read param by address value
 * @return non zero value if error success
 * */
int get_value_by_address(u32 address,u64* value,regs_flag_t flag){
    int res;
    regs_access_t reg;
    reg.flag = flag;
    res = 0;
    reg.value.op_u64 = 0;
    if(p_link_functions->regs_get((u16)address,&reg)!=0){
        *value = 0;
        res = -1;
    }else{
        *value = reg.value.op_u64;
    }
    return res;
}
int set_value_by_address(u32 address,u64 value,regs_flag_t flag){
    int res;
    regs_access_t reg;
    reg.flag = flag;
    reg.value.op_u64 = value;
    res = 0;
    if(p_link_functions->regs_set((u16)address,reg)!=0){
        res = -1;
    }
    return res;
}

