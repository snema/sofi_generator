/**
 * @file repeater.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup free_rtos/inc
 * @ingroup free_rtos/inc
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef REPEATER_H
#define REPEATER_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
#define MAX_ROUTES 40
typedef enum {
    REPEATER_IS_STARTED = (u32)(1<<0),/*!< occured after add packet to repeater */
    REPEATER_IS_HANDING = (u32)(1<<1),/*!< own for repeater use */
    REPEATER_IS_MDB_TCP = (u32)(1<<2),/*!< in packet we have tcp header*/
}   repeater_state_t;
typedef enum {
    REPEATER_ROUTE_MISMATCH = -2,/*!< did't find route in routes*/
    REPEATER_ROUTE_IS_BUSY = -3,/*!< have found route but it busy*/
} repeater_channel_state;
/*add functions and variable declarations below */
/**
 * @brief repeater_task using for retranslate packet
 * @param argument unused
 */
FNCT_NO_RETURN void repeater_task( void const * argument );
int repeater_get_channel_to(u8 channel_from, u8 * data, u16 len,u32 flags);
/**
 * @brief repeater_set_route which modbus packet where to retranslate
 * @param channel_from
 * @param channel_to
 * @param mdb_address 0xff all modbus address
 * @return < 0 if didnt add route
 */
int repeater_add_route (u8 channel_from, u8 channel_to, u8 mdb_address);
/**
 * @brief repeater_delete_route looking only mdb address corresponding and free it
 * @param mdb_address   [1:255]
 * @return < 0 if mdb address did'nt corresponde with any routes address
 */
int repeater_delete_route (u8 mdb_address);
/**
 * @brief repeater_get_channel_to return channel number if have in table
 * @param channel_from
 * @param data used for getting address
 * @param len
 * @return <0 value if not find route
 */
int repeater_get_channel_to(u8 channel_from, u8 * data, u16 len,u32 flags);
/**
 * @brief repeater_hand_packet begining sequence for send packet and wait answear
 * @param channel_from
 * @param data
 * @param len
 * @param socket pointer to connection for return packet
 * @return return > 0 if packet in handig < 0 if error 0 if channel to is busy
 */
int repeater_add_packet_to_hand(u8 channel_from, u8 * data, u16 len, void * socket,u32 flags);



/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //REPEATER_H
