﻿/**
 * @file    sofi_test.h
 * @author  Shoma Gane <shomagan@gmail.com>
 * @author  Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @ingroup sofi_test
 * @version 0.1
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef SOFI_TEST_H
#define SOFI_TEST_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */

/**
  * @brief internal test type
  * @ingroup sofi_test
  */
typedef enum{
    DISABLE_TYPE_TEST = 0,  /*!<all test disable*/
    CHECK_TEST = 1<<0,  /*!<simply control api and hardware*/
    STRESS_TEST = 1<<1, /*!<full corners control api and hardware*/
    PERFORMANCE_TEST = 1<<2 ,/*!<template for find time corresponding,speed,perfromance in different settings or platfrom*/
    ALL_TEST = (CHECK_TEST)|(STRESS_TEST)|(PERFORMANCE_TEST)
}sofi_test_type_t;

/**
  * @brief list of internal tests
  * @ingroup sofi_test
  */
typedef enum{
    NONE_BLOCK = 0,   /*!<dissable all test*/
    CRC_BLOCK = 1<<0, /*!<*/
    RTC_BLOCK = 1<<1, /*!<*/
    DI_BLOCK = 1<<2 , /*!<*/
    DO_BLOCK = 1<<3 ,
    AI_BLOCK = 1<<4 ,
    INTERNAL_FLASH_BLOCK = 1<<5,
    EXTERNAL_FLASH_BLOCK = 1<<6,
    FREERTOS_BLOCK = 1<<7,
    MATH_BLOCK = 1<<8,
    UART_BLOCK = 1<<9,
    MESO_BLOCK = 1<<10,
    RANDOM_BLOCK = 1<<11,   /*!< */
    CAN_BLOCK = 1<<12,
    PACKET_BLOCK = 1<<13,   /*!< start infinity stress test */
    REPEATER_BLOCK = 1<<14, /*!< retranslate packet test only CHECK(add new route) and STRESS(init reinit task) TEST */
    LFS_BLOCK = 1<<15,      /*!< little fs file test*/
    ETHERNET_BLOCK = 1<<16,
}sofi_test_block_t;

/**
  * @brief struct for run internal test
  * @ingroup sofi_test
  */
typedef struct{
    sofi_test_type_t type;
    sofi_test_block_t block;
}sofi_test_arg;

#if SOFI_TEST_ENABLE

void sofi_test(const void *parametrs);
/**
 * @brief sofi_test_print_info print test result after test passed
 * @param test_name
 * @param test_type
 * @param test_result
 * @return
 */
int sofi_test_print_info(char * test_name, const sofi_test_type_t test_type,const sofi_test_type_t test_result,u64 time_us);

#endif
/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //SOFI_TEST_H
