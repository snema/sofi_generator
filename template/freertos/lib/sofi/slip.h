/**
 * @file
 * SLIP Interface
 *
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved. 
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met: 
 * 1. Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution. 
 * 3. Neither the name of the Institute nor the names of its contributors 
 *    may be used to endorse or promote products derived from this software 
 *    without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. 
 *
 * This file is built upon the file: src/arch/rtxc/netif/sioslip.c
 *
 * Author: Magnus Ivarsson <magnus.ivarsson(at)volvo.com> 
 */

/* 
 * This is an arch independent SLIP netif. The specific serial hooks must be
 * provided by another file. They are sio_open, sio_read/sio_tryread and sio_send
 */
#include "lwip/opt.h"
#include "lwip/pbuf.h"
#include "lwip/ip_addr.h"
#define SLIP_BLOCK     1
#define SLIP_DONTBLOCK 0

#define SLIP_END     0xC0 /* 0xC0 */
#define SLIP_ESC     0xDB /* 0xDB */
#define SLIP_ESC_END 0xDC /* 0xDC */
#define SLIP_ESC_ESC 0xDD /* 0xDD */

#define SLIP_MAX_SIZE UART6_BUFFERS_SIZE

extern struct netif slipif;

err_t slipif_output(struct netif *netif, struct pbuf *p,const ip4_addr_t *ipaddr);
/**
 * Send a pbuf without doing the necessary SLIP encapsulation
 *
 * Uses the serial layer's sio_send()
 *
 * @param netif the lwip network interface structure for this slipif
 * @param p the pbuf chaing packet to send
 * @return always returns number byte sending ti serial line
 */
err_t slipif_output_raw(u8 * buff,u16 len);

struct pbuf* if_input(u8* buff,u16 length);
/**
 * @brief init all neccesery memory and mutex -> slipif_init
 *
 * */

int slip_init(void);
/**
 * @brief deinit all neccesery memory
 * */
int slipif_deinit(struct netif *netif);
err_t slip_send_parameters(void);
err_t slipif_init(struct netif *netif);
int slip_packet_handle(u8 * packet,u16 len);
u16 make_slip_packet(u8* slip_buffer,const u8* pure_buffer,u16 lenght);


