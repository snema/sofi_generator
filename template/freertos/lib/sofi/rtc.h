#ifndef RTC_H
#define RTC_H
#if defined __cplusplus
extern "C" {
#endif
#include "type_def.h"
#include "time.h"
#include "regs.h"
typedef enum {
    RTC_EXTERNAL = 0,
    RTC_INTERNAL
}rtc_type_t;

extern osSemaphoreId rtc_access_sem;
/*
*@brief init rtc 
*/
u8 rtc_init(rtc_type_t rtc_type);
void rtc_deinit(void);
u8 rtc_get_time(sofi_time_r *time);
u8 rtc_set_time(sofi_time_r *time);
int rtc_update_time_to_regs(void);
#if defined __cplusplus
}
#endif
#endif //RTC_H
