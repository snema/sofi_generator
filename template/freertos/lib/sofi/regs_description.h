/**
 * @file regs_description.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup free_rtos/inc
 * @ingroup free_rtos/inc
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef REGS_DESCRIPTION_H
#define REGS_DESCRIPTION_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "regs.h"
/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
#define  MAX_DESCRIPTION_SIZE 30
#define  MAX_ARRAY_SIZE 256
#define  MAX_DESCRIPTIONS_REGS  2048
#define NUM_OF_SELF_VARS 148

enum property{
    SELF      = 1<<0,
    READ_ONLY = 1<<1,
    SAVING    = 1<<2,
    USER_VARS = 1<<3,
};//!< @property this item must be unchangable name beacose using in beremiz user generator


extern const u16 def_mdb_addr;
extern const u8 def_mdb_revers;
extern const u8 def_mdb_shift;
extern const u8 def_ip[];
extern const u8 def_netmask[];
extern const u8 def_gateaway[];
extern const u8 def_eth_speed;
extern const u8 def_eth_duplex;
extern const u16 def_reset_num;
extern const u16 def_last_reset_reason;

extern const u16 def_uart1_sets;
extern const u16 def_uart2_sets;
extern const u16 def_uart3_sets;
extern const u16 def_uart5_sets;
extern const u16 def_uart6_sets;
extern const u16 def_uart7_sets;
extern const u32 def_channels_timeout[];
extern const u8 def_do_sc_ctrl;
extern const u8 def_mac_addr[];
extern const u8 def_os_version[];
extern const di_conf_t def_di_config[];
extern const u16 def_di_noise_fltr_us[];
extern const u32 def_di_pulseless_ms[];
extern const u16 def_di_mode[];
extern const u16 def_do_pwm_freq;
extern const u16 def_do_pwm_ctrl[];
extern const u16 def_num_of_vars;
extern const u16 def_module_number;
extern const u8 def_local_ip[];
extern const u8 def_local_netmask[];
extern const u8 def_local_gateaway[];
extern const u8 def_usb_local_ip[];
extern const u8 def_device_type;
extern const u8 def_board_ver;
extern const u16 def_user_task_state;
extern const u32 def_pass_key;
extern const u32 def_err_reg_0;
extern const u32 def_err_reg_1;

/*iec104section description start*/
typedef struct MCU_PACK{
    u16 * originator_address; //!< The originator address is optional on a system basis
    u16 * common_address_asdu; //!< associated with all objects contained within the ASDU
}iec104_group_description_t;

typedef struct MCU_PACK{
    iec104_group_description_t * const iec104_group_description;
    const int iec104_address;   //!< identification address
}iec104_description_t;
/*iec104section description end*/
/**
  *@brief struct regs_description using for storage description about vars
  * */
typedef struct MCU_PACK{//struct for reg description generic from regs.h
  const void * p_default;//!<pointer to default value or to value for user desc vars
  u8* p_value;//!<pointer to value
  u32 saved_address;    //!<address in bkram or flash where data stored, for variable needs it
  char* description;  //!<full name pointer
  char* name;  //!<short uniq name 
  u16 type;   //!<variable type u8 u16 u32 float double
  u16 ind;    //!<index
  u32 guid;   //!<guid uniq for variable
  u16 size;    //!<array size in type value 
  u8 property;      //!< @ref property (bit0 - writeable)
  u32 group;     //!< space number
  iec104_description_t * iec104_description;//!< use it for iec104 protocol
} regs_description_t;//!< @property this item must be unchangable name because using in beremiz user generator
/**
  * @brief struct regs_template_t use it for access to variable throuth regs access
  * */
typedef struct MCU_PACK{
  const u8* p_default;//!<pointer to default value
  u8* p_value;//!<pointer to value
  u32 saved_address;    //!<address in bkram or flash where data stored, for variable needs it
  char* description;  //!<full name pointer
  const char* name;  //!<short uniq name
  u16 type;   //!<variable type u8 u16 u32 float double
  u16 ind;    //!<index
  u32 guid;   //!<guid uniq for variable
  u16 size;    //!<array size in type value
  u8 property;      //!<property
  u32 group;     //!< space number
  iec104_description_t * iec104_description;//!< use it for iec104 protocol
  u16 size_in_bytes;   //!<array size in bytes
} regs_template_t;//!< @property this item must be unchangable name because using in beremiz user generator
typedef enum{
    GUID_ADDRESS_MASK   =    0x000FFFFF,
    GUID_TYPE_MASK      =    0xfff00000,
    GUID_OS_HEAD        =    0x00000000,
    GUID_USER_HEAD      =    0x10000000,
    GUID_USER_MDB_FIELD_MASK =    0x0F000000,/*!< modbus field mask*/
    GUID_USER_IS_01_MD  =    0x01000000,/*!< its belong modbus coils areas 01*/
    GUID_USER_IS_02_MD  =    0x02000000,/*!< its belong modbus input descretes areas 02*/
    GUID_USER_IS_03_MD  =    0x04000000,/*!< its belong modbus holdings regs areas 03*/
    GUID_USER_IS_04_MD  =    0x08000000,/*!< its belong modbus input regs areas 04*/
}regs_description_guid;

#define GUID_USER_ADDRESS_HEAD  BEREMIZ_ADDRESS_SPACE_START*2
#define GUID_USER_ARRAY_HEAD    BEREMIZ_ARRAY_ADDRESS_SPACE_START*2
extern regs_description_t const regs_description[];
int regs_description_get_by_name(regs_template_t * regs_template) MCU_ROOT_CODE;
int regs_description_get_by_ind(regs_template_t * regs_template) MCU_ROOT_CODE;
int regs_description_get_by_guid(regs_template_t * regs_template) MCU_ROOT_CODE;
int regs_description_add_user_vars(const regs_description_t * user_description, u16 num_of_user_vars)MCU_ROOT_CODE;
u8 regs_description_is_writeable (u16 reg_index)MCU_ROOT_CODE;
/**
 * @brief regs_get_index_by_byte_address return index of regs by byte address
 * @param byte_address - byte number
 * @return
 */
int regs_description_get_index_by_byte_address(u32 byte_address)MCU_ROOT_CODE;

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //REGS_DESCRIPTION_H
