/**
 * @file nrf24.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup free_rtos\inc\meso
 * @ingroup free_rtos\inc\meso
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef NRF24_H
#define NRF24_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "pin_map.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */

// --- NRF24 pin description ---

#define NRF24_CE_PIN            MESO_GP0_PIN
#define NRF24_CE_GPIO_PORT      MESO_GP0_GPIO_PORT
#define NRF24_CSN_PIN           MESO_GP0_PIN
#define NRF24_CSN_GPIO_PORT     MESO_GP0_GPIO_PORT
#define NRF24_IRQ_PIN           DAC1_PIN
#define NRF24_IRQ_GPIO_PORT     DAC1_GPIO_PORT
#define NRF24_SCK_PIN           MESO_SCK_PIN
#define NRF24_SCK_GPIO_PORT     MESO_SCK_GPIO_PORT
#define NRF24_MISO_PIN          MESO_MISO_PIN
#define NRF24_MISO_GPIO_PORT    MESO_MISO_GPIO_PORT
#define NRF24_MOSI_PIN          MESO_MOSI_PIN
#define NRF24_MOSI_GPIO_PORT    MESO_MOSI_GPIO_PORT

// --- NRF24 register adress and commands ---

#define ACTIVATE 0x50
#define RD_RX_PLOAD 0x61 // Define RX payload register address
#define WR_TX_PLOAD 0xA0 // Define TX payload register address
#define FLUSH_TX 0xE1
#define FLUSH_RX 0xE2

#define CONFIG 0x00     //'Config' register address
#define EN_AA 0x01      //'Enable Auto Acknowledgment' register address
#define EN_RXADDR 0x02  //'Enabled RX addresses' register address
#define SETUP_AW 0x03   //'Setup address width' register address
#define SETUP_RETR 0x04 //'Setup Auto. Retrans' register address
#define RF_CH 0x05      //'RF channel' register address
#define RF_SETUP 0x06   //'RF setup' register address
#define STATUS 0x07     //'Status' register address
#define RX_ADDR_P0 0x0A //'RX address pipe0' register address
#define RX_ADDR_P1 0x0B //'RX address pipe1' register address
#define TX_ADDR 0x10    //'TX address' register address
#define RX_PW_P0 0x11   //'RX payload width, pipe0' register address
#define RX_PW_P1 0x12   //'RX payload width, pipe1' register address
#define FIFO_STATUS 0x17 //'FIFO Status Register' register address
#define DYNPD 0x1C
#define FEATURE 0x1D

#define PRIM_RX 0x00    //RX/TX control (1: PRX, 0: PTX)
#define PWR_UP 0x01     //1: POWER UP, 0:POWER DOWN
#define RX_DR 0x40      //Data Ready RX FIFO interrupt
#define TX_DS 0x20      //Data Sent TX FIFO interrupt
#define MAX_RT 0x10     //Maximum number of TX retransmits interrupt

#define W_REGISTER 0x20 //write to register

u8 nrf_init(void);
u8 nrf_spi_init(void);
u8 nrf_gpio_init (void);
u8 nrf_read_reg (u8 addr);
u8 nrf_write_reg (u8 addr, u8 data);
void nrf_toggle_features(void);
void nrf_read_buff (u8 addr, u8 *buff, u8 len);
void nrf_write_buff (u8 addr, u8 *buff, u8 len);
void nrf_flush_rx (void);
void nrf_flush_tx (void);
void nrf_rx_mode (void);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //NRF24_H
