/**
 * @file external_flash.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup periph
 * @ingroup sofi
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef EXTERNAL_FLASH_H
#define EXTERNAL_FLASH_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_init.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "stm32f7xx_hal.h"
/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
#define EXTERNAL_FLASH_NAME "w25q128jvsiq"
/* Definition for QSPI DMA */
#define QSPI_DMA_INSTANCE          DMA2_Stream2
#define QSPI_DMA_CHANNEL           DMA_CHANNEL_11
#define QSPI_DMA_IRQ               DMA2_Stream2_IRQn
#define QSPI_DMA_IRQ_HANDLER       DMA2_Stream2_IRQn

/* winbond memory */
/* Size of the flash */
#define QSPI_FLASH_SIZE                      24
/* End address of the QSPI memory */
#define QSPI_END_ADDR              (1 << QSPI_FLASH_SIZE)

#define QSPI_PAGE_SIZE                       256
#define QSPI_SECTOR_SIZE                     4096
#define QSPI_BLOCK_SIZE                      65536
#define EXTERNAL_FLASH_START_ADDRESS_FOR_MEM_MAP_MODE 0x00000000

/* Identification Operations */
#define READ_ID_CMD                          0x90
#define READ_ID_CMD2                         0x9F
#define READ_UID_CMD                         0x4B
//#define MULTIPLE_IO_READ_ID_CMD              0xAF
#define READ_SERIAL_FLASH_DISCO_PARAM_CMD    0x5A

/* Read Operations */
#define READ_CMD                             0x03
//#define READ_4_BYTE_ADDR_CMD                 0x13

#define SET_READ_PARAM_CMD                        0xc0
#define FAST_READ_CMD                        0x0B
//#define FAST_READ_DTR_CMD                    0x0D
#define FAST_READ_4_BYTE_ADDR_CMD            0x0C

#define DUAL_OUT_FAST_READ_CMD               0x3B
#define DUAL_OUT_FAST_READ_LOCK_CMD           0x3D
//#define DUAL_OUT_FAST_READ_4_BYTE_ADDR_CMD   0x3C

#define DUAL_INOUT_FAST_READ_CMD             0xBB
//#define DUAL_INOUT_FAST_READ_DTR_CMD         0xBD
//#define DUAL_INOUT_FAST_READ_4_BYTE_ADDR_CMD 0xBC

#define QUAD_OUT_FAST_READ_CMD               0x6B
//#define QUAD_OUT_FAST_READ_DTR_CMD           0x6D
//#define QUAD_OUT_FAST_READ_4_BYTE_ADDR_CMD   0x6C

#define QUAD_INOUT_FAST_READ_CMD             0xEB
//#define QUAD_INOUT_FAST_READ_DTR_CMD         0xED
//#define QUAD_INOUT_FAST_READ_4_BYTE_ADDR_CMD 0xEC

/* Write Operations */
#define WRITE_ENABLE_CMD                     0x06
#define WRITE_DISABLE_CMD                    0x04

/* Register Operations */
#define QE_REG2_STATUS                        (1<<9)
#define READ_STATUS_REG1_CMD                  0x05
#define WRITE_STATUS_REG1_CMD                 0x01
#define READ_STATUS_REG2_CMD                  0x35
#define WRITE_STATUS_REG2_CMD                 0x31
#define READ_STATUS_REG3_CMD                  0x15
#define WRITE_STATUS_REG3_CMD                 0x11

//#define READ_LOCK_REG_CMD                    0xE8
//#define WRITE_LOCK_REG_CMD                   0xE5

//#define READ_FLAG_STATUS_REG_CMD             0x70
//#define CLEAR_FLAG_STATUS_REG_CMD            0x50

//#define READ_NONVOL_CFG_REG_CMD              0xB5
//#define WRITE_NONVOL_CFG_REG_CMD             0xB1

//#define READ_VOL_CFG_REG_CMD                 0x85
//#define WRITE_VOL_CFG_REG_CMD                0x81

//#define READ_ENHANCED_VOL_CFG_REG_CMD        0x65
//#define WRITE_ENHANCED_VOL_CFG_REG_CMD       0x61

//#define READ_EXT_ADDR_REG_CMD                0xC8
//#define WRITE_EXT_ADDR_REG_CMD               0xC5

/* Program Operations */
#define PAGE_PROG_CMD                        0x02
//#define PAGE_PROG_4_BYTE_ADDR_CMD            0x12

//#define DUAL_IN_FAST_PROG_CMD                0xA2
//#define EXT_DUAL_IN_FAST_PROG_CMD            0xD2

#define QUAD_IN_FAST_PROG_CMD                0x32
//#define EXT_QUAD_IN_FAST_PROG_CMD            0x12 /*0x38*/
//#define QUAD_IN_FAST_PROG_4_BYTE_ADDR_CMD    0x34

/* Erase Operations */
#define SECTOR_ERASE_CMD                  0x20
//#define SUBSECTOR_ERASE_4_BYTE_ADDR_CMD      0x21

#define BLOCK_ERASE_CMD                     0x52
//#define SECTOR_ERASE_4_BYTE_ADDR_CMD         0xDC

#define CHIP_ERASE_CMD                       0xC7

#define PROG_ERASE_RESUME_CMD                0x7A
#define PROG_ERASE_SUSPEND_CMD               0x75

/* One-Time Programmable Operations */
//#define READ_OTP_ARRAY_CMD                   0x4B
#define PROG_SEC_REG_CMD                   0x42
#define READ_SEC_REG_CMD                   0x48

/* 4-byte Address Mode Operations */
#define ENTER_4_BYTE_ADDR_MODE_CMD           0xB7
#define EXIT_4_BYTE_ADDR_MODE_CMD            0xE9

/* Quad Operations */
#define ENTER_QUAD_CMD                       0x38
#define EXIT_QUAD_CMD                        0xFF

/* Default dummy clocks cycles */
#define DUMMY_CLOCK_CYCLES_READ_QUAD         8



/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)        (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))


extern vu8 cmd_cplt_count;
extern vu8 rx_cplt_count;
extern vu8 tx_cplt_count;
extern vu8 status_match_count;
extern QSPI_HandleTypeDef hqspi_global;
extern DMA_HandleTypeDef hdma_quadspi;
typedef enum{
    NO_INIT_MODE,
    INDIRECT_MODE,
    STATUS_POLLING_MODE,
    MEMORY_MAPPED_MODE
}external_flash_mode_t;
extern external_flash_mode_t external_flash_mode;

extern osSemaphoreId flash_access_mutex;
int external_flash_init(void);
int external_flash_deinit(void);
u8 qspi_write_enable(QSPI_HandleTypeDef *hqspi);
u8 qspi_auto_polling_mem_ready(QSPI_HandleTypeDef *hqspi);
u32 qspi_read_status_reg(QSPI_HandleTypeDef *hqspi);
int external_flash_set_dummy_cycles(QSPI_HandleTypeDef *hqspi);
int external_flash_set_qspi_mode(QSPI_HandleTypeDef *hqspi);
int external_flash_switch_to_mem_maped(void);
int external_flash_switch_from_mem_maped(void);
/**
 * @brief erase sector
 * @param address is absolutly byte address
 * @return non zero value if error
 * */
int external_flash_erase_sector(u32 address);
/**
 * @brief erase sector with attempts
 * @param address is absolutly byte address
 * @return non zero value if error
 * */
int external_flash_erase_sector_with_attempts(u32 address,u8 attempts);

u16 external_flash_write(u32 address,const u8* buff,u16 length);
u16 external_flash_read(u32 address,u8* buff,u16 length);
int external_flash_is_flash(u32 addr, u32 size);
int external_flash_enter_4_byte_address(QSPI_HandleTypeDef *hqspi);
int external_flash_reinit(void);
u8 extenal_flash_in_error_state(void);
//ld space variable open
extern uint32_t _qspi_init_base;
extern uint32_t _qspi_init_length;
//ld space variable and
/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //EXTERNAL_FLASH_H
