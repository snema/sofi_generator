/* The total number of nodes, needed to support _all_ instances of the modbus plugin */
#define TOTAL_TCPNODE_COUNT       0
#define TOTAL_RTUNODE_COUNT       1
#define TOTAL_ASCNODE_COUNT       0
/* Values for instance 1 of the modbus plugin */
#define MAX_NUMBER_OF_TCPCLIENTS  0
#define NUMBER_OF_TCPSERVER_NODES 0
#define NUMBER_OF_TCPCLIENT_NODES 0
#define NUMBER_OF_TCPCLIENT_REQTS 0

#define NUMBER_OF_RTUSERVER_NODES 0
#define NUMBER_OF_RTUCLIENT_NODES 1
#define NUMBER_OF_RTUCLIENT_REQTS 1

#define NUMBER_OF_ASCIISERVER_NODES 0
#define NUMBER_OF_ASCIICLIENT_NODES 0
#define NUMBER_OF_ASCIICLIENT_REQTS 0

#define NUMBER_OF_SERVER_NODES (NUMBER_OF_TCPSERVER_NODES + \
                                NUMBER_OF_RTUSERVER_NODES + \
                               NUMBER_OF_ASCIISERVER_NODES)

#define NUMBER_OF_CLIENT_NODES (NUMBER_OF_TCPCLIENT_NODES + \
                                NUMBER_OF_RTUCLIENT_NODES + \
                                NUMBER_OF_ASCIICLIENT_NODES)

#define NUMBER_OF_CLIENT_REQTS (NUMBER_OF_TCPCLIENT_REQTS + \
                                NUMBER_OF_RTUCLIENT_REQTS + \
                                NUMBER_OF_ASCIICLIENT_REQTS)

#define MAX_READ_BITS 254
#define MAX_WORD_NUM 127
#define MAX_PACKET_LEN (MAX_WORD_NUM*2 + 7)

static client_node_t		client_nodes[NUMBER_OF_CLIENT_NODES] = {
{"2.2"/*location*/, {naf_rtu, {.rtu = {RS_232_UART, 115200 /*baud*/, 0 /*parity*/, 8 /*data bits*/, 1 /*stop bits*/, 0 /* ignore echo */}}}/*node_addr_t*/, -1 /* mb_nd */, 0 /* init_state */, 200 /* communication period */,0,NULL},
};

static client_request_t	client_requests[NUMBER_OF_CLIENT_REQTS] = {
{"2_2_0"/*location*/,RS_232_UART/*channel*/, 0/*client_node_id*/, 3/*slave_id*/, req_input/*req_type*/, 3/*mb_function*/, 8 /*first reg address*/,
1/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 600/* timeout */,
NULL, NULL},
};

u16 *__MW2_2_0_0_TEST;
static u16 plcv_buffer_req_0_TEST[2] = {0,0};
static u16 com_buffer_req_0_TEST[2] = {0,0};
