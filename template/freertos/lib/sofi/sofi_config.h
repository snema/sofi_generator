
#ifndef SOFI_CONFIG_H
#define SOFI_CONFIG_H 1
#include "type_def.h"

#ifdef __cplusplus 
   extern "C" {
#endif
#ifndef SOFI_RELEASE
    #define SOFI_RELEASE    0
#endif

//functions 
//kernel
#ifndef KERNEL_ENABLE
    #define KERNEL_ENABLE 1
#endif
//flash
//external_flash user_task
#ifndef SOFI_EXTERNAL_FLASH_USE
#define SOFI_EXTERNAL_FLASH_USE    1
#define EXTERNAL_FLASH_BASE_ADDRESS_MEM_MAP_MODE 0x90000000
#define EXTERNAL_FLASH_TASK_SPACE_START_SHIFT 0x00000000   //pointer to len downloaded file
#define FLASH_TASK_START_SHIFT 0x00000004 //code space after len
#define MAX_TASK_EXTERNAL_FLASH_SIZE                    0x100000
#endif
//internal_flash user_task
#define INTERNAL_FLASH_BASE_SECTOR      8           //int_user_task located in sectors from 8 to 11
#define INTERNAL_FLASH_MAX_SECTORS_USE  4           //int_user_task located in sectors from 8 to 11

#define INTERNAL_FLASH_TASK_SPACE_SHIFT 0x00000000  //pointer to len downloaded file
#define INTERNAL_FLASH_TASK_START_SHIFT 0x00000004  //code space after len
#define MAX_TASK_INTERNAL_FLASH_SIZE    0x100000     //1024 kb
#define RETAIN_BKRAM_START              1024
//internal_flash os
#define OS_LEN_ADDRESS_SHIFT            1020        //os len in flash before link_functions
#define MAX_OS_FLASH_SIZE               0x80000     //512 kb
#define OS1_FLASH_BASE_SECTOR           0           //os1 located in sectors from 0 to 5
#define OS1_FLASH_SECTOR_NUM            6
#define OS2_FLASH_BASE_SECTOR           6           //os2 located in sectors from 6 to 7
#define OS2_FLASH_SECTOR_NUM            2
#define LAST_OS_SECTOR                  7           //last sector part

//ai
//external spi ad7949
#ifndef SOFI_SPI_ADC_USE
#define SOFI_SPI_ADC_USE    1
#endif
//internal ai
#ifndef SOFI_INTERNAL_ADC_USE
#define SOFI_INTERNAL_ADC_USE    1
#endif

//di
#ifndef DI_FREQ_LIMIT_ENABLE
#define DI_FREQ_LIMIT_ENABLE 0
#endif

//lwip channel
#ifndef SOFI_ETHERNET
#define SOFI_ETHERNET    1
#endif
#ifndef SOFI_SLIP
#define SOFI_SLIP        1
#endif
#ifndef SOFI_USB
#define SOFI_USB         1
#endif
#if SOFI_USB
#define SOFI_USB_IP      1
#endif

//debug
#ifndef SOFI_LWIP_MONITOR
#define SOFI_LWIP_MONITOR         1
#endif

#ifndef SOFI_MONITOR
    #define SOFI_MONITOR 1
#endif
/*
#ifndef DEBUG
    #define DEBUG 0
#endif
*/

#if SOFI_ETHERNET||SOFI_SLIP||SOFI_USB_IP
#define SOFI_LWIP    1
#ifndef MODBUS_TCP_ENABLE
    #define MODBUS_TCP_ENABLE 1
#endif
#ifndef IEC_60870_ENABLE
    #define IEC_60870_ENABLE 1
#endif

#ifndef HTTP_SEVER_ENABLE
    #define HTTP_SEVER_ENABLE 1
    #define HTTPS_SEVER_ENABLE 0

    #define  HTTPS_USE_RSA 0
#endif

#ifndef MODBUS_UDP_ENABLE
    #define MODBUS_UDP_ENABLE 1
#endif

#else
#define SOFI_LWIP    0
#define MODBUS_TCP_ENABLE 0
#define HTTP_SEVER_ENABLE 0
#define MODBUS_UDP_ENABLE 0
#endif
#ifndef UDP_BROADCAST_ENABLE
    #define UDP_BROADCAST_ENABLE 1
#endif
#if UDP_BROADCAST_ENABLE
#define UDP_BROADCAST_ADVRTSMNT 1
#else
#define UDP_BROADCAST_ADVRTSMNT 0
#endif
#define STM_NUCLEO_BOARD 0
#if STM_NUCLEO_BOARD==1
    #define BOARD_VERSION -1
#else
   /*BOARD_VERSION [0;254]*/
    #define BOARD_VERSION 3
#endif

// MESO
#define MESO_INTERFACE 1
#if MESO_INTERFACE
    // select 1 variant
    #define MESO_I2C_EN 0
    #define MESO_UART_EN !MESO_I2C_EN

    #define MESO_SPI_EN 1
    #define MESO_NRF24_EN 0
#else
#define MESO_I2C_EN 0
#define MESO_UART_EN 0
#define MESO_SPI_EN 0
#endif//MESO_INTERFASE
#ifndef PACKET_REPEATER
    #define PACKET_REPEATER 1
#endif
//uarts enable start
#if MESO_UART_EN
    #ifndef UART1_EN
        #define UART1_EN 1  // MESO_UART_UART
    #endif
#endif
#ifndef UART2_EN
    #define UART2_EN 1  // RS_485_2_UART
#endif
#ifndef UART3_EN
    #define UART3_EN 1  // RS_232_UART
#endif
#ifndef UART4_EN
    #define UART4_EN 0  // nothing
#endif

#ifndef UART5_EN
    #define UART5_EN 1  // RS_485_1_UART
#endif

#ifndef UART6_EN
    #define UART6_EN 1  // RS_485_IMMO_UART
#endif
#ifndef UART7_EN
    #define UART7_EN 1  // HART
#endif
#if UART7_EN
    #ifndef HART_EN
        #define HART_EN 1
    #endif
#else
    #define HART_EN 0
#endif
#ifndef IMMO_CAN_EN
    #define IMMO_CAN_EN 1
#endif

#ifndef MODBUS_RTU
    #define MODBUS_RTU 1
#endif
#define CHANNELS_NUMBER   (UART1_EN+UART2_EN+UART3_EN+UART4_EN+UART5_EN+UART6_EN+HART_EN+IMMO_CAN_EN)
//uarts enable end

//test enable start
#ifndef SOFI_TEST_ENABLE
    #define SOFI_TEST_ENABLE 1
#endif

#if SOFI_TEST_ENABLE
#ifndef TASK_TEST
    #define TASK_TEST 0
#endif

#ifndef FREERTOS_TEST_ENABLE
    #define FREERTOS_TEST_ENABLE 0
#endif

#ifndef USER_TASK_TEST
    #define USER_TASK_TEST 0
#endif
#define ENABLE_EXTERNAL_FLASH_TEST 0
#define ENABLE_LFS_ARC_TEST 0
#else
#define TASK_TEST 0
#define MODBUS_TEST 0
#define UART_TEST 0
#define DI_TEST 0
#define DO_TEST 0
#define RANDOM_TEST 0
#define MESO_I2C_TEST 0
#define FREERTOS_TEST_ENABLE 0
#define EXTERNAL_FLASH_TEST 0
#define CAN_TEST_EN 0
#define SOFI_ANALOG_TEST 0
#define CRC_TEST 0
#define RTC_TEST 0
#define INTERNAL_FLASH_TEST 0
#define PACKET_TEST 0
#endif //SOFI_TEST_ENABLE
//test enable end

//priority start
#define SOFI_TIM3_PRIO    5
#define SOFI_TIM3_SUB_PRIO    0
#define SOFI_SYSTICK_PRIO 5
#define SOFI_SYSTICK_SUB_PRIO 2
#define SOFI_DI_PRIO   3
#define SOFI_DI_SUB_PRIO   1
#define SOFI_ETHERNET_PRIO   7
#define SOFI_ETHERNET_SUB_PRIO   0
#define SOFI_QSPI_DMA_PRIO   8
#define SOFI_QSPI_DMA_SUB_PRIO   0
#define SOFI_QSPI_PRIO   8
#define SOFI_QSPI_SUB_PRIO   1
#define SOFI_SPI4_PRIO   9
#define SOFI_SPI4_SUB_PRIO   2
#define SOFI_UARTS_PRIO   9
#define SOFI_UARTS_SUB_PRIO   0
#define SOFI_UARTS_PRIO_H   9
#define SOFI_UARTS_SUB_PRIO_H   1
#define SOFI_CAN_PRIO   9
#define SOFI_CAN_SUB_PRIO   3

#define SOFI_OTG_PRIO   9
#define SOFI_OTG_SUB_PRIO   2
#define SOFI_ADC_PRIO   13
#define SOFI_ADC_SUB_PRIO   0
#define SOFI_TIM2_PRIO    14
#define SOFI_TIM2_SUB_PRIO    0
#define SOFI_TIM6_PRIO    14
#define SOFI_TIM6_SUB_PRIO    0
#define SOFI_RTC_PRIO   14
#define SOFI_RTC_SUB_PRIO   1

#define MEM_MAPED_ENABLE 0

#include "pin_map.h"
//priority end
#ifdef __cplusplus
}
#endif
#endif //SOFI_CONFIG_H
