/**
 * @file analog.h
 * @brief
 * @author Shoma Gane <shomagan@gmail.com>
 * @author Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @version 0.1
 * @ingroup AI
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef ANALOG_H
#define ANALOG_H 1
 
/*add includes below */

#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */

/**
 * @addtogroup AI
 * @{
 */
#define AD7949_MAX_CHANNEL_NUMBER       9
#define AD7949_REEAD_TMPR_TEMPLATE      0x2C40
#define AD7949_REEAD_TMPR_TEMPLATE_EXT  0x2C58
#define AD7949_READ_MASK_TEMPLATE       0x3840  //0b0011110001000000
#define AD7949_READ_MASK_TEMPLATE_EXT   0x3858  //0b0011110001011000
#define AD7949_CHANNEL_MASK             0x0380  //0b0000xxx0000000
#define AD7949_CHANNEL_SHIFT            7       //0b0000xxx0000000
#define AD7949_CHANNEL_0                0x3c41  //0b11110001000001
#define AD7949_CHANNEL_1                0x3cc1  //0b11110011000001
#define AD7949_CHANNEL_2                0x3d41  //0b11110101000001
#define AD7949_CHANNEL_3                0x3dc1  //0b11110111000001
#define AD7949_CHANNEL_4                0x3e41  //0b11111001000001
#define AD7949_CHANNEL_5                0x3ec1  //0b11111011000001
#define AD7949_CHANNEL_6                0x3f41  //0b11111101000001
#define AD7949_CHANNEL_7                0x3fc1  //0b11111111000001
#define AD7949_CONVERSION_TIME_uS           4
#define AD7949_BETWEEN_CONVERSION_TIME_uS   5
/**
 * @}
 */

/**
 * @brief Define proper configuration register values of AD7949 chip in UNIPOLAR (SINGLE_ENDED) sampling mode
 * @ingroup AI
 */
typedef enum {
    AD7949_CONFIG_UPDATE          = 1<<13, /*!< 0 - keep current config,1 - overwrite */
    AD7949_INPUT_CHANNEL_CONFIG_2 = 1<<12, /*!< 1 - unipolar,0- bipolar */
    AD7949_INPUT_CHANNEL_CONFIG_1 = 1<<11, /*!< 1 - single,0- diff pairs */
    AD7949_INPUT_CHANNEL_CONFIG_0 = 1<<10, /*!< 1 - reference to gnd ,0- reference to com*/
    AD7949_INPUT_CHANNEL_SELECT_2 = 1<<9,  /*!< channel number [0;7]*/
    AD7949_INPUT_CHANNEL_SELECT_1 = 1<<8,  /*!< channel number [0;7]*/
    AD7949_INPUT_CHANNEL_SELECT_0 = 1<<7,  /*!< channel number [0;7]*/
    AD7949_SELECT_BW              = 1<<6,  /*!< 1- full BW, 0 -1/4 of BW*/
    AD7949_BUFFER_SELECTION_2     = 1<<5,  /*!< 0 - internal ref,temp enable,1 - ext ref 4.096*/
    AD7949_BUFFER_SELECTION_1     = 1<<4,  /*!< 1 - use external ref */
    AD7949_BUFFER_SELECTION_0     = 1<<3,  /*!< 1 - 4.096 v 0 - 2.5 v*/
    AD7949_CHANNEL_SEQUENCER_1    = 1<<2,  /*!< 00 - disable,01 - update during,*/
    AD7949_CHANNEL_SEQUENCER_0    = 1<<1,  /*!< 10 - scan with temp,11- scan with out temp*/
    AD7949_RB                     = 1<<0,  /*!< 0 - read back ,1- do not read back*/
}ad7949_definition_t;
/**
 * @brief struct for ai leds blinking control
 * @ingroup AI
 */
typedef struct MCU_PACK{
   u16 delay;
   u16 passed;
}ai_led_param_t;

extern const pin_map_t ai_led_pin_map[];

void analog_task(const void *parameters )__attribute__ ((noreturn));
int analog_spi_init(void);
void analog_spi_deinit(void);
int analog_spi_start_conversion(u8 channel_number);
void spi4_tx_interrupt_handing(void);
int analog_get_external(void);
int analog_gpio_init (void);
void analog_gpio_deinit (void);
int analog_init (void);
void analog_deinit(void);
void analog_get_unit(void);
void analog_leds_update(void);
void analog_pwr_state_update(void);


/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //ANALOG_H
