/**
 * @file 
 * @defgroup free_rtos/inc
 * @ingroup free_rtos/inc
 * use for configuration debug printf log and test sets
 * example  sofi_printf("Failed %s:%d", PRETTY_FUNCTION, LINE);
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef SOFI_DEBUG_H
#define SOFI_DEBUG_H 1
 
/*add includes below */
#include "sofi_config.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
extern u16 led_os_error_on_time;
extern u16 led_os_on_time;
extern u16 led_error_user_on_time;
extern u16 led_user_on_time;
extern u16 led_packet_recv_time;
extern u16 led_packet_send_time;
#define LEVEL_NONE         0 /* No log */
#define LEVEL_ERR          1 /* Errors usb*/
#define LEVEL_WARN         2 /* Warnings usb*/
#define LEVEL_INFO         3 /* Basic info usb*/
#define LEVEL_DBG          4 /* Detailled debug uart+usb*/
#define SOFI_LOG_ENABLE    1
#if SOFI_RELEASE
    #define PRINTF_LEVEL LEVEL_NONE
    #define MONITOR_PERIOD 1000 // 1 sec
#else
    #define MONITOR_PERIOD 30000 // 30 sec
#endif
#ifndef PRINTF_LEVEL
    #define PRINTF_LEVEL LEVEL_DBG
#endif
#ifndef LOG_LEVEL
    #define LOG_LEVEL LEVEL_DBG
#endif

// --- sofi_monitor ---

FNCT_NO_RETURN void sofi_monitor_task(const void *pvParameters);
void config_tim5_for_run_time_stats( void );

typedef struct {
    u8 task_number;
    u32 last_time;
    float percentage;
}task_time_t;
enum led_time{
    MODBUS_TCP_RECV_TIME_MS= 20,
    MODBUS_TCP_SEND_TIME_MS,
    INIT_ERROR_TIME_MS ,
    PACKET_ERROR_TIME_MS,
    PACKET_ERROR_CRITICAL_TIME_MS,
    PACKET_ERROR_TIMEOUT_TIME_MS,
    PACKET_ERROR_PULL_TIMEOUT_TIME_MS,/*<! whyly user did't get and free buffer*/
    MODBUS_ERROR_TIME_MS,
    MODBUS_RTU_RECV_TIME_MS,
    MODBUS_RTU_SEND_TIME_MS,
    MAIL_HANDLE_ERROR_TIME_MS,
    TEST_ERROR_MS,
    UART_INIT_ERROR,
    UART_SEND_TIME_MS,
    TEST_FAIL_TIME_MS,
    TEST_OK_TIME_MS,
    RTC_SET_TIME_ERROR_MS,
    RTC_SET_TIME_OK_MS,
    REG_BKRAM_WRITE_ERROR_MS,
    CAN_ERROR_TIME,
    EXTERNAL_FLASH_ERROR,
    REGS_WRITE_ERROR,
    LED_ARC_ADD_ERROR_MS,
    LED_ARC_MISTAKE_MS,
    MODBUS_TCP_SEND_ERROR,
    JSON_FILE_BUSY_TIMEOUT
};
#define  INIT_ERROR_FATAL  5000
void led_os_error_on(u16 time_ms) MCU_ROOT_CODE;
void led_os_on(u16 time_ms) MCU_ROOT_CODE;
void led_error_user_on(u16 time_ms) MCU_ROOT_CODE;
void led_user_on(u16 time_ms) MCU_ROOT_CODE;
void led_packet_recv_on(u16 time_ms) MCU_ROOT_CODE;
void led_packet_send_on(u16 time_ms) MCU_ROOT_CODE;
#define SOFI_PLATFORM_ASSERT(x) do {debug_printf("\"%s\" failed at line %d in %s\n", \
                                     x, __LINE__, __FILE__);} while(0)
#if SOFI_RELEASE
    #define SOFI_NOASSERT 1
#endif
#ifndef SOFI_NOASSERT
#define SOFI_ASSERT(message, assertion) do { if (!(assertion)) { \
  SOFI_PLATFORM_ASSERT(message); }} while(0)
#else
#define SOFI_ASSERT(message, assertion)
#endif  //sofi_noassert
#ifndef SOFI_ERROR
#define SOFI_ERROR LWIP_ERROR
#endif  //sofi_assert
#if STM_NUCLEO_BOARD
    #define DEBUG_UART_NUMBER UART3_NUMBER  // UART5 = MESO_UART
#else
    #define DEBUG_UART_NUMBER UART3_NUMBER  // UART3 = RS_232_UART
#endif
#if SOFI_LOG_ENABLE
#define sofi_log archive_log
#else
#define sofi_log
#endif

#if PRINTF_LEVEL
#define sofi_error_message debug_printf
#define sofi_printf debug_printf
#define sofi_printf_variable(name) sofi_printf(#name)
int debug_printf(const char * format, ...);
#else
int debug_printf(const char * format, ...);
#define sofi_printf(format, ...)
#define sofi_error_message(format, ...)
#endif
/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //SOFI_DEBUG_H
