/**
 * @file    kernel_starter.h
 * @author  Shoma Gane <shomagan@gmail.com>
 * @author  Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @ingroup os_start
 * @version 0.1
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef KERNEL_STARTER_H
#define KERNEL_STARTER_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */

/**
  * @addtogroup os_start
  * @{
  */
#define FIRST_OS_START_ADDR     0x8000000
#define SECOND_OS_START_ADDR    0x8080000
#define FIRST_OS_ICTM_ADDR      0x200000
#define SECOND_OS_ICTM_ADDR     0x280000
#define OS_FLASH_DISTANCE       0x80000
#define FIRST_OS_OB             0x00000080
#define SECOND_OS_OB            0x000000A0
/**
  * @}
  */

/**
  * @brief OS numbers
  * @ingroup os_start
  */
enum{
    NONE_OS = 0,
    FIRST_OS =1,
    SECOND_OS =2
} ;


void kernel_starter(void);
void start_os(u16 os);
void set_main_os(u16 os);
u16 get_main_os(void);
void disable_NVIC_IRQs(void);
u16 os_definition(void);
u8 check_os_crc (u16 os);
void shutdown_os(void);
int stop_user_task (u8 task,int signal);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //KERNEL_STARTER_H
