#ifndef SOFI_INIT_H
#define SOFI_INIT_H
#include "sofi.h"
#include "sofi_config.h"
#include "sofi_debug.h"
#include "stm32f7xx_hal.h"
#include "cmsis_os.h"

#define LAST_RST_TIME_BKP_ADDR      0
#define LAST_RST_REASON_BKP_ADDR    10
#define ERROR_OS_BKP_ADDR           11
#define WDG_RST_CNT_BKP_ADDR        12
#define SAVED_START_ADDRESS         16
#define IWGD_PERIOD_MS              512
/* These macros should be calculated by the preprocessor and are used
   with compile-time constants only (so that there is no little-endian
   overhead at runtime). */
#define HTONS(x) ((u16_t)((((x) & (u16_t)0x00ffU) << 8) | (((x) & (u16_t)0xff00U) >> 8)))
#define NTOHS(x) HTONS(x)
#define HTONL(x) ((((x) & (u32_t)0x000000ffUL) << 24) | \
                     (((x) & (u32_t)0x0000ff00UL) <<  8) | \
                     (((x) & (u32_t)0x00ff0000UL) >>  8) | \
                     (((x) & (u32_t)0xff000000UL) >> 24))
#define NTOHL(x) HTONL(x)

void vApplicationMallocFailedHook( void );
void vApplicationIdleHook( void );
void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName );
void vApplicationTickHook( void );
void vAssertCalled( uint32_t ulLine, const char *pcFile );
void preinit( void );
u8 it_is_ram(u8* space,u32 space_size);
void prvSystemClockConfig( void );
void sofi_init_task(const void *pvParameters )__attribute__ ((noreturn));
void init(void);
void init_system_clock(void);
void gpio_clk_enable(void);
void MX_IWDG_Init(void);
void random_gen_init(void);

void cpu_cache_enable(void);
void cpu_cache_disable(void);

int set_regs_def_values (void);
int sofi_init_ip_apps( void );
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority);
#ifndef htonl
/**
 * Convert an u32_t from host- to network byte order.
 *
 * @param n u32_t in host byte order
 * @return n in network byte order
 */
u32 htonl(u32 n);
#endif

#ifndef BIT
#define BIT(x)  (1 << x)
#endif

typedef enum {
    ERROR_OK = 0,
    ERROR_INIT
}error_t;

typedef union MCU_PACK{
   struct MCU_PACK{
       unsigned sys_tick             :1;
       unsigned rtc_wkup             :1;
   } bit;
   u8 reg;
} itterupt_start_flags_t;
extern itterupt_start_flags_t itterupt_start_flags;

extern RTC_HandleTypeDef hrtc;
extern IWDG_HandleTypeDef hiwdg;
extern RNG_HandleTypeDef hrng;
extern u32 reason_of_reset;


#endif
