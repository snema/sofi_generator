#ifndef TIMER_H
#define TIMER_H 1
#ifdef __cplusplus 
   extern "C" {
#endif
#include "sofi_init.h"
#define TIME_YIELD_THRESHOLD 100

//functions 
error_t tmr_1_init(void);
error_t tmr_1_deinit(void);
error_t tmr_2_init(void);
error_t tmr_2_deinit(void);
error_t tmr_3_init(void);
error_t tmr_3_deinit(void);
error_t tmr_5_init(void);
error_t tmr_5_deinit(void);
error_t tmr_6_init(void);
error_t tmr_6_deinit(void);
error_t tmr_7_init(void);
error_t tmr_7_stop(void);
error_t tmr_7_start(void);
error_t tmr_7_start_itterupt(void);
error_t tmr_7_stop_itterupt(void);

error_t tmr_7_set_counter(u32 counter);
u32 tmr_7_get_counter(void);
u32 tmr_7_get_arr(void);
error_t tmr_7_deinit(void);

u32 get_tim5_value( void );
void tim5_delay_us( u32 usec);
enum {
   TMR_FLAG_SET_TIMER /*! Configure the timer attributes */ = (1<<0),
   TMR_FLAG_IS_SOURCE_CPU /*! Use the CPU as the source for the clock (timer mode) */ = (1<<1),
   TMR_FLAG_IS_SOURCE_IC0 /*! Use input capture channel 0 for the clock source (counter mode) */ = (1<<2),
   TMR_FLAG_IS_SOURCE_IC1 /*! Use input capture channel 1 for the clock source (counter mode) */ = (1<<3),
   TMR_FLAG_IS_SOURCE_IC2 /*! Use input capture channel 2 for the clock source (counter mode) */ = (1<<4),
   TMR_FLAG_IS_SOURCE_IC3 /*! Use input capture channel 3 for the clock source (counter mode) */ = (1<<5),
   TMR_FLAG_IS_SOURCE_EDGERISING /*! Count rising edges */ = (1<<6),
   TMR_FLAG_IS_SOURCE_EDGEFALLING /*! Count falling edges */ = (1<<7),
   TMR_FLAG_IS_SOURCE_EDGEBOTH /*! Count both edges */ = (1<<8),
   TMR_FLAG_IS_SOURCE_COUNTDOWN /*! Count down (not up) */ = (1<<9),
   TMR_FLAG_IS_AUTO_RELOAD /*! Auto reload the timer */ = (1<<10),
   TMR_FLAG_SET_CHANNEL /*! Configure channel characteristics */ = (1<<11),
   TMR_FLAG_IS_CHANNEL_STOP_ON_RESET /*! Stop when the timer resets */ = (1<<12),
   TMR_FLAG_IS_CHANNEL_RESET_ON_MATCH /*! Reset when the timer finds a match */ = (1<<13),
   TMR_FLAG_IS_CHANNEL_STOP_ON_MATCH /*! Stop when the timer finds a match */ = (1<<14),
   TMR_FLAG_IS_CHANNEL_SET_OUTPUT_ON_MATCH /*! Set the output to 1 on a match */ = (1<<15),
   TMR_FLAG_IS_CHANNEL_CLEAR_OUTPUT_ON_MATCH /*! Clear the output to 0 on a match */ = (1<<16),
   TMR_FLAG_IS_CHANNEL_TOGGLE_OUTPUT_ON_MATCH /*! Toggle the output on a match */ = (1<<17),
   TMR_FLAG_IS_CHANNEL_PWM_MODE /*! Put the timer in PWM mode */ = (1<<18),
};

#ifdef __cplusplus
}
#endif
#endif //TIMER_H
