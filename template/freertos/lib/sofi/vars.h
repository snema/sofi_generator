/*
*global vars definition
*/
#ifndef VARS_H
#define VARS_H

#if defined __cplusplus
extern "C" {
#endif
#include "type_def.h"

#ifndef BIT
#define BIT(x)  (1 << x)
#endif

typedef enum {
    ERROR_OK = 0,
    ERROR_INIT
}error_t;

typedef union MCU_PACK{
    struct MCU_PACK{
        unsigned sys_tick             :1; 
        unsigned rtc_wkup             :1; 
    } bit;
    u8 reg;
} itterupt_start_flags_t;
extern itterupt_start_flags_t itterupt_start_flags;


#if defined __cplusplus
}
#endif

#endif //VARS_H
