/**
 * @file archive.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup free_rtos/littlefs/
 * @ingroup free_rtos/littlefs/
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef ARCHIVE_H
#define ARCHIVE_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "cmsis_os.h"
/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
typedef enum u32{
    ARC_IS_LAST = (1<<0),    //!<arc is last
    ARC_NOT_READED = (1<<1), //!< didt read this arc, sets mannualy
    ARC_IN_HANDLE = (1<<2), //!< didt read this arc, sets mannualy
    ARC_FLAGS_ERROR_WHILE_HANDLE = (1<<31),//!< seted when error occured while hanling message
}archive_flags;
typedef struct MCU_PACK{
       u8 sec;             /*!< RTC Time Seconds [0;59]*/
       u8 min;             /*!< RTC Time Minutes [0;59]*/
       u8 hour;            /*!< RTC Time Hour [0;23]*/
       u8 date;            /*!< RTC Date[1;31]*/
       u8 month;           /*!< RTC Date Month (in BCD format)*/
       u8 year;            /*!< RTC Date Year[0,99]*/
}archive_struct_time_t;
typedef enum u8{
    ARC_MAIL_ADD = 1,    //!<mail for add arc
    ARC_MAIL_GET = 2,    //!<mail for get arc
    ARC_TITLE_GET = 3,    //!<mail for get title
    ARC_CONFIRM_READ = 4,    //!<mail confirm read arc
    ARC_FULL_CLEAN = 5,    //!<mail delete all arc files
    ARC_LOG_GET = 6,    //!<mail delete all arc files
}archive_mail_command_t;

typedef enum {
   ARC_TYPE_EMPTY = 1,    //!< arc didn't add before
   ARC_NOT_EXIST = -1,    //!< arc with id not exist
   ARC_LEN_MISMATCH = -2,    //!< arc mismatch by len
   ARC_NUMBER_MISMATCH = -3,    //!< arc mismatch by number
   ARC_UNREACHABLE = -4,    //!< arc flash error

}archive_error_t;

typedef struct MCU_PACK{
    u16 id_number;                  /*!< uniq arc number [0:6] (uniq only inside plc)*/
    u16 header_len;                 /*!< len of header - sizeof(arc_header_t)*/
    u16 body_len;                   /*!< full len data and header*/
    archive_struct_time_t struct_time;  /*!< struct time of saving */
    u32 unix_time;                  /*!< unix time of saving   */
    archive_flags flags;                /*!< state flags of arc*/
    u32 id_crc;                  /*!< calculated from user data type*/
    u32 number;                     /*!< arc number by in order*/
}archive_header_t;

#define GET_LAST_BUFFER 0xffffffff
#define GET_FIRST_BUFFER 0x00
#define MIN_ARC_BODY_SIZE (sizeof(archive_header_t))
#define MAX_ARC_BODY_SIZE (sizeof(archive_header_t)+256)
#define MAX_ARC_SIZE ((sizeof(archive_header_t)+256)*1000)
#define ARC_CELL_SIZE (4096-512)
#define ARCHIVE_LOG_TEMP_BUFFER_SIZE (312+sizeof(u32))
#define LOG_FILE_NAME    "log"
#define LOG_FILE_SIZE    (4096 + sizeof(log_file_header_t))
#define LOG_FILE_ITEM_SIZE    256
#define MAX_ARCHIVES_NUM 10 //have only noteition characters,really restrictions has only in beremiz ide


typedef struct MCU_PACK{
   u16 id_number;                  /*!< uniq arc id (uniq only inside plc)*/
   u16 body_len;            /*!< len of arc header + data*/
   u32 unix_time_last_arc;  /*!< time for last arc writed*/
   u32 arcs_number;         /*!< number of arcs for this type*/
   u32 last_readed;         /*!< last arc readed*/
   u32 first_available;     /*!< first arc available in plc*/
}archive_file_title_t;
typedef struct MCU_PACK{
   archive_mail_command_t command;
   archive_flags state;
   union{
      archive_header_t * arc_header;
      u8 * data;
   }buffer;
   archive_file_title_t * arc_file_title;
}archive_mail_t;
typedef struct MCU_PACK{
   u16 header_len;
   u32 position;
}log_file_header_t;
extern osThreadId archive_thread_id;
FNCT_NO_RETURN void archive_sofi_task( void const * argument );
int archive_get_title(u16 id,archive_file_title_t * arc_file_title);
int archive_remove(u16 id);
int archive_remove_all(void);
/**
 * @brief arc_refresh_bkram_from_flash
 * @return 0 if succed
 */
int archive_refresh_bkram_from_flash(void);

/**
 * @brief arc_remove_bkram_mirror
 * @return 0 if removed
 */
int archive_remove_bkram_mirror(void);
/**
 * @brief archive_log_read
 * @param buff
 * @param position
 * @return
 */
int archive_log_read(u8 * buff,u16 position);
/**
 * @brief archive_log
 * @param format add log to ext flash
 * @return
 */
int archive_log(const char * format, ...);


/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //ARCHIVE_H
