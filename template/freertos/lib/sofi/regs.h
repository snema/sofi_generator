/**
 * @file    regs.h
 * @author  Shoma Gane <shomagan@gmail.com>
 * @author  Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @ingroup regs
 * @version 0.1
 */

#ifndef REGS_H
#define REGS_H 1
#include "sofi_config.h"
#include "sofi_debug.h"
#include "time.h"
#include "lwipopts.h"
#include "uart.h"
#include "packet.h"
#include "sofi_test.h"
#include "config.h"
#ifdef __cplusplus
   extern "C" {
#endif
extern osMutexId regs_access_mutex;
extern osMessageQId regs_event_queue;
//functions
void regs_event_handler (const void *pvParameters);
int write_reg_to_bkram(void *sofi_reg);
int read_reg_from_bkram(void *sofi_reg);
int write_reg_element_to_bkram(void *sofi_reg, u8 num);
#ifndef BIT
#define BIT(x)  (1 << x)
#endif


/**
  * @addtogroup regs
  * @{
  */
#define GLOBAL_UNION_SIZE 2048U
#define REGS_GLOBAL_ADDRESS(byte_number) (&sofi.bytes[(u16)byte_number])
#define REGS_NUMBER(reg_global_address) ((u16)((u32)reg_global_address - (u32)sofi.bytes))
/**
  * @}
  */

/**
  * @brief struct for real time
  * @ingroup regs
  */
typedef struct MCU_PACK{
    u8 hour;            /*!< RTC Time Hour [0;23]*/
    u8 min;             /*!< RTC Time Minutes [0;59]*/
    u8 sec;             /*!< RTC Time Seconds [0;59]*/
    u8 sub_sec;         /*!< RTC Sub Second register content [1 Sec / SecondFraction +1]*/
    u8 week_day;        /*!< RTC Date WeekDay*/
    u8 month;           /*!< RTC Date Month (in BCD format)*/
    u8 date;            /*!< RTC Date[1;31]*/
    u8 year;            /*!< RTC Date Year[0,99]*/
    u16 year_day;       /*!< RTC year of day */
}sofi_time_r;
/*
* for events
*/
   /*@todo add events NEED_REINIT_ETH_NETIF
                      NEED_REINIT_SLIP_NETIF
                      NEED_REINIT_FULL_LWIP
                      NEED_REINIT_ETH_MAC*/
/**
 * @todo add enumeration description
 */
enum {
    NEED_TO_WRITE_RTC = 0   /*!< NEED_TO_WRITE_RTC */
};
/**
  * @brief struct contain flags of runned tasks
  * @ingroup regs
  */
typedef union {
    struct {
        unsigned kernel               :1;/*!< kernel task must be work on*/
        unsigned init                 :1;/*!< init task must be work on*/
        unsigned ethernet_input       :1;/*!< ethernet_input task must be work on*/
        unsigned packet               :1;/*!< packet task must be work on*/
        unsigned analog               :1;/*!< analog task must be work on*/
        unsigned tcp_ip               :1;/*!< tcp_ip task inited */
        unsigned sofi_test            :1;/*!< analog test must be work on*/
        unsigned repeater             :1;/*!< task retranslates modbus packets*/
        unsigned lfs_sofi             :1;/*!< task works with flash and file system*/
        unsigned can_immod            :1;/*!< task works with can interface*/
        unsigned mezo                 :1;/*!< task works with mezo boards*/
        unsigned iec_handle_task      :1;/*!< iec handling task*/
    } bit;
    vu32 reg;
} task_created_t;

/**
  * @brief struct contain number of task restarts
  * @ingroup regs
  * @todo check implementation and brief
  */
typedef struct MCU_PACK{
   u64      kernel               ;/*!< kernel task count */
   u64      init                 ;/*!< init task count */
   u64      ethernet_input       ;/*!< ethernet task count */
   u64      packet               ;/*!< packet task count */
} task_count_t;
/**
  * @brief struct contain flags of initialized  modules
  * @ingroup regs
  */
typedef union MCU_PACK{
   struct MCU_PACK{
       unsigned ip             :1; /*!< ip was init*/
       unsigned usb                  :1; /*!< usb was init*/
       unsigned modbus_udp              :1; /*!< modbus udp server was init*/
       unsigned modbus_tcp              :1; /*!< modbus tcp server was init*/
       unsigned http                    :1; /*!< http server was init*/
       unsigned external_flash          :1; /*!< external flash was init*/
       unsigned analog_external         :1; /*!< spi adc was init*/
       unsigned get_value_from_bkram    :1; /*!< use settings from bkram*/
       unsigned set_value_from_bkram_to_flash    :1; /*!< use settings from bkram*/
       unsigned error_occured_while_init    :1; /*!< error was wjile init*/
       unsigned broadcast_udp    :1; /*!< broadcast udp had init*/
   } bit;
   vu32 reg;
} async_init_t;
/**
  * @brief struct contain flags of initialized  modules
  * @ingroup regs
  */
typedef union MCU_PACK{
   struct MCU_PACK{
       unsigned ip                      :1; /*!< ip was init*/
       unsigned usb                     :1; /*!< usb was init*/
       unsigned modbus_udp              :1; /*!< modbus udp server was init*/
       unsigned modbus_tcp              :1; /*!< modbus tcp server was init*/
       unsigned http                    :1; /*!< http server was init*/
       unsigned external_flash          :1; /*!< external flash was init*/
       unsigned analog_external         :1; /*!< spi adc was init*/
       unsigned repeater                :1; /*!< repeater*/
       unsigned lfs_sofi                :1;/*!< task works with flash and file system*/
       unsigned can_immod               :1;/*!< task works with can interface*/
       unsigned analog_task             :1;/*!< analog task*/
       unsigned lfs_sofi_format         :1;/*!< lfs was formated*/
   } bit;
   vu32 reg;
} async_init_success_t;

/**
  * @brief struct contain states of uarts
  * @ingroup regs
  */
typedef union MCU_PACK{
    struct MCU_PACK{
        unsigned uart_1_in_handing    :1;
        unsigned uart_2_in_handing    :1;
        unsigned uart_3_in_handing    :1;
        unsigned uart_4_in_handing    :1;
        unsigned uart_5_in_handing    :1;
        unsigned uart_6_in_handing    :1;
    } bit;
    vu32 reg;
} uart_state_t;
/**
  * @addtogroup sofi_debug
  * @{
  */
#define MONITOR_MAX_TASKS 32
/**
  * @}
  */
#define MAX_TASK_NAME_SIZE 16
/**
  * @brief struct of task information for sofi monitor
  * @ingroup sofi_debug
  */
typedef struct MCU_PACK{
    u8 task_number;
    u8 priority;
    u8 state;
    u8 flag;
    u32 free_stack;
    float runnig_time;
    u8 name[MAX_TASK_NAME_SIZE];
}task_info_t;
#define RISING 1    /** @todo check implementation */
#define FALLING 0   /** @todo check implementation */

/**
  * @brief mode of DI channel work
  * @ingroup di
  * @todo determine which group to include
  */
typedef enum{
    DI_NONE     = 0,
    DI_CNT_EN   = 1<<0,     /*!< enable counter for di*/
    DI_FREQ_EN  = 1<<1,     /*!< enable freq calc for di*/
}di_mode_t;
/**
  * @brief parameters of DI channel
  * @ingroup di
  * @todo determine which group to include
  */
typedef struct MCU_PACK{
    u16 flags;
    u16 noise_filter_us;
    u32 pulseless_ms;
}di_conf_t;
/**
  * @brief parameters of DO PWM channel
  * @ingroup do
  * @todo determine which group to include
  */
typedef union MCU_PACK{
    struct MCU_PACK{
        unsigned pwm_enable     :1;/*!< pwm is used*/
        unsigned pwm_run        :1;
    }bit;
    vu32 reg;
}do_pwm_state_t;
/**
  * @brief parameters of DO PWM channel
  * @ingroup do
  * @todo determine which group to include
  */
typedef struct MCU_PACK{
    do_pwm_state_t state;
    u8 duty;
}do_pwm_config_t;

/**
  * @brief parameters of DO PWM channel
  * @ingroup do
  * @todo determine which group to include
  */
typedef struct MCU_PACK{
    do_pwm_config_t config[4];
    u16 freq;
} do_pwm_t;

/**
 * @todo check implementation of enum
 */
enum{
    REGS_SET_FLAG=0,
    REGS_GET_FLAG
};

/**
  * @brief user task states
  * @ingroup regs
  * @todo determine which group to include
  */
typedef enum{
    INTERNAL_FLASH_TASK_RUNNING = (1<<0),   /*!<dinamic task running on intternal flash*/
    EXTERNAL_FLASH_TASK_RUNNING = (1<<1),   /*!<dinamic task running on external  flash*/
    INTERNAL_FLASH_TASK_OS_VERSION_BELOW_THEN_MINIMAL_TASK_VERSION = (1<<2),   /*!<dinamic task have internal warning*/
    INTERNAL_FLASH_TASK_READY = (1<<3),     /*!<internal flash dinamic task checked and ready to run*/
}dinamic_task_state_t;

/**
  * @brief user task commands
  * @ingroup regs
  * @todo determine which group to include
  */
typedef enum{
    INTERNAL_FLASH_TASK_START = (1<<0),     /*!<command to start dinamic task on intternal flash*/
    EXTERNAL_FLASH_TASK_START = (1<<1),     /*!<command to start dinamic task on external flash*/
    INTERNAL_FLASH_TASK_STOP  = (1<<2),     /*!<command to stop dinamic task on intternal flash*/
    EXTERNAL_FLASH_TASK_STOP  = (1<<3),     /*!<command to stop dinamic task on external flash*/
}dinamic_task_config_t;

/**
  * @brief sofi register types
  * @ingroup regs
  */
typedef union {
    float op_f;
    double op_d;
    int op_int;
    u8 op_u8;
    u16 op_u16;
    u32 op_u32;
    u64 op_u64;
    i8 op_i8;
    s16 op_i16;
    s32 op_s32;
    s64 op_s64;
} operand_t;

/**
  * @brief sofi register type flag
  * @ingroup regs
  */
typedef enum {
    U8_REGS_FLAG = (1<<0),/*!<unsigned char type(1 byte) */
    U16_REGS_FLAG = (1<<1),/*!<unsigned short type(2 byte) */
    U32_REGS_FLAG = (1<<2),/*!<unsigned int type(4 byte) */
    U64_REGS_FLAG = (1<<3),/*!<unsigned long long type(8 byte) */
    I8_REGS_FLAG = (1<<4),/*!<signed char type(1 byte) */
    S16_REGS_FLAG = (1<<5),/*!<signed short type(2 byte) */
    S32_REGS_FLAG = (1<<6),/*!<signed int type(4 byte) */
    S64_REGS_FLAG = (1<<7),/*!<signed long long type(8 byte) */
    FLOAT_REGS_FLAG = (1<<8),/*!<float type(4 byte) */
    DOUBLE_REGS_FLAG = (1<<9),/*!<double type(8 byte) */
    INT_REGS_FLAG = (1<<10),/*!<signed int type(4 byte) */
    TIME_REGS_FLAG = (1<<11)/*!<unsigned int type(4 byte) */
}regs_flag_t;

/**
  * @brief struct for regs access
  * @ingroup regs
  */
typedef struct {
    regs_flag_t flag;
    operand_t value;
} regs_access_t;
/**
  * @brief isol_pwr channels
  * @ingroup di_do
  */
typedef enum{
    NONE_PWR    = 0,
    DI_PWR_OK   = 1<<0,
    DO_PWR_OK   = 1<<1,
    ISOL_PWR_OK = 1<<2,
    AI_PWR_OK   = 1<<3,
    AO_PWR_OK   = 1<<4,
}isol_pwr_t;
/**
  * @brief device types
  * @ingroup
  */
typedef enum{
    DEVICE_PLC  = 10,
    DEVICE_AO_4 = 11,
    DEVICE_DO_8 = 12,
    DEVICE_AI_16 = 13,
    DEVICE_DI_16 = 14,
}device_t;
/**
  * @addtogroup
  * @todo determine which group to include
  * @{
  */
#define AI_NUMS 8
#define AI_MCU_NUMS 4
#define DI_NUMS 16
#define DO_NUMS 4
#define OS_VERSION_SIZE 4
#define OS_VERSION {0,40,0,0}
#define REGS_MAX_NAME_SIZE 64
#define BKRAM_BYTES_NUM 66
#define WRITE_REG_MASK 0xFFFF
#define WRITE_REG_CH_MASK 0xFFFF0000
#define CAN_MODULE_DEFAULT_VALUE (NMT_MAX_NODE_ID-1)
#define CAN_MODULE_VALUE_CHANGED BIT(15)
#define WIFI_NAME_LEN 12
#define WIFI_PASSWORD_LEN 8
#define WIFI_ROUTER_NAME_LEN 32
#define WIFI_ROUTER_PASSWORD_LEN 32

/**
  * @}
  */
/**
 * @brief main struct
 * name variables uses for generate name in description file and then in get value by name
 * and therefore use max size len name is 16 charackter \n
 * coment style :   "" - description, \n
 *                  &ro  - read only, \n
 *                  &def -> have const varibale with struct like def_name, \n
 *                  &save- will have saved in bkram, \n
 *                  &can - will be using in can transmition, \n
 *                  &pdor - autonatic receive description to can_open_dict.h, \n
 *                  &pdot - automatic transmit description to can_open_dict.h
 * @ingroup regs
 */
typedef union{
    struct MCU_PACK{
        u16 mdb_addr;                   //!<"modbus address",&save,&def, &can
        u8 mdb_revers;                  //!<"reverse 3 and 4 function",&save,&def
        u8 mdb_shift;                   //!<"shift start address regs from 0 to 1",&save,&def
        u8 ip[4];                       //!<"ip address",&save,&def
        u8 netmask[4];                  //!<"netmask address",&save,&def
        u8 gateaway[4];                 //!<"gateaway address",&save,&def
        u8 eth_speed;                   //!<"speed10-100mb",&save,          {0 - auto 10 - 10 100 - 100}
        u8 eth_duplex;                  //!<"duplex full or half",&save,     {0 - auto 1 - HALF 2 - FULL}
        u16 reset_num;                  //!<"number of system resets",&ro,&save,
        u16 last_reset;                 //!<"reason of last system reset",&ro,&save &can    See rst_reason_t
        u16 user_task_state;            //!<"user task current state",&save &ro
        u16 user_task_config;           //!<"user task config",&save
        u16 uart1_sets;                 //!<"settings MESO_UART",&save,&def &ro &can BitRateValue[0...3] = {0-default,1-2400,2-4800,3-9600,4-14400,
        u16 uart2_sets;                 //!<"settings RS_485_2",&save,&def     &can 5-19200,6-28800,7-38400,8-56000,9-57600,10-76800,11-115200,12-1200};
        u16 uart3_sets;                 //!<"settings RS_232",&save,&def       &can WordLen[4,5] = {0-7bit,1-8bit,2-9bit};
        u16 uart5_sets;                 //!<"settings RS_485_1",&save,&def     &can StopBit[6,7] = {1-1bit,2-2bit};
        u16 uart6_sets;                 //!<"settings RS_485_IMMO",&save,&def  &can Parity[8,9] = {0-none,1-odd,2-even};
        u16 uart7_sets;                 //!<"settings HART",&save,&def         &can RxDelay[10...15] {Delay(sec) = RxDelay * 20 / BitRateValue};
        u32 channels_timeout[DIRECT_CHANNELS_NUMM]; //!< "time outs for channel use for retranslations",&save,&def

        u8 do_state;                    //!<"state of digital output",&ro &can  DO_ON[0...3], DO_SC[4...7];
        u8 do_sc_ctrl;                  //!<"DO short circuit control",&save,&def &can &pdor     DO_SC_EN[0...3], DO_SC_FLAG[4...7];
        u16 do_ctrl;                    //!<"control digital output",&can &pdor   writeable with mask, DO_CTRL[0...3], DO_MASK[4...7];
        u16 do_pwm_freq;                //!<"PWM frequency Hz",&save,&def  &can          DO_PWM_frequency[0...15];
        u16 do_pwm_ctrl[DO_NUMS];       //!<"PWM control",&save,&def       &can          DO_PWM_Duty[0...7], DO_PWM_Run[8] = {0-stop,1-run};

        u16 di_noise_fltr_us[DI_NUMS];  //!<"digital inputs noise filter in us (x10)",&save,&def &can
        u32 di_pulseless_ms[DI_NUMS];   //!<"digital inputs pulseless time in ms",&save,&def &can
        u16 di_mode[DI_NUMS];           //!<"digital inputs mode",&save,&def   see di_mode_t &can
        u32 di_state;                   //!<"digital inputs state",&ro,&save &can &pdot
        u64 di_cnt[DI_NUMS];            //!<"digital inputs cnt values",&save &can
        float di_freq[DI_NUMS];         //!<"digital inputs frequency values",ro,&save &can

        u16 ai_unit[AI_NUMS];           //!<"14 bit capacity or 12 bit capacity for analog inputs",&ro &can &pdot
        u16 ai_state;                   //!< "bit range for adc channel, 1- if current > 4ma",&ro &can
        u16 ai_internal[AI_NUMS];       //!<"12 bit capacity internal analog inputs",&ro
        u16 ai_external[AI_NUMS];       //!<"14 bit capacity external analog inputs",&ro

        float internal_temp;            //!<"temperature internal sense value",&ro &can
        float external_temp;            //!<"temperature ADC sense value",&ro &can
        float v_pwr;                    //!< "PWR voltage",&ro &can
        float v_bat;                    //!< "3V battery voltage",&ro &can

        u64 sys_tick_counter;           //!< "tick in ms",&ro &can
        u64 tick100us;                  //!<"tick counter in 100us time",&ro
        sofi_time_r time_hms;               //!<"struct for real time"
        s32 unix_time_sec;              //!<"since the Epoch (00:00:00 UTC, January 1, 1970)" &can

        u8  os_version[OS_VERSION_SIZE];      //!<"version by 0.1.1",&ro,&def
        u8  mac_addr[6];                //!<"ethernet mac address",&ro,&def
        u32 flash_err_cnt;              //!<"flash_err cnt",&ro

        /* look implementation */
        task_created_t flags_task;      //!<"check for task created" &ro
        task_count_t  counter_task;     //!<"struct counter tasks" &ro
        async_init_t async_flags;        //!<"inited modules" &ro for init in task
        async_init_success_t flags_succ_init;   //!<"success inited modules" &ro for init in task

        u16 isol_pwr_state;             //!<"isolated power state",&ro &can

        u32 internal_task;              //!< "internal_flash_dinamic_task"
        u32 external_task;              //!< "external_flash_dinamic_task"

        u32 di_test_result;     //!< "di_test result",&ro       DI[0...15] = {1-OK, 0-error}, StateError[16], CntError[17], FreqError[18], 10*HandlingTime[19...30];
        u16 do_test_result;     //!< "do test result",&ro       DO[0...3] = {1-OK, 0-error}, SCError[4], StateError[5], DutyError[6], HandlingTime[7...15];
        u16 ai_test_result;     //!< "ai test result",&ro       AI[0...7] = {1-OK, 0-error}, MeasureError[8], NoiseError[9], InstableError[10], 0.5*ConvTime[11...15];
        u32 sofi_test_result;   //!< "sofi_test blocks results",&ro Results[0...31] = {see sofi_test_block_t, 1-OK, 0-failed};
        u32 sofi_test_blocks;   //!< "sofi test blocks"     Blocks[0...28] = {see sofi_test_block_t},CheckTest[29],StressTest[30],PerfTest[31];
        u16 run_test;           //!< "running tests",&ro
        u32 cur_free_heap;      //!< in bytes,&ro
        u32 min_free_heap;      //!< in bytes,&ro
        u8 debug_info[8];       //!< "reserved use for debug"

        u32 err_reg_0;          //!< "error flags register",     See @ref err_flag_reg0_t
        u32 err_reg_1;          //!< "error flags register",     See @ref err_flag_reg1_t
        u32 di_freq_overload;   //!< "DI frequency overload detecting"  DI_FREQ_OVERLOAD[0...15] = {1-overload, 0-OK};
        u32 reserve_0;          //!< "reserved"
        u32 reserve_1;          //!< "reserved"
        u32 reserve_2;          //!< "reserved"

        //add new regs after ->

        u16 command;            //!< "Command register"    &can     See @ref sofi_command_t
        u16 num_of_vars;        //!< "number of vars self + config(user)&ro &def &can
        u16 current_os;         //!< "using os 1 or 2"  &ro &can
        u16 module_number;      //!<"module number 0 - 127", &save, &def, &can
        u32 can_sdo_error;           //!< accumulate errors packet
        u8 local_ip[4];                       //!<"ip address for local net",&save , &def , &can
        u8 local_netmask[4];                  //!<"netmask address for local net", &save , &def , &can
        u8 local_gateaway[4];                 //!<"gateaway address for local net", &save, &def , &can
        u8 usb_local_ip[4];                   //!<"ip address for local usb net",&save , &def
        s32 bkram_flash_unix;                 //!<"last time bkram saved to flash", &save
        float ai_calib_a[AI_NUMS];      //!<"multiple coef for AI",&save,&def &can
        float ai_calib_b[AI_NUMS];      //!<"additive coef for AI",&save,&def &can
        float ai_physical[AI_NUMS];     //!<"ai_unit * ai_calib_a + ai_calib_b",&ro &can &pdot
        u8 uniq_id[12];                 //!<"uniq_id number" , &ro
        u8 device_type;                 //!<"type of device", &save, &ro, &def
        u8 board_ver;                   //!<"board version", &save, &ro, &def
        u32 pass_key;                   //!<"key for registers change", &def, &save, &ro
        u32 di_irq_time;                //!<"time in di interrupt in us", &ro
        u8 can_modules_status[16];      //!<"modules state by bits", &ro
        u8 wifi_name[WIFI_NAME_LEN];               //!<"must be strong full filled", &save &def
        u8 wifi_password[WIFI_PASSWORD_LEN];            //!<"must be strong 8 byte", &save &def
        u8 wifi_router_name[WIFI_ROUTER_NAME_LEN];               //!<"must be ended by zero", &save &def &crtcl
        u8 wifi_router_password[WIFI_ROUTER_PASSWORD_LEN];            //!<"must be more or equal 8 byte", &save &def &crtcl
        u16 wifi_setting;               //!<"type of wifi and settings" &save &def
        u16 wifi_state;                 //!<"wifi state" &ro
        //add new regs before <-

        u32 monitor_period;             //!< "sofi_monitor period in ms",&ro
        float total_tasks_time;         //!< "sum of running times of tasks in %",&ro
        task_info_t task[MONITOR_MAX_TASKS];   //!<tasks information,&ro
#if LWIP_STATS_DISPLAY
        u16 link;               //!< &ro
        u16 eth_arp;            //!< &ro
        u16 ip_frag;            //!< &ro
        u16 ip_proto;           //!< &ro
        u16 icmp;               //!< &ro
        u16 udp;                //!< &ro
        u16 tcp;                //!< &ro
        u16 memp_udp_pool;      //!< &ro
        u16 memp_tcp_pool;      //!< &ro
        u16 memp_listen_tcp;    //!< &ro
        u16 memp_seg_tcp;       //!< &ro
        u16 memp_altcp;         //!< &ro
        u16 memp_reassdata;     //!< &ro
        u16 memp_frag_pbuf;     //!< &ro
        u16 memp_net_buf;       //!< &ro
        u16 memp_net_conn;      //!< &ro
        u16 memp_tcpip_api;     //!< &ro
        u16 memp_tcpip_input;   //!< &ro
        u16 memp_sys_timeout;   //!< &ro
        u16 memp_pbuf_ref;      //!< &ro
        u16 memp_pbuf_pool;     //!< &ro
        u16 lwip_sys;           //!< &ro
#endif
    }vars;
    u8 bytes[GLOBAL_UNION_SIZE]; //for full bksram copy
}sofi_vars_t;

extern sofi_vars_t sofi;
int regs_set(u32 reg_address,regs_access_t reg) MCU_ROOT_CODE;
int regs_set_buffer(u32 reg_address,u8* buffer_from,u16 byte_numm) MCU_ROOT_CODE;
int regs_get(u32 reg_address,regs_access_t* reg) MCU_ROOT_CODE;
int regs_get_buffer(u32 reg_address,u8* buffer_to,u16 byte_numm) MCU_ROOT_CODE;
u8 regs_size_in_byte(regs_flag_t type);

/**
  * @brief registers for regs_event_handler
  * @ingroup regs
  * @todo check implementation
  */
typedef enum {
    none_reg = 0,
    change_ip_reg,
    eth_speed_reg,
    eth_duplex_reg,
    rtc_reg,
    rtc_unix_reg,
    sofi_test_blocks_reg,
    command_reg,
    user_task_config_reg,
    err_reg,
} write_regs_t;

/**
  * @brief register elements
  * @ingroup regs
  * @todo check implementation
  */
typedef enum {
    ch_none_reg =   0,
    ch_0_reg =      (1<<16),
    ch_1_reg =      (2<<16),
    ch_2_reg =      (3<<16),
    ch_3_reg =      (4<<16),
    ch_4_reg =      (5<<16),
    ch_5_reg =      (6<<16),
    ch_6_reg =      (7<<16),
    ch_7_reg =      (8<<16),
    ch_8_reg =      (9<<16),
    ch_9_reg =      (10<<16),
    ch_10_reg =     (11<<16),
    ch_11_reg =     (12<<16),
    ch_12_reg =     (13<<16),
    ch_13_reg =     (14<<16),
    ch_14_reg =     (15<<16),
    ch_15_reg =     (16<<16),
} write_regs_ch_t;

/**
  * @brief sofi command list
  * @ingroup regs
  */
typedef enum {
    NONE_COMM = 0,
    SW_RESET_COMM = 0x5500,   /*!< 0x5500 software reset command*/
    SAVE_ALL_RETAINS_COMM,    /*!< 0x5501 */
    SET_DEFAULT_VALUES_COMM,  /*!< 0x5502 set default value command(plc will reboot)*/
    CHANGE_OS_COMM,           /*!< 0x5503 Automatically return back*/
    RUN_OS1_COMM,             /*!< 0x5504 to main OS if not confirmed*/
    RUN_OS2_COMM,             /*!< 0x5505 during OS_CONFIRM_TIME*/
    CONFIRM_OS_COMM,          /*!< 0x5506 set current OS as main, command use after CHANGE_OS_COMM,RUN_OS1_COMM,RUN_OS2_COMM*/
    SET_MAIN_AND_RUN_OS1_COMM,/*!< 0x5507 run OS1 without confirmation*/
    SET_MAIN_AND_RUN_OS2_COMM,/*!< 0x5508 run OS2 without confirmation*/
    REMOTE_UPDATE_ENABLE,       /*!< 0x5509 ignore BOOTKEY for download OS*/
    REMOTE_UPDATE_DISABLE,      /*!< 0x550a check BOOTKEY for downloas OS*/
    ARC_REMOVE_ALL_COMM,           /*!< 0x550b remove all arc in flash*/
    LITTLE_FS_FORMAT_COMM,         /*!< 0x550c unmount FS in flash*/
    SAVE_BKRAM_TO_FLASH_COMM ,      /*!< 0x550d save bkram to flash command*/
    SWITCH_TO_BOOT      ,       /*!< 0x550e switch to boot mode for modules only*/
}sofi_command_t;

/**
  * @brief reason of reset
  * @ingroup regs
  * @todo determine which group to include
  */
typedef enum {
    BOR_RST_REASON =    (1 << 0),
    PIN_RST_REASON =    (1 << 1),
    POR_RST_REASON =    (1 << 2),
    SOFT_RST_REASON =   (1 << 3),
    IWDG_RST_REASON =   (1 << 4),
    WWDG_RST_REASON =   (1 << 5),
    LPWR_RST_REASON =   (1 << 6),

    BOOT_KEY_DOWNLOAD_DISABLE =     (1 << 13),
    BOOT_KEY_REMOTE_UPDATE_ENABLE = (1 << 14),
}rst_reason_t;
/**
  * @addtogroup
  * @{
  * @todo determine which group to include
  */
#define DINAMIC_ADDRESS_SPACE_NUMM 30
#define ALL_MDB_CMD 0xff
/**
  * @}
  */

/**
  * @brief struct of dynamic address space
  * @ingroup regs
  * @todo determine which group to include
  */
typedef struct {
    u32 address;  /*!< modbus address 0 - 65535*/
    u8* data;     /*!< pointer to readdressing*/
    u32 len;      /*!< len of space */
    u8 command;   /*!< special modbus command or use 0xff for all commands*/
}dinamic_address_space_t;

u8 write_comm_to_bkp (sofi_command_t command);
/**
 * @brief regs_of_bkram_and_have_def_value
 * @param reg_address
 * @return  1 if vars in saved space and have def value in description
 */
int regs_of_bkram_and_have_def_value(u32 reg_address);

/**
  * @brief error flags for err_reg_0
  * @ingroup regs
  */
typedef enum {
    ERR_FLAG_ISOL_PWR =             (1 << 0),
    ERR_FLAG_DO_SC =                (1 << 1),
    ERR_FLAG_DI_FREQ_OVERLOAD =     (1 << 2),
    ERR_FLAG_ADC_CONNECTION =       (1 << 3),
    ERR_FLAG_EXTERNAL_FLASH =       (1 << 4),
    ERR_FLAG_TASK_INIT =            (1 << 5),
    ERR_FLAG_CAN_SDO =              (1 << 6),
    ERR_FLAG_LAN_CONNECTION =       (1 << 7),
    ERR_FLAG_ARCHIEVE =             (1 << 8),
    ERR_FLAG_LWIP_MEMORY =          (1 << 9),
    ERR_FLAG_BATTERY_LOW =          (1 << 10),
}err_flag_reg_0_t;

typedef enum {
    WIFI_ACCESS_POINT = 1,
    WIFI_CLIENT       = 2,
    WIFI_AP_STA       = 3,
    WIFI_ESP32_CHANGED_ONLY_ACCESS_POINT = 4,
    WIFI_ESP32_CHANGED_ONLY_CLIENT      = 5 ,
    WIFI_ESP32_CHANGED_ONLY_AP_STA       = 6,
}wifi_setting_t;/*todo!!!rename to meso setting */

typedef enum {
    WIFI_SLIP_CONNECTED = (1<<0),
}wifi_state_t;/*todo!!!rename to meso setting */

/**
  * @brief error flags for err_reg_1
  * @ingroup regs
  */
typedef enum {
    NONE = 0,
}err_flag_reg_1_t;

void set_err_flag_0 (err_flag_reg_0_t err_flag);
void set_err_flag_1 (err_flag_reg_1_t err_flag);
/**
 * @brief is_ascii_symbol_or_digital
 * @param buff
 * @param len must be more then 0
 * @return 1 if consist if symbol or digital
 */
int is_ascii_symbol_or_digital(u8 * buff, u32 len);
#ifdef __cplusplus
}
#endif
#endif //REGS_H

