/* OPTIONAL: Pools to replace heap allocation
 * Optional: Pools can be used instead of the heap for mem_malloc. If
 * so, these should be defined here, in increasing order according to
 * the pool element size.
 *
 * LWIP_MALLOC_MEMPOOL(number_elements, element_size)
 */
LWIP_MALLOC_MEMPOOL_START
LWIP_MALLOC_MEMPOOL(64, 64)
LWIP_MALLOC_MEMPOOL(32, 128)
LWIP_MALLOC_MEMPOOL(16, 256)
LWIP_MALLOC_MEMPOOL(8, 512)
LWIP_MALLOC_MEMPOOL(5, 1024)
LWIP_MALLOC_MEMPOOL(2, 5440)
LWIP_MALLOC_MEMPOOL_END
