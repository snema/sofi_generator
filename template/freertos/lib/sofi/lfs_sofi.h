/**
 * @file lfs_sofi.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup free_rtos/littlefs/inc
 * @ingroup free_rtos/littlefs/inc
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef LFS_SOFI_H
#define LFS_SOFI_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "lfs.h"
#include "lfs_util.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
#define DEBUG_IN_RAM 0
// configuration of the filesystem is provided by this struct

#if DEBUG_IN_RAM
#define LFS_SOFI_READ_BUFFER_SIZE 16
#define LFS_SOFI_PROG_BUFFER_SIZE (LFS_SOFI_READ_BUFFER_SIZE * 1)
#define LFS_SOFI_ERASE_BLOCK_SIZE 256
#define LFS_SOFI_BLOCK_COUNT 10
#define LFS_SOFI_LOOKAHEAD_COUNT 10
#ifndef LFS_BLOCK_CYCLES
#define LFS_BLOCK_CYCLES 1024
#endif
#ifndef LFS_CACHE_SIZE
#define LFS_CACHE_SIZE (64 % LFS_PROG_SIZE == 0 ? 64 : LFS_PROG_SIZE)
#endif

#define LFS_SOFI_LOOKAHEAD_SIZE (LFS_SOFI_LOOKAHEAD_COUNT/8+1)
static u8 test_buf[LFS_SOFI_ERASE_BLOCK_SIZE * LFS_SOFI_BLOCK_COUNT];
#else
#define LFS_SOFI_READ_BUFFER_SIZE 256
#define LFS_SOFI_PROG_BUFFER_SIZE 256
#define LFS_SOFI_ERASE_BLOCK_SIZE QSPI_SECTOR_SIZE
#define LFS_SOFI_BLOCK_COUNT 4090
#ifndef LFS_BLOCK_CYCLES
#define LFS_BLOCK_CYCLES 500
#endif
#ifndef LFS_CACHE_SIZE
#define LFS_CACHE_SIZE 256
#endif
#define LFS_SOFI_LOOKAHEAD_SIZE 64
#endif

extern lfs_t lfs_sofi;
extern struct lfs_config cfg;
// The emu bd state
typedef struct lfs_context {
    struct {
        uint64_t read_count;
        uint64_t prog_count;
        uint64_t erase_count;
    } stats;
    struct {
        lfs_block_t blocks[4];
    } history;
    struct {
        uint32_t read_size;
        uint32_t prog_size;
        uint32_t block_size;
        uint32_t block_count;
    } cfg;
    u8 cache_buff[LFS_CACHE_SIZE];
} lfs_context_t;
extern lfs_context_t lfs_context_device;

/**
* @brief lfs_sofi_read - Read a region in a block. Negative error codes are propogated
* to the user.
* @param c
* @param block
* @param off
* @param buffer
* @param size
* @return
*/
int lfs_sofi_read (const struct lfs_config *c, lfs_block_t block,
       lfs_off_t off, void *buffer, lfs_size_t size);

/**
 * @brief lfs_sofi_prog - Program a region in a block. The block must have previously
 * been erased. Negative error codes are propogated to the user.
 * @param c
 * @param block
 * @param off
 * @param buffer
 * @param size
 * @return May return LFS_ERR_CORRUPT if the block should be considered bad.
 */
int lfs_sofi_prog(const struct lfs_config *c, lfs_block_t block,
       lfs_off_t off, const void *buffer, lfs_size_t size);


/**
 * @brief lfs_sofi_erase - Erase a block. A block must be erased before being programmed.
 * The state of an erased block is undefined. Negative error codes
 * are propogated to the user.
 * @param c
 * @param block
 * @return May return LFS_ERR_CORRUPT if the block should be considered bad.
 */
int lfs_sofi_erase(const struct lfs_config *c, lfs_block_t block);

/**
 * @brief lfs_sofi_sync -     Sync the state of the underlying block device. Negative error codes
 *   are propogated to the user.
 * @param c
 * @return
 */
int lfs_sofi_sync(const struct lfs_config *c);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //LFS_SOFI_H
