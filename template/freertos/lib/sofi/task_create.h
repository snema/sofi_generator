#ifndef TASK_CREATE_H
#define TASK_CREATE_H 1

#include "cmsis_os.h"
#include "type_def.h"
#ifdef __cplusplus 
   extern "C" {
#endif
typedef void (*task_pointer)(const void *arg) ;
#define MAX_DINAMIC_TASK_NUMBER 2
enum dinamic_task_event{
    STOP_CHILD_PROCCES          = (1<<0),   /*!< sended before kill external thread*/
    STOP_PROCESS_FOR_CHANGE_OS  = (1<<1),   /*!< sended before change os*/
    CAN_RX_EVENT                = (1<<2),   /*!< when can rx event */
    CAN_TX_EVENT                = (1<<3),   /*!< when can tx event */
    CAN_TIMER_EVENT             = (1<<4),   /*!< when can timer event */
    CAN_DICT_DEFAULT            = (1<<5),   /*!< return can dict to default*/
    PACKET_MAIL_SENDED          = (1<<6),   /*!< send mail through PM*/
    PACKET_RECEIVED             = (1<<7),   /*!< receved packet for PM*/
    SDO_TRANSMITHING            = (1<<8),   /*!< start transmith sdo parametr*/
    ETHERNET_RX_EVENT           = (1<<9),   /*!< we have received rx over ethernet*/
    SLIP_RX_EVENT               = (1<<10),   /*!< we have received rx over SLIP*/
    HARDWARE_DEINIT_EVENT       = (1<<11),   /*!< we have problem with hardware*/
    HAVE_DATA_TO_SEND           = (1<<12),   /*!< we have data to send*/
    SAVE_BKRAM_TO_FLASH         = (1<<13),   /*!< save bkram to flash command*/
    ARC_REMOVE_ALL              = (1<<14),   /*!< remove all arc in flash*/
    LOG_WRITE                   = (1<<15),   /*!< save log temp file to flash*/
    PACKET_TRANSMITION_CLOSE    = (1<<16),   /*!< sent last byte */
    WIFI_CONNECTED              = (1<<17),   /*!< wifi connected*/
    //.............................................................................
    UNUSED_EVENT_1              = (1<<28),   /*!< we should not use this bit!!*/
    RAM_ADDRESS_IN_EVENT        = (1<<29),   /*!< instead event use ram address*/
    UNUSED_EVENT_2              = (1<<30),   /*!< we should not use this bit!!*/
    FLASH_ADDRESS_IN_EVENT      = (1<<31),   /*!< instead event use flash address*/
};

enum task_type{
   INTERNAL_FLASH_TASK = 0,
   EXTERNAL_FLASH_TASK,
   ALL_USER_TASK,
};
typedef enum {
    SOFI_CONTROLLER_TYPE = 0
}controller_type_t;
typedef struct{
    char version[10];   //!< maybe it like "1.0.0">
    u32 unix_time;  //!<in sec since 1970>
    controller_type_t type;//!< corresponding to controller>
}dinmaic_task_preference;
extern TaskHandle_t dinamic_tasks_handle[];
extern TaskHandle_t di_handle;
extern TaskHandle_t do_handle;
extern TaskHandle_t packet_task_handle;
extern TaskHandle_t analog_handle;
extern TaskHandle_t sofi_test_handle;
extern TaskHandle_t duty_task_handle;
extern osMutexId user_regs_access_mutex;
//functions 
int dinamical_task_check(void);
void task_create_main(void)__attribute__ ((noreturn));
int dinamic_task_start(u32 mem_mapped_address);
/**
 * @brief rewresh watch dog timer
 * wath dog timer always enabled in reliase version
 * */
void refresh_watchdog(void) MCU_ROOT_CODE;
osMutexId get_user_regs_access_mutex(void);

#ifdef __cplusplus
}
#endif
#endif //TASK_CREATE_H
