#include "cmsis_os.h"
#include "teeny_usb.h"
#include "tusbd_cdc_rndis.h"
#include "lwip/err.h"
extern osMutexId usb_ip_access_mutex;
extern tusb_rndis_device_t cdc_dev;
extern struct netif usb_ip_if;

/*vars */
extern int cdc_len;
extern int send_done;

extern int link_state_changed;
extern int link_state;
extern tusb_device_config_t device_config;
extern osThreadId usb_ip_thread_id;



/*functions*/
int cdc_recv_data(tusb_rndis_device_t* cdc, const void* data, uint16_t len);
int cdc_send_done(tusb_rndis_device_t* cdc);
int cdc_eth_linkchange(tusb_rndis_device_t* cdc, int linkup);
int usb_ip_packet_handle(u8 * packet,u16 len);
err_t usb_ip_if_init(struct netif *netif);
FNCT_NO_RETURN void usb_ip_task( void const * argument );
