/**
 * @file    di.h
 * @author  Shoma Gane <shomagan@gmail.com>
 * @author  Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @ingroup DI
 * @version 0.1
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef DI_H
#define DI_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */

/**
 * @addtogroup DI
 * @{
 */
#define DI_NOISE_FILTER 3 // in ms
#define DI_PERIOD 2 // in ms
#define DI_FREQ_CALC_PERIOD 10 // in ms
#define SAMPLE_NUMS 5
#define MAX_SAMPLE_TIME 1000000 // in us this value = 1 sec (freq update max period)
#define MAX_PULSELESS_TIME 100000 // if during this time (ms) there are no pulses on di, the di_freq = 0
#define SAMPLE_STATISTIC 1000 // max number of pulses in the sample
#define CALIB_A 1.0f
#define DI_FREQ_LIMIT 11000.0f // max ferquency on di channel
#define DI_FREQ_OVERLOAD 99999.0f // frequency overload value
/**
 * @}
 */

/**
 * @brief struct for frequency calculating
 * @ingroup DI
 */
typedef struct MCU_PACK {
   u32 pulse_number;
   u32 time;
} sample_t;

/**
 * @brief struct for frequency calculating
 * @ingroup DI
 */
typedef struct MCU_PACK {
   u32 front_time;
   u32 rise_time;
   u32 last_imp_time;
   u32 cur_imp_time;
   u32 last_sample_time;
   u32 cur_sample_time;
   u64 sample_period;
   u64 last_value;
   u64 cur_value;
   u8 sample_cnt;
   u16 pulseless_time;
   float freq;
   u8 last_state;
   u8 need_calc_freq;
   sample_t sample[SAMPLE_NUMS];
} di_t;

extern const pin_map_t di_pin_map[];
extern di_t di_freq[];
extern u32 di_irq_time;

int di_init(void);
void di_deinit(void);
int di_to_in_init(void);
int di_to_out_init(void);
void di_gpio_deinit(void);
void di_task (const void *pvParameters);
void di_freq_calc(void);
int di_cnt_config (void);
int di_handling (void);
void di_pwr_read(void);
void di_irq_handler (u8 di_num);
void di_handler (u8 di_num);
void di_irq_time_calc (void);
void di_block_channel (u8 di_num);
void di_unblock_channel (u8 di_num);
/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //DI_H
