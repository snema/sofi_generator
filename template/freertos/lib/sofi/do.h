﻿/**
 * @file    di_do.h
 * @author  Shoma Gane <shomagan@gmail.com>
 * @author  Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @ingroup di_do
 * @version 0.1
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */

#ifndef DI_DO_H
#define DI_DO_H
#if defined __cplusplus
extern "C" {
#endif

#include "regs.h"

/**
  * @addtogroup DO
  * @{
  */
#define DO_PERIOD 1 // in ms
#define DO_OD_SHIFT 0
#define DO_TEST_SHIFT 4
#define DO_ON_SHIFT 0   //do enabled current in allowed limit
#define DO_SC_SHIFT 4   //do enabled current in not allowed limit
#define DO_SC_EN_SHIFT 0
#define DO_SC_FLAG_SHIFT 4
#define DO_SHORT_TIME 1 // max short circiut time in (ms)
#define DO_READ_TIME 1
#define DO_INDICATE_TIME 2
#define DO_ERR_MAX_DELAY 100 // do_err_led blink period in ms
#define DO_ERR_BLINK_DELAY DO_ERR_MAX_DELAY/2
/**
  * @}
  */

/**
  * @addtogroup PWM
  * @{
  */
#define PWM_MIN_FREQ 20
#define PWM_MAX_FREQ 10000
#define PWM_MIN_DUTY 10
#define PWM_MAX_DUTY 90
#define PWM_DUTY_MASK 0xFF
/**
  * @}
  */

/**
  * @brief do_pwm_ctrl bits
  * @ingroup DO
  */
typedef enum{
    PWM_RUN     = 1<<8,
}do_pwm_ctrl_t;
/**
  * @brief struct for easy access to PWM timers
  * @ingroup PWM
  */
typedef struct MCU_PACK{
    TIM_TypeDef *tim;
    uint32_t tim_channel;
    uint32_t alt_func;
}pwm_tim_t;
/**
  * @brief strict for do_err_led blinking control
  * @ingroup DO
  */
typedef struct MCU_PACK{
    u16 delay;
    u16 passed;
}do_err_led_t;

extern const pin_map_t do_od_pin_map[];
extern const pin_map_t do_on_pin_map[];
extern const pin_map_t do_sc_pin_map[];

void do_task (const void *pvParameters);
int do_init (void);
void do_deinit(void);
int do_od_gpio_init(u8 channel);
int do_handling (void);
int do_write (u8 set_value);
int do_to_in_init(void);
int do_to_out_init(void);
int do_pwm_freq_set (u16 freq);
int do_pwm_duty_set (u8 channel, u8 duty);
int do_pwm_run (u8 channel);
int do_pwm_stop (u8 channel);
int do_pwm_gpio_init (u8 channel);
int do_pwm_init (void);
int do_pwm_tims_init (void);
void do_pwm_tims_deinit (void);
void do_pwr_read(void);
void do_sc_protection_on(void);
void do_sc_protection_off(void);
int do_pwm_ctrl_reg_handle (u8 channel);

#if defined __cplusplus
}
#endif
#endif //DI_DO_H
