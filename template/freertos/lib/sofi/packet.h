#ifndef PACKET_MANAGER_H
#define PACKET_MANAGER_H 1
/*add includes below */
#include "type_def.h"
#include "cmsis_os.h"
#include "modbus.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions declarations below */

typedef enum {
    CHANNEL_STATE_ERASE =0, /*!<erase all options*/
    CHANNEL_STATE_ENABLE = (1<<0),/*!<channel had enabled*/
    CHANNEL_STATE_RX_ON = (1<<1),/*!<sending is had canceled and channels get on for receive*/
    CHANNEL_STATE_SENDING = (1<<2),/*!<native sending process for channels*/
    CHANNEL_STATE_SENDED = (1<<3),/*!<data sended while sended handle */
    CHANNEL_STATE_ERROR = (1<<4),
    CHANNEL_STATE_IN_HANDING= (1<<5),/*!<sets after receive packet before PM will handling*/
    CHANNEL_STATE_BUSY = (1<<6), /*!<sending data, channel is waiting for receive packet*/
    CHANNEL_STATE_IS_LAST_BYTE = (1<<7) ,
    CHANNEL_STATE_FREE = (~CHANNEL_STATE_IN_HANDING) & (~CHANNEL_STATE_BUSY),/*!<clear CHANNEL_STATE_IN_HANDING after PM handle*/
}chanel_state_enum_t;
   typedef u16 chanel_state_t;
#define PACKET_PULL_TIMEOUT_MS 2000
enum packet_type{
    PACKET_MODBUS_RTU = MODBUS_RTU_PACKET,
    PACKET_MODBUS_TCP = MODBUS_TCP_PACKET
};


enum packet_direct_channels{
    PACKET_CHANNEL_1,
    PACKET_CHANNEL_2,
    PACKET_CHANNEL_3,
    PACKET_CHANNEL_4,
    PACKET_CHANNEL_5,
    PACKET_CHANNEL_6,
    PACKET_CHANNEL_7,
    PACKET_CHANNEL_CAN,
    PACKET_CHANNEL_DIRECT_RADIO_NEAR,
    PACKET_CHANNEL_DIRECT_RADIO_FAR,
    PACKET_CHANNEL_DIRECT_RADIO_LINE,
    PACKET_CHANNEL_DIRECT_LAST,/*<! dummy*/
};
#define SEND_QUEUE_SIZE 6   //only two byte alligment
#define CAN_IN_PM   1
#define DIRECT_CHANNELS_NUMM UART_PORTS
#define PM_CHANNELS_NUMM DIRECT_CHANNELS_NUMM
#define TIMEOUT_MIN_MS 50
enum packet_socket_channels{
    PACKET_CHANNEL_UDP = PACKET_CHANNEL_DIRECT_LAST + 1,
    PACKET_CHANNEL_TCP,
};
enum send_options_state{
    SEND_OPTION_STATE_CLEAR =0,
    SEND_OPTION_STATE_SEND_PUSHED =1,
    SEND_OPTION_STATE_SEND_PULLED =2,
    SEND_OPTION_STATE_RECV_PUSHED =3,
    SEND_OPTION_STATE_RECV_TIMEOUT =4,/*!< warning use unclear struct in packet "send_options->state > SEND_OPTION_STATE_RECV_TIMEOUT"*/
};
/*send queue item size must include header with setting
 * */
typedef struct MCU_PACK{
    u8 * data_buff;
    int guid;
    u32 state;
    u32 time_out;
    u16 data_buff_len;
    u16 channel;
}send_options_t;

extern osPoolId dinamic_address;
extern TaskHandle_t repeater_handle;
/*@brief use PM
 * 1 - make new packet with option state "send_options = packet_add_new_send_packet(RS_232_UART,test_buff,sizeof(test_buff),5000);"
 *      use timeout if make request and expected answer, without timeout channel will not busy after send packet
 * 1' - use guid = packet_get_guid(send_options); get packet identification
 * 2 - if used timeout check received packet - "while(packet_have_for_handle(send_options,guid)==0){osDelay(10);}"
 * 3 - get packet from queue u8 recv_buff[255]; guid_temp= packet_get(send_options,recv_buff,len_recv_buff);
 *      packet get freeing pool from PM mail queue
   EX.
    void packet_simply_task(const void *parameters ){
        send_options_t * send_options = NULL;
        int guid;
        u8 test_buff[] ="hello world";
        u8 len_recv_buff=255;
        guid=0;
        while(1){
            if(send_options==NULL){
                send_options = packet_add_new_send_packet(RS_232_UART,test_buff,sizeof(test_buff),5000);
                guid = packet_get_guid(send_options);
            }else{
                int guid_temp;
                while(packet_have_for_handle(send_options,guid)==0){
                    osDelay(10);
                }

                u8 recv_buff[255];
                guid_temp= packet_get(send_options,recv_buff,len_recv_buff);
                if(guid_temp!=guid){
                    led_error_on(TEST_ERROR_MS);
                }
                send_options=NULL;
            }

            osDelay(10);
        }
    }
 * */

void packet_stream_init(void);
void packet_task( const void *pvParameters );
send_options_t * packet_add_new_send_packet(u16 channel,u8 * buff,u16 len,u32 busy_time);
/*@return true if we have packet received or timeouted in channel use in send_options*/
int packet_have_for_handle(send_options_t * send_options,int guid);
/*@brief get packet if then exist, copy to buff and clear packet mail
 *@param  len used and checked in function if *len < packet recved len return zero value
 * @return state
 * */
int packet_get(send_options_t * send_options,u8 * buff,u16 * len);
/* @brief use only for first uarts channels
 *
 * */
int sending_timer_start(u16 channel);

int packet_get_guid(send_options_t * send_options);
/* Execute a Query/Response !!!suspended!!! transaction between client and server */
/* returns: <0    -> ERROR: error codes
 *          >2    -> SUCCESS: frame length
 *
 */
int packet_transaction(u16 channel,u8  *send_packet,u16 send_length,
                          u8  *data,u16 recv_length,u32 timeout) ;
/**
 * @brief is_direct_channel use for control channel
 * @param channel
 * @return
 */
int is_direct_channel(u8 channel);

/*add functions declarations before */
#ifdef __cplusplus
}
#endif
#endif //PACKET_MANAGER_H
