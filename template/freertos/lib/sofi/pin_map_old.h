#ifndef PIN_MAP_OLD_H
#define PIN_MAP_OLD_H 1

#include "stm32f7xx_hal_gpio.h"
typedef struct MCU_PACK{
    GPIO_TypeDef * port;
    uint16_t pin;
}pin_map_t;
// --- defgroup for PORTA ---


#define PWR_CNTRL_PIN GPIO_PIN_0
#define PWR_CNTRL_GPIO_PORT GPIOA
#define RMII_REF_CLK_PIN GPIO_PIN_1
#define RMII_REF_CLK_GPIO_PORT GPIOA
#define RMII_MDIO_PIN GPIO_PIN_2
#define RMII_MDIO_GPIO_PORT GPIOA
#define AI_MCU_0_PIN GPIO_PIN_3
#define AI_MCU_0_GPIO_PORT GPIOA
#define DAC1_PIN GPIO_PIN_4
#define DAC1_GPIO_PORT GPIOA
#define DAC2_PIN GPIO_PIN_5
#define DAC2_GPIO_PORT GPIOA
#define AI_MCU_1_PIN GPIO_PIN_6
#define AI_MCU_1_GPIO_PORT GPIOA
#define RMII_CRS_DV_PIN GPIO_PIN_7
#define RMII_CRS_DV_GPIO_PORT GPIOA
#define USB_SOF_PIN GPIO_PIN_8
#define USB_SOF_GPIO_PORT GPIOA
#define USB_VBUS_PIN GPIO_PIN_9
#define USB_VBUS_GPIO_PORT GPIOA
#define USB_ID_PIN GPIO_PIN_10
#define USB_ID_GPIO_PORT GPIOA
#define USB_DM_PIN GPIO_PIN_11
#define USB_DM_GPIO_PORT GPIOA
#define USB_DP_PIN GPIO_PIN_12
#define USB_DP_GPIO_PORT GPIOA
#define DEBUG_TMS_PIN GPIO_PIN_13
#define DEBUG_TMS_GPIO_PORT GPIOA
#define DEBUG_TCK_PIN GPIO_PIN_14
#define DEBUG_TCK_GPIO_PORT GPIOA
#define LED1_R_PIN GPIO_PIN_15
#define LED1_R_GPIO_PORT GPIOA

// --- defgroup for PORTB ---
#define UART5_RX_PIN GPIO_PIN_8
#define UART5_RX_GPIO_PORT GPIOB
	#define MESO_RX_PIN UART5_RX_PIN
	#define MESO_RX_GPIO_PORT UART5_RX_GPIO_PORT
#define UART5_TX_PIN GPIO_PIN_9
#define UART5_TX_GPIO_PORT GPIOB
	#define MESO_TX_PIN UART5_TX_PIN
	#define MESO_TX_GPIO_PORT UART5_TX_GPIO_PORT
#define I2C1_SCL_PIN GPIO_PIN_8
#define I2C1_SCL_GPIO_PORT GPIOB
	#define MESO_SCL_PIN I2C1_SCL_PIN
	#define MESO_SCL_GPIO_PORT I2C1_SCL_GPIO_PORT

#if STM_NUCLEO_BOARD
#define LD1_PIN GPIO_PIN_0
#define LD1_GPIO_PORT GPIOB
#define LD2_PIN GPIO_PIN_7
#define LD2_GPIO_PORT GPIOB
#define LD3_PIN GPIO_PIN_14
#define LD3_GPIO_PORT GPIOB
#define USER_BTN_PIN GPIO_PIN_13
#define USER_BTN_GPIO_PORT GPIOC
#define UART3_RX_PIN GPIO_PIN_8
#define UART3_RX_GPIO_PORT GPIOD
#define UART3_TX_PIN GPIO_PIN_9
#define UART3_TX_GPIO_PORT GPIOD
#define RMII_TXD0_PIN GPIO_PIN_13
#define RMII_TXD0_GPIO_PORT GPIOG
#elif BOARD_VERSION>=0   //STM_NUCLEO_BOARD
#define AI_MCU_2_PIN GPIO_PIN_0
#define AI_MCU_2_GPIO_PORT GPIOB
#define UART1_RX_PIN GPIO_PIN_7
#define UART1_RX_GPIO_PORT GPIOB
#define RS_232_RXD_PIN UART1_RX_PIN
#define RS_232_RXD_PORT UART1_RX_GPIO_PORT
#define UART3_DE_PIN GPIO_PIN_14
#define UART3_DE_GPIO_PORT GPIOB
#define RS_485_1_DE_PIN UART3_DE_PIN
#define RS_485_1_DE_GPIO_PORT UART3_DE_GPIO_PORT
#define SW1_PIN GPIO_PIN_13
#define SW1_GPIO_PORT GPIOC
#define DI_1_PIN GPIO_PIN_8
#define DI_1_GPIO_PORT GPIOD
#define DI_2_PIN GPIO_PIN_9
#define DI_2_GPIO_PORT GPIOD
#define RMII_TXD0_PIN GPIO_PIN_12
#define RMII_TXD0_GPIO_PORT GPIOB
#define UART3_TX_PIN GPIO_PIN_10
#define UART3_TX_GPIO_PORT GPIOB
#define RS_485_1_TXD UART3_TX_PIN
#define RS_485_1_TXD_GPIO_PORT UART3_TX_GPIO_PORT
#define UART3_RX_PIN GPIO_PIN_11
#define UART3_RX_GPIO_PORT GPIOB
#define RS_485_1_RXD_PIN UART3_RX_PIN
#define RS_485_1_RXD_GPIO_PORT UART3_RX_GPIO_PORT
#define SPI6_SCK_PIN GPIO_PIN_13
#define SPI6_SCK_GPIO_PORT GPIOG
#define MESO_SCK_PIN SPI6_SCK_PIN
#define MESO_SCK_GPIO_PORT SPI6_SCK_GPIO_PORT
#define LD1_PIN LED1_G_PIN
#define LD1_GPIO_PORT LED1_G_GPIO_PORT
#define LD2_PIN LED2_G_PIN
#define LD2_GPIO_PORT LED2_G_GPIO_PORT
#define LD3_PIN LED3_G_PIN
#define LD3_GPIO_PORT LED3_G_GPIO_PORT
#endif  //SNEMA_PLC_BOARD

#define AI_MCU_3_PIN GPIO_PIN_1
#define AI_MCU_3_GPIO_PORT GPIOB
#define QSPI_BK2_CLK_PIN GPIO_PIN_2
#define QSPI_BK2_CLK_GPIO_PORT GPIOB
#define DEBUG_SWO_PIN GPIO_PIN_3
#define DEBUG_SWO_GPIO_PORT GPIOB
#define MESO_GP0_PIN GPIO_PIN_4
#define MESO_GP0_GPIO_PORT GPIOB
#define HART_RX_ALT_PIN GPIO_PIN_5
#define HART_RX_ALT_GPIO_PORT GPIOB
#define UART1_TX_PIN GPIO_PIN_6
#define UART1_TX_GPIO_PORT GPIOB
    #define RS_232_TXD_PIN UART1_TX_PIN
    #define RS_232_TXD_GPIOPORT UART1_TX_GPIO_PORT

#define I2C1_SDA_PIN GPIO_PIN_9
#define I2C1_SDA_GPIO_PORT GPIOB
    #define MESO_SDA_PIN I2C1_SDA_PIN
    #define MESO_SDA_GPIO_PORT I2C1_SDA_GPIO_PORT


#define RMII_TXD1_PIN GPIO_PIN_13
#define RMII_TXD1_GPIO_PORT GPIOB
#define DI_0_PIN GPIO_PIN_15
#define DI_0_GPIO_PORT GPIOB

// --- defgroup for PORTC ---

#define DO_ON_3_PIN GPIO_PIN_0
#define DO_ON_3_GPIO_PORT GPIOC
#define RMII_MDC_PIN GPIO_PIN_1
#define RMII_MDC_GPIO_PORT GPIOC
#define DO_TEST_3_PIN GPIO_PIN_2
#define DO_TEST_3_GPIO_PORT GPIOC
#define DO_SC_2_PIN GPIO_PIN_3
#define DO_SC_2_GPIO_PORT GPIOC
#define RMII_RXD0_PIN GPIO_PIN_4
#define RMII_RXD0_GPIO_PORT GPIOC
#define RMII_RXD1_PIN GPIO_PIN_5
#define RMII_RXD1_GPIO_PORT GPIOC

#define UART6_TX_PIN GPIO_PIN_6
#define UART6_TX_GPIO_PORT GPIOC
    #define IMM_RS_485_TXD_PIN UART6_TX_PIN
    #define IMM_RS_485_TXD_GPIO_PORT UART6_TX_GPIO_PORT
#define UART6_RX_PIN GPIO_PIN_7
#define UART6_RX_GPIO_PORT GPIOC
    #define IMM_RS_485_RXD_PIN UART6_RX_PIN
    #define IMM_RS_485_RXD_GPIO_PORT UART6_RX_GPIO_PORT

#define DI_15_PIN GPIO_PIN_8
#define DI_15_GPIO_PORT GPIOC
#define MCO_25MHZ_PIN GPIO_PIN_9
#define MCO_25MHZ_GPIO_PORT GPIOC
#define LED1_G_PIN GPIO_PIN_10
#define LED1_G_GPIO_PORT GPIOC
#define QSPI_BK2_NCS_PIN GPIO_PIN_11
#define QSPI_BK2_NCS_GPIO_PORT GPIOC
#define LED2_G_PIN GPIO_PIN_12
#define LED2_G_GPIO_PORT GPIOC

// --- defgroup for PORTD ---

#define IMM_CAN1_RX_PIN GPIO_PIN_0
#define IMM_CAN1_RX_GPIO_PORT GPIOD
#define IMM_CAN1_TX_PIN GPIO_PIN_1
#define IMM_CAN1_TX_GPIO_PORT GPIOD
#define LED2_R_PIN GPIO_PIN_2
#define LED2_R_GPIO_PORT GPIOD
#define LED3_R_PIN GPIO_PIN_3
#define LED3_R_GPIO_PORT GPIOD

#define UART2_DE_PIN GPIO_PIN_4
#define UART2_DE_GPIO_PORT GPIOD
    #define RS_485_2_DE_PIN UART2_DE_PIN
    #define RS_485_2_DE_GPIO_PORT UART2_DE_GPIO_PORT
#define UART2_TX_PIN GPIO_PIN_5
#define UART2_TX_GPIO_PORT GPIOD
    #define RS_485_2_TXD_PIN UART2_TX_PIN
    #define RS_485_2_TXD_GPIO_PORT UART2_TX_GPIO_PORT
#define UART2_RX_PIN GPIO_PIN_6
#define UART2_RX_GPIO_PORT GPIOD
    #define RS_485_2_RXD_PIN UART2_RX_PIN
    #define RS_485_2_RXD_GPIO_PORT UART2_RX_GPIO_PORT
#define DI_3_PIN GPIO_PIN_10
#define DI_3_GPIO_PORT GPIOD
#define DI_4_PIN GPIO_PIN_11
#define DI_4_GPIO_PORT GPIOD
#define DI_5_PIN GPIO_PIN_12
#define DI_5_GPIO_PORT GPIOD
#define DI_6_PIN GPIO_PIN_13
#define DI_6_GPIO_PORT GPIOD
#define DI_7_PIN GPIO_PIN_14
#define DI_7_GPIO_PORT GPIOD
#define DI_8_PIN GPIO_PIN_15
#define DI_8_GPIO_PORT GPIOD

// --- defgroup for PORTE ---

#define AI_TEST_PIN GPIO_PIN_0
#define AI_TEST_GPIO_PORT GPIOE
#define DI_TEST_PIN GPIO_PIN_1
#define DI_TEST_GPIO_PORT GPIOE

#define SPI4_SCK_PIN GPIO_PIN_2
#define SPI4_SCK_GPIO_PORT GPIOE
    #define ADC_SCK_PIN SPI4_SCK_PIN
    #define ADC_SCK_GPIO_PORT SPI4_SCK_GPIO_PORT
#define AI_A0_PIN GPIO_PIN_2
#define AI_A0_GPIO_PORT GPIOE

#define IMM_CAN1_S_PIN GPIO_PIN_3
#define IMM_CAN1_S_GPIO_PORT GPIOE
#define ADC_CNV_PIN GPIO_PIN_4
#define ADC_CNV_GPIO_PORT GPIOE

#define SPI4_MISO_PIN GPIO_PIN_5
#define SPI4_MISO_GPIO_PORT GPIOE
    #define ADC_SDO_PIN SPI4_MISO_PIN
    #define ADC_SDO_GPIO_PORT SPI4_MISO_PORT
#define SPI4_MOSI_PIN GPIO_PIN_6
#define SPI4_MOSI_GPIO_PORT GPIOE
    #define ADC_DIN_PIN SPI4_MOSI_PIN
    #define ADC_DIN_GPIO_PORT SPI4_MOSI_GPIO_PORT

#define AI_A1_PIN GPIO_PIN_5
#define AI_A1_GPIO_PORT GPIOE
#define AI_A2_PIN GPIO_PIN_6
#define AI_A2_GPIO_PORT GPIOE
#define QSPI_BK2_IO0_PIN GPIO_PIN_7
#define QSPI_BK2_IO0_GPIO_PORT GPIOE
#define QSPI_BK2_IO1_PIN GPIO_PIN_8
#define QSPI_BK2_IO1_GPIO_PORT GPIOE
#define QSPI_BK2_IO2_PIN GPIO_PIN_9
#define QSPI_BK2_IO2_GPIO_PORT GPIOE
#define QSPI_BK2_IO3_PIN GPIO_PIN_10
#define QSPI_BK2_IO3_GPIO_PORT GPIOE
#define DO_SC_0_PIN GPIO_PIN_11
#define DO_SC_0_GPIO_PORT GPIOE
#define DO_OD_1_PIN GPIO_PIN_12
#define DO_OD_1_GPIO_PORT GPIOE
#define DO_ON_0_PIN GPIO_PIN_13
#define DO_ON_0_GPIO_PORT GPIOE
#define DO_TEST_0_PIN GPIO_PIN_14
#define DO_TEST_0_GPIO_PORT GPIOE
#define DO_OD_0_PIN GPIO_PIN_15
#define DO_OD_0_GPIO_PORT GPIOE

// --- defgroup for PORTF ---

#define SW2_PIN GPIO_PIN_0
#define SW2_GPIO_PORT GPIOF
#define LED4_R_PIN GPIO_PIN_1
#define LED4_R_GPIO_PORT GPIOF
#define HART_A0_PIN GPIO_PIN_2
#define HART_A0_GPIO_PORT GPIOF
#define HART_A1_PIN GPIO_PIN_3
#define HART_A1_GPIO_PORT GPIOF
#define HART_A2_PIN GPIO_PIN_4
#define HART_A2_GPIO_PORT GPIOF
#define HART_DPLX_PIN GPIO_PIN_5
#define HART_DPLX_GPIO_PORT GPIOF

#define UART7_RX_PIN GPIO_PIN_6
#define UART7_RX_GPIO_PORT GPIOF
    #define HART_RXD_PIN UART7_RX_PIN
    #define HART_RXD_GPIO_PORT UART7_RX_GPIO_PORT
#define UART7_TX_PIN GPIO_PIN_7
#define UART7_TX_GPIO_PORT GPIOF
    #define HART_TXD_PIN UART7_TX_PIN
    #define HART_TXD_PORT UART7_TX_GPIO_PORT
#define UART7_DE_PIN GPIO_PIN_8
#define UART7_DE_GPIO_PORT GPIOF
    #define HART_NRTS_PIN UART7_DE_PIN
    #define HART_NRTS_PORT UART7_DE_GPIO_PORT

#define HART_CD_PIN GPIO_PIN_9
#define HART_CD_GPIO_PORT GPIOF
#define DO_SC_3_PIN GPIO_PIN_10
#define DO_SC_3_GPIO_PORT GPIOF
#define DO_OD_3_PIN GPIO_PIN_11
#define DO_OD_3_GPIO_PORT GPIOF
#define DO_ON_2_PIN GPIO_PIN_12
#define DO_ON_2_GPIO_PORT GPIOF
#define DO_TEST_2_PIN GPIO_PIN_13
#define DO_TEST_2_GPIO_PORT GPIOF
#define DO_OD_2_PIN GPIO_PIN_14
#define DO_OD_2_GPIO_PORT GPIOF
#define DO_SC_1_PIN GPIO_PIN_15
#define DO_SC_1_GPIO_PORT GPIOF

// --- defgroup for PORTG ---

#define DO_ON_1_PIN GPIO_PIN_0
#define DO_ON_1_GPIO_PORT GPIOG
#define DO_TEST_1_PIN GPIO_PIN_1
#define DO_TEST_1_GPIO_PORT GPIOG
#define DI_9_PIN GPIO_PIN_2
#define DI_9_GPIO_PORT GPIOG
#define DI_10_PIN GPIO_PIN_3
#define DI_10_GPIO_PORT GPIOG
#define DI_11_PIN GPIO_PIN_4
#define DI_11_GPIO_PORT GPIOG
#define DI_12_PIN GPIO_PIN_5
#define DI_12_GPIO_PORT GPIOG
#define DI_13_PIN GPIO_PIN_6
#define DI_13_GPIO_PORT GPIOG
#define DI_14_PIN GPIO_PIN_7
#define DI_14_GPIO_PORT GPIOG

#define UART6_DE_PIN GPIO_PIN_8
#define UART6_DE_GPIO_PORT GPIOG
    #define IMM_RS_485_DE_PIN UART6_DE_PIN
    #define IMM_RS_485_DE_GPIO_PORT UART6_DE_GPIO_PORT

#define LED3_G_PIN GPIO_PIN_9
#define LED3_G_GPIO_PORT GPIOG
#define LED4_G_PIN GPIO_PIN_10
#define LED4_G_GPIO_PORT GPIOG
#define RMII_TX_EN_PIN GPIO_PIN_11
#define RMII_TX_EN_GPIO_PORT GPIOG

#define SPI6_MISO_PIN GPIO_PIN_12
#define SPI6_MISO_GPIO_PORT GPIOG
    #define MESO_MISO_PIN SPI6_MISO_PIN
    #define MESO_MISO_GPIO_PORT SPI6_MISO_GPIO_PORT
#define SPI6_MOSI_PIN GPIO_PIN_14
#define SPI6_MOSI_GPIO_PORT GPIOG
    #define MESO_MOSI_PIN SPI6_MOSI_PIN
    #define MESO_MOSI_GPIO_PORT SPI6_MOSI_GPIO_PORT
#define MESO_GP1_PIN GPIO_PIN_15
#define MESO_GP1_GPIO_PORT GPIOG

#endif // PIN_MAP_OLD_H
