#ifndef PIN_MAP_H
#define PIN_MAP_H 1
//#define USE_OLD_MAP 1 //comment it
#include "sofi_config.h"

#if BOARD_VERSION == 0
#include "pin_map_old.h"
#endif // SNEMA_PLC_V0_BOARD

#if BOARD_VERSION == 1
#include "pin_map_new.h"
#endif // SNEMA_PLC_V1_BOARD

#if BOARD_VERSION == 2
#include "pin_map_v2.h"
#endif // SNEMA_PLC_V2_BOARD

#if BOARD_VERSION == 3
#include "pin_map_v2.h"
#endif // SNEMA_PLC_V3_BOARD

#endif // PIN_MAP_H
