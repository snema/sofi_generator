#ifndef UART_H
#define UART_H 1
#include "type_def.h"
#ifndef SOFI_LINK
#include "stm32f7xx_hal.h"
#endif
#include "packet.h"
#ifdef __cplusplus 
   extern "C" {
#endif
#define MCU_UART_IRQS { USART1_IRQn, USART2_IRQn, USART3_IRQn,\
                        UART4_IRQn, UART5_IRQn, USART6_IRQn, UART7_IRQn}
#define UART_PORTS 7

#define RX_DELAY_DEFAULT 1000
#define IMMODULE_UART_BIT_RATE 483871
#define MEZO_UART_BIT_RATE 400000
typedef enum{
    UART_IN_TYPE_TIMEOUT = 1<<0,
    UART_IN_TYPE_SLIP = 1<<1
}uart_in_type;
typedef struct MCU_PACK{
   void* send_option;
   u32 control_time ;  /*!< current time plus delay*/
}sending_timer_handle_t;

typedef  struct MCU_PACK {
    u32 last_time;          /*!< last time */
    u32 rx_time_delay;   //recieve timeout in bits
    u32 baud_rate;
    uart_in_type in_type;    //slip or timeout

}uart_self_t;

typedef struct MCU_PACK {
    u16 out_len;
    u16 out_ptr;
    u16 in_len;
    u16 in_ptr;
    u16 received_len;
    u16 max_len;
    u16 par_err_cnt;
    u16 frame_err_cnt;
    u16 noise_err_cnt;
    u16 ovrrun_err_cnt;
    u16 send_delay;/*!< delay before send(meuserements in 100 uSec ,1 ->100 uSec)*/
    chanel_state_t state;
    uart_self_t self;
    sending_timer_handle_t sending_timer;
    sending_timer_handle_t busy_timer;
    u8* buff_out;
    u8* buff_in;
    u8* buff_received;
    int (*send)(u8,const u8*,u16);
} uart_stream_t;

extern u8 uart1_buff_out[];
extern u8 uart1_buff_received[];
extern u8 uart1_buff_in[];

extern u8 uart2_buff_out[];
extern u8 uart2_buff_received[];
extern u8 uart2_buff_in[];

extern u8 uart3_buff_out[];
extern u8 uart3_buff_received[];
extern u8 uart3_buff_in[];

extern u8 uart4_buff_out[];
extern u8 uart4_buff_received[];
extern u8 uart4_buff_in[];

extern u8 uart5_buff_out[];
extern u8 uart5_buff_received[];
extern u8 uart5_buff_in[];

extern u8 uart6_buff_out[];
extern u8 uart6_buff_received[];
extern u8 uart6_buff_in[];

extern u8 uart7_buff_out[];
extern u8 uart7_buff_received[];
extern u8 uart7_buff_in[];

extern uart_stream_t uart_hand[];
extern USART_TypeDef *  uart_base[];

//buffer size
#define UART1_BUFFERS_SIZE (TCP_MSS + 128)
#define UART2_BUFFERS_SIZE 255
#define UART3_BUFFERS_SIZE 255
#define UART4_BUFFERS_SIZE 0
#define UART5_BUFFERS_SIZE 255
#define UART6_BUFFERS_SIZE (TCP_MSS + 128)
#define UART7_BUFFERS_SIZE 255

#define UART1_NUMBER 0
#define UART2_NUMBER 1
#define UART3_NUMBER 2
#define UART4_NUMBER 3
#define UART5_NUMBER 4
#define UART6_NUMBER 5
#define UART7_NUMBER 6

#define RS_232_UART      UART3_NUMBER
#define RS_485_1_UART    UART5_NUMBER
#define RS_485_2_UART    UART2_NUMBER
//#define UART4_NUMBER
#define MESO_UART_UART   UART1_NUMBER
#define RS_485_IMMO_UART UART6_NUMBER
#define HART_UART        UART7_NUMBER
#define RAW_FLAG        ((u16)0x8000)
#define CHANNEL_MASK    (~RAW_FLAG)
#define MESO_UART_RAW  (MESO_UART_UART|RAW_FLAG)
//functions 
void uarts_init(void);
/**
 * @brief uart_enable
 * @param port
 * @return
 */
u8 uart_enable(u8 port);
/**
 * @brief uart_disable
 * @param port
 * @return return none zero value if error occured
 */
int uart_disable(u8 port);
/**
 * @brief uart_setting
 * @param port
 * @param bit_rate
 * @param word_len
 * @param stop_bit_number
 * @param parity
 * @param rs_485_en
 * @param de_polarity
 * @param rx_delay  in bit number @ref RX_DELAY_DEFAULT "rx default value"
 * @return
 */
u8 uart_setting(u8 port,u32 bit_rate,u8 word_len,u8 stop_bit_number,u8 parity,u8 rs_485_en,u8 de_polarity,u16 rx_delay);
/**
 * @brief uart_send
 * @param port
 * @param buff
 * @param len
 * @return
 */
int uart_send(u8 port,const u8 * buff,u16 len);
/**
 * @brief uart_setting_from_regs use global sofi variable for init or reinit uarts
 * @param uart
 */
void uart_setting_from_regs(u8 uart);
int uart_timeout_handl(void);
#ifdef __cplusplus
}
#endif
#endif //UART_H
