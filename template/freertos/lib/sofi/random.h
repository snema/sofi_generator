/**
 * @file random.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup free_rtos/inc
 * @ingroup free_rtos/inc
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef RANDOM_H
#define RANDOM_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_init.h"
#include "stm32f7xx_hal.h"
/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */

#define POLINOM_2BIT(ps_rnd) (((ps_rnd & BIT(1)) >> 1)^((ps_rnd & BIT(0)) >> 0)^1)
#define POLINOM_3BIT(ps_rnd) (((ps_rnd & BIT(2)) >> 2)^((ps_rnd & BIT(0)) >> 0)^1)
#define POLINOM_4BIT(ps_rnd) (((ps_rnd & BIT(3)) >> 3)^((ps_rnd & BIT(0)) >> 0)^1)
#define POLINOM_5BIT(ps_rnd) (((ps_rnd & BIT(4)) >> 4)^((ps_rnd & BIT(1)) >> 1)^1)
#define POLINOM_6BIT(ps_rnd) (((ps_rnd & BIT(5)) >> 5)^((ps_rnd & BIT(0)) >> 0)^1)
#define POLINOM_7BIT(ps_rnd) (((ps_rnd & BIT(6)) >> 6)^((ps_rnd & BIT(2)) >> 2)^1)
#define POLINOM_8BIT(ps_rnd) (((ps_rnd & BIT(7)) >> 7)^((ps_rnd & BIT(3)) >> 3)^((ps_rnd & BIT(2)) >> 2)^((ps_rnd & BIT(1)) >> 1)^1)
#define POLINOM_9BIT(ps_rnd) (((ps_rnd & BIT(8)) >> 8)^((ps_rnd & BIT(3)) >> 3)^1)
#define POLINOM_10BIT(ps_rnd) (((ps_rnd & BIT(9)) >> 9)^((ps_rnd & BIT(2)) >> 2)^1)
#define POLINOM_11BIT(ps_rnd) (((ps_rnd & BIT(10)) >> 10)^((ps_rnd & BIT(1)) >> 1)^1)
#define POLINOM_12BIT(ps_rnd) (((ps_rnd & BIT(11)) >> 11)^((ps_rnd & BIT(5)) >> 5)^((ps_rnd & BIT(3)) >> 3)^((ps_rnd & BIT(0)) >> 0)^1)
#define POLINOM_13BIT(ps_rnd) (((ps_rnd & BIT(12)) >> 12)^((ps_rnd & BIT(3)) >> 3)^((ps_rnd & BIT(2)) >> 2)^((ps_rnd & BIT(0)) >> 0)^1)
#define POLINOM_14BIT(ps_rnd) (((ps_rnd & BIT(13)) >> 13)^((ps_rnd & BIT(9)) >> 9)^((ps_rnd & BIT(5)) >> 5)^((ps_rnd & BIT(0)) >> 0)^1)
#define POLINOM_15BIT(ps_rnd) (((ps_rnd & BIT(14)) >> 14)^((ps_rnd & BIT(0)) >> 0)^1)
#define POLINOM_16BIT(ps_rnd) (((ps_rnd & BIT(15)) >> 15)^((ps_rnd & BIT(11)) >> 11)^((ps_rnd & BIT(2)) >> 2)^((ps_rnd & BIT(0)) >> 0)^1)

extern RNG_HandleTypeDef hrng;
int set_random_buff(unsigned char *output, size_t len, size_t *olen );
int32_t random_segment (u32 min, u32 max);
u16 pseudo_rnd (u8 bit_number);
void random_gen_init(void);
void random_gen_deinit(void);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //RANDOM_H
