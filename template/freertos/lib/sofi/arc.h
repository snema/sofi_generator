/**
 * @file arc.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup free_rtos/littlefs/
 * @ingroup free_rtos/littlefs/
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef ARC_H
#define ARC_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "cmsis_os.h"
/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
   typedef enum u32{
       ARC_IS_LAST = (1<<0),    //!<arc is last
       ARC_NOT_READED = (1<<1), //!< didt read this arc, sets mannualy
   }arc_flags;
typedef struct MCU_PACK{
       u8 sec;             /*!< RTC Time Seconds [0;59]*/
       u8 min;             /*!< RTC Time Minutes [0;59]*/
       u8 hour;            /*!< RTC Time Hour [0;23]*/
       u8 date;            /*!< RTC Date[1;31]*/
       u8 month;           /*!< RTC Date Month (in BCD format)*/
       u8 year;            /*!< RTC Date Year[0,99]*/
}arc_struct_time_t;

typedef struct MCU_PACK{
    u32 unix_time;
    arc_struct_time_t struct_time;
    u32 number;
    arc_flags flags;
    u32 id;
}arc_header_t;
typedef struct MCU_PACK{
   u32 unix_time_last_arc;
   u32 last_writed;
   u32 number_to_read;
   u32 body_len;
   u32 id;
}arc_file_title_t;

#define MIN_ARC_BODY_SIZE (sizeof(arc_header_t))
#define MAX_ARC_BODY_SIZE (sizeof(arc_header_t)+256)
#define MAX_ARC_SIZE ((sizeof(arc_header_t)+256)*1000)
extern osThreadId arc_thread_id;
FNCT_NO_RETURN void arc_sofi_task( void const * argument );
int arc_get_title(const char * arc_name,arc_file_title_t * arc_file_title);
int arc_remove(const char * name);
int arc_remove_all(void);
/**
 * @brief arc_refresh_bkram_from_flash
 * @return 0 if succed
 */
int arc_refresh_bkram_from_flash(void);

/**
 * @brief arc_remove_bkram_mirror
 * @return 0 if removed
 */
int arc_remove_bkram_mirror(void);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //ARC_H
