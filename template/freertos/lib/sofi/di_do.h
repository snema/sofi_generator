﻿/**
 * @file    di_do.h
 * @author  Shoma Gane <shomagan@gmail.com>
 * @author  Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @ingroup di_do
 * @version 0.1
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */

#ifndef DI_DO_H
#define DI_DO_H
#if defined __cplusplus
extern "C" {
#endif

#include "regs.h"

/**
  * @addtogroup do
  * @{
  */
#define DO_OD_SHIFT 0
#define DO_TEST_SHIFT 4
#define DO_ON_SHIFT 0   //do enabled current in allowed limit
#define DO_SC_SHIFT 4   //do enabled current in not allowed limit
#define DO_SC_EN_SHIFT 0
#define DO_SC_FLAG_SHIFT 4
#define MAX_PWM_FREQ 10000
#define MIN_PWM_PULSE_LENGTH_US 20
#define PWM_MIN_FREQ 20
#define PWM_MAX_FREQ 10000
#define PWM_MIN_DUTY 10
#define PWM_MAX_DUTY 90
#define PWM_DUTY_MASK 0xFF
#define DO_SHORT_TIME 1 // max short circiut time in (ms)
#define DO_READ_TIME 1
#define DO_INDICATE_TIME 2
/**
  * @}
  */

/**
  * @addtogroup di
  * @{
  */
#define DI_NOISE_FILTER 3 // in ms
#define DI_DO_READ_PERIOD 1 // in ms
#define DI_FREQ_CALC_PERIOD 10 // in ms
#define SAMPLE_NUMS 5
#define MAX_SAMPLE_TIME 1000000 // in us this value = 1 sec (freq update max period)
#define MAX_PULSELESS_TIME 100000 // if during this time (ms) there are no pulses on di, the di_freq = 0
#define SAMPLE_STATISTIC 1000 // max number of pulses in the sample
#define CALIB_A 1.0f
/**
  * @}
  */


/**
  * @brief struct for frequency calculating
  * @ingroup di
  */
typedef struct MCU_PACK {
    u32 pulse_number;
    u32 time;
} sample_t;

/**
  * @brief struct for frequency calculating
  * @ingroup di
  */
typedef struct MCU_PACK {
    u32 last_imp_time;
    u32 cur_imp_time;
    u32 last_sample_time;
    u32 cur_sample_time;
    u64 sample_period;
    u64 last_value;
    u64 cur_value;
    u8 sample_cnt;
    u16 pulseless_time;
    float freq;
    u8 last_state;
    u8 need_calc_freq;
    sample_t sample[SAMPLE_NUMS];
} di_t;

/**
  * @brief isol_pwr channels
  * @ingroup di_do
  */
typedef enum{
    NONE_PWR    = 0,
    DI_PWR_OK   = 1<<0,
    DO_PWR_OK   = 1<<1,
    ISOL_PWR_OK = 1<<2,
    AI_PWR_OK   = 1<<3,
}isol_pwr_t;

/**
  * @brief do_pwm_ctrl bits
  * @ingroup do
  */
typedef enum{
    PWM_RUN     = 1<<8,
}do_pwm_ctrl_t;
extern const pin_map_t di_pin_map[];
extern const pin_map_t do_od_pin_map[];
extern const pin_map_t do_on_pin_map[];
extern const pin_map_t do_sc_pin_map[];
extern di_t di[];

u8 di_do_init(void);
u8 di_to_in_init(void);
u8 di_to_out_init(void);
u8 do_to_in_init(void);
u8 do_to_out_init(void);
void di_cnt_config (void);
u8 di_handling (void);
u8 do_handling (void);
u8 do_write (u8 set_value);
void di_do_read_task (const void *pvParameters);
void di_frq_calc_task (const void *pvParameters);
__STATIC_INLINE void di_is_1 (u32 di_num);
__STATIC_INLINE void di_is_0 (u32 di_num);
void di_freq_calc(void);
u8 do_pwm_freq_set (u16 freq);
u8 do_pwm_duty_set (u8 channel, u8 duty);
u8 do_pwm_run (u8 channel);
u8 do_pwm_stop (u8 channel);
u8 do_pwm_gpio_init (u8 channel);
u8 do_pwm_gpio_deinit(u8 channel);
void do_pwm_init (void);
void isol_pwr_read(void);
void do_sc_protection_on(void);
void do_sc_protection_off(void);

#if defined __cplusplus
}
#endif
#endif //DI_DO_H
