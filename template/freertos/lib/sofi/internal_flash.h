/**
 * @file internal_flash.h
 * @author Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @defgroup periph
 * @ingroup sofi
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef INTERNAL_FLASH_H
#define INTERNAL_FLASH_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "stm32f7xx_hal.h"
/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
extern u32 _isr_vector;
extern u32 _text;
extern u32 _etext;
extern u32 _sdata;
extern u32 _edata;
extern u32 _sidata;

extern u32 _sram_size;
extern u32 _sram_start;
extern u32 _flash_start_address;
extern u32 _internal_flash_size;
extern u32 _flash_page_count;

#if !defined DEVICE_RAM_PAGE_SIZE
#define DEVICE_RAM_PAGE_SIZE 1024
#endif

#define SRAM_SIZE ((int)&_sram_size)
#define SRAM_START ((int)&_sram_start)
#define SRAM_END (SRAM_START + SRAM_SIZE)
#define SRAM_PAGES (SRAM_SIZE / DEVICE_RAM_PAGE_SIZE)

#define FLASH_START ((u32)&_flash_start_address)
#define FLASH_CODE_START ((u32)&_isr_vector)
#define FLASH_CODE_END ((u32)&_sidata + (u32)&_edata - (u32)&_sdata)
#define FLASH_SIZE ((u32)&_internal_flash_size)
#define FLASH_PAGE_COUNT ((u32)&_flash_page_count)
#define SINGLE_BANK 1
#ifdef SINGLE_BANK
    #define STM32_FLASH_LAYOUT_32_32_32_32_128_256_256_256 1
#else
    STM32_FLASH_LAYOUT_16_16_16_16_64_128_128_128
#endif
int intrenal_flash_write(u32 addr, const void * buf, u32 nbyte);
int intrenal_flash_write_byte(u32 addr, const void * buf, u32 nbyte);
int intrenal_flash_erase_sector_num(u32 sector, u8 num);
int internal_flash_get_sector_size(u32 sector);
int internal_flash_get_sector_addr(u32 sector);
int internal_flash_is_flash(u32 addr, u32 size);
int internal_flash_is_code(u32 addr, u32 size);
int internal_flash_get_sector(u32 addr);


int internal_flash_blank_check(int loc, int nbyte);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //INTERNAL_FLASH_H
