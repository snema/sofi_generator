#ifndef CAN_FEST_BRIDGE_H
#define CAN_FEST_BRIDGE_H
#include "task_create.h"
#include "stm32f7xx_hal.h"

/*type of can variables*/
#define AO_VALUE 0x200
#define AI_VALUE 0x280
#define DO_VALUE 0x300
#define DI_VALUE 0x380
#define BRIC_PDO_VALUE 0x400
extern timer_hadle_t timer_hadle_can;
void can_tim_itterupt_handler(timer_hadle_t timer);
int initTimer(void);
int stop_timer(timer_hadle_t timer);
#endif //CAN_FEST_BRIDGE_H

