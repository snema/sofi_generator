/**
 * @file    bkram_access.h
 * @author  Shoma Gane <shomagan@gmail.com>
 * @author  Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 * @ingroup bkram
 * @version 0.1
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef BKRAM_ACCESS_H
#define BKRAM_ACCESS_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_init.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif

/**
  * @addtogroup bkram
  * @{
  */
#define BKSRAM_SIZE 0x1000  /* 4096 bytes */
#define BKRAM_VERIFY_NUMBER 0xFEC8FE64F731F762
#define BKRAM_VERIFY_ADDR 0xFF0
#define BKRAM_VERIFY_ADDR_USER 0xFF8
/**
  * @}
  */
//temporary regs dont' use it

//temporary regs dont' use it end
/*add functions and variable declarations below */
extern  u8 bk_ram[];
/* @brief enable bksram access
 */
void bkpsram_access_enable(void);
u32 bkram_access_get_size(void);
int bkram_access_write(u16 address,const u8* data,u16 size);
int bkram_access_read(u16 address,u8* data,u16 size);
int bkram_space_is_changing(u16 address,const u8* data,u16 size);
/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //BKRAM_ACCESS_H
