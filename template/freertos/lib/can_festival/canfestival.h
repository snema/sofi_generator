#include "applicfg.h"
#include "data.h"

void clearTimer(void);

unsigned char canSend(CAN_PORT notused, Message *m);
unsigned char can_festival_init(CO_Data * d);
void canClose(void);
