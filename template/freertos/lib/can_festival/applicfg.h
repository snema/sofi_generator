/*
This file is part of CanFestival, a library implementing CanOpen Stack. 

Copyright (C): Edouard TISSERANT and Francis DUPIN

See COPYING file for copyrights details.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __APPLICFG_STM32__
#define __APPLICFG_STM32__
#include "config.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

/*  Define the architecture : little_endian or big_endian
 -----------------------------------------------------
 Test :
 UNS32 v = 0x1234ABCD;
 char *data = &v;

 Result for a little_endian architecture :
 data[0] = 0xCD;
 data[1] = 0xAB;
 data[2] = 0x34;
 data[3] = 0x12;

 Result for a big_endian architecture :
 data[0] = 0x12;
 data[1] = 0x34;
 data[2] = 0xAB;
 data[3] = 0xCD;
 */

/* Integers */
typedef signed char INTEGER8;
typedef signed short INTEGER16;
typedef int INTEGER24;
typedef int INTEGER32;
typedef signed long long INTEGER40;
typedef signed long long INTEGER48;
typedef signed long long INTEGER56;
typedef signed long long INTEGER64;

/* Unsigned integers */
typedef unsigned char UNS8;
typedef unsigned short UNS16;
typedef unsigned long UNS32;
typedef unsigned long UNS24;
typedef unsigned long long UNS40;
typedef unsigned long long UNS48;
typedef unsigned long long UNS56;
typedef unsigned long long UNS64;

/* Reals */
typedef float REAL32;
typedef double REAL64;

/* Definition of error and warning macros */
/* -------------------------------------- */
#define MSG(...) sofi_printf(__VA_ARGS__)

/* Definition of MSG_ERR */
/* --------------------- */
#ifdef DEBUG_ERR_CONSOLE_ON
#define MSG_ERR(num, str, val)            \
          sofi_printf(MSG_ERROR,"ERR %s,%d : 0X%x %s 0X%x \n",__FILE__, __LINE__,num, str, val)
#else
#    define MSG_ERR(num, str, val)
#endif

/* Definition of MSG_WAR */
/* --------------------- */
#ifdef DEBUG_WAR_CONSOLE_ON
#define MSG_WAR(num, str, val)          \
          sofi_printf(MSG_ERROR,"WAR %s,%d : 0X%x %s 0X%x \n",__FILE__, __LINE__,num, str, val)
#else
#    define MSG_WAR(num, str, val)
#endif

typedef void* CAN_HANDLE;

typedef void* CAN_PORT;

#endif
