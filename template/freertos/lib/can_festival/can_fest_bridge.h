  
#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f7xx_hal.h"

/*type of can variables*/
#define AO_VALUE 0x200
#define AI_VALUE 0x280
#define DO_VALUE 0x300
#define DI_VALUE 0x380
#define BRIC_PDO_VALUE 0x400

void can_tim_itterupt_handler(void);
void can_fest_recv_handler(CAN_RxHeaderTypeDef *rx_struct,u8 * buff);

#endif 

