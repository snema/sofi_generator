/////////////////////////////////////////////////////////////
//// Auto generate by TeenyDT, http://dt.tusb.org
/////////////////////////////////////////////////////////////
#ifndef __CDC_RNDIS_TEENY_USB_INIT_H__
#define __CDC_RNDIS_TEENY_USB_INIT_H__
// forward declare the tusb_descriptors struct
typedef struct _tusb_descriptors tusb_descriptors;

#define CDC_RNDIS_VID                                            0x0483
#define CDC_RNDIS_PID                                            0x0011
#define CDC_RNDIS_STRING_COUNT                                   (4)

// device.bmAttributes & 0x40   USB_CONFIG_SELF_POWERED
// device.bmAttributes & 0x20   USB_CONFIG_REMOTE_WAKEUP
#define CDC_RNDIS_DEV_STATUS                                    (0 | 0)


// Endpoint usage:
#define CDC_RNDIS_MAX_EP                                         (3)
#define CDC_RNDIS_EP_NUM                                         (CDC_RNDIS_MAX_EP + 1)

///////////////////////////////////////////////
//// Endpoint define for STM32 FS Core
///////////////////////////////////////////////

#ifdef CDC_RNDIS_BTABLE_ADDRESS
#undef CDC_RNDIS_BTABLE_ADDRESS
#endif
#define CDC_RNDIS_BTABLE_ADDRESS                                 (0)
#define CDC_RNDIS_EP_BUF_DESC_TABLE_SIZE                         (8)
// PMA buffer reserved for buffer description table
#define CDC_RNDIS_USB_BUF_START                                  (CDC_RNDIS_EP_BUF_DESC_TABLE_SIZE * CDC_RNDIS_EP_NUM)

// EndPoints 0 defines
#define CDC_RNDIS_EP0_RX_SIZE                                    (64)
#define CDC_RNDIS_EP0_RX_ADDR                                    (CDC_RNDIS_USB_BUF_START + (0))
#define CDC_RNDIS_EP0_TX_SIZE                                    (64)
#define CDC_RNDIS_EP0_TX_ADDR                                    (CDC_RNDIS_USB_BUF_START + (64))
#define CDC_RNDIS_EP0_RX_TYPE                                    USB_EP_CONTROL
#define CDC_RNDIS_EP0_TX_TYPE                                    USB_EP_CONTROL

#define CDC_RNDIS_EP0_TYPE                                       USB_EP_CONTROL
#define CDC_RNDIS_EP0_TX0_ADDR                                   (CDC_RNDIS_USB_BUF_START + (0))
#define CDC_RNDIS_EP0_TX1_ADDR                                   (CDC_RNDIS_USB_BUF_START + (64))
#define CDC_RNDIS_EP0_RX0_ADDR                                   (CDC_RNDIS_USB_BUF_START + (0))
#define CDC_RNDIS_EP0_RX1_ADDR                                   (CDC_RNDIS_USB_BUF_START + (64))

// EndPoints 1 defines
#define CDC_RNDIS_EP1_RX_SIZE                                    (64)
#define CDC_RNDIS_EP1_RX_ADDR                                    (CDC_RNDIS_USB_BUF_START + (128))
#define CDC_RNDIS_EP1_TX_SIZE                                    (0)
#define CDC_RNDIS_EP1_TX_ADDR                                    (CDC_RNDIS_USB_BUF_START + (192))
#define CDC_RNDIS_EP1_RX_TYPE                                    USB_EP_BULK
#define CDC_RNDIS_EP1_TX_TYPE                                    USB_EP_Invalid

#define CDC_RNDIS_EP1_TYPE                                       USB_EP_BULK
#define CDC_RNDIS_EP1_TX0_ADDR                                   (CDC_RNDIS_USB_BUF_START + (128))
#define CDC_RNDIS_EP1_TX1_ADDR                                   (CDC_RNDIS_USB_BUF_START + (192))
#define CDC_RNDIS_EP1_RX0_ADDR                                   (CDC_RNDIS_USB_BUF_START + (128))
#define CDC_RNDIS_EP1_RX1_ADDR                                   (CDC_RNDIS_USB_BUF_START + (192))

// EndPoints 2 defines
#define CDC_RNDIS_EP2_RX_SIZE                                    (0)
#define CDC_RNDIS_EP2_RX_ADDR                                    (CDC_RNDIS_USB_BUF_START + (256))
#define CDC_RNDIS_EP2_TX_SIZE                                    (64)
#define CDC_RNDIS_EP2_TX_ADDR                                    (CDC_RNDIS_USB_BUF_START + (256))
#define CDC_RNDIS_EP2_RX_TYPE                                    USB_EP_Invalid
#define CDC_RNDIS_EP2_TX_TYPE                                    USB_EP_BULK

#define CDC_RNDIS_EP2_TYPE                                       USB_EP_BULK
#define CDC_RNDIS_EP2_TX0_ADDR                                   (CDC_RNDIS_USB_BUF_START + (256))
#define CDC_RNDIS_EP2_TX1_ADDR                                   (CDC_RNDIS_USB_BUF_START + (320))
#define CDC_RNDIS_EP2_RX0_ADDR                                   (CDC_RNDIS_USB_BUF_START + (256))
#define CDC_RNDIS_EP2_RX1_ADDR                                   (CDC_RNDIS_USB_BUF_START + (320))

// EndPoints 3 defines
#define CDC_RNDIS_EP3_RX_SIZE                                    (0)
#define CDC_RNDIS_EP3_RX_ADDR                                    (CDC_RNDIS_USB_BUF_START + (384))
#define CDC_RNDIS_EP3_TX_SIZE                                    (16)
#define CDC_RNDIS_EP3_TX_ADDR                                    (CDC_RNDIS_USB_BUF_START + (384))
#define CDC_RNDIS_EP3_RX_TYPE                                    USB_EP_Invalid
#define CDC_RNDIS_EP3_TX_TYPE                                    USB_EP_INTERRUPT

#define CDC_RNDIS_EP3_TYPE                                       USB_EP_INTERRUPT
#define CDC_RNDIS_EP3_TX0_ADDR                                   (CDC_RNDIS_USB_BUF_START + (384))
#define CDC_RNDIS_EP3_TX1_ADDR                                   (CDC_RNDIS_USB_BUF_START + (400))
#define CDC_RNDIS_EP3_RX0_ADDR                                   (CDC_RNDIS_USB_BUF_START + (384))
#define CDC_RNDIS_EP3_RX1_ADDR                                   (CDC_RNDIS_USB_BUF_START + (400))


// EndPoint max packed sizes
extern const uint8_t CDC_RNDIS_txEpMaxSize[];
#define CDC_RNDIS_TXEP_MAX_SIZE                                  \
const uint8_t CDC_RNDIS_txEpMaxSize[] = \
{ CDC_RNDIS_EP0_TX_SIZE, 0, CDC_RNDIS_EP2_TX_SIZE, CDC_RNDIS_EP3_TX_SIZE,  };
extern const uint8_t CDC_RNDIS_rxEpMaxSize[];
#define CDC_RNDIS_RXEP_MAX_SIZE                                  \
const uint8_t CDC_RNDIS_rxEpMaxSize[] = \
{ CDC_RNDIS_EP0_RX_SIZE, CDC_RNDIS_EP1_RX_SIZE, 0, 0,  };

// EndPoints init function for USB FS core
#define CDC_RNDIS_TUSB_INIT_EP_FS(dev) \
  do{\
    /* Init ep0 */ \
    INIT_EP_BiDirection(dev, PCD_ENDP0, CDC_RNDIS_EP0_TYPE);  \
    SET_TX_ADDR(dev, PCD_ENDP0, CDC_RNDIS_EP0_TX_ADDR);  \
    SET_RX_ADDR(dev, PCD_ENDP0, CDC_RNDIS_EP0_RX_ADDR);  \
    SET_RX_CNT(dev, PCD_ENDP0, CDC_RNDIS_EP0_RX_SIZE);  \
    /* Init ep1 */ \
    INIT_EP_RxDouble(dev, PCD_ENDP1, CDC_RNDIS_EP1_TYPE);     \
    SET_DOUBLE_ADDR(dev, PCD_ENDP1, CDC_RNDIS_EP1_RX0_ADDR, CDC_RNDIS_EP1_RX1_ADDR);  \
    SET_DBL_RX_CNT(dev, PCD_ENDP1, CDC_RNDIS_EP1_RX_SIZE);     \
    /* Init ep2 */ \
    INIT_EP_TxDouble(dev, PCD_ENDP2, CDC_RNDIS_EP2_TYPE);     \
    SET_DOUBLE_ADDR(dev, PCD_ENDP2, CDC_RNDIS_EP2_TX0_ADDR, CDC_RNDIS_EP2_TX1_ADDR);  \
    SET_DBL_TX_CNT(dev, PCD_ENDP2, 0);     \
    /* Init ep3 */ \
    INIT_EP_TxOnly(dev, PCD_ENDP3, CDC_RNDIS_EP3_TYPE);  \
    SET_TX_ADDR(dev, PCD_ENDP3, CDC_RNDIS_EP3_TX_ADDR);  \
}while(0)

///////////////////////////////////////////////
//// Endpoint define for STM32 OTG Core
///////////////////////////////////////////////
#define CDC_RNDIS_OTG_MAX_OUT_SIZE                               (64)
#define CDC_RNDIS_OTG_CONTROL_EP_NUM                             (1)
#define CDC_RNDIS_OTG_OUT_EP_NUM                                 (1)
// RX FIFO size / 4 > (CONTROL_EP_NUM * 5 + 8) +  (MAX_OUT_SIZE / 4 + 1) + (OUT_EP_NUM*2) + 1 = 33

///////////////////////////////////////////////
//// Endpoint define for STM32 OTG FS Core
///////////////////////////////////////////////
#define CDC_RNDIS_OTG_RX_FIFO_SIZE_FS                            (256)
#define CDC_RNDIS_OTG_RX_FIFO_ADDR_FS                            (0)
// Sum of IN ep max packet size is 144
// Remain Fifo size is 1024 in bytes, Rx Used 256 bytes 

// TODO:
// I don't know why the max count of TX fifo should <= (7 * EpMaxPacket)
// But it seems the STM32F7xx can be large than (7 * EpMaxPacket)
#define CDC_RNDIS_EP0_TX_FIFO_ADDR_FS                            (256)
#define CDC_RNDIS_EP0_TX_FIFO_SIZE_FS                            (CDC_RNDIS_EP0_TX_SIZE * 7)
#define CDC_RNDIS_EP2_TX_FIFO_ADDR_FS                            (704)
#define CDC_RNDIS_EP2_TX_FIFO_SIZE_FS                            (CDC_RNDIS_EP2_TX_SIZE * 7)
#define CDC_RNDIS_EP3_TX_FIFO_ADDR_FS                            (1152)
#define CDC_RNDIS_EP3_TX_FIFO_SIZE_FS                            (CDC_RNDIS_EP3_TX_SIZE * 7)
// EndPoints init function for USB OTG core
#if defined(USB_OTG_FS)
#define CDC_RNDIS_TUSB_INIT_EP_OTG_FS(dev) \
  do{\
    if(GetUSB(dev) == USB_OTG_FS) { \
      SET_RX_FIFO(dev, CDC_RNDIS_OTG_RX_FIFO_ADDR_FS, CDC_RNDIS_OTG_RX_FIFO_SIZE_FS);  \
      /* Init Ep0  */\
      INIT_EP_Tx(dev, PCD_ENDP0, CDC_RNDIS_EP0_TX_TYPE, CDC_RNDIS_EP0_TX_SIZE);  \
      SET_TX_FIFO(dev, PCD_ENDP0, CDC_RNDIS_EP0_TX_FIFO_ADDR_FS, CDC_RNDIS_EP0_TX_FIFO_SIZE_FS);  \
      INIT_EP_Rx(dev, PCD_ENDP0, CDC_RNDIS_EP0_RX_TYPE, CDC_RNDIS_EP0_RX_SIZE); \
      /* Init Ep1  */\
      INIT_EP_Rx(dev, PCD_ENDP1, CDC_RNDIS_EP1_RX_TYPE, CDC_RNDIS_EP1_RX_SIZE); \
      /* Init Ep2  */\
      INIT_EP_Tx(dev, PCD_ENDP2, CDC_RNDIS_EP2_TX_TYPE, CDC_RNDIS_EP2_TX_SIZE);  \
      SET_TX_FIFO(dev, PCD_ENDP2, CDC_RNDIS_EP2_TX_FIFO_ADDR_FS, CDC_RNDIS_EP2_TX_FIFO_SIZE_FS);  \
      /* Init Ep3  */\
      INIT_EP_Tx(dev, PCD_ENDP3, CDC_RNDIS_EP3_TX_TYPE, CDC_RNDIS_EP3_TX_SIZE);  \
      SET_TX_FIFO(dev, PCD_ENDP3, CDC_RNDIS_EP3_TX_FIFO_ADDR_FS, CDC_RNDIS_EP3_TX_FIFO_SIZE_FS);  \
    }\
  }while(0)

#else  // #if defined(USB_OTG_FS)
#define CDC_RNDIS_TUSB_INIT_EP_OTG_FS(dev) 
    
#endif  // #if defined(USB_OTG_FS)

///////////////////////////////////////////////
//// Endpoint define for STM32 OTG HS Core
///////////////////////////////////////////////
#define CDC_RNDIS_OTG_RX_FIFO_SIZE_HS                            (512)
#define CDC_RNDIS_OTG_RX_FIFO_ADDR_HS                            (0)
// Sum of IN ep max packet size is 144
// Remain Fifo size is 3584 in bytes, Rx Used 512 bytes 

// TODO:
// I don't know why the max count of TX fifo should <= (7 * EpMaxPacket)
// But it seems the STM32F7xx can be large than (7 * EpMaxPacket)
#define CDC_RNDIS_EP0_TX_FIFO_ADDR_HS                            (512)
#define CDC_RNDIS_EP0_TX_FIFO_SIZE_HS                            (CDC_RNDIS_EP0_TX_SIZE * 7)
#define CDC_RNDIS_EP2_TX_FIFO_ADDR_HS                            (960)
#define CDC_RNDIS_EP2_TX_FIFO_SIZE_HS                            (CDC_RNDIS_EP2_TX_SIZE * 7)
#define CDC_RNDIS_EP3_TX_FIFO_ADDR_HS                            (1408)
#define CDC_RNDIS_EP3_TX_FIFO_SIZE_HS                            (CDC_RNDIS_EP3_TX_SIZE * 7)
// EndPoints init function for USB OTG core
#if defined(USB_OTG_HS)
#define CDC_RNDIS_TUSB_INIT_EP_OTG_HS(dev) \
  do{\
    if(GetUSB(dev) == USB_OTG_HS) { \
      SET_RX_FIFO(dev, CDC_RNDIS_OTG_RX_FIFO_ADDR_HS, CDC_RNDIS_OTG_RX_FIFO_SIZE_HS);  \
      /* Init Ep0  */\
      INIT_EP_Tx(dev, PCD_ENDP0, CDC_RNDIS_EP0_TX_TYPE, CDC_RNDIS_EP0_TX_SIZE);  \
      SET_TX_FIFO(dev, PCD_ENDP0, CDC_RNDIS_EP0_TX_FIFO_ADDR_HS, CDC_RNDIS_EP0_TX_FIFO_SIZE_HS);  \
      INIT_EP_Rx(dev, PCD_ENDP0, CDC_RNDIS_EP0_RX_TYPE, CDC_RNDIS_EP0_RX_SIZE); \
      /* Init Ep1  */\
      INIT_EP_Rx(dev, PCD_ENDP1, CDC_RNDIS_EP1_RX_TYPE, CDC_RNDIS_EP1_RX_SIZE); \
      /* Init Ep2  */\
      INIT_EP_Tx(dev, PCD_ENDP2, CDC_RNDIS_EP2_TX_TYPE, CDC_RNDIS_EP2_TX_SIZE);  \
      SET_TX_FIFO(dev, PCD_ENDP2, CDC_RNDIS_EP2_TX_FIFO_ADDR_HS, CDC_RNDIS_EP2_TX_FIFO_SIZE_HS);  \
      /* Init Ep3  */\
      INIT_EP_Tx(dev, PCD_ENDP3, CDC_RNDIS_EP3_TX_TYPE, CDC_RNDIS_EP3_TX_SIZE);  \
      SET_TX_FIFO(dev, PCD_ENDP3, CDC_RNDIS_EP3_TX_FIFO_ADDR_HS, CDC_RNDIS_EP3_TX_FIFO_SIZE_HS);  \
    }\
  }while(0)

#else  // #if defined(USB_OTG_HS)
#define CDC_RNDIS_TUSB_INIT_EP_OTG_HS(dev) 
    
#endif  // #if defined(USB_OTG_HS)
#define CDC_RNDIS_TUSB_INIT_EP_OTG(dev) \
  do{\
    CDC_RNDIS_TUSB_INIT_EP_OTG_FS(dev); \
    CDC_RNDIS_TUSB_INIT_EP_OTG_HS(dev); \
  }while(0)


#if defined(USB)
#define CDC_RNDIS_TUSB_INIT_EP(dev) CDC_RNDIS_TUSB_INIT_EP_FS(dev)

// Teeny USB device init function for FS core
#define CDC_RNDIS_TUSB_INIT_DEVICE(dev) \
  do{\
    /* Init device features */       \
    memset(&dev->addr, 0, TUSB_DEVICE_SIZE);    \
    dev->status = CDC_RNDIS_DEV_STATUS;         \
    dev->rx_max_size = CDC_RNDIS_rxEpMaxSize;         \
    dev->tx_max_size = CDC_RNDIS_txEpMaxSize;         \
    dev->descriptors = &CDC_RNDIS_descriptors;         \
  }while(0)

#endif

#if defined(USB_OTG_FS) || defined(USB_OTG_HS)
#define CDC_RNDIS_TUSB_INIT_EP(dev) CDC_RNDIS_TUSB_INIT_EP_OTG(dev)

// Teeny USB device init function for OTG core
#define CDC_RNDIS_TUSB_INIT_DEVICE(dev) \
  do{\
    /* Init device features */       \
    memset(&dev->addr, 0, TUSB_DEVICE_SIZE);    \
    dev->status = CDC_RNDIS_DEV_STATUS;         \
    dev->descriptors = &CDC_RNDIS_descriptors;         \
  }while(0)

#endif

#define CDC_RNDIS_TUSB_INIT(dev) \
  do{\
    CDC_RNDIS_TUSB_INIT_EP(dev);   \
    CDC_RNDIS_TUSB_INIT_DEVICE(dev);   \
  }while(0)

// Get End Point count
#ifndef  EP_NUM
#define  EP_NUM 1
#endif
#if CDC_RNDIS_EP_NUM > EP_NUM
#undef   EP_NUM
#define  EP_NUM  CDC_RNDIS_EP_NUM
#endif

// Enable double buffer related code
#define  HAS_DOUBLE_BUFFER

extern const uint8_t* const CDC_RNDIS_StringDescriptors[CDC_RNDIS_STRING_COUNT];
extern const tusb_descriptors CDC_RNDIS_descriptors;

// Enable WCID related code
#define  HAS_WCID

#ifndef WCID_VENDOR_CODE
#define  WCID_VENDOR_CODE       0x17
extern const uint8_t WCID_StringDescriptor_MSOS[];
#endif


#endif   // #ifndef __CDC_RNDIS_TEENY_USB_INIT_H__
