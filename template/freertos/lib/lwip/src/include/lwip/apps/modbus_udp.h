/**
 * @file modbus_udp.h
 * @defgroup modbus udp server
 * @ingroup apps
 * @version 0.1 
 * @brief  modbus udp protocol on 7 port 
 */
/*
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef MODBUS_UDP_H
#define MODBUS_UDP_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "packet.h"
#include "modbus.h"
#include "lwip/ip_addr.h"
#include "lwip/err.h"
/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/*add functions and variable declarations below */
#define MODBUS_UDP_PORT 502u

struct modbus_udp_state_t{
    void *handle;
    struct udp_pcb *upcb;
    ip_addr_t addr_from;
    u16 port_from;
    int timer;
    u32 packet_cnt;
    chanel_state_t state;
    u16 len;
    u8 buff_received[MAX_MODBUS_PACKET];
    u8 buff_send[MAX_MODBUS_PACKET];
};
extern struct modbus_udp_state_t modbus_udp_state;
void modbus_udp_send_data_wrapped(void * ctx);
void udp_send_data(u8 * buff,u16 len,ip_addr_t addr,u16 port);
void modbus_udp_send_data(u8 * buff,u16 len);
err_t modbus_udp_init(void);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //MODBUS_UDP_H
