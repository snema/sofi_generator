/**
 * @file modbus_tcp.h
 * @defgroup free_rtos\lwip\src\include\lwip\apps\
 * @ingroup free_rtos\lwip\src\include\lwip\apps\
 * @version 0.1 
 * @brief  TODO!!! write brief in 
 */
/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef MODBUS_TCP_H
#define MODBUS_TCP_H 1
 
/*add includes below */
#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#include "packet.h"
#include "lwip/opt.h"
#include "lwip/err.h"
#include "lwip/tcpbase.h"

/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
#define MODBUS_TCP_MAX_PACKET_SIZE 255
#define LWIP_MODBUS_TCP_ABORT_ON_CLOSE_MEM_ERROR 1
#define MODBUS_TCP_USE_MEM_POOL 1
#define LWIP_MODBUS_TCP_KILL_OLD_ON_CONNECTIONS_EXCEEDED 1
#define LWIP_MODBUS_TCP_SUPPORT_11_KEEPALIVE 0
#define MODBUS_TCP_MAX_RETRIES 100
#define MEMP_NUM_PARALLEL_MODBUS_TCP_CONNS 4
/** The poll delay is X*500ms */
#if !defined MODBUS_TCP_POLL_INTERVAL || defined __DOXYGEN__
#define MODBUS_TCP_POLL_INTERVAL                 4
#endif

#if !defined MODBUS_TCP_DEBUG
#define MODBUS_TCP_DEBUG LWIP_DBG_OFF
#endif
#if !defined MODBUS_TCP_PRIO || defined __DOXYGEN__
#define MODBUS_TCP_PRIO TCP_PRIO_NORMAL
#endif
#define MODBUS_TCP_PORT 502u

/*add functions and variable declarations below */
/* structure for maintaing connection infos to be passed as argument
to LwIP callbacks*/
struct modbus_tcp_state{
#if LWIP_MODBUS_TCP_KILL_OLD_ON_CONNECTIONS_EXCEEDED
    struct modbus_tcp_state *next;
#endif /* LWIP_HTTPD_KILL_OLD_ON_CONNECTIONS_EXCEEDED */
    chanel_state_t state;             /* current connection state */
    u16 time_to_live;     //time in poll while to live with out packet
    struct altcp_pcb *pcb;    /* pointer on the current tcp_pcb */
    u32_t left;       /* Number of unsent bytes in buf. */
    u8_t retries;
    u8_t keepalive;
    u32_t time_started;
    u16 len;
    u8 flags;
    u8 buff_received[MODBUS_TCP_MAX_PACKET_SIZE];   //buff for modbus packet in tcp
    u8 buff_send[MODBUS_TCP_MAX_PACKET_SIZE];   //buff for modbus packet in tcp
};
struct modbus_tcp_state * modbus_tcp_find_handing_packet(void);
void modbus_tcp_write_wrapped(void * p);
err_t modbus_tcp_write(struct altcp_pcb *pcb, const void *ptr,\
                              u16_t *length, u8_t apiflags);
err_t modbus_tcp_init(void);
/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
#endif //MODBUS_TCP_H
