#ifndef UDP_BROADCAST_H
#define UDP_BROADCAST_H 1

#include "type_def.h"
#include "sofi_debug.h"
#include "sofi_config.h"
#define UDP_BROADCAST_SELF_PORT 65500
#define UDP_BROADCAST_MAX_PACKET_SIZE 128
#define UDP_BROADCAST_ADV_TIME_MS     60000
/**
 * @brief udp_broadcast_init
 * @return non zero value if error occured
 */
int udp_broadcast_init(void);

/**
 * @brief udp_broadcast_deinit
 * @return non zero value if error occured
 */
int udp_broadcast_deinit(void);
/**
 * @brief udp_broadcast_advertisement
 * @return none zero value if error occured
 */
int udp_broadcast_advertisement(void);
#endif  //UDP_BROADCAST_H
