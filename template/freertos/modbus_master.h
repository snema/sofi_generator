/*add includes below */
#include "type_def.h"
/*add includes before */
#ifdef __cplusplus 
   extern "C" {
#endif
/* this thread may be started for all nodes(port),one thread for one nodes
* may be have maximume DIRECT CHANNEL(RS485-1,RS485-2,RS232,HART,IMMODULE) representations
* */
#define PORT_FAILURE   -101
#define INTERNAL_ERROR -102
#define TIMEOUT        -103
#define INVALID_FRAME  -104
#define MODBUS_ERROR   -105
#define DEF_REQ_SEND_RETRIES 0
#define MODBUS_THREAD_SIZE 512
#define MODBUS_AREA_COUNT   0
#define MDB_REQTS_COUNT     0

typedef enum {
    optimize_speed,
    optimize_size
} optimization_t;

typedef enum {
    naf_ascii,
    naf_rtu,
    naf_tcp,
} node_addr_family_t;

typedef struct MCU_PACK{
    const char *host;
    const char *service;
    int         close_on_silence;
} node_addr_tcp_t;

typedef struct MCU_PACK {
    u16 channel;
    int         baud;       /* plain baud rate, eg 2400; zero for the default 9600 */
    int         parity;     /* 0 for none, 1 for odd, 2 for even                   */
    int         data_bits;
    int         stop_bits;
    int         ignore_echo; /* 1 => ignore echo; 0 => do not ignore echo */
} node_addr_rtu_t;

typedef node_addr_rtu_t node_addr_ascii_t;

typedef union {
    node_addr_ascii_t ascii;
    node_addr_rtu_t   rtu;
    node_addr_tcp_t   tcp;
} node_addr_common_t;

typedef struct MCU_PACK{
    node_addr_family_t  naf;
    node_addr_common_t  addr;
} node_addr_t;


typedef struct MCU_PACK{
    const char *location;
    u8		slave_id;
    node_addr_t	node_address;
    int		mb_nd;      // modbus library node used for this server 
    int		init_state; // store how far along the server's initialization has progressed
    osThreadId	thread_id;  // thread handling this server
} server_node_t;


  // Used by the Modbus client node
typedef struct MCU_PACK{
    const char *location;
    node_addr_t	node_address;
    int		mb_nd;
    int		init_state; // store how far along the client's initialization has progressed
    u32		comm_period;
    int		prev_error; // error code of the last printed error message (0 when no error) 
    osThreadId	thread_id;  // thread handling all communication with this client
} client_node_t;


  // Used by the Modbus client plugin
typedef enum {
    req_input,
    req_output,
    no_request		/* just for tests to quickly disable a request */
} iotype_t;

#define REQ_BUF_SIZE 64
typedef struct MCU_PACK{
    const char *location;
    u16 channel;
    int		client_node_id;
    u8		slave_id;
    iotype_t	req_type;
    u8		mb_function;
    u16		address;
    u16		count;
    int		retries;
    u8		error_code; // modbus error code (if any) of current request
    int		prev_error; // error code of the last printed error message (0 when no error) 
    u32 resp_timeout;
      // buffer used to store located PLC variables
    u16	* plcv_buffer;
      // buffer used to store data coming from / going to server
    u16 * coms_buffer;
    u8 location0;//more usable format then *location
    u8 location1;//more usable format then *location
    u8 location2;//more usable format then *location
} client_request_t;

typedef struct MCU_PACK{
    u16 * function;
    u16 * slave_address;
    u16 * regs_number;
    u16 * start_address;
    u16 * enable;
    u16 * error_counter;
    u16 * success_counter;
    u16 * reserv;
} modbus_request_state_t;
typedef struct{
	    u8 channel_from;
	    u8 channel_to;
	    u8 mdb_address;
        u8 res;
	} route_node_t;

typedef struct{
	    u8 command;
	    u16 regs_number;
	    u16 mdb_or_coil_address;
        u8 * data;
        u8 location0;
        u8 location1;
	} area_node_t;

extern client_request_t client_requests[];
extern area_node_t area_nodes[];

int start_modbus_master(void);
void modbus_master_thread(const void * param);

int modbus_master_init(void);
int modbus_master_deinit(void);
int modbus_master_init_structure(void);

/*add functions and variable declarations before */
#ifdef __cplusplus
}
#endif
