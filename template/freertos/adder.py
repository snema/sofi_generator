﻿import sys
import struct
import time
import re
custom_crc_table = {}


def generate_crc32_table(_poly):
    global custom_crc_table
    for i in range(256):
        c = i << 24
        for j in range(8):
            c = (c << 1) ^ _poly if (c & 0x80000000) else c << 1
        custom_crc_table[i] = c & 0xffffffff


def crc32_stm(bytes_arr,crc = 0xffffffff):
    length = len(bytes_arr)
    k = 0
    while length >= 4:
        v = ((bytes_arr[k] << 24) & 0xFF000000) | ((bytes_arr[k+1] << 16) & 0xFF0000) | \
        ((bytes_arr[k+2] << 8) & 0xFF00) | (bytes_arr[k+3] & 0xFF)
        crc = ((crc << 8) & 0xffffffff) ^ custom_crc_table[0xFF & ((crc >> 24) ^ v)]
        crc = ((crc << 8) & 0xffffffff) ^ custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 8))]
        crc = ((crc << 8) & 0xffffffff) ^ custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 16))]
        crc = ((crc << 8) & 0xffffffff) ^ custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 24))]
        k += 4
        length -= 4

    if length > 0:
        v = 0
        for i in range(length):
            v |= (bytes_arr[k+i] << 24-i*8)
        if length == 1:
            v &= 0xFF000000
        elif length == 2:
            v &= 0xFFFF0000
        elif length == 3:
            v &= 0xFFFFFF00
        crc = (( crc << 8 ) & 0xffffffff) ^ custom_crc_table[0xFF & ( (crc >> 24) ^ (v ) )];
        crc = (( crc << 8 ) & 0xffffffff) ^ custom_crc_table[0xFF & ( (crc >> 24) ^ (v >> 8) )];
        crc = (( crc << 8 ) & 0xffffffff) ^ custom_crc_table[0xFF & ( (crc >> 24) ^ (v >> 16) )];
        crc = (( crc << 8 ) & 0xffffffff) ^ custom_crc_table[0xFF & ( (crc >> 24) ^ (v >> 24) )];
    return crc


name = 'build/sofi_task'
file_without_crc = open(name+'.bin', 'rb')

file_crc = open(name+'_crc.bin', 'wb')
input_file = file_without_crc.read()
length = len(input_file)
#word allignment
len_add = 0
if length%4 == 1:
    len_add =3
elif length%4 ==2:
    len_add =2
elif length%4 ==3:
    len_add =1
#to length add crc and self length
length +=8
print('length output file - ', length,hex(length))
output = struct.pack('>I', length)
output += input_file

if len_add == 3:
    output += b'\x00\x00\x00'
elif len_add ==2:
    output += b'\x00\x00'
elif len_add ==1:
    output += b'\x00'

poly = 0x04c11db7
generate_crc32_table(poly)
buff = list(output)
crc = crc32_stm(buff)
buff.append(crc  &0xff)
buff.append((crc >>8)  &0xff)
buff.append((crc >>16)  &0xff)
buff.append((crc >>24)  &0xff)

custom_crc = crc32_stm(buff)
if custom_crc:
    print("check crc error")
else:
    print("check crc pass")
file_crc.write(struct.pack('>I', length))
file_crc.write(input_file)
print('crc = ', hex(crc))
file_crc.write(struct.pack('>I', crc))
file_without_crc.close()
file_crc.close()

