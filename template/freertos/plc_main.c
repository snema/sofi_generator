/**
 * Head of code common to all C targets
 **/


#include "plc_main.h"
#include "POUS.h"
#include "config_task.h"
#include "os_service.h"
#include "link_functions.h"
#include "beremiz_task.h"


/*
 * Prototypes of functions provided by generated C softPLC
 **/
void __init_debug(void);
void __publish_debug(void);

/*
 * Prototypes of functions provided by generated target C code
 * */
/*
 *  Variables used by generated C softPLC and plugins
 **/
IEC_TIME __CURRENT_TIME;
IEC_BOOL __DEBUG = 0;
unsigned long long __tick;
char *PLC_ID = NULL;


/* Help to quit cleanly when init fail at a certain level */
static int init_level = 0;


/*
 * Initialize variables according to PLC's default values,
 * and then init plugins with that values
 **/
int __init()
{
    init_level = 0;
    __tick = 0;
    config_init__();
    __init_debug();
    return 0;
}


/**
 * No platform specific code for "Generic" target
 **/
void PLC_GetTime(IEC_TIME *CURRENT_TIME){
    //time in second from 1970 year
    char name[] = "sys_tick_counter";
    regs_template_t  regs_template;
    regs_template.name = name;
    if(!p_link_functions->regs_description_get_by_name(&regs_template)){
        regs_access_t reg;
        reg.value.op_u64 = 0;
        reg.flag = regs_template.type;
        u16 address = regs_template.guid & GUID_ADDRESS_MASK;
        if(p_link_functions->regs_get(address,&reg)==0){
            CURRENT_TIME->tv_sec = reg.value.op_u64/1000;
            CURRENT_TIME->tv_nsec = (reg.value.op_u64%1000)*1000000;
        }else{
            p_link_functions->led_error_on(TEST_ERROR_MS);
        }
    }else{
        p_link_functions->led_error_on(TEST_ERROR_MS);
    }
}

static void PLC_timer_notify(void);

void PLC_timer_notify()
{
    PLC_GetTime(&__CURRENT_TIME);
    __tick++;
    config_run__(__tick);
    __publish_debug();
}
/* Variable used to stop plcloop thread */
void* PlcLoop(void * args)
{
    (void)args;
    PLC_timer_notify();
    return NULL;
}
