#include "link_functions.h"
/*for memcpy*/
#include <string.h>
#include <stdio.h>
#include "beremiz_task.h"
#include "os_service.h"
#include "beremiz_regs_description.h"
#include "modbus_master.h"

#define MIN_CONTROLLER_OS_VERSION {0,33,0,0}
#define MAX_MODBUS_MASTER_NODES 6
#define MODBUS_MASTER_NODES 6
link_functions_t * p_link_functions;
static u32 time_duration = 100;
static u32 time_period = 1000;
#define MDB_DINAMIC_ADDRESS_SPACE_SIZE 0
#define MDB_ARRAY_ADDRESS_SPACE_SIZE 0
#define CAN_FEST_ENABLE 0
#define ARCHIVE_ENABLE 0
#define IEC104_BEREMIZ_ENABLE 0

u8 mdb_address_space[MDB_DINAMIC_ADDRESS_SPACE_SIZE];
u8 mdb_array_address_space[MDB_ARRAY_ADDRESS_SPACE_SIZE];
static int add_modbus_address_space(void);
static int add_regs_description(void);
static int check_controller_os_version(void);
static void modbus_terminate(void);
#if CAN_FEST_ENABLE
#include "can_open_dict.h"
extern CO_Data ObjDict_Data;
static int change_can_dict(void * dict);
#endif
#if ARCHIVE_ENABLE
#include "archive_manager.h"
#endif
#if IEC104_BEREMIZ_ENABLE
#include "iec104_beremiz.h"
#endif

osMutexId user_mutex;

TASK_CODE int main(void *arg){
    osEvent event;
    TickType_t next_wake_time;
    u64 common_ticktime_msec;
    uint32_t task_tick;
    uint32_t loop_tick;
    /* Remove compiler warning about unused parameter. */
    u32 size_data,size_bss;
    void * p_link = arg;
    size_data = put_preinit_data();
    size_bss = clear_bss_data();
    if(p_link!=NULL){
        p_link_functions = (link_functions_t *)p_link;
    }else{
        p_link_functions = (link_functions_t *)&_sofi_link_start;/*!<init array if function*/
    }
    if (check_controller_os_version() <= 0){
        p_link_functions->printf("version mismatch, don't suitable os controller version");
        p_link_functions->printf("please update controller os");
        char name[] = "user_task_state";
        regs_template_t regs_template;
        regs_template.name = name;
        if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
            u16 temp = (u16)INTERNAL_FLASH_TASK_OS_VERSION_BELOW_THEN_MINIMAL_TASK_VERSION;
            memcpy(regs_template.p_value,&temp,sizeof(u16));            
        }
        p_link_functions->printf("terminate self ");
        p_link_functions->os_thread_terminate(p_link_functions->os_thread_get_id());
    }else{
        char name[] = "user_task_state";
        regs_template_t regs_template;
        regs_template.name = name;
        if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
            u16 temp = (u16)INTERNAL_FLASH_TASK_READY;
            memcpy(regs_template.p_value,&temp,sizeof(u16));
        }
        p_link_functions->printf("ready to start");
    }
    p_link_functions->printf("preinit data size %lu",size_data);
    p_link_functions->printf("bss section size %lu",size_bss);
    p_link_functions->printf("sended address %p",p_link);
    /* Add regs to sofi.vars */
    if(add_regs_description()>0){
        p_link_functions->printf("add user regs description table with %u",BEREMIZ_REGS_VAR_NUM);
    }
    /* Suspend self */
    p_link_functions->os_thread_suspend(p_link_functions->os_thread_get_id());

    /* Initialise xNextWakeTime - this only needs to be done once. */
    next_wake_time = p_link_functions->os_kernel_sys_tick();
    task_tick = 0;
    loop_tick = 0;
    common_ticktime__ = 1000000;//rewrite from SNEMA
    common_ticktime_msec = common_ticktime__/1000000;
    common_ticktime_msec = common_ticktime_msec==0?1:common_ticktime_msec;
    p_link_functions->printf("common_ticktime_msec %llu",common_ticktime_msec);
    p_link_functions->printf("common_ticktime %llu",common_ticktime__);
    p_link_functions->led_user_on(100);
    if(add_modbus_address_space()!=0){
        p_link_functions->printf("error on enhancmt modbus space");
        p_link_functions->led_error_on(INIT_ERROR_FATAL);
    }
    /*enhancement dinamical self address space at controller end */
    /*make modbus master thread if need it start */
    if(start_modbus_master()!=0){
        p_link_functions->printf("error while modbus master start");
        p_link_functions->led_error_on(INIT_ERROR_FATAL);
    }
#if CAN_FEST_ENABLE
    if(change_can_dict(&ObjDict_Data)<0){
    	p_link_functions->printf("error while init can fest dict");
        p_link_functions->led_error_on(INIT_ERROR_FATAL);
    }else{
        p_link_functions->printf("can dict moved to master value");
    }
#endif
#if ARCHIVE_ENABLE
    int archive_manager_number;
    archive_manager_number = archive_manager_start();
    if (archive_manager_number < 0){
    	p_link_functions->printf("error while init archives manager");
        p_link_functions->led_error_on(INIT_ERROR_FATAL);
    }else{
        p_link_functions->printf("archive managers number - %i",archive_manager_number);
    }
#endif
#if IEC104_BEREMIZ_ENABLE
    if (iec104_beremiz_init()!=0){
    	p_link_functions->printf("error while init iec104 variables");
        p_link_functions->led_error_on(INIT_ERROR_FATAL);
    }
#endif
    user_mutex = p_link_functions->get_user_regs_access_mutex();
    if(user_mutex==NULL){
        p_link_functions->printf("user_mutex error");
        p_link_functions->led_error_on(TEST_ERROR_MS);
    }
    /*make modbus master thread if need it end */
    __init();
    modbus_master_init_structure();
    while(1){
        p_link_functions->refresh_watchdog();
        p_link_functions->os_delay_until( &next_wake_time, 1);
        if(((task_tick++)%common_ticktime_msec)==0u){
            PlcLoop(NULL);
            loop_tick++;
#if ARCHIVE_ENABLE
            if(archive_manager_control_check() < 0){
                p_link_functions->led_error_on(INIT_ERROR_FATAL);
            }
#endif
        }
        if(((task_tick)%time_period)==0u){
            p_link_functions->led_user_on((u16)time_duration);
        }
        if(loop_tick==10 ||
            ((common_ticktime__ > 1000000000)&&(loop_tick==1))){//set state only after several times passes
            regs_access_t reg;
            reg.flag = U16_REGS_FLAG;
            char name[] = "user_task_state";
            regs_template_t regs_template;
            regs_template.name = name;
            if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
                u16 temp;
                memcpy(&temp,regs_template.p_value,sizeof(u16));
                temp &= ~(u16)INTERNAL_FLASH_TASK_OS_VERSION_BELOW_THEN_MINIMAL_TASK_VERSION;
                temp |= (u16)INTERNAL_FLASH_TASK_RUNNING;
                memcpy(regs_template.p_value,&temp,sizeof(u16));
                p_link_functions->write_reg_to_bkram(regs_template.p_value);
                p_link_functions->os_thread_yield();
            }
        }
        event = p_link_functions->os_signal_wait(0,0);
        if((event.status == osEventSignal)&&
           ((event.value.signals == STOP_CHILD_PROCCES)||(event.value.signals == STOP_PROCESS_FOR_CHANGE_OS))){
            char name[] = "user_task_state";
            modbus_master_deinit();
            modbus_terminate();
            if(event.value.signals == STOP_CHILD_PROCCES){
                regs_template_t regs_template;
                regs_template.name = name;
                if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
                    u16 temp;
                    memcpy(&temp,regs_template.p_value,sizeof(u16));
                    temp &= ~(u16)INTERNAL_FLASH_TASK_RUNNING;
                    temp &= ~(u16)INTERNAL_FLASH_TASK_READY;
                    memcpy(regs_template.p_value,&temp,sizeof(u16));
                    p_link_functions->write_reg_to_bkram(regs_template.p_value);
                    p_link_functions->os_thread_yield();
                }
            }

#if CAN_FEST_ENABLE
            if(change_can_dict(NULL)>=0){//to default value
                p_link_functions->printf("can dict moved to default value");
            }else{
                p_link_functions->printf("error while moving can fest dict to default");
            }
#endif
#if ARCHIVE_ENABLE
            int archive_manager_number;
            archive_manager_number = archive_manager_stop();
            if (archive_manager_number < 0){
            	p_link_functions->printf("error while stop archives manager");
                p_link_functions->led_error_on(INIT_ERROR_FATAL);
            }
#endif

            p_link_functions->printf("terminate self ");
            p_link_functions->os_thread_terminate(p_link_functions->os_thread_get_id());
        }
    }
}
    /*get controller os version*/

/**
*
*@return 0 - if version is't suitable,1 if suitable, less then zero if error
*/
static int check_controller_os_version(){
    int res = 0;
    char name[] = "os_version";
    u8 min_version[OS_VERSION_SIZE] = MIN_CONTROLLER_OS_VERSION;
    regs_template_t regs_template;
    regs_template.name = name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)==0){
        if (min_version[0] < regs_template.p_value[0]){
            res = 1;
        }else if(min_version[0] == regs_template.p_value[0]){
            if (min_version[1] < regs_template.p_value[1]){
                res = 1;
            }else if(min_version[1] == regs_template.p_value[1]){
                if (min_version[2] < regs_template.p_value[2]){
                    res = 1;
                }else if(min_version[2] == regs_template.p_value[2]){
                    if (min_version[3] <= regs_template.p_value[3]){
                        res = 1;
                    }
                }
            }
        }
    }
    return res;
}
static int add_modbus_address_space(){
    int result;
    result = 0;
    osPoolId mdb_enh_pool =NULL;
    u32 len1 = MDB_DINAMIC_ADDRESS_SPACE_SIZE%2? MDB_DINAMIC_ADDRESS_SPACE_SIZE/2+1:MDB_DINAMIC_ADDRESS_SPACE_SIZE/2;
    u32 len2 = MDB_ARRAY_ADDRESS_SPACE_SIZE%2? MDB_ARRAY_ADDRESS_SPACE_SIZE/2+1:MDB_ARRAY_ADDRESS_SPACE_SIZE/2;
    if(len1){
        mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ADDRESS_SPACE_START,mdb_address_space,&len1);
    }else{
        len1 =1;
    }
    if(len2){
        mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ARRAY_ADDRESS_SPACE_START,mdb_array_address_space,&len2);
    }else{
        len2 =1;
    }

    if((len1==0) || (len2==0)){
        p_link_functions->printf("pool for enhancment modbus space is full");
        //clear all space
        if(mdb_enh_pool !=NULL){
            for(u8 i=0;i<mdb_enh_pool->pool_sz;i++){
                dinamic_address_space_t* pointer;
                pointer = p_link_functions->os_pool_get_by_index(mdb_enh_pool,i);
                if(pointer!=NULL){
                    if(((u32)pointer->data >= (u32)(&_ram_start_address)) && \
                            ((u32)pointer->data < ((u32)(&_ram_start_address) + (u32)(&_task_ram_size)))){
                        p_link_functions->os_pool_free(mdb_enh_pool,pointer);
                    }
                }
            }
            len1 = MDB_DINAMIC_ADDRESS_SPACE_SIZE%2? MDB_DINAMIC_ADDRESS_SPACE_SIZE/2+1:MDB_DINAMIC_ADDRESS_SPACE_SIZE/2;
            len2 = MDB_ARRAY_ADDRESS_SPACE_SIZE%2? MDB_ARRAY_ADDRESS_SPACE_SIZE/2+1:MDB_ARRAY_ADDRESS_SPACE_SIZE/2;
            if(len1){
                mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ADDRESS_SPACE_START,mdb_address_space,&len1);
            }else{
                len1 =1;
            }
            if(len2){
                mdb_enh_pool = p_link_functions->modbus_bind_address_space(BEREMIZ_ARRAY_ADDRESS_SPACE_START,mdb_array_address_space,&len2);
            }else{
                len2 =1;
            }
            if((len1==0) || (len2==0)){
                result =-1;
            }
        }else{
            result =-1;
        }
    }

    return result;
}

static void modbus_terminate(void){
    static osThreadId modbus_master_id;
    for (u8 i=0;i<MAX_MODBUS_MASTER_NODES && i< MODBUS_MASTER_NODES;i++){
        char MODBUS_THREAD_NAME[] = "modbus_master_1";
        MODBUS_THREAD_NAME[14] = 0x30 + i;
        modbus_master_id = p_link_functions->os_thread_get_id_by_name(MODBUS_THREAD_NAME);
        if(modbus_master_id!=NULL){
            p_link_functions->os_thread_terminate(modbus_master_id);
        }
    }
}

static int add_regs_description(void){
	int result = 0;
	if(BEREMIZ_REGS_VAR_NUM){
		if(p_link_functions->regs_description_add_user_vars(beremiz_regs_description,(u16)BEREMIZ_REGS_VAR_NUM)==0){
			result = 1;
		}
	}
	return result;
}
#if CAN_FEST_ENABLE
/** if dict == null, set to default
* return less zero vslue if error occured
*/
static int change_can_dict(void * dict){
    int result = 0;
    static osThreadId can_task_id = NULL;
    char CAN_TASK_NAME[] = "can_task";
    can_task_id = p_link_functions->os_thread_get_id_by_name(CAN_TASK_NAME);
    if(can_task_id != NULL){
        if(dict!=NULL){
            while(p_link_functions->os_signal_set(can_task_id,(s32)dict) < 0){p_link_functions->os_delay(1);}
        }else{
            while(p_link_functions->os_signal_set(can_task_id,(s32)CAN_DICT_DEFAULT) < 0){p_link_functions->os_delay(1);}
        }
    }else{
        result = -1;
    }
    return result;
}
#endif
