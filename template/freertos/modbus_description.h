/* File generated by Beremiz (PlugGenerate_C method of modbus Plugin instance) */

/*
 * Copyright (c) 2016 Mario de Sousa (msousa@fe.up.pt)
 *
 * This file is part of the Modbus library for Beremiz and matiec.
 *
 * This Modbus library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this Modbus library.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This code is made available on the understanding that it will not be
 * used in safety-critical situations without a full and competent review.
 */

/* The total number of nodes, needed to support _all_ instances of the modbus plugin */
#define TOTAL_TCPNODE_COUNT       10
#define TOTAL_RTUNODE_COUNT       1
#define TOTAL_ASCNODE_COUNT       0

/* Values for instance 1 of the modbus plugin */
#define MAX_NUMBER_OF_TCPCLIENTS  10

#define NUMBER_OF_TCPSERVER_NODES 0
#define NUMBER_OF_TCPCLIENT_NODES 0
#define NUMBER_OF_TCPCLIENT_REQTS 0

#define NUMBER_OF_RTUSERVER_NODES 0
#define NUMBER_OF_RTUCLIENT_NODES 1
#define NUMBER_OF_RTUCLIENT_REQTS 2

#define NUMBER_OF_ASCIISERVER_NODES 0
#define NUMBER_OF_ASCIICLIENT_NODES 0
#define NUMBER_OF_ASCIICLIENT_REQTS 0

#define NUMBER_OF_SERVER_NODES (NUMBER_OF_TCPSERVER_NODES + \
                                NUMBER_OF_RTUSERVER_NODES + \
                                NUMBER_OF_ASCIISERVER_NODES)

#define NUMBER_OF_CLIENT_NODES (NUMBER_OF_TCPCLIENT_NODES + \
                                NUMBER_OF_RTUCLIENT_NODES + \
                                NUMBER_OF_ASCIICLIENT_NODES)

#define NUMBER_OF_CLIENT_REQTS (NUMBER_OF_TCPCLIENT_REQTS + \
                                NUMBER_OF_RTUCLIENT_REQTS + \
                                NUMBER_OF_ASCIICLIENT_REQTS)

#define MAX_READ_BITS 254
#define MAX_WORD_NUM 127
#define MAX_PACKET_LEN (MAX_WORD_NUM*2 + 7)

static client_node_t		client_nodes[NUMBER_OF_CLIENT_NODES] = {
/*node 1.0*/
{"1.0"/*location*/, {naf_rtu, {.rtu = {RS_232_UART, 115200 /*baud*/, 2 /*parity*/, 8 /*data bits*/, 1, 0 /* ignore echo */}}}/*node_addr_t*/, -1 /* mb_nd */, 0 /* init_state */, 200 /* communication period */,0,NULL}
};

static client_request_t	client_requests[NUMBER_OF_CLIENT_REQTS] = {
/*request 1_0_0*/

{"1_0_0"/*location*/,RS_232_UART/*channel*/, 0/*client_node_id*/, 3/*slave_id*/, req_input/*req_type*/, 3/*mb_function*/, 0 /*first reg address*/,
 2/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */, 100/* timeout */,
NULL, NULL},

/*request 1_0_1*/
{"1_0_1",RS_232_UART/*channel*/, 0, 5, req_input, 3, 33 , 6,
DEF_REQ_SEND_RETRIES, 0 /* error_code */, 0 /* prev_code */, 180/* timeout */,
NULL, NULL}
};

u16 * __MW1_0_0_0;
static u16 plcv_buffer_req1[] = {0,0};
static u16 plcv_buffer_req2[] = {0,0,0,0,0,0};
static u16 com_buffer_req1[] = {0,0};
static u16 com_buffer_req2[] = {0,0,0,0,0,0};

/*initialization following all parameters given by user in application*/

/*******************/
/*located variables*/
/*******************/


