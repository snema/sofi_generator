
#if defined __cplusplus
extern "C" {
#endif
#include "plc_main.h"
#include "link_functions.h"
extern uint32_t _sofi_link_start;
extern uint32_t _ram_start_address;
extern uint32_t _task_ram_size;

extern link_functions_t * p_link_functions;  
int main(void *arg) TASK_CODE;
#if defined __cplusplus
}
#endif
