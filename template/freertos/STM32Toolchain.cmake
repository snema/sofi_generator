set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(MCU_ARCH cortex-m7)
set(MCU_FLOAT_ABI hard)
set(MCU_FPU fpv5-d16)
set(FLASH_TYPE internal)
if (${FLASH_TYPE} STREQUAL internal)
    set(MCU_LINKER_SCRIPT ldscript/task_internal_flash.ld)
else()
    set(MCU_LINKER_SCRIPT ldscript/task_external_flash.ld)
endif()



set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)

set(COMMON_FLAGS "-mcpu=${MCU_ARCH} -mthumb -mthumb-interwork -mfloat-abi=${MCU_FLOAT_ABI} -ffunction-sections -fdata-sections -g -Os -fno-common -fmessage-length=0 -mfpu=${MCU_FPU} -Xlinker -Map=output.map")

set(COMPILER_DIRECT C:/Beremiz/gcc_6_3_1/bin)
set(CMAKE_C_COMPILER ${COMPILER_DIRECT}arm-none-eabi-gcc.exe)
set(CMAKE_CXX_COMPILER ${COMPILER_DIRECT}arm-none-eabi-g++.exe)
set(CMAKE_ASM_COMPILER ${COMPILER_DIRECT}arm-none-eabi-g++.exe)
set(CMAKE_OBJCOPY ${COMPILER_DIRECT}arm-none-eabi-objcopy.exe CACHE INTERNAL "GCC TOOLCHAIN OBJCOPY")
set(CMAKE_OBJDUMP ${COMPILER_DIRECT}arm-none-eabi-objdump.exe CACHE INTERNAL "GCC TOOLCHAIN OBJDUMP")
set(CMAKE_SIZE ${COMPILER_DIRECT}arm-none-eabi-size.exe CACHE INTERNAL "GCC TOOLCHAIN SIZE")

set(CMAKE_CXX_FLAGS "${COMMON_FLAGS} -std=c++11")
set(CMAKE_C_FLAGS "${COMMON_FLAGS} -std=gnu99")
#set(CMAKE_EXE_LINKER_FLAGS "-Wl,-gc-sections -T${MCU_LINKER_SCRIPT}")
