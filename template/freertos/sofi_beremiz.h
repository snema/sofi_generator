#include "beremiz.h"

#include "matiec/accessor.h"
#include "matiec/iec_std_lib.h"

extern BOOL FIRST_CYCLE;

typedef struct{
    char * task_name;
    u32 task_period_ms;
} task_description_t;

// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(DINT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_UDINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(DINT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_UDINT;
// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(INT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_UINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(INT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_UINT;
// FUNCTION_BLOCK READ_PARAM_TEMPLATE
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(BOOL,CHECK)
  __DECLARE_VAR(SINT,VALUE)
  // FB private variables - TEMP, private and located variables
} READ_PARAM_USINT;
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ENABLE)
  __DECLARE_VAR(DINT,ADDRESS)
  __DECLARE_VAR(SINT,VALUE)
  __DECLARE_VAR(BOOL,CHECK)
  // FB private variables - TEMP, private and located variables
} SET_PARAM_USINT;

// FUNCTION_BLOCK READ_DI
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UDINT,DI_OUT)
  // FB private variables - TEMP, private and located variables
} READ_DI;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(ULINT,DI_CNT_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_DI_CNT;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(REAL,DI_FREQ_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_DI_FREQ;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,AI_STATE_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_AI_STATE;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,AI_NUMBER)
  __DECLARE_VAR(UINT,AI_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_AI;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,AI_NUMBER)
  __DECLARE_VAR(REAL,AI_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_AI_REAL;


// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,DO_SC_FLAG)
  __DECLARE_VAR(USINT,DO_SC_EN)
  // FB private variables - TEMP, private and located variables
} READ_DO_SC;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,DO_OUT)
  // FB private variables - TEMP, private and located variables
} READ_DO;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,RESET_NUM)
  __DECLARE_VAR(UINT,LAST_RESET)
  // FB private variables - TEMP, private and located variables
} READ_RESET;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,V_PWR)
  __DECLARE_VAR(REAL,V_BAT)
  // FB private variables - TEMP, private and located variables
} READ_PWR;

// FUNCTION_BLOCK READ_INTERNAL_TEMP
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,INTERNAL_TEMP_OUT)
  // FB private variables - TEMP, private and located variables
} READ_INTERNAL_TEMP;

// FUNCTION_BLOCK READ_TEMP
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,MCU_TEMP)
  __DECLARE_VAR(REAL,ADC_TEMP)
  // FB private variables - TEMP, private and located variables
} READ_TEMP;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(ULINT,SYS_TICK_COUNTER_VALUE)
  // FB private variables - TEMP, private and located variables
} READ_SYS_TICK_COUNTER;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,MDB_ADDR)
  // FB private variables - TEMP, private and located variables
} WRITE_MDB_ADDRESS;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,MESO_UART)
  __DECLARE_VAR(UINT,SET_RS_485_2)
  __DECLARE_VAR(UINT,SET_RS_232)
  __DECLARE_VAR(UINT,SET_RS_485_1)
  __DECLARE_VAR(UINT,SET_RS_485_IMMO)
  __DECLARE_VAR(UINT,SET_HART)
  // FB private variables - TEMP, private and located variables
} WRITE_UART_SETS;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,CH_NUMBER)
  __DECLARE_VAR(UDINT,CHANNEL_TIMEOUT)
  // FB private variables - TEMP, private and located variables
} WRITE_CH_TIMEOUT;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(UINT,DI_NOISE_FLTR_VALUE_10US)
  // FB private variables - TEMP, private and located variables
} WRITE_DI_NOISE_FLTR_10US;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(UDINT,DI_PULSELESS_VALUE)
  // FB private variables - TEMP, private and located variables
} WRITE_DI_PULSELESS;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DI_NUMBER)
  __DECLARE_VAR(UINT,DI_MODE_VALUE)
  // FB private variables - TEMP, private and located variables
} WRITE_DI_MODE;

// FUNCTION_BLOCK WRITE_DO
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DO_VALUE)
  __DECLARE_VAR(UINT,DO_MASK)
  // FB private variables - TEMP, private and located variables
} WRITE_DO;

// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DO_SC_FLAG)
  __DECLARE_VAR(UINT,DO_SC_EN)
  // FB private variables - TEMP, private and located variables
} WRITE_DO_SC;

// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,DO_PWM_FREQ)
  // FB private variables - TEMP, private and located variables
} WRITE_DO_PWM_FREQ;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,DO_NUMBER)
  __DECLARE_VAR(UINT,DO_PWM_DUTY)
  __DECLARE_VAR(BOOL,DO_PWM_RUN)
  // FB private variables - TEMP, private and located variables
} WRITE_DO_PWM_CTRL;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,HOUR_TIME)
  __DECLARE_VAR(USINT,MINUTE_TIME)
  __DECLARE_VAR(USINT,SEC_TIME)
  __DECLARE_VAR(USINT,SUB_SEC_TIME)
  __DECLARE_VAR(USINT,WEEK_DAY_TIME)
  __DECLARE_VAR(USINT,MONTH_TIME)
  __DECLARE_VAR(USINT,DATE_TIME)
  __DECLARE_VAR(USINT,YEAR_TIME)
  __DECLARE_VAR(UINT,YEAR_DAY_TIME)
  // FB private variables - TEMP, private and located variables
} STRUCT_REAL_TIME;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,SEC_IN_MIN)
  __DECLARE_VAR(USINT,MINUTE_IN_HOUR)
  __DECLARE_VAR(USINT,HOUR_IN_DAY)
  __DECLARE_VAR(USINT,DATE_IN_MONTH)
  __DECLARE_VAR(USINT,MONTH_IN_YEAR)
  __DECLARE_VAR(USINT,YEAR_SINCE_2000)
  __DECLARE_VAR(USINT,SEC_IN_MIN_WRITED)
  __DECLARE_VAR(USINT,MINUTE_IN_HOUR_WRITED)
  __DECLARE_VAR(USINT,HOUR_IN_DAY_WRITED)
  __DECLARE_VAR(USINT,DATE_IN_MONTH_WRITED)
  __DECLARE_VAR(USINT,MONTH_IN_YEAR_WRITED)
  __DECLARE_VAR(USINT,YEAR_SINCE_2000_WRITED)
  // FB private variables - TEMP, private and located variables
} WRITE_STRUCT_TIME;

// FUNCTION_BLOCK UNIX_TIME
// Data part
typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UDINT,UNIX_TIME_WRITE)
  __DECLARE_VAR(UDINT,UNIX_TIME_READ)
  __DECLARE_VAR(UDINT,UNIX_TIME_WRITED)
} UNIX_TIME;

// FUNCTION_BLOCK PARSING_UINT
// Data part
typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,IN_VAL)
  __DECLARE_VAR(BOOL,BIT_1)
  __DECLARE_VAR(BOOL,BIT_2)
  __DECLARE_VAR(BOOL,BIT_3)
  __DECLARE_VAR(BOOL,BIT_4)
  __DECLARE_VAR(BOOL,BIT_5)
  __DECLARE_VAR(BOOL,BIT_6)
  __DECLARE_VAR(BOOL,BIT_7)
  __DECLARE_VAR(BOOL,BIT_8)
  __DECLARE_VAR(BOOL,BIT_9)
  __DECLARE_VAR(BOOL,BIT_10)
  __DECLARE_VAR(BOOL,BIT_11)
  __DECLARE_VAR(BOOL,BIT_12)
  __DECLARE_VAR(BOOL,BIT_13)
  __DECLARE_VAR(BOOL,BIT_14)
  __DECLARE_VAR(BOOL,BIT_15)
  __DECLARE_VAR(BOOL,BIT_16)
} PARSING_UINT;

// FUNCTION_BLOCK PARSING_USINT
// Data part
typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(USINT,IN_VAL)
  __DECLARE_VAR(BOOL,BIT_1)
  __DECLARE_VAR(BOOL,BIT_2)
  __DECLARE_VAR(BOOL,BIT_3)
  __DECLARE_VAR(BOOL,BIT_4)
  __DECLARE_VAR(BOOL,BIT_5)
  __DECLARE_VAR(BOOL,BIT_6)
  __DECLARE_VAR(BOOL,BIT_7)
  __DECLARE_VAR(BOOL,BIT_8) 
} PARSING_USINT;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,XIN)
  __DECLARE_VAR(UINT,X1)
  __DECLARE_VAR(UINT,X2)
  __DECLARE_VAR(REAL,Y1)
  __DECLARE_VAR(REAL,Y2)
  __DECLARE_VAR(REAL,Y_OUT)
} SCALE_UINT_TO_FLOAT;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(DWORD,IN_VAL)
  __DECLARE_VAR(REAL,OUT_VAL)
} DWORD_TO_FLOAT;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(WORD,WORDH)
  __DECLARE_VAR(WORD,WORDL)
  __DECLARE_VAR(REAL,OUT_VAL)
} WORDS_TO_FLOAT;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,VLT_V)
  __DECLARE_VAR(REAL,CUR_MA)
  __DECLARE_VAR(UINT,SENSOR_TYPE)
  __DECLARE_VAR(REAL,R_0)
  __DECLARE_VAR(REAL,R_WIRE)
  __DECLARE_VAR(REAL,R_SENSE)
  __DECLARE_VAR(REAL,T_SENSE)
} RES_TEMP_SENSOR;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(WORD,VALUE)
  __DECLARE_VAR(UINT,LOCATION0)
  __DECLARE_VAR(UINT,LOCATION1)
  __DECLARE_VAR(UINT,LOCATION2)
  __DECLARE_VAR(BOOL,LOCATION_WRITED)
} WRITE_AREA_LOCATION;
typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,LOCATION0)
  __DECLARE_VAR(UINT,LOCATION1)
  __DECLARE_VAR(UINT,LOCATION2)
  __DECLARE_VAR(WORD,VALUE)
  __DECLARE_VAR(BOOL,LOCATION_READED)
} READ_AREA_LOCATION;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(WORD,VALUE)
  __DECLARE_VAR(UINT,LOCATION0)
  __DECLARE_VAR(UINT,LOCATION1)
  __DECLARE_VAR(UINT,LOCATION2)
  __DECLARE_VAR(UINT,LOCATION3)
  __DECLARE_VAR(BOOL,LOCATION_WRITED)
} WRITE_REQUEST_LOCATION;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,LOCATION0)
  __DECLARE_VAR(UINT,LOCATION1)
  __DECLARE_VAR(UINT,LOCATION2)
  __DECLARE_VAR(UINT,LOCATION3)
  __DECLARE_VAR(WORD,VALUE)
  __DECLARE_VAR(BOOL,LOCATION_READED)
} READ_REQUEST_LOCATION;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,IN_REAL)
  __DECLARE_VAR(USINT,DECIMAL_POINT)
  __DECLARE_VAR(REAL,OUT_REAL)
} ROUND_REAL;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,IN)
  __DECLARE_VAR(ULINT,PT)
  __DECLARE_VAR(ULINT,ET)
  __DECLARE_VAR(ULINT,CMS)
  __DECLARE_VAR(ULINT,SMS)
  __DECLARE_VAR(BOOL,NIN)
  __DECLARE_VAR(BOOL,Q)
  __DECLARE_VAR(BOOL,TT)
  READ_SYS_TICK_COUNTER R_SYS;
} UTON;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,IN)
  __DECLARE_VAR(ULINT,PT)
  __DECLARE_VAR(ULINT,ET)
  __DECLARE_VAR(ULINT,CMS)
  __DECLARE_VAR(ULINT,SMS)
  __DECLARE_VAR(BOOL,NIN)
  __DECLARE_VAR(BOOL,Q)
  __DECLARE_VAR(BOOL,TT)
  READ_SYS_TICK_COUNTER R_SYS;
} UTOF;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(WORD,WORD_IN)
  __DECLARE_VAR(BOOL,B_OUT0)
  __DECLARE_VAR(BOOL,B_OUT1)
  __DECLARE_VAR(BOOL,B_OUT2)
  __DECLARE_VAR(BOOL,B_OUT3)
  __DECLARE_VAR(BOOL,B_OUT4)
  __DECLARE_VAR(BOOL,B_OUT5)
  __DECLARE_VAR(BOOL,B_OUT6)
  __DECLARE_VAR(BOOL,B_OUT7)
  __DECLARE_VAR(BOOL,B_OUT8)
  __DECLARE_VAR(BOOL,B_OUT9)
  __DECLARE_VAR(BOOL,B_OUT10)
  __DECLARE_VAR(BOOL,B_OUT11)
  __DECLARE_VAR(BOOL,B_OUT12)
  __DECLARE_VAR(BOOL,B_OUT13)
  __DECLARE_VAR(BOOL,B_OUT14)
  __DECLARE_VAR(BOOL,B_OUT15)
} WORD_ON_BOOL;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,B_IN0)
  __DECLARE_VAR(BOOL,B_IN1)
  __DECLARE_VAR(BOOL,B_IN2)
  __DECLARE_VAR(BOOL,B_IN3)
  __DECLARE_VAR(BOOL,B_IN4)
  __DECLARE_VAR(BOOL,B_IN5)
  __DECLARE_VAR(BOOL,B_IN6)
  __DECLARE_VAR(BOOL,B_IN7)
  __DECLARE_VAR(BOOL,B_IN8)
  __DECLARE_VAR(BOOL,B_IN9)
  __DECLARE_VAR(BOOL,B_IN10)
  __DECLARE_VAR(BOOL,B_IN11)
  __DECLARE_VAR(BOOL,B_IN12)
  __DECLARE_VAR(BOOL,B_IN13)
  __DECLARE_VAR(BOOL,B_IN14)
  __DECLARE_VAR(BOOL,B_IN15)
  __DECLARE_VAR(WORD,WORD_OUT)
} BOOL_ON_WORD;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(DWORD,IN)
  __DECLARE_VAR(USINT,BIT_N)
  __DECLARE_VAR(BOOL,BIT_VALUE)
  __DECLARE_VAR(DWORD,OUT)
} SETBIT;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(DWORD,IN)
  __DECLARE_VAR(USINT,BIT_N)
  __DECLARE_VAR(BOOL,OUT)
} GETBIT;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,AI_CH)
  __DECLARE_VAR(REAL,VAL_FORCE)
  __DECLARE_VAR(REAL,EUMAX)
  __DECLARE_VAR(REAL,EUMIN)
  __DECLARE_VAR(REAL,SP_DB)
  __DECLARE_VAR(REAL,SP_K)
  __DECLARE_VAR(REAL,SP_HH)
  __DECLARE_VAR(REAL,SP_H)
  __DECLARE_VAR(REAL,SP_L)
  __DECLARE_VAR(REAL,SP_LL)
  __DECLARE_VAR(UINT,AI_TIME)
  __DECLARE_VAR(WORD,AI_REG)
  __DECLARE_VAR(REAL,AI_PV)
  __DECLARE_VAR(REAL,AI_SR)
  __DECLARE_VAR(BOOL,EN_HH)
  __DECLARE_VAR(BOOL,EN_H)
  __DECLARE_VAR(BOOL,EN_L)
  __DECLARE_VAR(BOOL,EN_LL)
  __DECLARE_VAR(BOOL,EN_FORCE)
  __DECLARE_VAR(BOOL,EN_ARCH)
  UTON UT;
  WORD_ON_BOOL WB;
} AI_HANDLER;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,HART_CH_MASK)
} HART_CH_SEL;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,OUT)
  // FB private variables - TEMP, private and located variables
} FIRST_SCAN;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(REAL,PV)
  __DECLARE_VAR(REAL,SP)
  __DECLARE_VAR(REAL,TR)
  __DECLARE_VAR(BOOL,DIRECT)
  __DECLARE_VAR(BOOL,ON_OFF)
  __DECLARE_VAR(BOOL,HALT)
  __DECLARE_VAR(BOOL,RESET)
  __DECLARE_VAR(REAL,KP)
  __DECLARE_VAR(REAL,KI)
  __DECLARE_VAR(REAL,KD)
  __DECLARE_VAR(REAL,SP_TUBE)
  __DECLARE_VAR(REAL,VSPMAX)
  __DECLARE_VAR(REAL,PV_MAX)
  __DECLARE_VAR(REAL,PV_MIN)
  __DECLARE_VAR(REAL,PV_NOISE)
  __DECLARE_VAR(REAL,PV_OVER)
  __DECLARE_VAR(USINT,IN_MODE)
  __DECLARE_VAR(USINT,PID_MODE)
  __DECLARE_VAR(USINT,OUT_MODE)
  __DECLARE_VAR(BOOL,STATUS)
  __DECLARE_VAR(BOOL,ALARM)
  __DECLARE_VAR(REAL,Y)
  __DECLARE_VAR(REAL,ERR)
  __DECLARE_VAR(BOOL,PV_RANGE_ERR)
  __DECLARE_VAR(BOOL,PV_ERR)
  __DECLARE_VAR(BOOL,SP_ERR)
  __DECLARE_VAR(BOOL,PID_ERR)
  __DECLARE_VAR(BOOL,TR_ERR)
  __DECLARE_VAR(BOOL,SP_TUBE_ERR)
  __DECLARE_VAR(BOOL,VSPMAX_ERR)
  __DECLARE_VAR(BOOL,Y_ERR)
  __DECLARE_VAR(BOOL,PV_OVER_ERR)
  __DECLARE_VAR(USINT,MODE)
  __DECLARE_VAR(REAL,PVMAX)
  __DECLARE_VAR(REAL,PVMIN)
  __DECLARE_VAR(REAL,PVRANGE)
  __DECLARE_VAR(BOOL,VSPMAX_ON)
  __DECLARE_VAR(BOOL,SP_TUBE_ON)
  __DECLARE_VAR(REAL,SP_QUANT)
  __DECLARE_VAR(REAL,Y_QUANT)
  __DECLARE_VAR(REAL,YMAX)
  __DECLARE_VAR(REAL,YMIN)
  __DECLARE_VAR(REAL,YRANGE)
  __DECLARE_VAR(BOOL,ALARM_TR)
  __DECLARE_VAR(BOOL,ALARM_PV)
  __DECLARE_VAR(REAL,ERR1)
  __DECLARE_VAR(REAL,ERR2)
  __DECLARE_VAR(REAL,DERR1)
  __DECLARE_VAR(REAL,DERR2)
  __DECLARE_VAR(USINT,MODE_PREV)

} PID_FB;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(WORD,IN)
  __DECLARE_VAR(USINT,BIT_N)
  __DECLARE_VAR(BOOL,BIT_VALUE)
  __DECLARE_VAR(WORD,OUT)
} SETBIT_WORD;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(WORD,IN)
  __DECLARE_VAR(USINT,BIT_N)
  __DECLARE_VAR(BOOL,OUT)
} GETBIT_WORD;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(USINT,ADDR)
  __DECLARE_VAR(BOOL,CONN_STATE)
  // FB private variables - TEMP, private and located variables
} MODULE_STATE;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(UINT,CHANNEL)
  __DECLARE_VAR(REAL,CURRENT)
  __DECLARE_VAR(REAL,PV)
  __DECLARE_VAR(REAL,SV)
  __DECLARE_VAR(REAL,TV)
  __DECLARE_VAR(REAL,FV)
} READ_HART_CH;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ON_OFF)
  __DECLARE_VAR(BOOL,MAN_MODE)
  __DECLARE_VAR(REAL,SAFE)
  __DECLARE_VAR(REAL,TRACK)
  __DECLARE_VAR(REAL,STATE)
  __DECLARE_VAR(REAL,TR_MAX)
  __DECLARE_VAR(REAL,TR_MIN)
  __DECLARE_VAR(BOOL,STATUS)
  __DECLARE_VAR(BOOL,ALARM)
  __DECLARE_VAR(BOOL,DIRECT)
  __DECLARE_VAR(REAL,TR)
  __DECLARE_VAR(BOOL,TRANGE_ERR)
  __DECLARE_VAR(BOOL,SAFE_ERR)
  __DECLARE_VAR(BOOL,TRACK_ERR)
  __DECLARE_VAR(BOOL,STATE_ERR)
} SERVOCONTROL_FB;

typedef struct {
  // FB Interface - IN, OUT, IN_OUT variables
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,RUN)
  __DECLARE_VAR(BOOL,DI_IN)
  __DECLARE_VAR(UDINT,T_RTLY)
  __DECLARE_VAR(UDINT,CLK_RTLY)
  __DECLARE_VAR(UDINT,CLK_ARCH)
  __DECLARE_VAR(BOOL,DI_OUT)
  __DECLARE_VAR(BOOL,SAVE)
  __DECLARE_VAR(WORD,ARCH_WORD)
  __DECLARE_VAR(BOOL,RTLY)

  // FB private variables - TEMP, private and located variables
  FIRST_SCAN FS;
  __DECLARE_VAR(BOOL,FS_YES)
  __DECLARE_VAR(BOOL,SAVE_NEW)
  __DECLARE_VAR(BOOL,OUT_OLD)
  __DECLARE_VAR(UDINT,T_ARCH)
  __DECLARE_VAR(UDINT,CNT_NO_RTLY)
  __DECLARE_VAR(UINT,CNT_CICL_ARCH)
  __DECLARE_VAR(WORD,WORD_MASK)
  __DECLARE_VAR(WORD,WORD_ARCH)
  __DECLARE_VAR(UDINT,CNT_UP)
  __DECLARE_VAR(UDINT,CNT_DOWN)
  __DECLARE_VAR(UDINT,CNT_UP_DOWN)
  __DECLARE_VAR(UINT,N)
  __DECLARE_VAR(UDINT,J)

} FB_RATTLY_DI;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,IN)
  __DECLARE_VAR(ULINT,PT)
  __DECLARE_VAR(BOOL,RES)
  __DECLARE_VAR(ULINT,ET)
  __DECLARE_VAR(ULINT,CMS)
  __DECLARE_VAR(ULINT,SMS)
  __DECLARE_VAR(BOOL,NIN)
  __DECLARE_VAR(BOOL,Q)
  __DECLARE_VAR(BOOL,TT)
  READ_SYS_TICK_COUNTER R_SYS;
} UTOFR;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,ERR_RESET)
  __DECLARE_VAR(BOOL,ISOL_PWR)
  __DECLARE_VAR(BOOL,DO_SC)
  __DECLARE_VAR(BOOL,DI_FREQ_OVERLOAD)
  __DECLARE_VAR(BOOL,ADC_CONNECTION)
  __DECLARE_VAR(BOOL,EXTERNAL_FLASH)
  __DECLARE_VAR(BOOL,TASK_INIT)
  __DECLARE_VAR(BOOL,CAN_SDO)
  __DECLARE_VAR(BOOL,LAN_CONNECTION)
  __DECLARE_VAR(BOOL,ARCHIEVE)
  __DECLARE_VAR(BOOL,LWIP_MEMORY)
  __DECLARE_VAR(BOOL,BATTERY_LOW)
  WORD_ON_BOOL WOB0;
  WORD_ON_BOOL WOB1;
} ERR_PARSE;

typedef struct {
  __DECLARE_VAR(BOOL,EN)
  __DECLARE_VAR(BOOL,ENO)
  __DECLARE_VAR(BOOL,DI_0)
  __DECLARE_VAR(BOOL,DI_1)
  __DECLARE_VAR(BOOL,DI_2)
  __DECLARE_VAR(BOOL,DI_3)
  __DECLARE_VAR(BOOL,DI_4)
  __DECLARE_VAR(BOOL,DI_5)
  __DECLARE_VAR(BOOL,DI_6)
  __DECLARE_VAR(BOOL,DI_7)
  __DECLARE_VAR(BOOL,DI_8)
  __DECLARE_VAR(BOOL,DI_9)
  __DECLARE_VAR(BOOL,DI_10)
  __DECLARE_VAR(BOOL,DI_11)
  __DECLARE_VAR(BOOL,DI_12)
  __DECLARE_VAR(BOOL,DI_13)
  __DECLARE_VAR(BOOL,DI_14)
  __DECLARE_VAR(BOOL,DI_15)
  WORD_ON_BOOL WOB;
} READ_DI_CH;

/*functions for read parametrs from controller*/
void READ_PARAM_UDINT_init__(READ_PARAM_UDINT *data__, BOOL retain);
void READ_PARAM_UDINT_body__(READ_PARAM_UDINT *data__);
void SET_PARAM_UDINT_init__(SET_PARAM_UDINT *data__, BOOL retain);
void SET_PARAM_UDINT_body__(SET_PARAM_UDINT *data__);
void READ_PARAM_UINT_init__(READ_PARAM_UINT *data__, BOOL retain);
void READ_PARAM_UINT_body__(READ_PARAM_UINT *data__);
void SET_PARAM_UINT_init__(SET_PARAM_UINT *data__, BOOL retain);
void SET_PARAM_UINT_body__(SET_PARAM_UINT *data__);
void READ_PARAM_USINT_init__(READ_PARAM_USINT *data__, BOOL retain);
void READ_PARAM_USINT_body__(READ_PARAM_USINT *data__);
void SET_PARAM_USINT_init__(SET_PARAM_USINT *data__, BOOL retain);
void SET_PARAM_USINT_body__(SET_PARAM_USINT *data__);

void READ_DI_init__(READ_DI *data__, BOOL retain);
void READ_DI_body__(READ_DI *data__);
void READ_DI_CNT_init__(READ_DI_CNT *data__, BOOL retain);
void READ_DI_CNT_body__(READ_DI_CNT *data__);
void READ_DI_FREQ_init__(READ_DI_FREQ *data__, BOOL retain);
void READ_DI_FREQ_body__(READ_DI_FREQ *data__);
void READ_AI_STATE_init__(READ_AI_STATE *data__, BOOL retain);
void READ_AI_STATE_body__(READ_AI_STATE *data__);
void READ_AI_init__(READ_AI *data__, BOOL retain);
void READ_AI_body__(READ_AI *data__);
void READ_AI_REAL_init__(READ_AI_REAL *data__, BOOL retain);
void READ_AI_REAL_body__(READ_AI_REAL *data__);
void READ_DO_SC_init__(READ_DO_SC *data__, BOOL retain);
void READ_DO_SC_body__(READ_DO_SC *data__);
void READ_DO_init__(READ_DO *data__, BOOL retain);
void READ_DO_body__(READ_DO *data__);
void READ_RESET_init__(READ_RESET *data__, BOOL retain);
void READ_RESET_body__(READ_RESET *data__);
void READ_PWR_init__(READ_PWR *data__, BOOL retain);
void READ_PWR_body__(READ_PWR *data__);
void READ_INTERNAL_TEMP_init__(READ_INTERNAL_TEMP *data__, BOOL retain);
void READ_INTERNAL_TEMP_body__(READ_INTERNAL_TEMP *data__);
void READ_TEMP_init__(READ_TEMP *data__, BOOL retain);
void READ_TEMP_body__(READ_TEMP *data__);
void READ_SYS_TICK_COUNTER_init__(READ_SYS_TICK_COUNTER *data__, BOOL retain);
void READ_SYS_TICK_COUNTER_body__(READ_SYS_TICK_COUNTER *data__);
void WRITE_MDB_ADDRESS_init__(WRITE_MDB_ADDRESS *data__, BOOL retain);
void WRITE_MDB_ADDRESS_body__(WRITE_MDB_ADDRESS *data__);
void WRITE_UART_SETS_init__(WRITE_UART_SETS *data__, BOOL retain);
void WRITE_UART_SETS_body__(WRITE_UART_SETS *data__);
void WRITE_CH_TIMEOUT_init__(WRITE_CH_TIMEOUT *data__, BOOL retain);
void WRITE_CH_TIMEOUT_body__(WRITE_CH_TIMEOUT *data__);
void WRITE_DI_NOISE_FLTR_10US_init__(WRITE_DI_NOISE_FLTR_10US *data__, BOOL retain);
void WRITE_DI_NOISE_FLTR_10US_body__(WRITE_DI_NOISE_FLTR_10US *data__);
void WRITE_DI_PULSELESS_init__(WRITE_DI_PULSELESS *data__, BOOL retain);
void WRITE_DI_PULSELESS_body__(WRITE_DI_PULSELESS *data__);
void WRITE_DI_MODE_init__(WRITE_DI_MODE *data__, BOOL retain);
void WRITE_DI_MODE_body__(WRITE_DI_MODE *data__);
void WRITE_DO_init__(WRITE_DO *data__, BOOL retain);
void WRITE_DO_body__(WRITE_DO *data__);
void WRITE_DO_SC_init__(WRITE_DO_SC *data__, BOOL retain);
void WRITE_DO_SC_body__(WRITE_DO_SC *data__);
void WRITE_DO_PWM_FREQ_init__(WRITE_DO_PWM_FREQ *data__, BOOL retain);
void WRITE_DO_PWM_FREQ_body__(WRITE_DO_PWM_FREQ *data__);
void WRITE_DO_PWM_CTRL_init__(WRITE_DO_PWM_CTRL *data__, BOOL retain);
void WRITE_DO_PWM_CTRL_body__(WRITE_DO_PWM_CTRL *data__);
void STRUCT_REAL_TIME_init__(STRUCT_REAL_TIME *data__, BOOL retain);
void STRUCT_REAL_TIME_body__(STRUCT_REAL_TIME *data__);
void UNIX_TIME_init__(UNIX_TIME *data__, BOOL retain);
void UNIX_TIME_body__(UNIX_TIME *data__);
void WRITE_STRUCT_TIME_init__(WRITE_STRUCT_TIME *data__, BOOL retain);
void WRITE_STRUCT_TIME_body__(WRITE_STRUCT_TIME *data__);
void PARSING_UINT_init__(PARSING_UINT *data__, BOOL retain);
void PARSING_UINT_body__(PARSING_UINT *data__);
void PARSING_USINT_init__(PARSING_USINT *data__, BOOL retain);
void PARSING_USINT_body__(PARSING_USINT *data__);
void MA_init__(SCALE_UINT_TO_FLOAT *data__, BOOL retain);
void MA_body__(SCALE_UINT_TO_FLOAT *data__);
uint32_t TASK_PERIOD(BOOL EN, BOOL * ENO, STRING task_name);
u8 WRITE_TO_CONSOLE(BOOL EN, BOOL * ENO, STRING msg);
void DWORD_TO_FLOAT_init__(DWORD_TO_FLOAT *data__, BOOL retain);
void DWORD_TO_FLOAT_body__(DWORD_TO_FLOAT *data__);
void WORDS_TO_FLOAT_init__(WORDS_TO_FLOAT *data__, BOOL retain);
void WORDS_TO_FLOAT_body__(WORDS_TO_FLOAT *data__);
void RES_TEMP_SENSOR_init__(RES_TEMP_SENSOR *data__, BOOL retain);
void RES_TEMP_SENSOR_body__(RES_TEMP_SENSOR *data__);
void WRITE_AREA_LOCATION_init__(WRITE_AREA_LOCATION *data__, BOOL retain);
void WRITE_AREA_LOCATION_body__(WRITE_AREA_LOCATION *data__);
void READ_AREA_LOCATION_init__(READ_AREA_LOCATION *data__, BOOL retain);
void READ_AREA_LOCATION_body__(READ_AREA_LOCATION *data__);
void WRITE_REQUEST_LOCATION_init__(WRITE_REQUEST_LOCATION *data__, BOOL retain);
void WRITE_REQUEST_LOCATION_body__(WRITE_REQUEST_LOCATION *data__);
void READ_REQUEST_LOCATION_init__(READ_REQUEST_LOCATION *data__, BOOL retain);
void READ_REQUEST_LOCATION_body__(READ_REQUEST_LOCATION *data__);
void ROUND_REAL_init__(ROUND_REAL *data__, BOOL retain);
void ROUND_REAL_body__(ROUND_REAL *data__);
void UTON_init__(UTON *data__, BOOL retain);
void UTON_body__(UTON *data__);
void UTOF_init__(UTOF *data__, BOOL retain);
void UTOF_body__(UTOF *data__);
void WORD_ON_BOOL_init__(WORD_ON_BOOL *data__, BOOL retain);
void WORD_ON_BOOL_body__(WORD_ON_BOOL *data__);
void BOOL_ON_WORD_init__(BOOL_ON_WORD *data__, BOOL retain);
void BOOL_ON_WORD_body__(BOOL_ON_WORD *data__);
void SETBIT_init__(SETBIT *data__, BOOL retain);
void SETBIT_body__(SETBIT *data__);
void GETBIT_init__(GETBIT *data__, BOOL retain);
void GETBIT_body__(GETBIT *data__);
void AI_HANDLER_init__(AI_HANDLER *data__, BOOL retain);
void AI_HANDLER_body__(AI_HANDLER *data__);
void HART_CH_SEL_init__(HART_CH_SEL *data__, BOOL retain);
void HART_CH_SEL_body__(HART_CH_SEL *data__);
void FIRST_SCAN_init__(FIRST_SCAN *data__, BOOL retain);
void FIRST_SCAN_body__(FIRST_SCAN *data__);
void PID_FB_init__(PID_FB *data__, BOOL retain);
void PID_FB_body__(PID_FB *data__);
void SETBIT_WORD_init__(SETBIT_WORD *data__, BOOL retain);
void SETBIT_WORD_body__(SETBIT_WORD *data__);
void GETBIT_WORD_init__(GETBIT_WORD *data__, BOOL retain);
void GETBIT_WORD_body__(GETBIT_WORD *data__);
void MODULE_STATE_init__(MODULE_STATE *data__, BOOL retain);
void MODULE_STATE_body__(MODULE_STATE *data__);
void READ_HART_CH_init__(READ_HART_CH *data__, BOOL retain);
void READ_HART_CH_body__(READ_HART_CH *data__);
void SERVOCONTROL_FB_init__(SERVOCONTROL_FB *data__, BOOL retain);
void SERVOCONTROL_FB_body__(SERVOCONTROL_FB *data__);
void FB_RATTLY_DI_init__(FB_RATTLY_DI *data__, BOOL retain);
void FB_RATTLY_DI_body__(FB_RATTLY_DI *data__);
void UTOFR_init__(UTOFR *data__, BOOL retain);
void UTOFR_body__(UTOFR *data__);
void ERR_PARSE_init__(ERR_PARSE *data__, BOOL retain);
void ERR_PARSE_body__(ERR_PARSE *data__);
void READ_DI_CH_init__(READ_DI_CH *data__, BOOL retain);
void READ_DI_CH_body__(READ_DI_CH *data__);
