/*
 * Copyright (c) 2018 Snema Service
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the sofi PLC.
 *
 * Author: Shoma Gane <shomagan@gmail.com>
 *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>
 */
#ifndef ARCHIVE_MANAGER_C
#define ARCHIVE_MANAGER_C 1
#include "archive_manager.h"
extern osMutexId user_mutex;

#if ARCHIVES_MANAGERS_NUMBER
/*__DECLARE_GLOBAL_LOCATED start*/
/*__DECLARE_GLOBAL_LOCATED stop*/
/*archive manager static function declaration start */

/*archive manager static function declaration stop */
static osThreadId archive_manager_id;
/*ARCHIVE VARS START*/
static u8 archive_manager_save_arc_control[ARCHIVES_MANAGERS_NUMBER] = {0}; /*sets in archive_manager_control_check reset in archive_manager_thread*/
static u8 archive_manager_arc_for_read_control[ARCHIVES_MANAGERS_NUMBER] = {0}; /*sets in archive_manager_control_check reset in archive_manager_thread*/
/*ARCHIVE VARS STOP*/
/*ARCHIVE STRUCTURES START*/
/*ARCHIVE STRUCTURES STOP*/
/** @brief called from beremiz_task after PlcLoop(NULL);
*   control changes in save_arc for all archives
*
*
*/
int archive_manager_control_check(){
/*archive_manager_control_check static declarations start*/
    static u16 preview_state_save_arc[ARCHIVES_MANAGERS_NUMBER] = {0};
    static u32 preview_state_arc_for_read[ARCHIVES_MANAGERS_NUMBER] = {0};
    archive_complete_t * archive_complete = NULL;
/*archive_manager_control_check static declarations stop*/
    for(u32 i=0;i<ARCHIVES_MANAGERS_NUMBER;i++){
        if (archives_completes[i]!=NULL){
            archive_complete = archives_completes[i];
            if(*archive_complete->archive_manager_access.save_arc->value && (preview_state_save_arc[i]==0)){
                archive_manager_save_arc_control[i] = 1;
                if (archive_manager_id!=NULL){
                    p_link_functions->os_signal_set(archive_manager_id, ARCHIVE_MANAGER_SIGNAL);
                }
            }
            if(*archive_complete->archive_manager_access.arc_for_read->value != preview_state_arc_for_read[i]){
                archive_manager_arc_for_read_control[i] = 1;
                if (archive_manager_id!=NULL){
                    p_link_functions->os_signal_set(archive_manager_id, ARCHIVE_MANAGER_SIGNAL);
                }
            }
            preview_state_arc_for_read[i] = *archive_complete->archive_manager_access.arc_for_read->value;
            preview_state_save_arc[i] = *archive_complete->archive_manager_access.save_arc->value;
        }
    }
}
int archive_manager_start(void){
    int res = 0;
    regs_template_t  regs_template;
    char self_var_num_name[]  = "brmz_task_vrsn";
    regs_template.name = self_var_num_name;
    if(p_link_functions->regs_description_get_by_name(&regs_template)!=0){
        p_link_functions->printf("error doesn't find brmz_task_vrsn");
        p_link_functions->led_error_on(INIT_ERROR_FATAL);
        regs_template.ind = 0;
        res = -1;
    }

/*__INIT_GLOBAL_LOCATED start*/
/*__INIT_GLOBAL_LOCATED stop*/

    for(u32 i=0;i<ARCHIVES_MANAGERS_NUMBER;i++){
        archive_manager_save_arc_control[i] = 0;
        archive_manager_arc_for_read_control[i] = 1;
    }
/*archive manager preinit vars start*/
/*archive manager preinit vars stop*/
    osThreadDef(archive_manager, archive_manager_thread, osPriorityLow, 0, ARCHIVE_MANAGER_THREAD_SIZE);
    archive_manager_id = p_link_functions->os_thread_create(osThread(archive_manager), (void*)NULL);
    return res;
}


int archive_manager_stop(void){
    static osThreadId thread_id;
    int res = 0;
    char THREAD_NAME[] = "archive_manager";
    thread_id = p_link_functions->os_thread_get_id_by_name(THREAD_NAME);
    if(thread_id!=NULL){
        if (p_link_functions->os_thread_terminate(thread_id)!=0){
            res = -1;
        }
    }
    return res;
}
/**
* @brief this thread use for archive handle , max item of thread  - MAX_ARCHIVES_MANAGERS
*
*
*/
void archive_manager_thread(const void * param){
    archive_mail_t arc_mail_handle;
    archive_file_title_t arc_file_title;
    archive_header_t * archive_header_p;
    osEvent event;
    static osThreadId sofi_arc_thread_id = NULL;
    while(sofi_arc_thread_id==NULL){
        sofi_arc_thread_id = p_link_functions->os_thread_get_id_by_name("sofi_arc");
        p_link_functions->os_delay(1);
    }

    while (1) {
        event = p_link_functions->os_signal_wait((int)0xffffffff,100);
/*handle for create archive START*/
/*handle for create archive STOP*/
/*handle for read archive START*/
/*handle for read archive STOP*/
    }
}
#endif  //ARCHIVE_ENABLE
#endif //ARCHIVE_MANAGER_C