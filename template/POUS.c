#include "POUS.h"
#include "plc_main.h"

void COUNTERSFC_init__(COUNTERSFC *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RESET,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OUT,0,retain)
  __INIT_VAR(data__->CNT,0,retain)
  __INIT_EXTERNAL(UDINT,RESETCOUNTERVALUE,data__->RESETCOUNTERVALUE,retain)
  UINT i;
  data__->__nb_steps = 3;
  static const STEP temp_step = {{0, 0}, 0, {{0, 0}, 0}};
  for(i = 0; i < data__->__nb_steps; i++) {
    data__->__step_list[i] = temp_step;
  }
  __SET_VAR(data__->,__step_list[0].X,,1);
  data__->__nb_actions = 4;
  static const ACTION temp_action = {0, {0, 0}, 0, 0, {0, 0}, {0, 0}};
  for(i = 0; i < data__->__nb_actions; i++) {
    data__->__action_list[i] = temp_action;
  }
  data__->__nb_transitions = 4;
  data__->__lasttick_time = __CURRENT_TIME;
}

// Steps definitions
#define START __step_list[0]
#define __SFC_START 0
#define RESETCOUNTER __step_list[1]
#define __SFC_RESETCOUNTER 1
#define COUNT __step_list[2]
#define __SFC_COUNT 2

// Actions definitions
#define __SFC_RESETCOUNTER_INLINE1 0
#define __SFC_RESETCOUNTER_INLINE2 1
#define __SFC_COUNT_INLINE3 2
#define __SFC_COUNT_INLINE4 3

// Code part
void COUNTERSFC_body__(COUNTERSFC *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  INT i;
  TIME elapsed_time, current_time;

  // Calculate elapsed_time
  current_time = __CURRENT_TIME;
  elapsed_time = __time_sub(current_time, data__->__lasttick_time);
  data__->__lasttick_time = current_time;
  // Transitions initialization
  if (__DEBUG) {
    for (i = 0; i < data__->__nb_transitions; i++) {
      data__->__transition_list[i] = data__->__debug_transition_list[i];
    }
  }
  // Steps initialization
  for (i = 0; i < data__->__nb_steps; i++) {
    data__->__step_list[i].prev_state = __GET_VAR(data__->__step_list[i].X);
    if (__GET_VAR(data__->__step_list[i].X)) {
      data__->__step_list[i].T.value = __time_add(data__->__step_list[i].T.value, elapsed_time);
    }
  }
  // Actions initialization
  for (i = 0; i < data__->__nb_actions; i++) {
    __SET_VAR(data__->,__action_list[i].state,,0);
    data__->__action_list[i].set = 0;
    data__->__action_list[i].reset = 0;
    if (__time_cmp(data__->__action_list[i].set_remaining_time, __time_to_timespec(1, 0, 0, 0, 0, 0)) > 0) {
      data__->__action_list[i].set_remaining_time = __time_sub(data__->__action_list[i].set_remaining_time, elapsed_time);
      if (__time_cmp(data__->__action_list[i].set_remaining_time, __time_to_timespec(1, 0, 0, 0, 0, 0)) <= 0) {
        data__->__action_list[i].set_remaining_time = __time_to_timespec(1, 0, 0, 0, 0, 0);
        data__->__action_list[i].set = 1;
      }
    }
    if (__time_cmp(data__->__action_list[i].reset_remaining_time, __time_to_timespec(1, 0, 0, 0, 0, 0)) > 0) {
      data__->__action_list[i].reset_remaining_time = __time_sub(data__->__action_list[i].reset_remaining_time, elapsed_time);
      if (__time_cmp(data__->__action_list[i].reset_remaining_time, __time_to_timespec(1, 0, 0, 0, 0, 0)) <= 0) {
        data__->__action_list[i].reset_remaining_time = __time_to_timespec(1, 0, 0, 0, 0, 0);
        data__->__action_list[i].reset = 1;
      }
    }
  }

  // Transitions fire test
  if (__GET_VAR(data__->START.X)) {
    __SET_VAR(data__->,__transition_list[0],,__GET_VAR(data__->RESET,));
    if (__DEBUG) {
      __SET_VAR(data__->,__debug_transition_list[0],,__GET_VAR(data__->__transition_list[0]));
    }
  }
  else {
    if (__DEBUG) {
      __SET_VAR(data__->,__debug_transition_list[0],,__GET_VAR(data__->RESET,));
    }
    __SET_VAR(data__->,__transition_list[0],,0);
  }
  if (__GET_VAR(data__->RESETCOUNTER.X)) {
    __SET_VAR(data__->,__transition_list[1],,!(__GET_VAR(data__->RESET,)));
    if (__DEBUG) {
      __SET_VAR(data__->,__debug_transition_list[1],,__GET_VAR(data__->__transition_list[1]));
    }
  }
  else {
    if (__DEBUG) {
      __SET_VAR(data__->,__debug_transition_list[1],,!(__GET_VAR(data__->RESET,)));
    }
    __SET_VAR(data__->,__transition_list[1],,0);
  }
  if (__GET_VAR(data__->START.X)) {
    __SET_VAR(data__->,__transition_list[2],,!(__GET_VAR(data__->RESET,)));
    if (__DEBUG) {
      __SET_VAR(data__->,__debug_transition_list[2],,__GET_VAR(data__->__transition_list[2]));
    }
  }
  else {
    if (__DEBUG) {
      __SET_VAR(data__->,__debug_transition_list[2],,!(__GET_VAR(data__->RESET,)));
    }
    __SET_VAR(data__->,__transition_list[2],,0);
  }
  if (__GET_VAR(data__->COUNT.X)) {
    __SET_VAR(data__->,__transition_list[3],,__GET_VAR(data__->RESET,));
    if (__DEBUG) {
      __SET_VAR(data__->,__debug_transition_list[3],,__GET_VAR(data__->__transition_list[3]));
    }
  }
  else {
    if (__DEBUG) {
      __SET_VAR(data__->,__debug_transition_list[3],,__GET_VAR(data__->RESET,));
    }
    __SET_VAR(data__->,__transition_list[3],,0);
  }

  // Transitions reset steps
  if (__GET_VAR(data__->__transition_list[0])) {
    __SET_VAR(data__->,START.X,,0);
  }
  if (__GET_VAR(data__->__transition_list[1])) {
    __SET_VAR(data__->,RESETCOUNTER.X,,0);
  }
  if (__GET_VAR(data__->__transition_list[2])) {
    __SET_VAR(data__->,START.X,,0);
  }
  if (__GET_VAR(data__->__transition_list[3])) {
    __SET_VAR(data__->,COUNT.X,,0);
  }

  // Transitions set steps
  if (__GET_VAR(data__->__transition_list[0])) {
    __SET_VAR(data__->,RESETCOUNTER.X,,1);
    data__->RESETCOUNTER.T.value = __time_to_timespec(1, 0, 0, 0, 0, 0);
  }
  if (__GET_VAR(data__->__transition_list[1])) {
    __SET_VAR(data__->,START.X,,1);
    data__->START.T.value = __time_to_timespec(1, 0, 0, 0, 0, 0);
  }
  if (__GET_VAR(data__->__transition_list[2])) {
    __SET_VAR(data__->,COUNT.X,,1);
    data__->COUNT.T.value = __time_to_timespec(1, 0, 0, 0, 0, 0);
  }
  if (__GET_VAR(data__->__transition_list[3])) {
    __SET_VAR(data__->,START.X,,1);
    data__->START.T.value = __time_to_timespec(1, 0, 0, 0, 0, 0);
  }

  // Steps association
  // RESETCOUNTER action associations
  {
    char active = __GET_VAR(data__->RESETCOUNTER.X);
    char activated = active && !data__->RESETCOUNTER.prev_state;
    char desactivated = !active && data__->RESETCOUNTER.prev_state;

    if (active)       {__SET_VAR(data__->,__action_list[__SFC_RESETCOUNTER_INLINE1].state,,1);};
    if (desactivated) {__SET_VAR(data__->,__action_list[__SFC_RESETCOUNTER_INLINE1].state,,0);};

    if (active)       {__SET_VAR(data__->,__action_list[__SFC_RESETCOUNTER_INLINE2].state,,1);};
    if (desactivated) {__SET_VAR(data__->,__action_list[__SFC_RESETCOUNTER_INLINE2].state,,0);};

  }

  // COUNT action associations
  {
    char active = __GET_VAR(data__->COUNT.X);
    char activated = active && !data__->COUNT.prev_state;
    char desactivated = !active && data__->COUNT.prev_state;

    if (active)       {__SET_VAR(data__->,__action_list[__SFC_COUNT_INLINE3].state,,1);};
    if (desactivated) {__SET_VAR(data__->,__action_list[__SFC_COUNT_INLINE3].state,,0);};

    if (active)       {__SET_VAR(data__->,__action_list[__SFC_COUNT_INLINE4].state,,1);};
    if (desactivated) {__SET_VAR(data__->,__action_list[__SFC_COUNT_INLINE4].state,,0);};

  }


  // Actions state evaluation
  for (i = 0; i < data__->__nb_actions; i++) {
    if (data__->__action_list[i].set) {
      data__->__action_list[i].set_remaining_time = __time_to_timespec(1, 0, 0, 0, 0, 0);
      data__->__action_list[i].stored = 1;
    }
    if (data__->__action_list[i].reset) {
      data__->__action_list[i].reset_remaining_time = __time_to_timespec(1, 0, 0, 0, 0, 0);
      data__->__action_list[i].stored = 0;
    }
    __SET_VAR(data__->,__action_list[i].state,,__GET_VAR(data__->__action_list[i].state) | data__->__action_list[i].stored);
  }

  // Actions execution
  if(__GET_VAR(data__->__action_list[__SFC_RESETCOUNTER_INLINE1].state)) {
    __SET_VAR(data__->,CNT,,__GET_EXTERNAL(data__->RESETCOUNTERVALUE,));
  }

  if(__GET_VAR(data__->__action_list[__SFC_RESETCOUNTER_INLINE2].state)) {
    __SET_VAR(data__->,OUT,,__GET_VAR(data__->CNT,));
  }

  if(__GET_VAR(data__->__action_list[__SFC_COUNT_INLINE3].state)) {
    __SET_VAR(data__->,CNT,,(__GET_VAR(data__->CNT,) + 1));
  }

  if(__GET_VAR(data__->__action_list[__SFC_COUNT_INLINE4].state)) {
    __SET_VAR(data__->,OUT,,__GET_VAR(data__->CNT,));
  }



  goto __end;

__end:
  return;
} // COUNTERSFC_body__() 

// Steps undefinitions
#undef START
#undef __SFC_START
#undef RESETCOUNTER
#undef __SFC_RESETCOUNTER
#undef COUNT
#undef __SFC_COUNT

// Actions undefinitions
#undef __SFC_RESETCOUNTER_INLINE1
#undef __SFC_RESETCOUNTER_INLINE2
#undef __SFC_COUNT_INLINE3
#undef __SFC_COUNT_INLINE4





void COUNTERFBD_init__(COUNTERFBD *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RESET,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OUT,0,retain)
  __INIT_VAR(data__->CNT,0,retain)
  __INIT_EXTERNAL(UDINT,RESETCOUNTERVALUE,data__->RESETCOUNTERVALUE,retain)
  __INIT_VAR(data__->ADD4_OUT,0,retain)
  __INIT_VAR(data__->SEL7_OUT,0,retain)
}

// Code part
void COUNTERFBD_body__(COUNTERFBD *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,ADD4_OUT,,ADD__UDINT__UDINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UDINT)1,
    (UDINT)__GET_VAR(data__->CNT,)));
  __SET_VAR(data__->,SEL7_OUT,,SEL__UDINT__BOOL__UDINT__UDINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->RESET,),
    (UDINT)__GET_VAR(data__->ADD4_OUT,),
    (UDINT)__GET_EXTERNAL(data__->RESETCOUNTERVALUE,)));
  __SET_VAR(data__->,CNT,,__GET_VAR(data__->SEL7_OUT,));
  __SET_VAR(data__->,OUT,,__GET_VAR(data__->CNT,));

  goto __end;

__end:
  return;
} // COUNTERFBD_body__() 





void PLC_TEST_init__(PLC_TEST *data__, BOOL retain) {
  __INIT_VAR(data__->INPUT_ADD,12,retain)
  __INIT_VAR(data__->OUTPUT_ADD,0,retain)
  __INIT_VAR(data__->ADD1_OUT,0,retain)
}

// Code part
void PLC_TEST_body__(PLC_TEST *data__) {
  // Initialise TEMP variables

  __SET_VAR(data__->,ADD1_OUT,,ADD__DINT__DINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (DINT)__GET_VAR(data__->INPUT_ADD,),
    (DINT)__GET_VAR(data__->INPUT_ADD,)));
  __SET_VAR(data__->,OUTPUT_ADD,,__GET_VAR(data__->ADD1_OUT,));

  goto __end;

__end:
  return;
} // PLC_TEST_body__() 





void COUNTERIL_init__(COUNTERIL *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->CNT,0,retain)
  __INIT_VAR(data__->RESET,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OUT,0,retain)
  __INIT_EXTERNAL(UDINT,RESETCOUNTERVALUE,data__->RESETCOUNTERVALUE,retain)
}

// Code part
void COUNTERIL_body__(COUNTERIL *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __IL_DEFVAR_T __IL_DEFVAR;
  __IL_DEFVAR_T __IL_DEFVAR_BACK;
  __IL_DEFVAR.BOOLvar = __GET_VAR(data__->RESET,);
  if (__IL_DEFVAR.BOOLvar) goto RESETCNT;
  __IL_DEFVAR.UDINTvar = __GET_VAR(data__->CNT,);
  __IL_DEFVAR.UDINTvar += 1;
  goto QUITFB;
  RESETCNT:
  ;
  __IL_DEFVAR.UDINTvar = __GET_EXTERNAL(data__->RESETCOUNTERVALUE,);
  QUITFB:
  ;
  __SET_VAR(data__->,CNT,,__IL_DEFVAR.UDINTvar);
  __SET_VAR(data__->,OUT,,__IL_DEFVAR.UDINTvar);

  goto __end;

__end:
  return;
} // COUNTERIL_body__() 





void COUNTERST_init__(COUNTERST *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RESET,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CNT,0,retain)
  __INIT_VAR(data__->OUT,0,retain)
  __INIT_EXTERNAL(UDINT,RESETCOUNTERVALUE,data__->RESETCOUNTERVALUE,retain)
}

// Code part
void COUNTERST_body__(COUNTERST *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  if (__GET_VAR(data__->RESET,)) {
    __SET_VAR(data__->,CNT,,__GET_EXTERNAL(data__->RESETCOUNTERVALUE,));
  } else {
    __SET_VAR(data__->,CNT,,(__GET_VAR(data__->CNT,) + 1));
  };
  __SET_VAR(data__->,OUT,,__GET_VAR(data__->CNT,));

  goto __end;

__end:
  return;
} // COUNTERST_body__() 





void COUNTERLD_init__(COUNTERLD *data__, BOOL retain) {
  __INIT_VAR(data__->EN,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->ENO,__BOOL_LITERAL(TRUE),retain)
  __INIT_VAR(data__->RESET,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->OUT,0,retain)
  __INIT_VAR(data__->CNT,0,retain)
  __INIT_EXTERNAL(UDINT,RESETCOUNTERVALUE,data__->RESETCOUNTERVALUE,retain)
  __INIT_VAR(data__->ADD4_OUT,0,retain)
  __INIT_VAR(data__->SEL7_OUT,0,retain)
}

// Code part
void COUNTERLD_body__(COUNTERLD *data__) {
  // Control execution
  if (!__GET_VAR(data__->EN)) {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(FALSE));
    return;
  }
  else {
    __SET_VAR(data__->,ENO,,__BOOL_LITERAL(TRUE));
  }
  // Initialise TEMP variables

  __SET_VAR(data__->,ADD4_OUT,,ADD__UDINT__UDINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (UINT)2,
    (UDINT)1,
    (UDINT)__GET_VAR(data__->CNT,)));
  __SET_VAR(data__->,SEL7_OUT,,SEL__UDINT__BOOL__UDINT__UDINT(
    (BOOL)__BOOL_LITERAL(TRUE),
    NULL,
    (BOOL)__GET_VAR(data__->RESET,),
    (UDINT)__GET_VAR(data__->ADD4_OUT,),
    (UDINT)__GET_EXTERNAL(data__->RESETCOUNTERVALUE,)));
  __SET_VAR(data__->,CNT,,__GET_VAR(data__->SEL7_OUT,));
  __SET_VAR(data__->,OUT,,__GET_VAR(data__->CNT,));

  goto __end;

__end:
  return;
} // COUNTERLD_body__() 





void PLC_PRG_init__(PLC_PRG *data__, BOOL retain) {
  __INIT_VAR(data__->RESET,__BOOL_LITERAL(FALSE),retain)
  __INIT_VAR(data__->CNT1,0,retain)
  __INIT_VAR(data__->CNT2,0,retain)
  __INIT_VAR(data__->CNT3,0,retain)
  __INIT_VAR(data__->CNT4,0,retain)
  __INIT_VAR(data__->CNT5,0,retain)
  COUNTERST_init__(&data__->COUNTERST0,retain);
  COUNTERFBD_init__(&data__->COUNTERFBD0,retain);
  COUNTERSFC_init__(&data__->COUNTERSFC0,retain);
  COUNTERIL_init__(&data__->COUNTERIL0,retain);
  COUNTERLD_init__(&data__->COUNTERLD0,retain);
}

// Code part
void PLC_PRG_body__(PLC_PRG *data__) {
  // Initialise TEMP variables

  __SET_VAR(data__->COUNTERST0.,RESET,,__GET_VAR(data__->RESET,));
  COUNTERST_body__(&data__->COUNTERST0);
  __SET_VAR(data__->,CNT1,,__GET_VAR(data__->COUNTERST0.OUT,));
  __SET_VAR(data__->COUNTERFBD0.,RESET,,__GET_VAR(data__->RESET,));
  COUNTERFBD_body__(&data__->COUNTERFBD0);
  __SET_VAR(data__->,CNT2,,__GET_VAR(data__->COUNTERFBD0.OUT,));
  __SET_VAR(data__->COUNTERSFC0.,RESET,,__GET_VAR(data__->RESET,));
  COUNTERSFC_body__(&data__->COUNTERSFC0);
  __SET_VAR(data__->,CNT3,,__GET_VAR(data__->COUNTERSFC0.OUT,));
  __SET_VAR(data__->COUNTERIL0.,RESET,,__GET_VAR(data__->RESET,));
  COUNTERIL_body__(&data__->COUNTERIL0);
  __SET_VAR(data__->,CNT4,,__GET_VAR(data__->COUNTERIL0.OUT,));
  __SET_VAR(data__->COUNTERLD0.,RESET,,__GET_VAR(data__->RESET,));
  COUNTERLD_body__(&data__->COUNTERLD0);
  __SET_VAR(data__->,CNT5,,__GET_VAR(data__->COUNTERLD0.OUT,));

  goto __end;

__end:
  return;
} // PLC_PRG_body__() 





