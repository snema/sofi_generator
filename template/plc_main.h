#include "beremiz.h"
#include "POUS.h"

#if defined __cplusplus
extern "C" {
#endif

void PLC_GetTime(IEC_TIME *CURRENT_TIME);
void PLC_SetTimer(unsigned long long next, unsigned long long period);
void* PlcLoop(void * args);
int __init(void);

//global variable for use
extern IEC_TIME __CURRENT_TIME;    //time 

#if defined __cplusplus
}
#endif

