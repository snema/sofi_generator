#ifndef __ACCESSOR_H
#define __ACCESSOR_H

#define __INITIAL_VALUE(...) __VA_ARGS__

// variable declaration macros
#define __DECLARE_VAR(type, name)\
	__IEC_##type##_t name;
#define __DECLARE_GLOBAL(type, domain, name)\
	__IEC_##type##_t domain##__##name;\
	static __IEC_##type##_t *GLOBAL__##name = &(domain##__##name);\
	void __INIT_GLOBAL_##name(type value) {\
		(*GLOBAL__##name).value = value;\
	}\
	IEC_BYTE __IS_GLOBAL_##name##_FORCED(void) {\
		return (*GLOBAL__##name).flags & __IEC_FORCE_FLAG;\
	}\
	type* __GET_GLOBAL_##name(void) {\
		return &((*GLOBAL__##name).value);\
	}
#define __DECLARE_GLOBAL_FB(type, domain, name)\
	type domain##__##name;\
	static type *GLOBAL__##name = &(domain##__##name);\
    type* __GET_GLOBAL_##name(void) {\
		return &(*GLOBAL__##name);\
	}\
	extern void type##_init__(type* data__, BOOL retain);
#define __DECLARE_GLOBAL_LOCATION(type, location)\
    extern type *location;
#define __DECLARE_GLOBAL_LOCATED(type, resource, name)\
	__IEC_##type##_p resource##__##name;\
    static __IEC_##type##_p *GLOBAL__##name = &(resource##__##name);\
    void __INIT_GLOBAL_##name(type value) {\
        memcpy((*GLOBAL__##name).value,&value,sizeof(type));\
    }\
	IEC_BYTE __IS_GLOBAL_##name##_FORCED(void) {\
		return (*GLOBAL__##name).flags & __IEC_FORCE_FLAG;\
	}\
	type* __GET_GLOBAL_##name(void) {\
		return (*GLOBAL__##name).value;\
    }\
    type __GET_GLOBAL_LOCATED_VALUE##name(void) {\
        type temp_value;\
        memcpy(&temp_value,(*GLOBAL__##name).value,sizeof(type));\
        return temp_value;\
    }

#define __DECLARE_GLOBAL_PROTOTYPE(type, name)\
    extern type* __GET_GLOBAL_##name(void);
#define __DECLARE_EXTERNAL(type, name)\
	__IEC_##type##_p name;
#define __DECLARE_EXTERNAL_FB(type, name)\
	type* name;
#define __DECLARE_LOCATED(type, name)\
	__IEC_##type##_p name;

#define IEC_BOOL_REGS_TYPE_FLAG 0x0000
#define IEC_USINT_REGS_TYPE_FLAG 0x0000
#define IEC_UINT_REGS_TYPE_FLAG 0x0000
#define IEC_UDINT_REGS_TYPE_FLAG 0x0000
#define IEC_ULINT_REGS_TYPE_FLAG 0x0000
#define IEC_BYTE_REGS_TYPE_FLAG 0x0000
#define IEC_WORD_REGS_TYPE_FLAG 0x0000
#define IEC_DWORD_REGS_TYPE_FLAG 0x0000
#define IEC_LWORD_REGS_TYPE_FLAG 0x0000
#define IEC_SINT_REGS_TYPE_FLAG 0x1000
#define IEC_INT_REGS_TYPE_FLAG 0x1000
#define IEC_DINT_REGS_TYPE_FLAG 0x1000
#define IEC_LINT_REGS_TYPE_FLAG 0x1000
#define IEC_REAL_REGS_TYPE_FLAG 0x2000
#define IEC_LREAL_REGS_TYPE_FLAG 0x4000

#define BOOL_REGS_TYPE_FLAG 0x0000
#define USINT_REGS_TYPE_FLAG 0x0000
#define UINT_REGS_TYPE_FLAG 0x0000
#define UDINT_REGS_TYPE_FLAG 0x0000
#define ULINT_REGS_TYPE_FLAG 0x0000
#define BYTE_REGS_TYPE_FLAG 0x0000
#define WORD_REGS_TYPE_FLAG 0x0000
#define DWORD_REGS_TYPE_FLAG 0x0000
#define LWORD_REGS_TYPE_FLAG 0x0000
#define SINT_REGS_TYPE_FLAG 0x1000
#define INT_REGS_TYPE_FLAG 0x1000
#define DINT_REGS_TYPE_FLAG 0x1000
#define LINT_REGS_TYPE_FLAG 0x1000
#define REAL_REGS_TYPE_FLAG 0x2000
#define LREAL_REGS_TYPE_FLAG 0x4000

// variable initialization macros
#define __INIT_RETAIN(name, retained)\
    name.flags |= retained?__IEC_RETAIN_FLAG:0;
#define __INIT_SIZE(type_v, name)\
    name.flags &= 0x00ff;\
    name.flags |= sizeof(type_v)<<8;\
    name.flags |= type_v##_REGS_TYPE_FLAG;

#define __INIT_ARRAY_FLAG(name)\
    name.flags |= __IEC_ARRAY_FLAG;
#define __INIT_VAR(name, initial, retained)\
	name.value = initial;\
	__INIT_RETAIN(name, retained)
#define __INIT_GLOBAL(type, name, initial, retained)\
    {\
	    type temp = initial;\
	    __INIT_GLOBAL_##name(temp);\
	    __INIT_RETAIN((*GLOBAL__##name), retained)\
    }
#define __INIT_GLOBAL_ARRAY(type, name, initial, retained)\
    {\
	    type temp = initial;\
	    __INIT_GLOBAL_##name(temp);\
	    __INIT_RETAIN((*GLOBAL__##name), retained)\
	    __INIT_ARRAY_FLAG((*GLOBAL__##name))\
    }
#define __INIT_GLOBAL_FB(type, name, retained)\
	type##_init__(&(*GLOBAL__##name), retained);
#define __INIT_GLOBAL_LOCATED(domain, name, location, retained)\
    domain##__##name.value = location;\
	__INIT_RETAIN(domain##__##name, retained)
#define __INIT_EXTERNAL(type, global, name, retained)\
    {\
		name.value = __GET_GLOBAL_##global();\
		__INIT_RETAIN(name, retained)\
        __INIT_SIZE(type, name)\
    }
#define __INIT_EXTERNAL_FB(type, global, name, retained)\
	name = __GET_GLOBAL_##global();
#define __INIT_LOCATED(type, location, name, retained)\
	{\
		extern type *location;\
		name.value = location;\
		__INIT_RETAIN(name, retained)\
        __INIT_SIZE(type, name)\
    }
#define __INIT_LOCATED_VALUE(name, initial)\
	*(name.value) = initial;
extern uint64_t GET_EXTERNAL_UNSIGNED(void * value, uint16_t flags);
extern int64_t GET_EXTERNAL_SIGNED(void * value, uint16_t flags);
extern float GET_EXTERNAL_REAL(void * value, uint16_t flags);
extern double GET_EXTERNAL_LREAL(void * value, uint16_t flags);


// variable getting macros
#define __GET_VAR(name, ...)\
	name.value __VA_ARGS__
#define __GET_EXTERNAL(name, ...)\
    ((name.flags & __IEC_FORCE_FLAG) ? name.fvalue __VA_ARGS__ : \
        ((name.flags & 0xff00)==0)? (*(name.value)) __VA_ARGS__:\
            ((name.flags & 0xf000)==IEC_BOOL_REGS_TYPE_FLAG)?GET_EXTERNAL_UNSIGNED(name.value, name.flags) :\
                ((name.flags & 0xf000)==IEC_SINT_REGS_TYPE_FLAG)?GET_EXTERNAL_SIGNED(name.value, name.flags) :\
                    ((name.flags & 0xf000)==IEC_REAL_REGS_TYPE_FLAG)?GET_EXTERNAL_REAL(name.value, name.flags) :\
                        ((name.flags & 0xf000)==IEC_LREAL_REGS_TYPE_FLAG)?GET_EXTERNAL_LREAL(name.value, name.flags):(*(name.value)) __VA_ARGS__)
#define __GET_EXTERNAL_FB(name, ...)\
	__GET_VAR(((*name) __VA_ARGS__))
#define __GET_LOCATED(name, ...)\
    ((name.flags & __IEC_FORCE_FLAG) ? name.fvalue __VA_ARGS__ : \
        ((name.flags & 0xff00)==0)? (*(name.value)) __VA_ARGS__:\
            ((name.flags & 0xf000)==IEC_BOOL_REGS_TYPE_FLAG)?GET_EXTERNAL_UNSIGNED(name.value, name.flags) :\
                ((name.flags & 0xf000)==IEC_SINT_REGS_TYPE_FLAG)?GET_EXTERNAL_SIGNED(name.value, name.flags) :\
                    ((name.flags & 0xf000)==IEC_REAL_REGS_TYPE_FLAG)?GET_EXTERNAL_REAL(name.value, name.flags) :\
                        ((name.flags & 0xf000)==IEC_LREAL_REGS_TYPE_FLAG)?GET_EXTERNAL_LREAL(name.value, name.flags):(*(name.value)) __VA_ARGS__)
#define __GET_VAR_BY_REF(name, ...)\
	((name.flags & __IEC_FORCE_FLAG) ? &(name.fvalue __VA_ARGS__) : &(name.value __VA_ARGS__))
#define __GET_EXTERNAL_BY_REF(name, ...)\
	((name.flags & __IEC_FORCE_FLAG) ? &(name.fvalue __VA_ARGS__) : &((*(name.value)) __VA_ARGS__))
#define __GET_EXTERNAL_FB_BY_REF(name, ...)\
	__GET_EXTERNAL_BY_REF(((*name) __VA_ARGS__))
#define __GET_LOCATED_BY_REF(name, ...)\
	((name.flags & __IEC_FORCE_FLAG) ? &(name.fvalue __VA_ARGS__) : &((*(name.value)) __VA_ARGS__))

#define __GET_VAR_REF(name, ...)\
	(&(name.value __VA_ARGS__))
#define __GET_EXTERNAL_REF(name, ...)\
	(&((*(name.value)) __VA_ARGS__))
#define __GET_EXTERNAL_FB_REF(name, ...)\
	(&(__GET_VAR(((*name) __VA_ARGS__))))
#define __GET_LOCATED_REF(name, ...)\
	(&((*(name.value)) __VA_ARGS__))

#define __GET_VAR_DREF(name, ...)\
	(*(name.value __VA_ARGS__))
#define __GET_EXTERNAL_DREF(name, ...)\
	(*((*(name.value)) __VA_ARGS__))
#define __GET_EXTERNAL_FB_DREF(name, ...)\
	(*(__GET_VAR(((*name) __VA_ARGS__))))
#define __GET_LOCATED_DREF(name, ...)\
	(*((*(name.value)) __VA_ARGS__))


// variable setting macros
#define __SET_VAR(prefix, name, suffix, new_value)\
	if (!(prefix name.flags & __IEC_FORCE_FLAG)) prefix name.value suffix = new_value
#define __SET_EXTERNAL(prefix, name, suffix, new_value)\
    {extern IEC_BYTE __IS_GLOBAL_##name##_FORCED(void);\
    __typeof__(*(prefix name.value)) temp_value = (__typeof__(*(prefix name.value)))new_value;\
    if (!(prefix name.flags & __IEC_FORCE_FLAG || __IS_GLOBAL_##name##_FORCED()))\
        memcpy(prefix name.value,&temp_value,(prefix name.flags&0x0f00)>>8);}
#define __SET_EXTERNAL_FB(prefix, name, suffix, new_value)\
	__SET_VAR((*(prefix name)), suffix, new_value)
#define __SET_LOCATED(prefix, name, suffix, new_value)\
    {__typeof__(*(prefix name.value)) temp_value = (__typeof__(*(prefix name.value)))new_value;\
    if (!(prefix name.flags & __IEC_FORCE_FLAG || __IS_GLOBAL_##name##_FORCED()))\
        memcpy(prefix name.value,&temp_value,(prefix name.flags&0x0f00)>>8);}

#endif //__ACCESSOR_H
