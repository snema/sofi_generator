/**
 * Head of code common to all C targets
 **/

#include <time.h>
#include "plc_main.h"
#include "POUS.h"
#include "config.h"
#include "cortexm/cortexm.h"


/*
 * Prototypes of functions provided by generated C softPLC
 **/

/*
 * Prototypes of functions provided by generated target C code
 * */
/*
 *  Variables used by generated C softPLC and plugins
 **/
IEC_TIME __CURRENT_TIME;
IEC_BOOL __DEBUG = 0;
unsigned long long __tick;
char *PLC_ID = NULL;


/* Help to quit cleanly when init fail at a certain level */
static int init_level = 0;


/*
 * Initialize variables according to PLC's default values,
 * and then init plugins with that values
 **/
int __init(void)
{
    init_level = 0;
    __tick = 0;
    config_init__();
    return 0;
}


/**
 * No platform specific code for "Generic" target
 **/
void PLC_GetTime(IEC_TIME *CURRENT_TIME)
{
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);
    //time in second from 1970 year
    (*CURRENT_TIME).tv_sec = now.tv_sec;
    //part in milliseconds fractional (ex. sec.millisec)
    (*CURRENT_TIME).tv_nsec = now.tv_nsec;
}

static void PLC_timer_notify(void);

void PLC_timer_notify(void)
{
    PLC_GetTime(&__CURRENT_TIME);
    __tick++;
    config_run__(__tick);
}
/* Variable used to stop plcloop thread */
void* PlcLoop(void * args)
{   
    (void)args;
    PLC_timer_notify();
    return NULL;
}
