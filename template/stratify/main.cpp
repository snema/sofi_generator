#include "POUS.h"
#include "config.h"
#include "plc_main.h"
#include <sapi/sys.hpp>

void* plc_thread(void* arg);
int create_thread(void * (*func)(void*), int stacksize, int detachstate);

int main(int argc, char * argv[]){
    Cli cli(argc, argv);
    cli.handle_version();
    printf("start beremiz program");
    __init();
    if ( create_thread(plc_thread, 16384, PTHREAD_CREATE_JOINABLE) < 0){
        printf ("loop not created");
        return -1;
    }else{
        printf ("loop created");
    }
    Timer t;
    t.start(); //start
    u32 tck=0;
    while(1){
        tck++;
/*        u32 count_l,count_h;
        count_l = RESOURCE1__PLC_PRG_TASK_INSTANCE.COUNTERFBD0.OUT.value;
        count_h = RESOURCE1__PLC_PRG_TASK_INSTANCE.COUNTERFBD0.OUT.value>>32;
        printf("tick number %ld value FBD 0x%08x%08x TEST %lu\n",tck,
               count_h,count_l,
               RESOURCE1__PLC_TEST_TASK_INSTANCE.ADD1_OUT.value
               );
        fflush(stdout);*/
        t.wait_sec(10);
    }
    t.stop();
}

int create_thread(void * (*func)(void*), int stacksize, int detachstate){
    pthread_t t;
    pthread_attr_t pattr;

    if ( pthread_attr_init(&pattr) < 0 ){
        fflush(stdout);
        perror("attr_init failed");
        return -1;
    }

    if ( pthread_attr_setdetachstate(&pattr, detachstate) < 0 ){
        fflush(stdout);
        perror("setdetachstate failed");
        return -1;
    }

    if ( pthread_attr_setstacksize(&pattr, stacksize) < 0 ){
        fflush(stdout);
        perror("setstacksize failed");
        return -1;
    }

    if ( pthread_create(&t, &pattr, func, NULL) ){
        fflush(stdout);
        perror("create failed");
        return -1;
    }
    return t;
}
void* plc_thread(void* arg){
    Timer plc_sheduler_timer;
    static u32 time;
    plc_sheduler_timer.start();
    u32 common_ticktime_usec;
    common_ticktime__= 1000000;
    common_ticktime_usec = common_ticktime__/1000;
    time = plc_sheduler_timer.calc_usec()/common_ticktime_usec;
    printf("common_ticktime_usec %lu",common_ticktime_usec);
    printf("common_ticktime %lu",common_ticktime__);
    while(1){
        if((plc_sheduler_timer.calc_usec()/common_ticktime_usec)>time){
            PlcLoop(arg);
            time = plc_sheduler_timer.calc_usec()/common_ticktime_usec;
        }
        //plc_sheduler_timer.wait_usec(500);
        if(plc_sheduler_timer.calc_usec() > (0xffffffff-(common_ticktime_usec+1))){
            plc_sheduler_timer.reset();
            plc_sheduler_timer.start();
            time = plc_sheduler_timer.calc_usec()/common_ticktime_usec;
        }
    }
    plc_sheduler_timer.stop();
}



