import re
import base_object
from os import walk
import os
import random

class Header(base_object.Base):
    name = ""
    not_used_function_decl = ['^\s*int\s+LogMessage.*$',
                         '^\s*long\s+AtomicCompareExchange.*$']

    def add_use_define(self, file_name):
        path_to_file = self.path_project_src + file_name.name
        file_name.write("/**\n")
        file_name.write(" * @file " + file_name.name + "\n")
        file_name.write(" * @author Shoma Gane <shomagan@gmail.com>\n")
        file_name.write(" *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>\n")
        file_name.write(" * @defgroup beremiz \n")
        file_name.write(" * @ingroup beremiz \n")
        file_name.write(" * @version 0.1 \n")
        file_name.write(" * @brief  TODO!!! write brief in \n")
        file_name.write(" */\n")
        file_name.write(self.license_copy)
        if not os.path.exists(path_to_file) or not self.find_sentence(
                path_to_file, self.name[:-2].upper() + '_H'):
            file_name.write('#ifndef ' + self.name[:-2].upper() + '_H\n')
            file_name.write('#define ' + self.name[:-2].upper() + '_H\n')

        if self.os_type == base_object.FREERTOS_OS:
            file_name.write("/*add includes below */\n")
            file_name.write("#include \"sofi_beremiz.h\"\n")
            file_name.write("#include \"type_def.h\"\n")
            file_name.write("#include \"sofi_init.h\"\n")
            file_name.write("\n")
            file_name.write("/*add includes before */\n")

    @staticmethod
    def add_extern_c_start(file_name):
        file_name.write('\n')
        file_name.write('#if defined __cplusplus\n')
        file_name.write('extern \"C\" {\n')
        file_name.write('#endif\n')

    @staticmethod
    def add_extern_c_stop(file_name):
        file_name.write('\n')
        file_name.write('#if defined __cplusplus\n')
        file_name.write('}\n')
        file_name.write('#endif\n')

    def add_endif_define(self, file_name):
        file_name.write('#endif //' + self.name[:-2].upper() + '_H\n')

    def at_not_used_template(self, line):
        """in header handing for delete functions declaration"""
        for i in range(len(self.not_used_function_decl)):
            f = re.compile(self.not_used_function_decl[i])
            s = f.search(line)
            if s:
                return 1
        return 0


class Beremiz(Header):
    """copy from source beremiz precompiled code
        did't should be in template"""
    name = "beremiz.h"

    def make_header(self):
        path_beremiz = self.path_project_src + self.name
        try:
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_beremiz:
                last_include = re.compile('^\s*#include\s+[<\"]iec_types\.h[>\"]')
                last_include = last_include.search(line)
                line = self.add_matiec_include(line)
                if not self.at_not_used_template(line):
                    file_make.write(line)
                if last_include:
                    self.add_extern_c_start(file_make)
            self.add_extern_c_stop(file_make)
            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_beremiz)


class SofiBeremiz(Header):
    """copy from template"""
    name = "sofi_beremiz.h"

    def make_header(self):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_beremiz:
                file_make.write(line)

            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template)


class SofiDev(Header):
    """copy from template"""
    name = "sofi_dev.h"

    def make_header(self):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_beremiz:
                file_make.write(line)

            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template)


class OsService(Header):
    """copy from template"""
    name = "os_service.h"

    def make_header(self):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_beremiz:
                file_make.write(line)
            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template)


class Config(Header):
    """simply copy from beremiz project with head write"""
    name = "config.h"
    new_name = "config_task.h"

    def make_header(self, archives, iec104s):
        path_beremiz = self.path_project_src + self.name
        try:
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.new_name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            DECLARE_GLOBAL_PROTOTYPE = re.compile(r"\s*__DECLARE_GLOBAL_PROTOTYPE\("
                                                  r"\s*[0-9A-Za-z_]+\s*,"
                                                  r"\s*(?P<NAME>[0-9A-Za-z_]*)\s*\)")
            for line in file_beremiz:
                last_include = re.compile('^\s*#include\s+[<\"]beremiz\.h[>\"]')
                last_include = last_include.search(line)
                line = self.add_matiec_include(line)
                declare_global_prototype = DECLARE_GLOBAL_PROTOTYPE.match(line)
                delete_line = 0
                if declare_global_prototype:
                    if self.vars_from_archives(declare_global_prototype["NAME"], archives):
                        delete_line = 0
                    elif self.vars_from_iec104s(declare_global_prototype["NAME"], iec104s):
                        delete_line = 0
                if delete_line == 0:
                    if last_include:
                        self.add_extern_c_start(file_make)
                        file_make.write("#include \"matiec/accessor.h\"\n")
                        file_make.write("void config_init__(void);\n")
                        file_make.write("void config_run__(unsigned long long tick);\n")
                    elif not self.at_not_used_template(line):
                        file_make.write(line)
            self.add_extern_c_stop(file_make)
            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_beremiz)

    @staticmethod
    def vars_from_archives(name, archives):
        for archive in archives:
            for manage_var in archive["arc_manage_vars"]:
                if name == manage_var["NAME"][8:]:
                    return 1
            for header_var in archive["arc_header_vars"]:
                if name == header_var["NAME"][8:]:
                    return 1
            for user_var in archive["arc_user_vars"]:
                if name == user_var["NAME"][8:]:
                    return 1
        return 0

    @staticmethod
    def vars_from_iec104s(name, iec104s):
        for item in iec104s:
            for user_var in item["vars"]:
                if name == user_var["NAME"][8:]:
                    return 1
        return 0



class PlcMain(Header):
    """copy from template, with head add
       for freertos add include """
    name = "plc_main.h"

    def make_header(self):
        path_template = self.path_template+self.name
        try:
            file_template = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            if self.os_type == base_object.FREERTOS_OS:
                file_make.write("#include \"regs.h\"\n")
            for line in file_template:
                file_make.write(line)
            self.add_endif_define(file_make)
            file_template.close()
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_template + '\n')


class Pous(Header):
    """ MAKE POUS.H FILE, pous.h have all struct fo types in beremiz use"""
    name = "POUS.h"

    def make_header(self):
        path_beremiz = self.path_project_src + self.name
        try:
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_beremiz:
                line = self.add_matiec_include(line)
                if re.search('^\s*#(ifndef)\s+__POUS_H', line):
                    line = self.make_comment(line)
                    file_make.write(line)
                elif re.search('^\s*#(define)\s+__POUS_H', line):
                    line = self.make_comment(line)
                    file_make.write(line)
                    rand_val = random.randint(0x0000000000000000, 0xffffffffffffffff)
                    file_make.write("#define BKRAM_VERIFY_NUMBER_USER {}"
                                    " //random value after compile".format(hex(rand_val)))
                elif re.search('^\s*#endif\s+//\s*__POUS_H', line):
                    line = self.make_comment(line)
                    file_make.write(line)
                elif re.search('^\s*#include\s+\"matiec/accessor\.h\"', line):
                    self.add_extern_c_start(file_make)
                    file_make.write(line)
                else:
                    if not self.at_not_used_template(line):
                        file_make.write(line)

            self.add_extern_c_stop(file_make)
            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()
            #rename all rtc use in file
            self.substitute(self.path_inc+self.name, "RTC", "RTC_BEREMIZ")
        except FileNotFoundError:
            self.print_error('did\'t find '+path_beremiz)


class Resource(Header):
    """get resource from st file and generate"""
    name = ""
    name_list = []
    resource_list = []

    def add_resource(self, resource_list):
        self.resource_list = resource_list
        for l in range(len(resource_list)):
            self.name_list.append(resource_list[l]["name"]+".h")

    def make_headers(self):
        for i in range(len(self.name_list)):
            file_make = open(self.path_inc+self.name_list[i], 'w', encoding="utf-8")
            self.name = self.name_list[i]
            self.add_use_define(file_make)
            file_make.write('#include \"POUS.H\"\n')
            self.add_extern_c_start(file_make)
            list_temp = self.resource_list[i]["program"]
            for j in range(len(list_temp)):
                file_make.write('extern ' + list_temp[j]["program_name"].upper() + ' ' +
                                self.resource_list[i]["name"].upper() + '__' +
                                list_temp[j]["instance"].upper()+';\n')
            file_make.write('extern BOOL FIRST_CYCLE;\n')
            file_make.write('void ' + self.name_list[i][:-2].upper() + '_init__(void);\n')
            file_make.write('void ' + self.name_list[i][:-2].upper() + '_run__(unsigned long long tick);\n')
            self.add_extern_c_stop(file_make)
            self.add_endif_define(file_make)
            file_make.close()


class BeremizTask(Header):
    """header for main file"""
    name = "beremiz_task.h"

    def make_header(self):
        path_template = self.path_template+'freertos/'+self.name
        try:
            file_template = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_template:
                file_make.write(line)
            self.add_endif_define(file_make)

            file_template.close()
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_template + '\n')


class LocatedVariables(Header):
    """set locations var like this
    UDINT * __QD0_1 = &mdb_address_space[1];"""
    name = "LOCATED_VARIABLES.h"

    def make_header(self, locations, vars):
        file_make = open(self.path_inc + self.name, 'w', encoding="utf-8")
        for i in range(len(locations)):
            size = locations[i]['LOC'][-1]
            start_address = locations[i]['LOC'][1] * size
            file_make.write('{} * {} = ({}*)&mdb_address_space[{}];\n'.
                            format(locations[i]['IEC_TYPE'], locations[i]['NAME'],
                                   locations[i]['IEC_TYPE'], str(start_address)))
        for i in range(len(vars)):
            if vars[i]["array"]:
                name = '__' + vars[i]['name']
                file_make.write(
                    '{} * {} = ({}*)&mdb_array_address_space[{}];\n'.format(vars[i]['type'], name,
                                                                            vars[i]['type'], str(vars[i]['start_byte'])))

        file_make.close()


class Matiec(Header):
    """rename rtc in all file in path matiec, should be
        copy to path projekt before!"""
    name = ''

    def rename_rtc(self):
        matiec_path = self.path_inc + "matiec/"
        matiec_head = []
        for (dirpath, dirnames, filenames) in walk(matiec_path):
            matiec_head.extend(filenames)
            break
        for i in range(len(matiec_head)):
            self.substitute(matiec_path + matiec_head[i], "RTC", "RTC_BEREMIZ")

    def iec_types_handing(self):
        matiec_path = self.path_inc + "matiec/"
        iec_types_all = matiec_path + "iec_types_all.h"
        file_temp = open(iec_types_all, 'r', encoding="utf-8")
        temp_buff = ''
        for line in file_temp:
            if re.search("#define\s+FALSE\s+\d", line):
                temp_buff += '  #ifndef FALSE\n'
                temp_buff += '  #define FALSE 0\n'
                temp_buff += '  #endif\n'
            else:
                temp_buff += line
        file_temp.close()
        file_write = open(iec_types_all, 'w', encoding="utf-8")
        file_write.write(temp_buff)
        file_write.close()


class ModbusMaster(Header):
    """copy from template"""
    name = "modbus_master.h"

    def make(self, locations, requests, areas):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            for line in file_beremiz:
                if re.search("#include\s+\"type_def\.h\"", line):
                    file_make.write(line)
                    for i in range(len(locations)):
                        file_make.write("extern {} *{};\n".format(locations[i]['IEC_TYPE'], locations[i]['NAME']))
                elif re.search("#define\s+MODBUS_AREA_COUNT\s+\d+",line):
                    file_make.write("#define MODBUS_AREA_COUNT   {}\n".format(len(areas)))
                elif re.search("#define\s+MDB_REQTS_COUNT\s+\d+", line):
                    file_make.write("#define MDB_REQTS_COUNT {}\n".format(len(requests)))
                else:
                    file_make.write(line)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template )


class ModbusDescription(Header):
    """copy from template"""
    name = "modbus_description.h"

    def make(self, locations, nodes, requests, routes, areas):
        file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
        have_iec104 = 0
        for area in areas:
            if "iec104" in area["LOCATION"]:
                have_iec104 = 1
        if have_iec104:
            file_make.write("#include \"iec104_beremiz.h\"\n")
        file_make.write("/* The total number of nodes, needed to support _all_ instances of the modbus plugin */\n")
        file_make.write("#define TOTAL_TCPNODE_COUNT       0\n")
        file_make.write("#define TOTAL_RTUNODE_COUNT       1\n")
        file_make.write("#define TOTAL_ASCNODE_COUNT       0\n")
        file_make.write("#define TOTAL_ROUT_NODE_COUNT     {}\n".format(len(routes)))
        file_make.write("#define TOTAL_MODBUS_AREA_COUNT   {}\n".format(len(areas)))
        file_make.write("/* Values for instance 1 of the modbus plugin */\n")
        file_make.write("#define MAX_NUMBER_OF_TCPCLIENTS  0\n")
        file_make.write("#define NUMBER_OF_TCPSERVER_NODES 0\n")
        file_make.write("#define NUMBER_OF_TCPCLIENT_NODES 0\n")
        file_make.write("#define NUMBER_OF_TCPCLIENT_REQTS 0\n\n")
        file_make.write("#define NUMBER_OF_RTUSERVER_NODES 0\n")
        file_make.write("#define NUMBER_OF_RTUCLIENT_NODES {}\n".format(len(nodes)))
        file_make.write("#define NUMBER_OF_RTUCLIENT_REQTS {}\n\n".format(len(requests)))
        file_make.write("#define NUMBER_OF_ASCIISERVER_NODES 0\n")
        file_make.write("#define NUMBER_OF_ASCIICLIENT_NODES 0\n")
        file_make.write("#define NUMBER_OF_ASCIICLIENT_REQTS 0\n\n")
        file_make.write("#define NUMBER_OF_SERVER_NODES (NUMBER_OF_TCPSERVER_NODES + \\\n")
        file_make.write("                                NUMBER_OF_RTUSERVER_NODES + \\\n")
        file_make.write("                               NUMBER_OF_ASCIISERVER_NODES)\n\n")
        file_make.write("#define NUMBER_OF_CLIENT_NODES (NUMBER_OF_TCPCLIENT_NODES + \\\n")
        file_make.write("                                NUMBER_OF_RTUCLIENT_NODES + \\\n")
        file_make.write("                                NUMBER_OF_ASCIICLIENT_NODES)\n\n")
        file_make.write("#define NUMBER_OF_CLIENT_REQTS (NUMBER_OF_TCPCLIENT_REQTS + \\\n")
        file_make.write("                                NUMBER_OF_RTUCLIENT_REQTS + \\\n")
        file_make.write("                                NUMBER_OF_ASCIICLIENT_REQTS)\n\n")
        file_make.write("#define MAX_READ_BITS 254\n")
        file_make.write("#define MAX_WORD_NUM 127\n")
        file_make.write("#define MAX_PACKET_LEN (MAX_WORD_NUM*2 + 7)\n\n")
        file_make.write("#define COILS_01 1\n")
        file_make.write("#define INPUT_DISCRETES_02 2\n")
        file_make.write("#define HOLDING_REGISTERS_03 3\n")
        file_make.write("#define INPUT_REGISTERS_04 4\n")
        file_make.write("client_node_t		client_nodes[NUMBER_OF_CLIENT_NODES] = {\n")
        for node in nodes:
            file_make.write("{{\"{}\"/*location*/, {{naf_rtu, {{.rtu = {{{}, {} /*baud*/, {} /*parity*/,"
                            " {} /*data bits*/, {} /*stop bits*/, {} /* ignore echo */}}}}}}/*node_addr_t*/,"
                            " {} /* mb_nd */, 0 /* init_state */,"
                            " {} /* communication period */,0,NULL}},\n".
                            format(node["NODE_NAME"], node["CHANNEL_NAME"],
                                   node["BAUD_RATE"], node["PARITY"], node["DATA_BITS"],
                                   node["STOP_BIT"], node["IGNORE_ECHO"], node["MB_ND"],
                                   node["PERIOD"]))
        file_make.write("};\n\n")
        file_make.write("client_request_t	client_requests[NUMBER_OF_CLIENT_REQTS] = {\n")
        for i in range(len(requests)):
            location_l = [int(k) for k in re.split('[_|.]', requests[i]["LOCATION"]) if k.isdigit()]
            file_make.write("{{\"{}\"/*location*/,{}/*channel*/, {}/*client_node_id*/, {}/*slave_id*/, "
                            "{}/*req_type*/, {}/*mb_function*/, {} /*first reg address*/,\n".
                            format(requests[i]["LOCATION"], nodes[int(requests[i]["NODE_ID"])]["CHANNEL_NAME"],
                                   requests[i]["NODE_ID"], requests[i]["SLAVE_ADDRESS"], requests[i]["REQ_TYPE"],
                                   requests[i]["FUNCTION"], requests[i]["START_ADDRESS"]))
            timeout = int(requests[i]["SEC_TIME_OUT"])*1000
            timeout += int(requests[i]["NANO_SEC_TIME_OUT"])//1000000
            file_make.write("{}/*reg number*/,DEF_REQ_SEND_RETRIES/*retries*/, 0 /* error_code */, 0 /* prev_code */,"
                            " {}/* timeout */,\n".format(requests[i]["REG_NUM"], timeout))
            file_make.write("NULL, NULL, {}/*location0*/, {}/*location1*/, {}/*location2*/ }},\n".format(location_l[0],
                                                                                                         location_l[1],
                                                                                                         location_l[2]))
        file_make.write("};\n\n")
        file_make.write("static route_node_t	route_nodes[TOTAL_ROUT_NODE_COUNT] = {\n")
        for i in range(len(routes)):
            file_make.write("{{{}, {},{},{}}},".format(routes[i]["CHANNEL_FROM"],routes[i]["CHANNEL_TO"],
                                                       routes[i]["ADDRESS"],routes[i]["RES"]))
        file_make.write("};\n\n")
        for i in range(len(areas)):
            array = "{"
            for j in range(int(areas[i]["REGS_NUMBER"])):
                array += str(0)
                array += ","
            array += "}"
            if "archive" not in areas[i]["LOCATION"] and \
                    "iec104" not in areas[i]["LOCATION"]:
                file_make.write("u16 area_buffer_{}[{}] = {};\n".format(i, int(areas[i]["REGS_NUMBER"]), array))
        for area in areas:
            if "archive" in area["LOCATION"]:
                item = [int(s) for s in re.split('[_|.]', area["LOCATION"]) if s.isdigit()][-1]
                if "manage" in area["LOCATION"]:
                    file_make.write("extern u8 archive_manage_buffer_{}[];\n".format(item))
                else:
                    file_make.write("extern u8 archive_read_buffer_{}[];\n".format(item))
            if "iec104" in area["LOCATION"]:
                item = [int(s) for s in re.split('[_|.]', area["LOCATION"]) if s.isdigit()][-1]
                file_make.write("extern iec104_union_{}_t iec104_union_{};\n".format(item, item))
        file_make.write("area_node_t area_nodes[TOTAL_MODBUS_AREA_COUNT] = {\n")
        for area in areas:
            if "archive" in area["LOCATION"]:
                item = [int(s) for s in re.split('[_|.]', area["LOCATION"]) if s.isdigit()][-1]
                location_l = [int(k) for k in re.split('[_|.]', area["LOCATION"]) if k.isdigit()]
                if "manage" in area["LOCATION"]:
                    file_make.write("{{{}, {}, {}, (u8*)archive_manage_buffer_{}, {}/*location0*/,"
                                    " {}/*location1*/}},\n".
                                    format(area["FUNC_TYPE"], area["REGS_NUMBER"],
                                           area["ADDRESS"], item, location_l[0], location_l[1]))
                else:
                    file_make.write("{{{}, {}, {}, (u8*)archive_read_buffer_{}, {}/*location0*/,"
                                    " {}/*location1*/}},\n".
                                    format(area["FUNC_TYPE"], area["REGS_NUMBER"],
                                           area["ADDRESS"], item, location_l[0], location_l[1]))
            elif "iec104" in area["LOCATION"]:
                item = [int(s) for s in re.split('[_|.]', area["LOCATION"]) if s.isdigit()][-1]
                location_l = [int(k) for k in re.split('[_|.]', area["LOCATION"]) if k.isdigit()]
                file_make.write("{{{}, {}, {}, (u8*)iec104_union_{}.bytes, {}/*location0*/,"
                                " {}/*location1*/}},\n".
                                format(area["FUNC_TYPE"], area["REGS_NUMBER"],
                                       area["ADDRESS"], item, location_l[0], location_l[1]))
            else:
                location_l = [int(k) for k in re.split('[_|.]', area["LOCATION"]) if k.isdigit()]
                file_make.write("{{{}, {}, {}, (u8*)area_buffer_{},{}/*location0*/,{}/*location1*/}},\n".
                                format(area["FUNC_TYPE"], area["REGS_NUMBER"],
                                       area["ADDRESS"], area["ID"], location_l[0], location_l[1]))
        file_make.write("};\n\n")

        for i in range(len(locations)):
            file_make.write("{} *{};\n".format(locations[i]['IEC_TYPE'], locations[i]['NAME']))
        for i in range(len(requests)):
            array = "{"
            for j in range(int(requests[i]["REG_NUM"])):
                array += str(0)
                array += ","
            array += "}"
            file_make.write("u16 plcv_buffer_req_{}[1] = {{0}}; // now it's not used \n".format(i))
            file_make.write("u16 com_buffer_req_{}[{}] = {};\n".format(i, int(requests[i]["REG_NUM"]), array))

        file_make.close()


class RegsDescription(Header):
    """copy from template"""
    name = "beremiz_regs_description.h"

    def make_header(self, last_index):
        path_template = self.path_template+"freertos/"+self.name
        try:
            regs_description_template = {"beremiz_regs_var_num_template": str(last_index)}
            file_beremiz = open(path_template, 'r', encoding="utf-8").read() % regs_description_template
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            file_make.write(file_beremiz)
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find '+path_template)


class ArchiveManager(Header):
    """copy from template"""
    name = "archive_manager.h"

    def make(self, archives):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            for line in file_beremiz:
                if re.search("#define\s+ARCHIVES_MANAGERS_NUMBER\s+\d", line):
                    file_make.write("#define ARCHIVES_MANAGERS_NUMBER {}\n".format(len(archives)))
                elif re.search("\/\*USERs\s+STRUCT\s+TYPES\s+START\*\/", line):
                    file_make.write(line)
                    item = 0
                    for archive in archives:
                        file_make.write("typedef struct MCU_PACK{\n")
                        for user_var in archive["arc_user_vars"]:
                            file_make.write("   {}_p *const {};\n".format(user_var['TYPE'][:-2], user_var['NAME']))
                        file_make.write("}}archive_user_data_access_{}_t;\n".format(item))
                        file_make.write("typedef struct MCU_PACK{\n")
                        for user_var in archive["arc_user_vars"]:
                                file_make.write("   {} {};\n".format(user_var['TYPE'][2:-2], user_var['NAME']))
                        file_make.write("}}archive_user_data_{}_t;\n".format(item))
                        item += 1
                elif re.search("\/\*COMPLITE\s+STRUCT\s+TYPES\s+START\*\/", line):
                    file_make.write(line)
                    item = 0
                    for archive in archives:
                        file_make.write("typedef struct MCU_PACK{\n"\
                                        "    const archive_manager_access_t archive_manager_access;\n"\
                                        "    const archive_header_access_t archive_header_access;\n")
                        file_make.write("    const archive_user_data_access_{}_t archive_user_data;\n".format(item))
                        file_make.write("}}archive_complete_{}_t;\n".format(item))
                        file_make.write("#define INDEX_TABLE_SIZE_GA_{} {}\n".format(item, len(archive["arc_user_vars"])))
                        item += 1

                elif re.search("\/\*__DECLARE_GLOBAL_PROTOTYPE\s+start\*\/", line):
                    '''__DECLARE_GLOBAL_PROTOTYPE(DINT,LOCALVAR0)'''
                    file_make.write(line)
                    for archive in archives:
                        for manage_var in archive["arc_manage_vars"]:
                            file_make.write("__DECLARE_GLOBAL_PROTOTYPE({}, {})\n".
                                            format(manage_var["TYPE"][6:-2], manage_var["NAME"][8:]))
                        for header_var in archive["arc_header_vars"]:
                            file_make.write("__DECLARE_GLOBAL_PROTOTYPE({}, {})\n".
                                            format(header_var["TYPE"][6:-2], header_var["NAME"][8:]))
                        for user_var in archive["arc_user_vars"]:
                            file_make.write("__DECLARE_GLOBAL_PROTOTYPE({}, {})\n".
                                            format(user_var["TYPE"][6:-2], user_var["NAME"][8:]))
                else:
                    file_make.write(line)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template )


class IEC104Manager(Header):
    """copy from template"""
    name = "iec104_beremiz.h"

    def make(self, iec104s):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_inc+self.name, 'w', encoding="utf-8")
            for line in file_beremiz:
                if re.search("#define\s+IEC104_BEREMIZ_GROUPS_NUM\s+\d", line):
                    file_make.write("#define IEC104_BEREMIZ_GROUPS_NUM {}\n".format(len(iec104s)))

                elif re.search("\/\*__DECLARE_GLOBAL_PROTOTYPE\s+start\*\/", line):
                    '''__DECLARE_GLOBAL_PROTOTYPE(DINT,LOCALVAR0)'''
                    file_make.write(line)
                    for iec104_item in iec104s:
                        item = [int(s) for s in re.split('[_|.]', iec104_item["location"]) if s.isdigit()][-1]
                        file_make.write("extern iec104_group_description_t iec104_group_description_{};\n".format(item))
                        for var_item in iec104_item["vars"]:
                            file_make.write("extern iec104_description_t iec104_description_{}_{};\n".
                                            format(item, var_item["NAME"]))
                        file_make.write("typedef union {\n")
                        file_make.write("   struct MCU_PACK{\n")
                        for var_item in iec104_item["vars"]:
                            file_make.write("       {} {};\n".format(self.REGS_TYPE_ST_TO_C_CODE[var_item["ORIGIN_TYPE"]],
                                                                     var_item["NAME"]))
                        file_make.write("   } vars;\n")
                        file_make.write("   u8 bytes[{}];\n".format(iec104_item["buffer_size"]))
                        file_make.write("}} iec104_union_{}_t;\n".format(item))
                        for manage_var in iec104_item["vars"]:
                            file_make.write("__DECLARE_GLOBAL_PROTOTYPE({}, {})\n".
                                            format(manage_var["TYPE"][6:-2], manage_var["NAME"][8:]))

                else:
                    file_make.write(line)
            file_beremiz.close()
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find '+path_template )



