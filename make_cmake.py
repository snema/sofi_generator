import re
import base_object
from os import walk


class MakeCmake(base_object.Base):
    name = ""


class CmakeSrc(MakeCmake):
    name = "CMakeLists.txt"

    def make(self):
        file_make = open(self.path_src + self.name, 'w', encoding="utf-8")
        file_make.write("set(SOURCES ${SOURCES}\n")
        file_src = []
        for (dirpath, dirnames, filenames) in walk(self.path_src):
            file_src.extend(filenames)
            break
        for file in file_src:
            if file[-2:] == ".c":
                file_make.write("   ${SOURCES_PREFIX}/" + file + "\n")
        file_make.write("   PARENT_SCOPE)\n")
        if self.os_type == base_object.STRATIFY_OS:
            file_make.write("set(SOURCES_PREFIX ${CMAKE_SOURCE_DIR}/matiec)\n")
            file_make.write("add_subdirectory(matiec)\n")
        file_make.close()


class CmakeMain(MakeCmake):
    name = "CMakeLists.txt"

    def make(self):
        if self.os_type == base_object.STRATIFY_OS:
            path_template = self.path_template + "stratify/" + self.name
        else:
            path_template = self.path_template + "freertos/" + self.name
        file_make = open(self.path_project + self.name, 'w', encoding="utf-8")

        num_lines_beremiz = sum(1 for line in open(path_template, encoding="utf-8"))
        file_template = open(path_template, 'r', encoding="utf-8")
        lines_template = file_template.readlines()
        for current_line in range(num_lines_beremiz):
            if re.search("^\s*set\s*\(\s*LIB_PATH", lines_template[current_line]):
                temp_path = base_object.Base.path_lib.replace("\\", "/")
                file_make.write("set (LIB_PATH " + temp_path + ")\n")
            elif re.search("^\s*COMMAND\s*python", lines_template[current_line]):
                temp_path = base_object.Base.path_beremiz_app.replace("\\", "/")+"python3/python-3.7.2/python"
                file_make.write(lines_template[current_line].replace("python",temp_path))
            else:
                file_make.write(lines_template[current_line])
        file_template.close()
        file_make.close()
        if self.os_type == base_object.STRATIFY_OS:
            file_make = open("sos.bat", 'w', encoding="utf-8")
            file_make.write("cd " + self.path_global + "/beremiz/cmake_arm\n")
            file_make.write("cmake .. -G \"MinGW Makefiles\"\n")
            file_make.write("cmake --build . --target v7em_f4sh\n")
            file_make.close()


class CmakeToolChain(MakeCmake):
    name = "STM32Toolchain.cmake"

    def make(self):
        if self.os_type == base_object.FREERTOS_OS:
            try:
                path_template = self.path_template + "freertos/" + self.name
                file_make = open(self.path_project + self.name, 'w', encoding="utf-8")
                num_lines_beremiz = sum(1 for line in open(path_template, encoding="utf-8"))
                file_template = open(path_template, 'r', encoding="utf-8")
                lines_template = file_template.readlines()
                for current_line in range(num_lines_beremiz):
                    if re.search("^\s*set\s*\(\s*COMPILER_DIRECT", lines_template[current_line]):
                        temp_path = base_object.Base.path_gcc.replace("\\", "/")
                        file_make.write("set (COMPILER_DIRECT " + temp_path + ")\n")
                    else:
                        file_make.write(lines_template[current_line])
                file_template.close()
                file_make.close()
            except FileNotFoundError:
                self.print_error('did\'t find ' + path_template + 'or' +
                                 self.path_project + self.name)

