#1. use base components like template 
#2. use build path from beremiz generated

import argparse
import os
import sys
import time
from distutils.dir_util import copy_tree
import base_object
import make_header
import make_source
import source_analysis
import st_analysis
import h_analysis
import xml_analysis
import make_json
import make_cmake
import subprocess
from subprocess import Popen
import make_ld
import re
from shutil import copyfile
import update_control
import logging
import sofi_plc_loader
__description__ = 'generating code for freertos os from beremiz .c and .st'


def main():
    parser = argparse.ArgumentParser(description=__description__)
    parser.add_argument('-path_b', '--path_beremiz', type=str, default="beremiz",
                        help=('path to pre build beremiz code'
                              '(default: %(default)s)'))

    parser.add_argument('-path_l', '--path_lib', type=str, default="",
                        help=('path to lib sofi location may consist template path'
                              '(default: %(default)s)'))

    parser.add_argument('-path_gcc', '--path_gcc', type=str, default="",
                        help=('path to gcc compiler for arm'
                              '(default: %(default)s)'))

    parser.add_argument('-path_cmake', '--path_cmake', type=str, default="",
                        help=('path to cmake util for build project'
                              '(default: %(default)s)'))

    parser.add_argument('-command', '--command', type=str, default='none',
                        help=('get command'
                              '(default: %(default)s)'))

    parser.add_argument('-ip', '--ip', type=str, default='http://192.168.1.232',
                        help=('ip address off plc'
                              '(default: %(default)s)'))

    parser.add_argument('-path_bin', '--path_bin', type=str, default='',
                        help=('path to built bin'
                              '(default: %(default)s)'))

    args = parser.parse_args()
    '''for stratify'''
    '''path_global = args.path_global + "stratify/"
    if not os.path.exists(path_global + "beremiz/src"):
        os.makedirs(path_global + "beremiz/src")
    base_object.Base.path_global = path_global
    base_object.Base.path_beremiz = args.path_beremiz
    base_object.Base.path_project = base_object.Base.path_global + 'beremiz/'
    base_object.Base.path_src = base_object.Base.path_project + 'src/'
    base_object.Base.path_template = base_object.Base.path_global + '../template/'
    generate(base_object.STRATIFY_OS)
    '''
    '''for sofi'''
    if args.command == "check":
        updater = update_control.Updater(base_object.FREERTOS_OS)
        #updater.get_current_version()
        #updater.get_last_version()
        updater.print_current_and_last_version()
    elif args.command == "update":
        updater = update_control.Updater(base_object.FREERTOS_OS)
        #updater.print_result("sofi start update")
        #updater.get_current_version()
        #updater.get_last_version()
        updater.update_to_last()
    elif args.command == "download_task":
        sofi_plc_loader.main(ip_address=args.ip, load_type='task', bin_path=args.path_bin, min_os_version=base_object.Base.MIN_CONTROLLER_VERSION)
    else:
        make(args.path_beremiz, args.path_lib, args.path_gcc, args.path_cmake)


def make(path_beremiz, path_lib, path_gcc, path_cmake):
    path_global = path_beremiz + "/sofi/"
    if not os.path.exists(path_global + "freertos/src"):
        os.makedirs(path_global + "freertos/src")
    if not os.path.exists(path_global + "freertos/inc"):
        os.makedirs(path_global + "freertos/inc")
    if not os.path.exists(path_global + "freertos/ldscript"):
        os.makedirs(path_global + "freertos/ldscript")

    # beremiz.h
    base_object.Base.path_project_xml = path_beremiz + "../"
    base_object.Base.path_global = path_global + "freertos/"
    base_object.Base.path_project_src = path_beremiz
    base_object.Base.path_lib = path_lib
    base_object.Base.path_project = base_object.Base.path_global
    base_object.Base.path_log = base_object.Base.path_project + 'log.log'
    base_object.Base.path_src = base_object.Base.path_project + 'src/'
    base_object.Base.path_inc = base_object.Base.path_project + 'inc/'
    base_object.Base.path_cmake = path_cmake
    base_object.Base.path_gcc = path_gcc
    base_object.Base.path_beremiz_app  = base_object.Base.path_gcc + '../../'

    base_object.Base.logger = logging.getLogger('Build_procces_logging')
    if not os.path.exists(base_object.Base.path_log):
        file_temp = open(base_object.Base.path_log, 'w', encoding="utf-8")
        file_temp.close()
    base_object.Base.log_path = os.path.join(os.getcwd(), base_object.Base.path_log)
    print(base_object.Base.log_path)
    file_handler = logging.FileHandler(base_object.Base.log_path)
    formatter = logging.Formatter('%(asctime)s | %(levelname)-10s | %(message)s')
    file_handler.setFormatter(formatter)
    base_object.Base.logger.addHandler(file_handler)
    base_object.Base.logger.setLevel(logging.DEBUG)

    print(sys.argv[0], repr(os.getcwd()))
    path_temp = delete_name_from_path(sys.argv[0])
    if path_temp == '':
        base_object.Base.path_template = os.getcwd() + '/template/'
    else:
        base_object.Base.path_template = path_temp + 'template/'
    '''start handing '''
    generate(base_object.FREERTOS_OS)
    '''end handing and generating files '''
    if path_lib != "":
        '''building'''
        print("start building")
        try:
            template = open(base_object.Base.path_template + "freertos/" + "sos.bat", 'r', encoding="utf-8")
            write_bat = open(base_object.Base.path_project + "sos.bat", 'w', encoding="utf-8")
            for line in template:
                write_bat.write(line)
        except FileNotFoundError:
            base_object.print_error('did\'t find ' + base_object.Base.path_template + "freertos/" + "sos.bat")
        #os.system(base_object.Base.path_project + "sos.bat")
        #print(base_object.Base.path_project + "sos.bat")
        cmake_app = base_object.Base.path_cmake + 'cmake.exe'
        p = Popen([cmake_app, '-G', 'MinGW Makefiles', '..', '-DCMAKE_SH=CMAKE_SH-NOTFOUND',
                   '-DCMAKE_MAKE_PROGRAM=' + base_object.Base.path_gcc + '/make.exe'],
                  cwd=base_object.Base.path_project + 'cmake_arm/',stdin=subprocess.PIPE,
                     stdout=subprocess.PIPE)
        out, _ = p.communicate() # wait while procces will terminate
        print(out.decode())
        p = Popen([cmake_app, '--build', '.', '--target', 'all'],
                  cwd=base_object.Base.path_project + 'cmake_arm/',stdin=subprocess.PIPE,
                     stdout=subprocess.PIPE)
        out, _ = p.communicate() # wait while procces will terminate
        if "[100%] Built target" not in str(out):
            base_object.print_error("target doesn't built")
        print(out.decode())
        copy_tree(base_object.Base.path_project + "build",\
                  base_object.Base.path_project + "../../../bin")
        print("Minimal controller os version for this task - {}".format(base_object.Base.MIN_CONTROLLER_VERSION))
        '''subprocess.run([base_object.Base.path_project + 'cmake_arm/cmake', '.. -G \"MinGW Makefiles\"'],
                             shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        
        subprocess.run([base_object.Base.path_project + 'cmake_arm/cmake', '--build . --target all'],
                             shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        
        subprocess.run(base_object.Base.path_project + "cmake_arm cmake .. -G \"MinGW Makefiles\"", shell=True)
        stdout, stderr = p.communicate()
        '''


def generate(os_type):
    # analyze config.c
    analyze_config = source_analysis.Config(os_type)
    analyze_config.common()
    # analyze plc.st
    st = st_analysis.Plc(os_type)
    st.find_resource()
    # analyze location var
    location_var = h_analysis.LocatedVariables(os_type)
    location_var.handle_location()
    config_array = h_analysis.Config(os_type)
    config_array.handle_array_prototype()
    '''mb headers analise'''
    plc_xml = xml_analysis.Plc(os_type)
    plc_xml.find_vars_description()
    plc_xml.find_project_description()
    mb_analise = h_analysis.MB(os_type)
    mb_head_num = mb_analise.find_mb_headers()
    mb_analise.route_and_area_check(location_var.max_address_use/2, plc_xml.max_address/2)
    plc_debbuger = source_analysis.PlcDebugger(os_type)
    plc_debbuger.find_buffers_size()
    plc_debbuger.find_prog_declaration()
    plc_debbuger.find_vars_declaration()
    plc_debbuger.find_array()
    '''xml analise'''
    can_open_analysis = source_analysis.CanOpenModule(os_type)
    can_open_analysis.module_vars()
    archive_analysis = source_analysis.ArchiveModule(os_type)
    archive_analysis.module_vars()
    for archives in archive_analysis.archives:
        mb_analise.add_area("manage" + archives["location"], "HOLDING_REGISTERS_03",
                            archives["manage_buffer_regs_num"], archives["manage_buffer_start_address"],
                            location_var.max_address_use/2, plc_xml.max_address/2)
        mb_analise.add_area("read" + archives["location"], "HOLDING_REGISTERS_03",
                            archives["buffer_size"], archives["buffer_start_address"],
                            location_var.max_address_use/2, plc_xml.max_address/2)
    iec104_analysis = source_analysis.IEC104Module(os_type)
    iec104_analysis.module_vars()
    for item in iec104_analysis.iec104s:
        if item["use_modbus"] > 0:
            mb_analise.add_area("read" + item["location"], "HOLDING_REGISTERS_03",
                                item["buffer_size"], item["buffer_start_address"],
                                location_var.max_address_use / 2, plc_xml.max_address / 2)

    can_open_dict = make_source.CanOpenDictionary(os_type)
    can_open_dict.generate(can_open_analysis)
    header = make_header.Beremiz(os_type)
    header.make_header()
    header = make_header.Config(os_type)
    header.make_header(archive_analysis.archives, iec104_analysis.iec104s)
    # plc_main.h
    header = make_header.PlcMain(os_type)
    header.make_header()
    header = make_header.Pous(os_type)
    header.make_header()
    header = make_header.SofiBeremiz(os_type)
    header.make_header()
    header = make_header.SofiDev(os_type)
    header.make_header()
    header = make_header.OsService(os_type)
    header.make_header()
    #LOCATED_VARIABLES.h
    header_located = make_header.LocatedVariables(os_type)
    header_located.make_header(location_var.self_locations, plc_xml.config_variables_list)

    # make resource headers
    header_resource = make_header.Resource(os_type)
    header_resource.add_resource(st.resource_list)
    header_resource.make_headers()
    header_archive = make_header.ArchiveManager(os_type)
    header_archive.make(archive_analysis.archives)
    header_iec104 = make_header.IEC104Manager(os_type)
    header_iec104.make(iec104_analysis.iec104s)

    # make source
    # config.c
    source_config = make_source.Config(os_type)
    source_config.add_resource(st.resource_list)
    source_config.make_source(archive_analysis.archives,iec104_analysis.iec104s)
    # resource.c
    source_resource = make_source.Resource(os_type)
    source_resource.add_resource(st.resource_list)
    source_resource.make_source()
    # plc_main.c
    source_plc_main = make_source.PlcMain(os_type)
    source_plc_main.make_source()
    # POUS.c
    source_pous = make_source.Pous(os_type)
    source_pous.make_source()
    source_sofi_beremiz = make_source.SofiBeremiz(os_type)
    source_sofi_beremiz.make_source(source_resource.resource_list, analyze_config.tick_time)
    source_sofi_dev = make_source.SofiDev(os_type)
    source_sofi_dev.make_source()
    source_os_service = make_source.OsService(os_type)
    source_os_service.make_source()
    mdb_requests = 0
    print("routes - {} areas - {}".format(mb_analise.routes_num, mb_analise.areas_num))
    if mb_analise.node_check() >= 0:
        base_object.Base.print_progress(base_object.Base, mb_analise.nodes)
        if mb_analise.request_check(location_var.locations) >= 0:
            for i in range(len(mb_analise.requests)):
                base_object.Base.print_progress(base_object.Base, mb_analise.requests[i])
            mdb_requests = 1
            modbus_master_c = make_source.ModbusMaster(os_type)
            modbus_master_c.make(mb_analise.nodes, location_var.masters_locations,
                                 mb_analise.requests, mb_analise.areas)
            modbus_master = make_header.ModbusMaster(os_type)
            modbus_master.make(location_var.masters_locations, mb_analise.requests, mb_analise.areas)
            modbus_master = make_header.ModbusDescription(os_type)
            modbus_master.make(location_var.masters_locations, mb_analise.nodes,
                               mb_analise.requests, mb_analise.routes, mb_analise.areas)
    # main.cpp
    source_main = make_source.Main(os_type)
    source_main.make_source(analyze_config.tick_time, st.resource_list)
    # beremiz_task.h
    header_main = make_header.BeremizTask(os_type)
    header_main.make_header()
    # beremiz_task.c
    source_main = make_source.BeremizTask(os_type)
    source_main.make_source(analyze_config.tick_time, st.resource_list, location_var.max_address_use, mb_head_num,
                            plc_xml.max_address, len(can_open_analysis.files_name), mb_analise.nodes,
                            archive_analysis.archives, iec104_analysis.iec104s)
    # plc_debugger.c
    source_main = make_source.PlcDebugger(os_type)
    source_main.make_source(plc_debbuger.buffer_size, plc_debbuger.prog_decl, plc_debbuger.vars_decl, plc_debbuger.dbg_array_decl, plc_xml.config_variables_list)

    # beremiz_regs_description.c
    archive_ids_table = []
    iec104_ids_table = []
    source_regs_description = make_source.RegsDescription(os_type)
    source_regs_description.make_source(plc_xml.config_variables_list, location_var.self_locations,
                                        mb_head_num, mb_analise.requests, mb_analise.areas, archive_analysis.archives,
                                        iec104_analysis.iec104s, plc_xml.project_name, plc_xml.modification_time, plc_xml.version,
                                        archive_ids_table, iec104_ids_table)
    # beremiz_regs_description.h
    header_regs_description = make_header.RegsDescription(os_type)
    header_regs_description.make_header(source_regs_description.index)
    # archive.c
    source_archive = make_source.ArchiveManager(os_type)
    source_archive.make_source(archive_analysis.archives, archive_ids_table)
    # iec104_beremiz.c
    source_iec104 = make_source.IEC104Manager(os_type)
    source_iec104.make_source(iec104_analysis.iec104s, iec104_ids_table)

    # StratifyLocalSettings.json
    source_json_local = make_json.LocalJson(os_type)
    source_json_local.make()
    # StratifyCommonSettings.json
    source_json_common = make_json.CommonJson(os_type)
    source_json_common.make()
    # src/CmakeList.txt
    src_cmake = make_cmake.CmakeSrc(os_type)
    src_cmake.make()
    # CmakeList.txt
    main_cmake = make_cmake.CmakeMain(os_type)
    main_cmake.make()
    # CmakeToolChain
    toolchain_cmake = make_cmake.CmakeToolChain(os_type)
    toolchain_cmake.make()
    # if not os.path.exists(main_cmake.path_global+"beremiz/src/matiec"):
    copy_tree(main_cmake.path_template+"matiec", main_cmake.path_inc + "matiec")
    copyfile(base_object.Base.path_template + "/freertos/adder.py",
             base_object.Base.path_project + "/adder.py")
    # change matiec header
    # change matiec header end
    header_matiec = make_header.Matiec(os_type)
    header_matiec.rename_rtc()
    # change matiec header end
    # change matiec iec_types_all
    header_matiec.iec_types_handing()
    ld_main = make_ld.Ld(os_type)
    ld_main.make()
    if mdb_requests and modbus_master_c is not None:
        modbus_master_c.modbus_regs_add_retain_flag(mb_analise.requests)
        modbus_master_c.modbus_master_add_pointers(mb_analise.requests)

    if not os.path.exists(main_cmake.path_global+"cmake_arm"):
        os.makedirs(main_cmake.path_global+"cmake_arm")
    if os_type == base_object.STRATIFY_OS:
        if not os.path.exists("sos.bat"):
            print("did'nt find sos.bat!!!")
        p = Popen("sos.bat", shell=True, stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()


def delete_name_from_path(path):
    path = re.sub('\w+\.py', '', path)
    return path


if __name__ == "__main__":
    main()
