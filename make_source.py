import base_object
import re
import fileinput
import functools
import os

class Source(base_object.Base):
    name = ""

    def add_use_define(self, file_name):
        file_name.write("/**\n")
        file_name.write(" * @file " + file_name.name + "\n")
        file_name.write(" * @author Shoma Gane <shomagan@gmail.com>\n")
        file_name.write(" *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>\n")
        file_name.write(" * @defgroup beremiz \n")
        file_name.write(" * @ingroup beremiz \n")
        file_name.write(" * @version 0.1 \n")
        file_name.write(" * @brief  TODO!!! write brief in \n")
        file_name.write(" */\n")
        file_name.write(self.license_copy)
        if re.search("\.cpp", self.name):
            file_name.write('#ifndef ' + self.name[:-4].upper() + '_CPP\n')
            file_name.write('#define ' + self.name[:-4].upper() + '_CPP\n')
        else:
            file_name.write('#ifndef ' + self.name[:-2].upper() + '_C\n')
            file_name.write('#define ' + self.name[:-2].upper() + '_C\n')

    def add_endif_define(self, file_name):
        if re.search("\.cpp", self.name):
            file_name.write('#endif //' + self.name[:-4].upper() + '_CPP\n')
        else:
            file_name.write('#endif //' + self.name[:-2].upper() + '_C\n')


class Config(Source):
    """handing from beremiz projekt file """
    name = "config.c"
    new_name = "config_task.c"
    name_list = []
    resource_list = []

    def add_resource(self, resource_list):
        self.resource_list = resource_list
        for i in range(len(resource_list)):
            self.name_list.append(resource_list[i]["name"]+".h")

    def make_source(self, archives, iec104s):
        path_beremiz = self.path_project_src + '/' + self.name
        num_lines_beremiz = sum(1 for line in open(path_beremiz, encoding="utf-8"))
        file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
        file_make = open(self.path_src+self.new_name, 'w', encoding="utf-8")
        self.add_use_define(file_make)
        lines_beremiz = file_beremiz.readlines()
        DECLARE_ARRAY = re.compile(r"\s*__DECLARE_GLOBAL\(__ARRAY_OF_(?P<TYPE>[0-9A-Za-z_]*)_(?P<SIZE>[0-9\.]*),CONFIG,(?P<NAME>[0-9A-Za-z_]*)\)")
        INIT_ARRAY = re.compile(
            r"\s*__INIT_GLOBAL\(__ARRAY_OF_(?P<TYPE>[0-9A-Za-z_]*)_(?P<SIZE>[0-9\.]*)\,(?P<NAME>[0-9A-Za-z_]*),__INITIAL_VALUE\([0-9A-Za-z_\.\,\}\{]*\),(?P<RETAIN>[0-9A-Za-z_]*)\)")
        INIT_GLOBAL = re.compile(r"\s*__INIT_GLOBAL\(\s*[0-9A-Za-z_]+\s*,\s*(?P<NAME>[0-9A-Za-z_]*),\s*[0-9A-Za-z_\(\),\;\:]+\s*,\s*retain\)")
        DECLARE_GLOBAL = re.compile(r"\s*__DECLARE_GLOBAL\(\s*[0-9A-Za-z_]+\s*,\s*CONFIG\s*,\s*(?P<NAME>[0-9A-Za-z_]*)\s*\)")

        for current_line in range(num_lines_beremiz):
            '''change matiec include if need it'''
            declare_array = DECLARE_ARRAY.match(lines_beremiz[current_line])
            init_array = INIT_ARRAY.match(lines_beremiz[current_line])
            init_global = INIT_GLOBAL.match(lines_beremiz[current_line])
            declare_global = DECLARE_GLOBAL.match(lines_beremiz[current_line])
            lines_beremiz[current_line] = self.add_matiec_include(lines_beremiz[current_line])
            delete_line = 0
            if init_global:
                if self.vars_from_archives(init_global["NAME"], archives):
                    delete_line = 1
                elif self.vars_from_iec104s(init_global["NAME"], iec104s):
                    delete_line = 1
            if declare_global:
                if self.vars_from_archives(declare_global["NAME"], archives):
                    delete_line = 1
                elif self.vars_from_iec104s(declare_global["NAME"], iec104s):
                    delete_line = 1

            if delete_line == 0:
                if re.search("^\s*#include\s+\"\s*POUS\.h\s*\"", lines_beremiz[current_line]):
                    for i in range(len(self.name_list)):
                        file_make.write("#include \"" + self.name_list[i] + "\"\n")
                    file_make.write("#include \"config_task.h\"\n")
                elif re.search("greatest_tick_count__", lines_beremiz[current_line]):
                    file_make.write("unsigned long long greatest_tick_count__ = (unsigned long long)0xffffffffffffffff;\n")
                elif re.search("void\s+config_run__",  lines_beremiz[current_line]):
                    file_make.write("void config_run__(unsigned long long tick){\n")
                elif declare_array:
                    file_make.write("__DECLARE_GLOBAL_LOCATION(__ARRAY_OF_{}_{},__{})\n".format(declare_array["TYPE"],declare_array["SIZE"],declare_array["NAME"]))
                    file_make.write("__DECLARE_GLOBAL_LOCATED(__ARRAY_OF_{}_{},CONFIG,{})\n".format(declare_array["TYPE"],declare_array["SIZE"],declare_array["NAME"]))

                elif init_array:
                    file_make.write("  __INIT_GLOBAL_LOCATED(CONFIG,{},__{},{})\n".format(init_array["NAME"],init_array["NAME"],init_array["RETAIN"]))
                    file_make.write(lines_beremiz[current_line].replace("__INIT_GLOBAL","__INIT_GLOBAL_ARRAY",1))
                else:
                    replaced = 0
                    for name in self.name_list:
                        if re.search("void\s+" + name[:-2].upper(), lines_beremiz[current_line]):
                            file_make.write(self.make_comment(lines_beremiz[current_line]))
                            replaced = 1
                    if not replaced:
                        file_make.write(lines_beremiz[current_line])
        self.add_endif_define(file_make)
        file_make.close()
        file_beremiz.close()

    @staticmethod
    def vars_from_archives(name, archives):
        for archive in archives:
            for manage_var in archive["arc_manage_vars"]:
                if name == manage_var["NAME"][8:]:
                    return 1
            for header_var in archive["arc_header_vars"]:
                if name == header_var["NAME"][8:]:
                    return 1
            for user_var in archive["arc_user_vars"]:
                if name == user_var["NAME"][8:]:
                    return 1
        return 0

    @staticmethod
    def vars_from_iec104s(name, iec104s):
        for item in iec104s:
            for manage_var in item["vars"]:
                if name == manage_var["NAME"][8:]:
                    return 1
        return 0



class Resource(Source):
    """use beremiz projekt files """
    name = ""
    name_list = []
    resource_list = []

    def add_resource(self, resource_list):
        self.resource_list = resource_list
        for i in range(len(resource_list)):
            self.name_list.append(resource_list[i]["name"]+".c")

    def make_source(self):
        for i in range(len(self.name_list)):
            self.name = self.name_list[i]
            path_beremiz = self.path_project_src + self.name
            try:
                file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
                file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
                self.add_use_define(file_make)
                lines_beremiz = file_beremiz.readlines()
                for current_line in range(len(lines_beremiz)):
                    for task_item in self.resource_list[i]["task"]:
                        task_name = task_item["name"].upper()
                        task_tick_control = re.match('^\s*' + task_name + '\s*=\s*!\(\s*tick\s*%\s*(\d{1,6})\s*\)\s*;', lines_beremiz[current_line])
                        if task_tick_control is not None:
                            task_item["divider"] = task_tick_control.group(1)
                        lines_beremiz[current_line] = re.sub('^\s*'+task_name +
                                                                 '\s*=\s*!\(\s*tick\s*%\s*(\d{1,6})\s*\)\s*;',
                                                                 '  '+task_name+' = !((unsigned long)tick%\g<1>);',
                                                                 lines_beremiz[current_line])
                for current_line in range(len(lines_beremiz)):
                    lines_beremiz[current_line] = self.add_matiec_include(lines_beremiz[current_line])
                    if re.search(self.name_list[i][:-2].upper()+"_run__", lines_beremiz[current_line]):
                        file_make.write("void "+self.name_list[i][:-2].upper() +
                                        "_run__(unsigned long long tick){\n")
                        for task_item in self.resource_list[i]["task"]:
                            task_name = task_item["name"].upper()
                            file_make.write('  static BOOL '+ task_name +'_FIRST_CYCLE = 1;\n')
                        for task_item in self.resource_list[i]["task"]:
                            task_name = task_item["name"].upper()
                            file_make.write('  if(tick > ' + task_item["divider"] + ') {' + task_name +'_FIRST_CYCLE = 0;}\n')
                    elif re.search("#include\s*\"\s*POUS\.c\s*\"", lines_beremiz[current_line]):
                        lines_beremiz[current_line] = self.make_comment(lines_beremiz[current_line])
                        file_make.write(lines_beremiz[current_line])
                        file_make.write("#include \"" + self.name_list[i][:-2] + ".h" + '\"\n')
                        file_make.write('#include \"link_functions.h\"\n')
                        file_make.write('extern link_functions_t * p_link_functions;\n')
                        file_make.write("extern osMutexId user_mutex;\n")
                        file_make.write("BOOL FIRST_CYCLE;\n")
                    elif "#include \"config.h\"" in lines_beremiz[current_line]:
                        file_make.write("#include \"config_task.h\"")
                    else:
                        '''for task_item in self.resource_list[i]["task"]:
                            task_name = task_item["name"].upper()
                            task_tick_control = re.match('^\s*' + task_name + '\s*=\s*!\(\s*tick\s*%\s*(\d{1,6})\s*\)\s*;', lines_beremiz[current_line])
                            if task_tick_control is not None:
                                task_item["divider"] = task_tick_control.group(1)
                            lines_beremiz[current_line] = re.sub('^\s*'+task_name +
                                                                 '\s*=\s*!\(\s*tick\s*%\s*(\d{1,6})\s*\)\s*;',
                                                                 '  '+task_name+' = !((unsigned long)tick%\g<1>);',
                                                                 lines_beremiz[current_line])
                        
                            '''
                        if 'body__(' in lines_beremiz[current_line]:
                            task_name = lines_beremiz[current_line-1][lines_beremiz[current_line-1].find('(')+1:lines_beremiz[current_line-1].find(')')]
                            file_make.write('    FIRST_CYCLE = ' + task_name + '_FIRST_CYCLE;\n')
                            file_make.write('    p_link_functions->os_mutex_wait(user_mutex,0xffffffff);\n')
                            file_make.write(lines_beremiz[current_line])
                            file_make.write('    p_link_functions->os_mutex_release(user_mutex);\n')
                        else:
                            file_make.write(lines_beremiz[current_line])
                self.add_endif_define(file_make)
                for task_item in self.resource_list[i]["task"]:
                    task_divider = '#'+task_item["name"].upper()+'_divider'
                    for current_line in range(len(lines_beremiz)):
                        if task_divider in lines_beremiz[current_line]:
                            print("Need replace: "+task_divider+" to "+task_item['divider'])
                file_make.close()
                file_beremiz.close()
            except FileNotFoundError:
                self.print_error('did\'t find ' + path_beremiz)


class PlcMain(Source):
    """use template files"""
    name = "plc_main.c"

    def make_source(self):
        if self.os_type == base_object.STRATIFY_OS:
            path_template = self.path_template + "stratify/" + self.name
        else:
            path_template = self.path_template + "freertos/" + self.name
        try:
            file_template = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_template:
                file_make.write(line)
            file_template.close()
            self.add_endif_define(file_make)
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_template)


class Pous(Source):
    """use beremiz projekt files"""
    name = "POUS.c"
    num_lines = 0
    current_line = 0

    def make_source(self):
        path_beremiz = self.path_project_src + self.name
        try:
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            file_make = open(self.path_src + self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            file_make.write("#include \"POUS.h\"\n")
            file_make.write("#include \"config_task.h\"\n")
            file_make.write("#include \"plc_main.h\"\n")
            self.add_get_external_functions(file_make)
            self.current_line = 0
            while self.current_line < len(lines):
                if self.find_not_used_function(lines[self.current_line]):
                    self.current_line = self.find_end_of_function(lines, self.current_line)
                else:
                    file_make.write(lines[self.current_line])
                self.current_line += 1
            file_beremiz.close()
            self.add_endif_define(file_make)
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_beremiz)

    @staticmethod
    def find_not_used_function(line):
        """ find not used function for name
            return 1 if name the same"""
        if (re.search("void\s+LOGGER_init__\s*\(", line) or
                re.search("void\s+LOGGER_body__", line) or
                re.search("void\s+PYTHON_EVAL_init__", line) or
                re.search("void\s+PYTHON_EVAL_body__", line) or
                re.search("void\s+PYTHON_POLL_init__", line) or
                re.search("void\s+PYTHON_POLL_body__", line) or
                re.search("void\s+PYTHON_GEAR_init__", line) or
                re.search("void\s+PYTHON_GEAR_body__", line) or
                re.search("void\s+GETBOOLSTRING_init__", line) or
                re.search("void\s+GETBOOLSTRING_body__", line) or
                re.search("void\s+BUTTON_init__", line) or
                re.search("void\s+BUTTON_body__", line) or
                re.search("void\s+LED_init__", line) or
                re.search("void\s+LED_body__", line) or
                re.search("void\s+TEXTCTRL_init__", line) or
                re.search("void\s+TEXTCTRL_body__", line)):
            return 1
        else:
            return 0

    @staticmethod
    def find_end_of_function(lines, current_line):
        """ find not used function for name
            return 1 if name the same"""
        while current_line < len(lines):
            if re.search("^\}", lines[current_line]):
                break
            current_line += 1
        return current_line

    @staticmethod
    def add_get_external_functions(file_make):
        """ add functions used for unaligned access"""
        file_make.write("uint64_t GET_EXTERNAL_UNSIGNED(void * value, uint16_t flags){\n"\
                        "   uint64_t temp = 0;\n"\
                        "    memcpy(&temp,value,(flags&0x0f00)>>8);\n"\
                        "    return temp;\n"\
                        "}\n"\
                        "int64_t GET_EXTERNAL_SIGNED(void * value, uint16_t flags){\n"\
                        "    int64_t temp = 0;\n"\
                        "    memcpy(&temp,value,(flags&0x0f00)>>8);\n"\
                        "    return temp;\n"\
                        "}\n"\
                        "float GET_EXTERNAL_REAL(void * value, uint16_t flags){\n"\
                        "    float temp = 0;\n"\
                        "    memcpy(&temp,value,(flags&0x0f00)>>8);\n"\
                        "    return temp;\n"\
                        "}\n"\
                        "double GET_EXTERNAL_LREAL(void * value, uint16_t flags){\n"\
                        "    double temp = 0;\n"\
                        "    memcpy(&temp,value,(flags&0x0f00)>>8);\n"\
                        "    return temp;\n"\
                        "}\n")\


class Main(Source):
    name = "main.cpp"
    num_lines = 0
    current_line = 0

    def make_source(self, tick_time, resource_list):
        if self.os_type == base_object.STRATIFY_OS:
            path_template = self.path_template+'stratify/'+self.name
            try:
                file_template = open(path_template, 'r', encoding="utf-8")
                lines = file_template.readlines()
                file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
                self.add_use_define(file_make)
                self.current_line = 0
                while self.current_line < len(lines):
                    # include <sapi/sys.hpp>
                    if re.search("^\s*#include <sapi/sys\.hpp>", lines[self.current_line]):
                        for i in range(len(resource_list)):
                            file_make.write("#include \"" +
                                            resource_list[i]["name"] + ".h\"\n")

                    if re.search("^\s+common_ticktime__\s*=\s*\d+", lines[self.current_line]):
                        file_make.write('    common_ticktime__ = ' +
                                        tick_time + ";//rewrite from SNEMA\n")
                    else:
                        file_make.write(lines[self.current_line])
                    self.current_line += 1
                file_template.close()
                self.add_endif_define(file_make)
                file_make.close()
            except FileNotFoundError:
                self.print_error('did\'t find ' + path_template)


class BeremizTask(Source):
    """use for generate file with freertos task"""
    name = "beremiz_task.c"
    num_lines = 0
    current_line = 0

    def make_source(self, tick_time, resource_list, local_var_size, mb_heads, array_max_address,
                    can_modules_number, mdb_nodes, archives, iec104s):
        path_template = self.path_template + 'freertos/' + self.name
        try:
            file_template = open(path_template, 'r', encoding="utf-8")
            lines = file_template.readlines()
            file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            self.current_line = 0
            while self.current_line < len(lines):
                # include <sapi/sys.hpp>
                if re.search("^\s*#include \"beremiz_task\.h\"", lines[self.current_line]):
                    for i in range(len(resource_list)):
                        file_make.write("#include \"" +
                                        resource_list[i]["name"] + ".h\"\n")
                    # define MDB_DINAMIC_ADDRESS_SPACE_SIZE 512

                if re.search("^\s+common_ticktime__", lines[self.current_line]):
                    file_make.write("    common_ticktime__ = " +
                                    tick_time + ";//rewrite from SNEMA\n")
                elif re.search("^\s*#define\s+MDB_DINAMIC_ADDRESS_SPACE_SIZE\s+\d+", lines[self.current_line]):
                    if local_var_size > 0:
                        file_make.write("#define MDB_DINAMIC_ADDRESS_SPACE_SIZE "+str(local_var_size) + '\n')
                    else:
                        file_make.write(lines[self.current_line])
                elif re.search("^\s*#define\s+MDB_ARRAY_ADDRESS_SPACE_SIZE\s+\d+", lines[self.current_line]):
                    if array_max_address > 0:
                        file_make.write("#define MDB_ARRAY_ADDRESS_SPACE_SIZE "+str(array_max_address) + '\n')
                    else:
                        file_make.write(lines[self.current_line])

                elif re.search("u8\s+mdb_array_address_space\[MDB_ARRAY_ADDRESS_SPACE_SIZE\];", lines[self.current_line]):
                    file_make.write(lines[self.current_line])
                    if (local_var_size > 0) or (array_max_address > 0):
                        file_make.write("#include \"LOCATED_VARIABLES.h\"\n")
                elif re.search("\#define\s+CAN_FEST_ENABLE\s+\d", lines[self.current_line]):
                    """if we have can support module then add init function"""
                    file_make.write("#define CAN_FEST_ENABLE {}\n".format(int(can_modules_number > 0)))
                elif re.search("\#define\s+ARCHIVE_ENABLE\s+\d", lines[self.current_line]):
                    """if we have can support module then add init function"""
                    file_make.write("#define ARCHIVE_ENABLE {}\n".format(int(len(archives) > 0)))
                elif re.search("\#define\s+IEC104_BEREMIZ_ENABLE\s+\d", lines[self.current_line]):
                    """if we have can support module then add init function"""
                    file_make.write("#define IEC104_BEREMIZ_ENABLE {}\n".format(int(len(iec104s) > 0)))
                elif re.search("\#define\s+MODBUS_MASTER_NODES\s+\d", lines[self.current_line]):
                    file_make.write("#define MODBUS_MASTER_NODES {}\n".format(int(len(mdb_nodes))))
                elif re.search(r"\#define\s+MIN_CONTROLLER_OS_VERSION\s+\{[\d,]+\}", lines[self.current_line]):
                    file_make.write("#define MIN_CONTROLLER_OS_VERSION {{{},{},{},{}}}\n".format(
                        self.MIN_CONTROLLER_VERSION[0], self.MIN_CONTROLLER_VERSION[1], self.MIN_CONTROLLER_VERSION[2],
                        self.MIN_CONTROLLER_VERSION[3]))
                else:
                    file_make.write(lines[self.current_line])
                self.current_line += 1

            self.add_endif_define(file_make)
            file_template.close()
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_template)


class SofiDev(Source):
    """copy from template"""
    name = "sofi_dev.c"

    def make_source(self):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_beremiz:
                file_make.write(line)
            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template )


class SofiBeremiz(Source):
    """copy from template"""
    name = "sofi_beremiz.c"

    def make_source(self, resource_list, tick_time):
        path_template = self.path_template+"freertos/"+self.name
        try:
            task_num = 0
            for resource_item in resource_list:
                for task_item in resource_item["task"]:
                    task_num += 1
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_beremiz:
                if re.search(r"\s*#define\s+TASKS_NUM\s+\d+\s*", line):
                    file_make.write("#define TASKS_NUM {}\n".format(task_num))
                else:
                    if "generator_comment:additional_vars" in line:
                        for resource_item in resource_list:
                            for task_item in resource_item["task"]:
                                if task_item["divider"] is not None:
                                    tick_time_int = int(re.findall(r"\d+", tick_time)[0])
                                    tick_in_ms = (tick_time_int//1000000) * int(task_item["divider"])
                                    task_description = "{{.task_name = \"{}\"," \
                                                       ".task_period_ms = {}}},\n".format(task_item["name"], tick_in_ms)
                                    file_make.write(task_description)
                                    print(str(task_item))
                    file_make.write(line)
            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template )


class OsService(Source):
    """copy from template"""
    name = "os_service.c"

    def make_source(self):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
            self.add_use_define(file_make)
            for line in file_beremiz:
                file_make.write(line)
            self.add_endif_define(file_make)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template )


class ModbusMaster(Source):
    """copy from template"""
    name = "modbus_master.c"
    modbus_request_common_prefix = "modbus_request_MF_"
    modbus_request_prefixs = ["_function", "_slave_address", "_regs_number", "_start_address", "_enable",
                              "_error_counter", "_success_counter"]

    def make(self, nodes, locations, requests, areas):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
            for line in file_beremiz:
                if re.search("\s*static\s+int\s+node_id\[\]\s*\=\s*\{[0-9,]+\}\;",line):
                    array = "{"
                    for i in range(len(nodes)):
                        array += str(i)
                        array += ","
                    array += "}"
                    file_make.write("static int node_id[] = {};\n".format(array))
                    for i in range(len(nodes)):
                        file_make.write("static osThreadId modbus_master_id_{};\n".format(i))

                elif re.search("\s*res\s*=\s*modbus_master_init\(\);", line):
                    file_make.write(line)
                    for i in range(len(nodes)):
                        file_make.write("    uarts_settings_write(client_nodes[{}].node_address.addr.rtu.channel,"
                                        "client_nodes[{}].node_address.addr.rtu.baud,"
                                        "client_nodes[{}].node_address.addr.rtu.data_bits,"
                                        "client_nodes[{}].node_address.addr.rtu.stop_bits,"
                                        "client_nodes[{}].node_address.addr.rtu.parity);\n".
                                        format(i, i, i, i, i))
                    for i in range(len(nodes)):
                        file_make.write("    osThreadDef(modbus_master_{}, modbus_master_thread, osPriorityNormal,"
                                        " 0, MODBUS_THREAD_SIZE);\n".format(i))
                        file_make.write("    modbus_master_id_{} = p_link_functions->os_thread_create"
                                        "(osThread(modbus_master_{}), (void*)&node_id[{}]);\n".format(i, i, i))
                    for i in range(len(requests)):
                        file_make.write("    client_requests[{}].plcv_buffer = NULL; \n".format(i))
                        file_make.write("    client_requests[{}].coms_buffer = (u16*)com_buffer_req_{}; \n".
                                        format(i, i))
                    for i in range(len(locations)):
                        find = 0
                        if len(locations[i]['LOC']) == 5:
                            for j in range(len(requests)):
                                requests_id = list(map(int, requests[j]["LOCATION"].split('_')))
                                if requests_id[0] == locations[i]['LOC'][0] and requests_id[1] == locations[i]['LOC'][1]\
                                   and requests_id[2] == locations[i]['LOC'][2]:
                                    if (requests[j]["FUNCTION"] in ["1", "2", "5", "15"]) and\
                                       (int(locations[i]['LOC'][4]) > 1):
                                        self.print_error("vars type {} must be byte size".format(locations[i]['NAME']))
                                    file_make.write("    {} = (void*)&com_buffer_req_{}[{}];\n".format
                                                    (locations[i]['NAME'], j, locations[i]['LOC'][3]))
                                    find = 1
                                    break
                        if not find:
                            if len(locations[i]['LOC']) == 4:
                                for j in range(len(areas)):
                                    area_id = list(map(int, areas[j]["LOCATION"].split('_')))
                                    if area_id[0] == locations[i]['LOC'][0] and \
                                            area_id[1] == locations[i]['LOC'][1]:
                                        if int(locations[i]['LOC'][3]) == 1:
                                            if (int(locations[i]['LOC'][2]) + int(locations[i]['LOC'][3])) > \
                                                    int(areas[j]["REGS_NUMBER"]):
                                                self.print_error(
                                                    "went out of the array {} , max - {}, use - {}".format(
                                                        locations[i]['NAME'], int(areas[j]["REGS_NUMBER"]),
                                                        (int(locations[i]['LOC'][2]) + int(locations[i]['LOC'][3]))))
                                        else:
                                            if (int(locations[i]['LOC'][2]) + int(locations[i]['LOC'][3])//2) > \
                                                    int(areas[j]["REGS_NUMBER"]):
                                                self.print_error(
                                                "went out of the array {} , max - {}, use - {}".format(
                                                    locations[i]['NAME'], int(areas[j]["REGS_NUMBER"]),
                                                    (int(locations[i]['LOC'][2]) + int(locations[i]['LOC'][3])//2)))
                                        file_make.write("    {} = (void*)&area_buffer_{}[{}];\n".format
                                                        (locations[i]['NAME'], j, locations[i]['LOC'][2]))
                                        find = 1
                                        break
                        if not find:
                            self.print_error("did not find id for location {}".format(locations[i]['NAME']))
                else:
                    file_make.write(line)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+path_template)

    def modbus_regs_add_retain_flag(self, requests):
        """only post proccessing function for ready files
            change non retain modbus register to retain
            changing config_task.c and beremiz_regs_description.c
            :param requests: get it from h_analysis.MB()
            :return: None
        """
        if len(requests):
            config_task = "config_task.c"
            for line in fileinput.input(self.path_src + config_task, inplace=1):
                for j in range(len(requests)):
                    for prefix in self.modbus_request_prefixs:
                        var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + prefix
                        var_name = var_name.upper()
                        init_global = re.compile(
                            r"\s*__INIT_GLOBAL\([\w]+,\s*" + var_name + ",\s*__INITIAL_VALUE\([\d]+\),\s*retain\)")
                        init_global_located = re.compile(
                            r"\s*__INIT_GLOBAL_LOCATED\(CONFIG,\s*" + var_name + ",\s*[_\d\w]+,\s*retain\)")
                        if init_global_located.match(line):
                            line = line.replace("retain", "1")
                        elif init_global.match(line):
                            line = line.replace("retain", "1")
                            if prefix == "_function":
                                line = line.replace("__INITIAL_VALUE(0)", requests[j]["FUNCTION"])
                            if prefix == "_slave_address":
                                line = line.replace("__INITIAL_VALUE(0)", requests[j]["SLAVE_ADDRESS"])
                            if prefix == "_regs_number":
                                line = line.replace("__INITIAL_VALUE(0)", requests[j]["REG_NUM"])
                            if prefix == "_start_address":
                                line = line.replace("__INITIAL_VALUE(0)", requests[j]["START_ADDRESS"])
                            if prefix == "_enable":
                                line = line.replace("__INITIAL_VALUE(0)", "__INITIAL_VALUE(1)")
                print(line)
            beremiz_regs_description = "beremiz_regs_description.c"
            beremiz_regs_description_temp = "beremiz_regs_description.cold"
            file_read = open(self.path_src + beremiz_regs_description, 'r', encoding="utf-8")
            file_write = open(self.path_src + beremiz_regs_description_temp, 'w', encoding="utf-8")
            for line in file_read:
                for j in range(len(requests)):
                    for prefix in self.modbus_request_prefixs:
                        var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + prefix
                        var_name = var_name.upper()
                        if (var_name in line) and ("USER_VARS" in line) and\
                           ("SAVING" not in line):
                            line = line.replace("USER_VARS", "USER_VARS|SAVING")
                file_write.write(line)
            file_read.close()
            file_write.close()
            os.remove(self.path_src + beremiz_regs_description)  # remove original
            os.rename(self.path_src + beremiz_regs_description_temp, self.path_src + beremiz_regs_description)

    def modbus_master_add_pointers(self, requests):
        """only post proccessing function for ready files
            changing modbus_master.c, add pointers on global value
            :param requests: get it from h_analysis.MB()
            :return: None
        """
        modbus_master = "modbus_master.c"
        for line in fileinput.input(self.path_src + modbus_master, inplace=1):
            if "generator_comment:modbus_requests_additional_declaration" in line:
                print("modbus_request_state_t modbus_request_state[{}];\n".format(len(requests)))
                print("static const u16 MODBUS_MASTER_REGS_NUMBERS[{}] = {{".format(len(requests)))
                for request in requests:
                    print("{},".format(request["REG_NUM"]))
                print("};\n")
            elif "generator_comment:modbus_requests_state" in line:
                for j in range(len(requests)):
                    var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + "_function"
                    print(" modbus_request_state[{}].function = __GET_GLOBAL_{}();\n".format(j, var_name.upper()))
                    var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + "_slave_address"
                    print(" modbus_request_state[{}].slave_address = __GET_GLOBAL_{}();\n".format(j, var_name.upper()))
                    var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + "_regs_number"
                    print(" modbus_request_state[{}].regs_number = __GET_GLOBAL_{}();\n".format(j, var_name.upper()))
                    var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + "_start_address"
                    print(" modbus_request_state[{}].start_address = __GET_GLOBAL_{}();\n".format(j, var_name.upper()))
                    var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + "_enable"
                    print(" modbus_request_state[{}].enable = __GET_GLOBAL_{}();\n".format(j, var_name.upper()))
                    var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + "_error_counter"
                    print(" modbus_request_state[{}].error_counter = __GET_GLOBAL_{}();\n".format(j, var_name.upper()))
                    var_name = self.modbus_request_common_prefix + requests[j]["LOCATION"] + "_success_counter"
                    print(" modbus_request_state[{}].success_counter = __GET_GLOBAL_{}();\n".format(j, var_name.upper()))
            print(line)





class PlcDebugger(Source):
    """copy from template"""
    name = "plc_debugger.c"

    def make_source(self, size, prog_decl, vars_decl, dbg_array, config_vars):
        TEMPLATE_BUFFER_SIZE = re.compile(r"^\s*\#define\s+BUFFER_SIZE\s+(?P<SIZE>[0-9]*)")
        TEMPLATE_PROG_DECL = re.compile(r"^\s*\*\s*Declare\s+programs\s*")
        TEMPLATE_VARS_DECL = re.compile(r"^\s*\*\s*Declare\s+global\s+variables\s+from\s+resources\s+and\s+conf\s*")

        path_template = self.path_template + "freertos/" + self.name
        try:
            num_lines_beremiz = sum(1 for line in open(path_template, encoding="utf-8"))
            file_template = open(path_template, 'r', encoding="utf-8")
            lines = file_template.readlines()
            file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
            line = 0
            while line < num_lines_beremiz:
                if TEMPLATE_BUFFER_SIZE.match(lines[line]):
                    #file_make.write("#define BUFFER_SIZE {}".format(size))
                    file_make.write("#define BUFFER_SIZE {}".format(12))
                elif TEMPLATE_PROG_DECL.match(lines[line]):
                    file_make.write(lines[line])
                    line += 1
                    file_make.write(lines[line])
                    file_make.write(prog_decl)
                    self.print_progress(prog_decl)
                elif TEMPLATE_VARS_DECL.match(lines[line]):
                    file_make.write(lines[line])
                    line += 1
                    file_make.write(lines[line])
                    file_make.write(vars_decl)
                    for i in range(len(config_vars)):
                        if config_vars[i]["array"]:
                            name = 'CONFIG__' + config_vars[i]['name']
                            file_make.write('extern __IEC___ARRAY_OF_{}_{}_p {};\n'.format(config_vars[i]['type'],config_vars[i]['size'], name))
                    self.print_progress(vars_decl)
                elif re.search("static\s+dbgvardsc_t\s+dbgvardsc\[\]\s*=\s*\{", lines[line]):
                    file_make.write(lines[line])
                    file_make.write(dbg_array)
                    for i in range(len(config_vars)):
                        if config_vars[i]["array"]:
                            name = 'CONFIG__' + config_vars[i]['name']
                            file_make.write('{{&({}), {}_P_ENUM, {}}},\n'.format(name, config_vars[i]['type'], config_vars[i]['byte_size']))

                else:
                    file_make.write(lines[line])
                line += 1
            file_template.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find '+self.path_project_src+self.name )


class RegsDescription(Source):
    """giving data from xml_analysis"""
    # default save_address description  name  type         ind guid  size flags
    # '{&NULL,0xFFFFFFFF,     {},         {},   U16_REGS_FLAG,0  ,  0 , 1 , 5},//!<"modbus address",&save,&def'
    name = "beremiz_regs_description.c"
    index = 0
    def make_source(self, variables, locations, mb_heads, requests, areas, archives,
                    iec104s, project_name, modification_time,
                    project_version, archive_ids_table, iec104_ids_table):
        path_template = self.path_template + "freertos/" + self.name
        try:
            regs_description_template = {"beremiz_regs_description_template": "\n",
                                         "extern_variables": "\n",
                                         "modbus_master": "\n"}
            project_version_declaration = "static u32 project_version_declaration[] = {"
            for x in project_version:
                project_version_declaration += str(x) + ","
            project_version_declaration += "};\n"
            regs_description_template["extern_variables"] += project_version_declaration
            self.index = 0
            not_available_vars = 0
            common_vars_address = 65536*2
            guid = " GUID_USER_HEAD | " + str(common_vars_address + not_available_vars)
            description_type = self.get_description_type("IEC_UDINT")
            description_var = '{{/*default value*/NULL,/*pointer to value*/(u8*)project_version_declaration,/*saves address*/0,'\
                              '/*description*/\"project name-{} modification time-{} \",/*name*/\"{}\",/*type*/{},/*ind*/{}  ,/*guid*/{} ,'\
                              ' /*size*/{} , /*flags*/USER_VARS|READ_ONLY,/*group number*/4096,/*iec*/NULL}},\n'.format(
                               project_name, modification_time, self.first_var_name, description_type,
                               self.index, guid, len(project_version))
            regs_description_template["beremiz_regs_description_template"] += description_var
            self.index += 1
            not_available_vars += 1
            if mb_heads > 0:
                regs_description_template["modbus_master"] = "#include \"modbus_master.h\"\n"
                for i in range(len(requests)):
                    regs_description_template["modbus_master"] += "extern u16 com_buffer_req_{}[] ;\n".format(i)
                for i in range(len(areas)):
                    regs_description_template["modbus_master"] += "extern u16 area_buffer_{}[] ;\n".format(i)
            if len(archives):
                regs_description_template["archive_include"] = "#include \"archive_manager.h\"\n"
            else:
                regs_description_template["archive_include"] = "\n"
            if len(iec104s):
                regs_description_template["iec104_include"] = "#include \"iec104_beremiz.h\"\n"
            else:
                regs_description_template["iec104_include"] = "\n"

            for i in range(len(variables)):
                '''add config variables'''
                if "noweb" in variables[i]["description"]:
                    self.print_progress("unprintable var {}".format(variables[i]["name"]))
                else:
                    guid = "GUID_USER_HEAD"
                    pointer_or_value = "t"
                    if variables[i]["description"] == "":
                        variables[i]["description"] = variables[i]["name"]
                    flags = "USER_VARS"
                    if variables[i]["retain"] == 1:
                        flags += "|SAVING"
                    if variables[i]["constant"] == 1:
                        flags += "|READ_ONLY"
                    if variables[i]["address"]:
                        # "%MW1.0.0.0"
                        address_list = list(map(int, variables[i]["address"][3:].split('.')))
                        if address_list[0] == 0:
                            pointer_or_value = "p"
                            variable_type = "__IEC_{}_{}".format(variables[i]["type"], pointer_or_value)
                            guid += "|(GUID_USER_ADDRESS_HEAD + "
                            start_address = common_vars_address
                            for j in range(len(locations)):
                                address = variables[i]["address"][1:]
                                address = address.replace(".", "_")
                                if locations[j]['NAME'][2:] == address:
                                    size = locations[j]['LOC'][-1]
                                    start_address = locations[j]['LOC'][1] * size
                                    break
                            p_value = "(u8*)&mdb_address_space[{}]".format(start_address)
                            guid += str(start_address) + " ) "
                        else:
                            pointer_or_value = "p"
                            variable_type = "__IEC_{}_{}".format(variables[i]["type"], pointer_or_value)
                            guid += "|("
                            find = 0
                            start_address = common_vars_address + not_available_vars
                            if len(address_list) == 4:
                                for j in range(len(requests)):
                                    requests_id = list(map(int, requests[j]["LOCATION"].split('_')))
                                    if requests_id[0] == address_list[0] and requests_id[1] == address_list[1] and \
                                       requests_id[2] == address_list[2]:
                                        not_available_vars += 1
                                        p_value = "(u8*)&com_buffer_req_{}[{}]".format(j, address_list[3])
                                        find = 1
                                        break
                            if not find:
                                start_address = common_vars_address + not_available_vars
                                if len(address_list) == 3:
                                    for j in range(len(areas)):
                                        area_id = list(map(int, areas[j]["LOCATION"].split('_')))
                                        if area_id[0] == address_list[0] and \
                                                area_id[1] == address_list[1]:
                                            if areas[j]["FUNC_TYPE"] == 'COILS_01':
                                                guid += "GUID_USER_IS_01_MD + "
                                                start_address = int(areas[j]["ADDRESS"]) + address_list[2]
                                            elif areas[j]["FUNC_TYPE"] == 'INPUT_DISCRETES_02':
                                                guid += "GUID_USER_IS_02_MD + "
                                                start_address = int(areas[j]["ADDRESS"]) + address_list[2]
                                            elif areas[j]["FUNC_TYPE"] == 'HOLDING_REGISTERS_03':
                                                guid += "GUID_USER_IS_03_MD + "
                                                start_address = int(areas[j]["ADDRESS"]) + address_list[2]
                                            elif areas[j]["FUNC_TYPE"] == 'INPUT_REGISTERS_04':
                                                guid += "GUID_USER_IS_04_MD + "
                                                if "READ_ONLY" not in flags:
                                                    flags += "|READ_ONLY"
                                                start_address = int(areas[j]["ADDRESS"]) + address_list[2]
                                            p_value = "(u8*)&area_buffer_{}[{}]".format(j, address_list[2])
                                            find = 1
                                            break
                            if not find:
                                self.print_error("did not find id for location")
                            guid += str(start_address) + " ) "
                    elif variables[i]["array"]:
                        pointer_or_value = "p"
                        variable_type = "__IEC___ARRAY_OF_{}_{}_{}".format(variables[i]["type"],variables[i]['size'], pointer_or_value)
                        p_value = "(u8*)&mdb_array_address_space[{}]".format(variables[i]["start_byte"])
                        guid += "|(GUID_USER_ARRAY_HEAD + "
                        start_address = variables[i]["start_byte"]
                        guid += str(start_address) + " ) "
                    else:
                        variable_type = "__IEC_{}_{}".format(variables[i]["type"], pointer_or_value)
                        guid += " | "+str(common_vars_address + not_available_vars)
                        not_available_vars += 1
                    # 'BEREMIZ_ADDRESS_SPACE_START'
                    description_type = self.get_description_type(variables[i]["type"])
                    variable_name = "CONFIG__{}".format(variables[i]["name"])
                    if pointer_or_value == "t":
                        p_value = "(u8*)&(({} *)(&{}))->value".format(variable_type, variable_name)
                    extern_declaration = 'extern {} {};\n'.format(variable_type, variable_name)
                    description_var = '{{/*default value*/NULL,/*pointer to value*/{},/*saves address*/0,' \
                                      '/*description*/\"{}\",/*name*/\"{}\",/*type*/{},/*ind*/{}  ,/*guid*/{} ,' \
                                      ' /*size*/{} , /*flags*/{},/*group number*/4096,/*iec*/NULL}},\n'.format(
                                        p_value, variables[i]["description"], variables[i]["name"], description_type,
                                        str(self.index), guid, variables[i]["size"], flags)
                    regs_description_template["beremiz_regs_description_template"] += description_var
                    regs_description_template["extern_variables"] += extern_declaration
                    self.index += 1
            item = 0
            for i in range(len(archives)):
                archive_ids_table.append([])
            for archive in archives:
                '''add variables for archives handling'''
                arch_buffer_name = "archive_read_buffer_{}".format(item)
                arch_manage_buffer_name = "archive_manage_buffer_{}".format(item)
                arch_buffer_desc = "archive read buffer {}".format(item)
                regs_description_template["extern_variables"] += "extern u8 {}[];\n".format(arch_buffer_name)
                regs_description_template["extern_variables"] += "extern u8 {}[];\n".format(arch_manage_buffer_name)

                description_type = self.get_description_type("IEC_USINT")
                flags = "USER_VARS|READ_ONLY"
                description_var = '{{/*default value*/NULL,/*pointer to value*/(u8*){},/*saves address*/0,' \
                                  '/*description*/\"{}\",/*name*/\"{}\",/*type*/{},/*ind*/{}  ,/*guid*/GUID_USER_HEAD|{} ,' \
                                  ' /*size*/{} , /*flags*/{},/*group number*/65536,/*iec*/NULL}},\n'.format(
                                    arch_buffer_name, arch_buffer_desc,
                                    arch_buffer_name, description_type,
                                    str(self.index), archive["buffer_start_address"]*2,
                                    archive["buffer_size"], flags)
                regs_description_template["beremiz_regs_description_template"] += description_var
                self.index += 1
                position = 0
                for manage_var in archive["arc_manage_vars"]:
                    flags = "USER_VARS"
                    if manage_var["POLLING"] == "read":
                        flags += "|READ_ONLY"

                    extern_declaration = 'extern {} {};\n'.format(manage_var["TYPE"], manage_var["NAME"])
                    description_type = self.get_description_type(manage_var["TYPE"][2:-2])
                    description_var = '{{/*default value*/NULL,/*pointer to value*/(u8*)&{}[{}],/*saves address*/0,' \
                                      '/*description*/\"{}\",/*name*/\"{}\",/*type*/{},/*ind*/{}  ,/*guid*/GUID_USER_HEAD|{} ,' \
                                      ' /*size*/1 , /*flags*/{},/*group number*/65536,/*iec*/NULL}},\n'.format(
                                       arch_manage_buffer_name, position, manage_var["DESC"], manage_var["NAME"],
                                       description_type, str(self.index), int(manage_var["ADDRESS"])*2, flags)
                    position += self.iec_type[manage_var["TYPE"][2:-2]]
                    regs_description_template["beremiz_regs_description_template"] += description_var
                    regs_description_template["extern_variables"] += extern_declaration
                    self.index += 1
                for header_var in archive["arc_header_vars"]:
                    position += self.iec_type[header_var["TYPE"][2:-2]]
                for user_var in archive["arc_user_vars"]:
                    flags = "USER_VARS"
                    flags += "|READ_ONLY"
                    extern_declaration = 'extern {} {};\n'.format(user_var["TYPE"][:-1]+"p", user_var["NAME"])
                    description_type = self.get_description_type(user_var["TYPE"][2:-2])
                    address_temp = int(float(user_var["ADDRESS"]))*2
                    description_var = '{{/*default value*/NULL,/*pointer to value*/(u8*)&{}[{}],/*saves address*/0,' \
                                      '/*description*/\"{}\",/*name*/\"arc_cur_GA_user_{}_{}\",/*type*/{},/*ind*/{}  ,/*guid*/GUID_USER_HEAD|{} ,' \
                                      ' /*size*/1 , /*flags*/{},/*group number*/65536,/*iec*/NULL}},\n'.format(
                                       arch_manage_buffer_name, position, user_var["DESC"], archive["module_number"],
                                       user_var["NAME"], description_type, str(self.index), address_temp, flags)
                    position += self.iec_type[user_var["TYPE"][2:-2]]
                    regs_description_template["beremiz_regs_description_template"] += description_var
                    regs_description_template["extern_variables"] += extern_declaration
                    archive_ids_table[item].append(self.index)
                    self.index += 1
                flags = "USER_VARS|READ_ONLY"
                guid = str(common_vars_address + not_available_vars)
                description_var = '{{/*default value*/NULL,/*pointer to value*/(u8*)archive_index_table_GA_{},/*saves address*/0,' \
                                  '/*description*/\"{}\",/*name*/\"archive_index_table_GA_{}\",/*type*/{},/*ind*/{}  ,/*guid*/GUID_USER_HEAD|{} ,' \
                                  ' /*size*/INDEX_TABLE_SIZE_GA_{} , /*flags*/{},/*group number*/65536,/*iec*/NULL}},\n'.format(
                    archive["module_number"], arch_buffer_desc,
                    archive["module_number"], self.get_description_type("IEC_UINT"),
                    str(self.index),  guid,
                    archive["module_number"], flags)
                not_available_vars += 1
                regs_description_template["beremiz_regs_description_template"] += description_var
                extern_declaration = 'extern u16 archive_index_table_GA_{}[];\n'.format(archive["module_number"])
                regs_description_template["extern_variables"] += extern_declaration
                self.index += 1
                item += 1
            #iec104 start
            for i in range(len(iec104s)):
                iec104_ids_table.append([])
            for iec104_item in iec104s:
                '''add variables for iec104 handling'''
                item = [int(s) for s in re.split('[_|.]', iec104_item["location"]) if s.isdigit()][-1]
                iec104_buffer_name = "iec104_union_{}.bytes".format(item)
                regs_description_template["extern_variables"] += "extern iec104_union_{}_t iec104_union_{};\n".format(item, item)
                position = 0
                for user_var in iec104_item["vars"]:
                    flags = "USER_VARS"
                    if "retain" in user_var["OPTS"]:
                        flags += "|SAVING"
                    extern_declaration = 'extern {} {};\n'.format(user_var["TYPE"][:-1]+"p", user_var["NAME"])
                    description_type = self.get_description_type(user_var["TYPE"][2:-2])
                    if iec104_item["use_modbus"] > 0:
                        guid = " GUID_USER_HEAD | GUID_USER_IS_03_MD |" + str(iec104_item["buffer_start_address"] + position)
                    else:
                        guid = str(common_vars_address + not_available_vars)
                        not_available_vars += 1
                    description_var = '{{/*default value*/NULL,/*pointer to value*/(u8*)&{}[{}],/*saves address*/0,' \
                                      '/*description*/\"{}\",/*name*/\"{}\",/*type*/{},/*ind*/{}  ' \
                                      ',/*guid*/GUID_USER_HEAD|{} ,' \
                                      ' /*size*/1 , /*flags*/{},/*group number*/135169,/*iec*/&iec104_description_{}_{}}},\n'.format(
                                       iec104_buffer_name, position, user_var["DESC"],
                                       user_var["ORIGIN_NAME"], description_type, str(self.index), guid, flags,
                                       item, user_var["NAME"])
                    position += self.iec_type[user_var["TYPE"][2:-2]]
                    regs_description_template["beremiz_regs_description_template"] += description_var
                    regs_description_template["extern_variables"] += extern_declaration
                    iec104_ids_table[item].append(self.index)
                    self.index += 1
            #iec104end
            file_beremiz = open(path_template, 'r', encoding="utf-8").read() % regs_description_template
            file_make = open(self.path_src + self.name, 'w', encoding="utf-8")
            file_make.write(file_beremiz)
            file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_template)


class CanOpenDictionary(Source):
    name = "can_open_dict.c"
    LAST_CONST_INDEX = 7

    def generate(self, can_open_module):
        path_template = self.path_template + "freertos/" + self.name
        self.print_progress("can open generator\n")
        pdor_parameters = ''
        pdor_parameters_number = 0
        pdot_parameters = ''
        pdot_parameters_number = 0
        sdo_clients = ''
        sdo_clients_number = 0
        sdo_servers_number = 1
        pdor_map = ''
        pdor_map_number = 0
        pdot_map = ''
        pdot_map_number = 0
        vars_dict = ''  # will have pdor pdot and polling sdo
        dict_index_struct = ''
        dict_index_function = ''
        quick_index = ''
        pdo_status_num = ''
        dict_index = []
        pdor = []
        pdot = []
        pdor_map_id = []
        pdot_map_id = []
        sdo_polling = []
        try:
            if len(can_open_module.modules) > 0:
                pdot_current_number = 0
                pdot_temp = []
                for module_dict_temp in can_open_module.modules:
                    for with_pdor in module_dict_temp["module_vars_pdor_dict"]:
                        # vars pdor to TRANSMIT PARAMETERS
                        cob_id = int(with_pdor["OPTS"][-5:], 0) + module_dict_temp["module_number"]
                        if cob_id not in pdot_temp:
                            pdot_parameters_number += 1
                            pdot_temp.append(cob_id)

                for module_dict_temp in can_open_module.modules:
                    # file
                    self.print_progress("vars num" + str(module_dict_temp["vars_num"]))
# --------------------------------------------------------------------------------------------------------------------#
                    for with_pdor in module_dict_temp["module_vars_pdor_dict"]:
                        # vars pdor to TRANSMIT PARAMETERS
                        cob_id = int(with_pdor["OPTS"][-5:], 0) + module_dict_temp["module_number"]
                        if cob_id not in pdot:
                            index = str(hex((pdot_current_number+0x1800) & 0xffff))
                            pdot_current_number += 1
                            index = index[-4:]
                            pdot_parameters += "/* index 0x{}: Transmit PDO {} Parameter. */\n".format(
                                                                index, module_dict_temp["module_number"])
                            pdot_parameters += "static UNS8 dict_highestSubIndex_obj{} = 5; /* number of subindex - 1" \
                                               "*/\n".format(index)
                            pdot_parameters += "static UNS32 dict_obj{}_COB_ID_used_by_PDO = {};	/* {} */\n".format(
                                                                                            index, hex(cob_id), cob_id)
                            pdot_parameters += "static UNS8 dict_obj{}_Transmission_Type = {};	/* every {}" \
                                               " sync sycle */\n".format(index, (pdot_parameters_number//2) % 240,
                                                                         (pdot_parameters_number//2) % 240)

                            pdot_parameters += "static UNS16 dict_obj{}_Inhibit_Time = 0x0;	/* 0 */\n".format(index)
                            pdot_parameters += "static UNS8 dict_obj{}_Compatibility_Entry = 0x0;	/* 0 */\n".format(index)
                            pdot_parameters += "static UNS16 dict_obj{}_Event_Timer = 0;	/* 0 */\n".format(index)
                            pdot_parameters += "static subindex dict_Index{}[] = {{\n".format(index)
                            dict_index.append((index, 0, 0))
                            pdot_parameters += "    {{ RO, uint8, sizeof(UNS8), (void*)&dict_highestSubIndex_obj{}, NULL }},\n".format(index)
                            pdot_parameters += "    {{ RW, uint32, sizeof(UNS32), (void*)&dict_obj{}_COB_ID_used_by_PDO, NULL }},\n".format(index)
                            pdot_parameters += "    {{ RW, uint8, sizeof(UNS8), (void*)&dict_obj{}_Transmission_Type, NULL }},\n".format(index)
                            pdot_parameters += "    {{ RW, uint16, sizeof(UNS16), (void*)&dict_obj{}_Inhibit_Time, NULL }},\n".format(index)
                            pdot_parameters += "    {{ RW, uint8, sizeof(UNS8), (void*)&dict_obj{}_Compatibility_Entry, NULL }},\n".format(index)
                            pdot_parameters += "    {{ RW, uint16, sizeof(UNS16), (void*)&dict_obj{}_Event_Timer, NULL }}\n".format(index)
                            pdot_parameters += "};\n"
                            pdot.append(cob_id)
                        """/* index 0x2000 :   Mapped variable  */
                        static subindex dict_Index200e[] = {
                            { RW, uint16, sizeof (UNS16), (void*)&ao_test[0], NULL }
                        };
                        """
                        index = with_pdor["ADDRESS"][:-4]
                        index = (int(index, 0) & 0x00ff) | \
                                (((module_dict_temp["module_number"]-1) << 8) & 0xffff) | 0x8000
                        vars_dict += "extern __IEC_{}_t {};\n".format(with_pdor["ORIGIN_TYPE"], with_pdor["NAME"])
                        vars_dict += "/* index 0x{} :   Mapped variable  */\n".format(str(hex(index))[-4:])
                        vars_dict += "static subindex dict_Index{}[] = {{\n".format(str(hex(index))[-4:])
                        dict_index.append((str(hex(index))[-4:], 0, 0))
                        vars_dict += "{{ RO, {}, {}, (void*)&{}.value, NULL }}" \
                                     "\n".format(self.REGS_ST_TO_CAN_CODE[with_pdor["ORIGIN_TYPE"]],
                                                 self.REGS_SIZE[with_pdor["ORIGIN_TYPE"]], with_pdor["NAME"])
                        vars_dict += "};\n"
# ---------------------------------------------------------------------------------------------------------------------#
                    for with_pdot in module_dict_temp["module_vars_pdot_dict"]:
                        # vars pdot to RECEIVE PARAMETRS
                        cob_id = int(with_pdot["OPTS"][-5:], 0) + module_dict_temp["module_number"]
                        if cob_id not in pdor:
                            index =str(hex((pdor_parameters_number+0x1400)&0xffff))
                            index = index[-4:]
                            pdor_parameters += "/* index 0x{}: Receive PDO {} Parameter. */\n".\
                                format(index, pdor_parameters_number)
                            pdor_parameters += "static UNS8 dict_highestSubIndex_obj{} = 5;" \
                                               " /* number of subindex - 1*/\n".format(index)
                            pdor_parameters += "static UNS32 dict_obj{}_COB_ID_used_by_PDO = {};" \
                                               "	/* {} */\n".format(index, hex(cob_id), cob_id)
                            pdor_parameters += "static UNS8 dict_obj{}_Transmission_Type = 1;	/* 1 */\n".format(index)
                            pdor_parameters += "static UNS16 dict_obj{}_Inhibit_Time = 0;	/* 0 */\n".format(index)
                            pdor_parameters += "static UNS8 dict_obj{}_Compatibility_Entry = 0;	/* 0 */\n".format(index)
                            pdor_parameters += "static UNS16 dict_obj{}_Event_Timer = 0;	/* 0 */\n".format(index)
                            pdor_parameters += "static subindex dict_Index{}[] = {{\n".format(index)
                            pdor_parameters_number += 1
                            dict_index.append((index, 0, 0))
                            pdor_parameters += "    {{ RO, uint8, sizeof(UNS8), (void*)&dict_highestSubIndex_obj{}," \
                                               " NULL }},\n".format(index)
                            pdor_parameters += "    {{ RW, uint32, sizeof(UNS32), (void*)&dict_obj{}_COB_ID_used_by_PDO," \
                                               " NULL }},\n".format(index)
                            pdor_parameters += "    {{ RW, uint8, sizeof(UNS8), (void*)&dict_obj{}_Transmission_Type," \
                                               " NULL }},\n".format(index)
                            pdor_parameters += "    {{ RW, uint16, sizeof(UNS16), (void*)&dict_obj{}_Inhibit_Time," \
                                               " NULL }},\n".format(index)
                            pdor_parameters += "    {{ RW, uint8, sizeof(UNS8), (void*)&dict_obj{}_Compatibility_Entry," \
                                               " NULL }},\n".format(index)
                            pdor_parameters += "    {{ RW, uint16, sizeof(UNS16), (void*)&dict_obj{}_Event_Timer," \
                                               " NULL }}\n".format(index)
                            pdor_parameters += "};\n"
                            pdor.append(cob_id)
                        index = with_pdot["ADDRESS"][:-4]
                        index = (int(index, 0) & 0x00ff) |\
                                (((module_dict_temp["module_number"]-1) << 8) & 0xffff) | 0x8000
                        vars_dict += "/* index 0x{} :   Mapped variable  */\n".format(str(hex(index))[-4:])
                        vars_dict += "extern __IEC_{}_t {};\n".format(with_pdot["ORIGIN_TYPE"], with_pdot["NAME"])
                        vars_dict += "static subindex dict_Index{}[] = {{\n".format(str(hex(index))[-4:])
                        dict_index.append((str(hex(index))[-4:], 0, 0))
                        vars_dict += "{{ RW, {}, {}, (void*)&{}.value, NULL }}" \
                                     "\n".format(self.REGS_ST_TO_CAN_CODE[with_pdot["ORIGIN_TYPE"]],
                                                 self.REGS_SIZE[with_pdot["ORIGIN_TYPE"]],with_pdot["NAME"])
                        vars_dict += "};\n"

# ---------------------------------------------------------------------------------------------------------------------#

                    dict_descpiption = ""#{ RW, uint32, sizeof (UNS32), (void*)&dict_obj1600[0], NULL },
                    dict_descpiption_num = 0

                    temp_pdot_map_id = []#collect all cob id
                    for with_pdot in module_dict_temp["module_vars_pdot_dict"]:
                        cob_id = int(with_pdot["OPTS"][-5:], 0) + module_dict_temp["module_number"]
                        temp_pdot_map_id.append(cob_id)
                    all_in = 0
                    for with_pdot in module_dict_temp["module_vars_pdot_dict"]:
                        # vars pdot to RECEIVE map
                        """/* index 0x1600 :   Receive PDO 1 Mapping. */
                        static UNS8 dict_highestSubIndex_obj1600 = 2; /* number of subindex - 1*/
                        static UNS32 dict_obj1600[] = {
                            0x20000040	/* 536870920 */,
                            0x20010040	/* 536870920 */,
                        };
                        static subindex dict_Index1600[] = {
                            { RW, uint8, sizeof (UNS8), (void*)&dict_highestSubIndex_obj1600, NULL },
                            { RW, uint32, sizeof (UNS32), (void*)&dict_obj1600[0], NULL },
                            { RW, uint32, sizeof (UNS32), (void*)&dict_obj1600[1], NULL },
                        };

                        """
                        cob_id = int(with_pdot["OPTS"][-5:], 0) + module_dict_temp["module_number"]
                        if cob_id not in pdot_map_id:
                            if all_in:
                                dict_descpiption_num = 0
                                all_in = 0
                                pdor_map += "};\n"
                                dict_descpiption += "};\n"
                            for id in temp_pdot_map_id:
                                if cob_id == id:
                                    all_in += 1
                            pdot_map_id.append(cob_id)
                            index = str(hex((pdor_map_number + 0x1600) & 0xffff))
                            index = index[-4:]
                            pdor_map += "/* index 0x{} :   Receive PDO {} Mapping. */\n".format(index, pdor_map_number)
                            pdor_map += "static UNS8 dict_highestSubIndex_obj{} = {}; \n".format(index, all_in)
                            pdor_map += "static UNS32 dict_obj{}[] = {{\n".format(index)
                            pdor_map_number += 1
                            dict_descpiption += "static subindex dict_Index{}[] = {{\n".format(index)
                            dict_index.append((index, 0, 0))
                            dict_descpiption += "{{ RW, uint8, sizeof (UNS8), (void*)&dict_highestSubIndex_obj{}," \
                                                " NULL }},\n".format(index)
                        address = (int(with_pdot["ADDRESS"], 0) & 0x00ffffff) | \
                                  (((module_dict_temp["module_number"]-1) << 24) & 0xffffffff) | 0x80000000
                        pdor_map += "   {}  ,\n".format(hex(address))
                        dict_descpiption += "{{ RW, uint32, sizeof (UNS32), (void*)&dict_obj{}[{}], NULL }}," \
                                            "\n".format(index, dict_descpiption_num)
                        dict_descpiption_num += 1

                    if all_in and len(module_dict_temp["module_vars_pdot_dict"]):
                        pdor_map += "};\n"
                        if len(dict_descpiption) > 0:
                            dict_descpiption += "};\n"
                            pdor_map += dict_descpiption
                        else:
                            self.print_error("we have not dictionary for map")
# ---------------------------------------------------------------------------------------------------------------------#
                    temp_pdor_map_id = []
                    for with_pdor in module_dict_temp["module_vars_pdor_dict"]:
                        cob_id = int(with_pdor["OPTS"][-5:], 0) + module_dict_temp["module_number"]
                        temp_pdor_map_id.append(cob_id)

                    dict_descpiption = ""#{ RW, uint32, sizeof (UNS32), (void*)&dict_obj1600[0], NULL },
                    dict_descpiption_num = 0
                    all_in = 0
                    for with_pdor in module_dict_temp["module_vars_pdor_dict"]:
                        # vars pdor to TRANSMIT map
                        """/* index 0x1A00 :   Transmit PDO 1 Mapping. */
                        static UNS8 dict_highestSubIndex_obj1A00 = 4; /* number of subindex - 1*/
                        static UNS32 dict_obj1A00[] = {
                            0x200e0010	,
                            0x20100010	,
                            0x20120010	,
                            0x20140010	,
                        };
                        static subindex dict_Index1A00[] = {
                            { RW, uint8, sizeof (UNS8), (void*)&dict_highestSubIndex_obj1A00, NULL },
                            { RW, uint32, sizeof (UNS32), (void*)&dict_obj1A00[0], NULL },
                            { RW, uint32, sizeof (UNS32), (void*)&dict_obj1A00[1], NULL },
                            { RW, uint32, sizeof (UNS32), (void*)&dict_obj1A00[2], NULL },
                            { RW, uint32, sizeof (UNS32), (void*)&dict_obj1A00[3], NULL },
                        };
                        """
                        cob_id = int(with_pdor["OPTS"][-5:], 0) + module_dict_temp["module_number"]
                        if cob_id not in pdor_map_id:
                            if all_in:
                                dict_descpiption_num = 0
                                all_in = 0
                                pdot_map += "};\n"
                                dict_descpiption += "};\n"
                            for id in temp_pdor_map_id:
                                if cob_id == id:
                                    all_in += 1
                            pdor_map_id.append(cob_id)
                            index = str(hex((pdot_map_number + 0x1A00) & 0xffff))
                            index = index[-4:]
                            pdot_map += "/* index 0x{} :   Transmit PDO {} Mapping. */\n".format(index, pdot_map_number)
                            pdot_map += "static UNS8 dict_highestSubIndex_obj{} = {}; \n".format(index, all_in)
                            pdot_map += "static UNS32 dict_obj{}[] = {{\n".format(index)
                            pdot_map_number += 1
                            dict_descpiption += "static subindex dict_Index{}[] = {{\n".format(index)
                            dict_index.append((index, 0, 0))
                            dict_descpiption += "{{ RW, uint8, sizeof (UNS8), (void*)&dict_highestSubIndex_obj{}, NULL }},\n".format(index)
                        dict_descpiption += "{{ RW, uint32, sizeof (UNS32), (void*)&dict_obj{}[{}], NULL }},\n".format(index,dict_descpiption_num)
                        dict_descpiption_num += 1
                        address = (int(with_pdor["ADDRESS"], 0) & 0x00ffffff) | \
                                  (((module_dict_temp["module_number"]-1) << 24) & 0xffffffff) | 0x80000000
                        pdot_map += "   {}    ,\n".format(hex(address))

                    if all_in and len(module_dict_temp["module_vars_pdor_dict"]):
                        pdot_map += "};\n"
                        if len(dict_descpiption) > 0:
                            dict_descpiption += "};\n"
                            pdot_map += dict_descpiption
                        else:
                            self.print_error("we have not dictionary for map")
# ---------------------------------------------------------------------------------------------------------------------#
                    for with_sdo_polling in module_dict_temp["module_sdo_polling"]:
                        """/* index 0x1280 :   Client SDO 1 Parameter. */
                            static UNS8 dict_highestSubIndex_obj1280 = 3; /* number of subindex - 1*/
                            static UNS32 dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO = 0x601;	/*  */
                            static UNS32 dict_obj1280_COB_ID_Server_to_Client_Receive_SDO = 0x581;	/*  */
                            static UNS8 dict_obj1280_Node_ID_of_the_SDO_Server = 0x00;	/* 64 */
                            static subindex dict_Index1280[] ={
                                { RO, uint8, sizeof (UNS8), (void*)&dict_highestSubIndex_obj1280, NULL },
                                { RW, uint32, sizeof (UNS32), (void*)&dict_obj1280_COB_ID_Client_to_Server_Transmit_SDO, NULL },
                                { RW, uint32, sizeof (UNS32), (void*)&dict_obj1280_COB_ID_Server_to_Client_Receive_SDO, NULL },
                                { RW, uint8, sizeof (UNS8), (void*)&dict_obj1280_Node_ID_of_the_SDO_Server, NULL }
                            };
                        """
                        cob_id_transmit = str(hex(0x600 + module_dict_temp["module_number"]))
                        cob_id_receive = str(hex(0x580 + module_dict_temp["module_number"]))
                        if cob_id_transmit not in sdo_polling:
                            sdo_polling.append(cob_id_transmit)
                            index = str(hex((module_dict_temp["module_number"]-1 + 0x1280) & 0xffff))
                            index = index[-4:]
                            sdo_clients_number += 1
                            sdo_clients += "/*index 0x{} :   Client SDO {} Parameter. */\n".format(index,module_dict_temp["module_number"])
                            sdo_clients += "static UNS8 dict_highestSubIndex_obj{} = 3; /* number of subindex - 1*/\n".format(index)
                            sdo_clients += "static UNS32 dict_obj{}_COB_ID_Client_to_Server_Transmit_SDO = {};\n".format(index,cob_id_transmit)
                            sdo_clients += "static UNS32 dict_obj{}_COB_ID_Server_to_Client_Receive_SDO = {};\n".format(index,cob_id_receive)
                            sdo_clients += "static UNS8 dict_obj{}_Node_ID_of_the_SDO_Server = {};	/* 64 */\n".format(index,module_dict_temp["module_number"])
                            sdo_clients += "static subindex dict_Index{}[] ={{\n".format(index)
                            dict_index.append((index, 0, 0))
                            sdo_clients += "{{ RO, uint8, sizeof (UNS8), (void*)&dict_highestSubIndex_obj{}, NULL }},\n".format(index)
                            sdo_clients += "{{ RW, uint32, sizeof (UNS32), (void*)&dict_obj{}_COB_ID_Client_to_Server_Transmit_SDO, NULL }},\n".format(index)
                            sdo_clients += "{{ RW, uint32, sizeof (UNS32), (void*)&dict_obj{}_COB_ID_Server_to_Client_Receive_SDO, NULL }},\n".format(index)
                            sdo_clients += "{{ RW, uint8, sizeof (UNS8), (void*)&dict_obj{}_Node_ID_of_the_SDO_Server, NULL }}\n".format(index)
                            sdo_clients += "};\n"
                        index = with_sdo_polling["ADDRESS"][:-4]
                        index = (int(index, 0) & 0x00ff) | \
                                (((module_dict_temp["module_number"]-1) << 8) & 0xffff) | 0x8000
                        vars_dict +="/* index 0x{} :   Mapped variable  */\n".format(str(hex(index))[-4:])
                        vars_dict += "extern __IEC_{}_t {};\n".format(with_sdo_polling["ORIGIN_TYPE"], with_sdo_polling["NAME"])
                        vars_dict += "static subindex dict_Index{}[] = {{\n".format(str(hex(index))[-4:])
                        dict_index.append((str(hex(index))[-4:], module_dict_temp["module_number"],
                                           with_sdo_polling["ADDRESS"][:-4]))
                        if with_sdo_polling["POLLING"] == "read":
                            write_opt = "SDO|RO"
                        elif with_sdo_polling["POLLING"] == "write":
                            write_opt = "SDO|WO"
                        else:
                            write_opt = "RW"
                        vars_dict += "{{ {}, {}, {}, (void*)&{}.value, NULL " \
                                     "}}\n".format(write_opt, self.REGS_ST_TO_CAN_CODE[with_sdo_polling["ORIGIN_TYPE"]],
                                                   self.REGS_SIZE[with_sdo_polling["ORIGIN_TYPE"]],
                                                   with_sdo_polling["NAME"])
                        vars_dict += "};\n"
# ---------------------------------------------------------------------------------------------------------------------#
                dict_index_function += "const indextable * dict_scanIndexOD (CO_Data *d, UNS16 wIndex, UNS32 * errorCode) {\n"
                dict_index_function += "int i;\n"
                dict_index_function += "switch(wIndex){\n"\
                                       "    case 0x1000: i = 0;break;\n"\
                                       "    case 0x1001: i = 1;break;\n"\
                                       "    case 0x1005: i = 2;break;\n"\
                                       "    case 0x1006: i = 3;break;\n"\
                                       "    case 0x1016: i = 4;break;\n"\
                                       "    case 0x1018: i = 5;break;\n"\
                                       "    case 0x1200: i = 6;break;\n"
# ---------------------------------------------------------------------------------------------------------------------#
# ---------------------------------------------------------------------------------------------------------------------#
                if len(pdor_parameters):
                    file_make = open(self.path_src + "pdor_preference.cold", 'w', encoding="utf-8")
                    file_make.write(pdor_parameters)
                    file_make.close()
                else:
                    dict_index.append(("1400", 0, 0))
                    dict_index.append(("1600", 0, 0))
                    pdor_parameters_number = 1
                    pdor_map_number = 1
                    pdor_parameters += "/*pdor_parameters*/\n" \
                                       "/* index 0x1400 :   Receive PDO 1 Parameter. */\n" \
                                       "static UNS8 dict_highestSubIndex_obj1400 = 5; /* number of subindex - 1*/\n" \
                                       "static UNS32 dict_obj1400_COB_ID_used_by_PDO = 0x180;	/* 385 */\n" \
                                       "static UNS8 dict_obj1400_Transmission_Type = 0x1;	/* 1 */\n" \
                                       "static UNS16 dict_obj1400_Inhibit_Time = 0x0;	/* 0 */\n" \
                                       "static UNS8 dict_obj1400_Compatibility_Entry = 0x0;	/* 0 */\n" \
                                       "static UNS16 dict_obj1400_Event_Timer = 0x0;	/* 0 */\n" \
                                       "static subindex dict_Index1400[] =\n" \
                                       "{\n" \
                                       "    { RO, uint8, sizeof (UNS8), (void*)&dict_highestSubIndex_obj1400, NULL },\n" \
                                       "    { RW, uint32, sizeof (UNS32), (void*)&dict_obj1400_COB_ID_used_by_PDO, NULL },\n" \
                                       "    { RW, uint8, sizeof (UNS8), (void*)&dict_obj1400_Transmission_Type, NULL },\n" \
                                       "    { RW, uint16, sizeof (UNS16), (void*)&dict_obj1400_Inhibit_Time, NULL },\n" \
                                       "    { RW, uint8, sizeof (UNS8), (void*)&dict_obj1400_Compatibility_Entry, NULL },\n" \
                                       "    { RW, uint16, sizeof (UNS16), (void*)&dict_obj1400_Event_Timer, NULL }\n" \
                                       "};\n" \
                                       "/* index 0x1600 :   Receive PDO 1 Mapping. */\n" \
                                       "static UNS8 dict_highestSubIndex_obj1600 = 0; /* number of subindex - 1*/\n" \
                                       "static UNS32 dict_obj1600[] =\n" \
                                       "{\n" \
                                       "};\n" \
                                       "static subindex dict_Index1600[] =\n" \
                                       "{\n" \
                                       "    { RW, uint8, sizeof (UNS8), (void*)&dict_highestSubIndex_obj1600, NULL },\n" \
                                       "    { RW, uint32, sizeof (UNS32), (void*)&dict_obj1600[0], NULL },\n" \
                                       "    { RW, uint32, sizeof (UNS32), (void*)&dict_obj1600[1], NULL },\n" \
                                       "};\n"

                if len(pdot_parameters):
                    file_make = open(self.path_src + "pdot_preference.cold", 'w', encoding="utf-8")
                    file_make.write(pdot_parameters)
                    file_make.close()

                if len(sdo_clients):
                    file_make = open(self.path_src + "sdo_clients.cold", 'w', encoding="utf-8")
                    file_make.write(sdo_clients)
                    file_make.close()

                if len(pdor_map):
                    file_make = open(self.path_src + "pdor_map.cold", 'w', encoding="utf-8")
                    file_make.write(pdor_map)
                    file_make.close()

                if len(pdot_map):
                    file_make = open(self.path_src + "pdot_map.cold", 'w', encoding="utf-8")
                    file_make.write(pdot_map)
                    file_make.close()

                if len(vars_dict):
                    file_make = open(self.path_src + "vars_dict.cold", 'w', encoding="utf-8")
                    file_make.write(vars_dict)
                    file_make.close()
                """
                static const quick_index dict_firstIndex = {
                    6, /* SDO_SVR */
                    7, /* SDO_CLT */
                    9, /* PDO_RCV */
                    10, /* PDO_RCV_MAP */
                    11, /* PDO_TRS */
                    15 /* PDO_TRS_MAP */
                };
                static const quick_index dict_lastIndex = {
                    6, /* SDO_SVR */
                    8, /* SDO_CLT */
                    9, /* PDO_RCV */
                    10, /* PDO_RCV_MAP */
                    14, /* PDO_TRS */
                    18 /* PDO_TRS_MAP */
                };"""
                item = self.LAST_CONST_INDEX-1
                quick_index += "static const quick_index dict_firstIndex = {\n"
                quick_index += "    {}, /* SDO_SVR */\n".format(item)
                item += sdo_servers_number
                quick_index += "    {}, /* SDO_CLT */\n".format(item)
                item += sdo_clients_number
                quick_index += "    {}, /* PDO_RCV */\n".format(item)
                item += pdor_parameters_number
                quick_index += "    {}, /* PDO_RCV_MAP */\n".format(item)
                item += pdor_map_number
                quick_index += "    {}, /* PDO_TRS */\n".format(item)
                item += pdot_parameters_number
                quick_index += "    {}  /* PDO_TRS_MAP */\n".format(item)
                quick_index += "};\n"
                item = self.LAST_CONST_INDEX-1
                quick_index += "static const quick_index dict_lastIndex = {\n"
                quick_index += "    {}, /* SDO_SVR */\n".format(item)
                item += sdo_clients_number
                quick_index += "    {}, /* SDO_CLT */\n".format(item)
                item += pdor_parameters_number
                quick_index += "    {}, /* PDO_RCV */\n".format(item)
                item += pdor_map_number
                quick_index += "    {}, /* PDO_RCV_MAP */\n".format(item)
                item += pdot_parameters_number
                quick_index += "    {}, /* PDO_TRS */\n".format(item)
                item += pdot_map_number
                quick_index += "    {}  /* PDO_TRS_MAP */\n".format(item)
                quick_index += "};\n"
                fill = ""
                if pdor_parameters_number == 0 and pdot_parameters_number == 0:
                    quantity = 1
                else:
                    quantity = pdor_parameters_number+pdot_parameters_number
                for i in range(quantity):
                    if i < pdot_parameters_number:
                        tpdo_shift = (i // 2) % 240
                    else:
                        tpdo_shift = 0
                    fill += "{{{}, TIMER_NONE, TIMER_NONE, Message_Initializer}},".format(tpdo_shift)
                pdo_status_num += "static s_PDO_status dict_PDO_status[{}] = {{{}}};\n".format(quantity, fill)
                item = self.LAST_CONST_INDEX
                dict_index_struct += "static const indextable dict_objdict[] = {\n"\
                                    "    { (subindex*)dict_Index1000,sizeof(dict_Index1000)/sizeof(dict_Index1000[0]), 0x1000, 0},\n"\
                                    "    { (subindex*)dict_Index1001,sizeof(dict_Index1001)/sizeof(dict_Index1001[0]), 0x1001, 0},\n"\
                                    "    { (subindex*)dict_Index1005,sizeof(dict_Index1005)/sizeof(dict_Index1005[0]), 0x1005, 0},\n"\
                                    "    { (subindex*)dict_Index1006,sizeof(dict_Index1006)/sizeof(dict_Index1006[0]), 0x1006, 0},\n"\
                                    "    { (subindex*)dict_Index1016,sizeof(dict_Index1016)/sizeof(dict_Index1016[0]), 0x1016, 0},\n"\
                                    "    { (subindex*)dict_Index1018,sizeof(dict_Index1018)/sizeof(dict_Index1018[0]), 0x1018, 0},\n"\
                                    "    { (subindex*)dict_Index1200,sizeof(dict_Index1200)/sizeof(dict_Index1200[0]), 0x1200, 0},\n"

                dict_int = [(int(i, 16), j, k) for (i, j, k) in dict_index]
                dict_int.sort()
                for dict_temp in dict_int:
                    '''static const indextable dict_objdict[] = {
                            { (subindex*)dict_Index1000,sizeof(dict_Index1000)/sizeof(dict_Index1000[0]), 0x1000},
                        };
                        const indextable * dict_scanIndexOD (CO_Data *d, UNS16 wIndex, UNS32 * errorCode) {
                        int i;
                        switch(wIndex){
                            case 0x1000: i = 0;break;
                            default:
                                *errorCode = OD_NO_SUCH_OBJECT;
                                return NULL;
                            }
                            *errorCode = OD_SUCCESSFUL;
                            return &dict_objdict[i];
                        }
                    '''
                    ind_hex = str(hex(dict_temp[0]))[-4:]
                    if dict_temp[1] != 0:
                        dict_index_struct += "    {{ (subindex*)dict_Index{},sizeof(dict_Index{})/sizeof(dict_Index{}[0])" \
                                             ", {}, {}}},\n".format(ind_hex, ind_hex, ind_hex, dict_temp[2],
                                                                      dict_temp[1])
                    else:
                        dict_index_struct += "    {{ (subindex*)dict_Index{},sizeof(dict_Index{})/sizeof(dict_Index{}[0])" \
                                             ", 0x{}, {}}},\n".format(ind_hex, ind_hex, ind_hex, ind_hex,
                                                                      dict_temp[1])

                    dict_index_function += "        case 0x{}: i = {};break;\n".format(ind_hex, item)
                    item += 1
                dict_index_struct += "};\n"
                dict_index_function += "    default:\n"\
                                       "        *errorCode = OD_NO_SUCH_OBJECT;\n"\
                                       "        return NULL;\n"\
                                       "    }\n"\
                                       "    *errorCode = OD_SUCCESSFUL;\n"\
                                       "    return &dict_objdict[i];\n"\
                                       "}\n"
                if len(dict_index_struct):
                    file_make = open(self.path_src + "dict_index_struct.cold", 'w', encoding="utf-8")
                    file_make.write(dict_index_struct)
                    file_make.close()

                if len(dict_index_function):
                    file_make = open(self.path_src + "dict_index_function.cold", 'w', encoding="utf-8")
                    file_make.write(dict_index_function)
                    file_make.close()

                file_template = open(path_template, 'r', encoding="utf-8")
                lines = file_template.readlines()
                file_template.close()
                file_generate = ""
                for line in lines:
                    file_generate += line
                    if "/*sdo_clients*/" in line:
                        file_generate += sdo_clients
                    if "/*pdor_parameters*/" in line:
                        file_generate += pdor_parameters
                    if "/*pdot_parameters*/" in line:
                        file_generate += pdot_parameters
                    if "/*pdor_map*/" in line:
                        file_generate += pdor_map
                    if "/*pdot_map*/" in line:
                        file_generate += pdot_map
                    if "/*vars_dict*/" in line:
                        file_generate += vars_dict
                    if "/*dict_index_struct*/" in line:
                        file_generate += dict_index_struct
                    if "/*dict_index_function*/" in line:
                        file_generate += dict_index_function
                    if "/*quick_index*/" in line:
                        file_generate += quick_index
                    if "/*pdo_status_num*/" in line:
                        file_generate += pdo_status_num


                file_make = open(self.path_src + self.name, 'w', encoding="utf-8")
                file_make.write(file_generate)
                file_make.close()
        except FileNotFoundError:
            self.print_error('did\'t find ' + path_template)


class ArchiveManager(Source):
    """copy from template"""
    name = "archive_manager.c"

    def make_source(self, archives, archive_ids_table):
        path_template = self.path_template+"freertos/"+self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_src+self.name, 'w', encoding="utf-8")
            for line in file_beremiz:
                if re.search("\/\*ARCHIVE\s+VARS\s+START\*\/", line):
                    file_make.write(line)
                    item = 0
                    for archive in archives:
                        file_make.write("u8 archive_read_buffer_{}[{}] = {{0}};\n".format(item, archive["buffer_size"]))
                        file_make.write("u8 archive_write_buffer_{}[{}] = {{0}};\n".format(item, archive["buffer_size"]))
                        file_make.write("u8 archive_manage_buffer_{}[{}+ARCHIVE_MANAGER_STRUCTURE_SIZE] = {{0}};\n".
                                        format(item, archive["buffer_size"]))
                        file_make.write("static const u32 archive_file_crc_{} = {};\n".format(item, hex(archive["file_crc"])))
                        archive_ids_templ = functools.reduce(lambda a, b: a+str(b)+",", archive_ids_table[item], "")
                        print("archive ids - " + archive_ids_templ)
                        file_make.write("u16 archive_index_table_GA_{}[INDEX_TABLE_SIZE_GA_{}] = {{{}}};\n".format(item, item, archive_ids_templ))
                        item += 1
                elif re.search("\/\*ARCHIVE\s+STRUCTURES\s+START\*\/", line):
                    file_make.write(line)
                    item = 0
                    for archive in archives:
                        file_make.write("static archive_complete_{}_t archive_complete_{} = {{\n".format(item, item))
                        file_make.write("/*manage vars */\n")
                        file_make.write("{")
                        for manage_var in archive["arc_manage_vars"]:
                            file_make.write("&{},".format(manage_var["NAME"]))
                        file_make.write("}\n/*header vars*/,\n{")
                        for header_var in archive["arc_header_vars"]:
                            file_make.write("&{},".format(header_var["NAME"]))
                        file_make.write("}\n/*user vars*/,\n{")
                        for user_var in archive["arc_user_vars"]:
                            file_make.write("&{},".format(user_var["NAME"]))
                        file_make.write("}};\n")
                        item += 1
                    item = 0
                    file_make.write("static void *const archives_completes[ARCHIVES_MANAGERS_NUMBER] = {\n")
                    for archive in archives:
                        file_make.write("&archive_complete_{},\n".format(item))
                        item += 1
                    file_make.write("};\n")
                elif re.search("\/\*archive\s+manager\s+preinit\s+vars\s+start\*\/", line):
                    file_make.write(line)
                    item = 0
                    for archive in archives:
                        file_make.write("   *archive_complete_{}.archive_manager_access."
                                        "save_arc->value = 0;\n".format(item))
                        file_make.write("   *archive_complete_{}.archive_manager_access."
                                        "arc_for_read->value = 0;\n".format(item))
                        file_make.write("   *archive_complete_{}.archive_manager_access."
                                        "id_number->value = {};\n".format(item, item))
                        file_make.write("   *archive_complete_{}.archive_manager_access."
                                        "body_len->value = {};\n".format(item, archive["buffer_size"]))
                        file_make.write("   *archive_complete_{}.archive_manager_access."
                                        "unix_time_last_arc->value = 0;\n".format(item))
                        file_make.write("   *archive_complete_{}.archive_manager_access."
                                        "arcs_number->value = 0;\n".format(item))
                        file_make.write("   *archive_complete_{}.archive_manager_access."
                                        "last_readed->value = 0;\n".format(item))
                        file_make.write("   *archive_complete_{}.archive_manager_access."
                                        "first_available->value = 0;\n".format(item))
                        file_make.write("   *archive_complete_{}.archive_header_access."
                                        "id_number->value = {};\n".format(item, item))
                        file_make.write("   *archive_complete_{}.archive_header_access."
                                        "body_len->value = {};\n".format(item, archive["buffer_size"]))
                        file_make.write("   *archive_complete_{}.archive_header_access."
                                        "id_crc->value = archive_file_crc_{};\n".format(item, item))
                        file_make.write("   for (u16 i =0;i<INDEX_TABLE_SIZE_GA_{};i++){{\n"
                                        "       archive_index_table_GA_{}[i] += regs_template.ind;}}\n".format(item, item))

                        item += 1
                elif re.search("\/\*handle\s+for\s+create\s+archive\s+START\*\/", line):
                    file_make.write(line)
                    item = 0
                    for archive in archives:
                        file_make.write("       if(archive_manager_save_arc_control[{}]){{\n".format(item))
                        file_make.write("           arc_mail_handle.command = ARC_MAIL_ADD;\n")
                        file_make.write("           arc_mail_handle.buffer.data = archive_write_buffer_{};\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header = "
                                        "(archive_header_t *)archive_write_buffer_{};\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header->id_number ="
                                        " *archive_complete_{}.archive_manager_access.id_number->value;\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header->body_len ="
                                        " *archive_complete_{}.archive_manager_access.body_len->value;\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header->id_crc ="
                                        " *archive_complete_{}.archive_header_access.id_crc->value;\n".format(item))
                        data_user_position = 0
                        for user_var in archive["arc_user_vars"]:
                            file_make.write("           memcpy(&archive_write_buffer_{}[sizeof(archive_header_t)+{}],"
                                            "archive_complete_{}.archive_user_data.{}->value,sizeof({}));\n"
                                            .format(item, data_user_position, item, user_var["NAME"],
                                                    user_var['TYPE'][2:-2]))
                            data_user_position += self.iec_type[user_var['TYPE'][2:-2]]
                        file_make.write("           arc_mail_handle.state = ARC_IN_HANDLE;\n")
                        file_make.write("           p_link_functions->os_signal_set(sofi_arc_thread_id,"
                                        " (int32_t)&arc_mail_handle);\n")
                        file_make.write("           u32 time_temp = p_link_functions->os_kernel_sys_tick();\n"
                                        "           while((arc_mail_handle.state & ARC_IN_HANDLE) && \n"
                                        "               ((p_link_functions->os_kernel_sys_tick()-time_temp)<"
                                        "   ARCHIVE_MANAGER_ADD_GET_TIMEOUT)){{\n"
                                        "               p_link_functions->os_delay(3);\n"
                                        "           }}\n"
                                        "           if ((arc_mail_handle.state & ARC_FLAGS_ERROR_WHILE_HANDLE) ||\n"
                                        "               (arc_mail_handle.state & ARC_IN_HANDLE)){{\n"
                                        "               p_link_functions->led_error_on(INIT_ERROR_FATAL);\n"
                                        "           }}else{{\n"
                                        "              archive_manager_save_arc_control[{}] = 0;\n"
                                        "           }}\n".format(item))
                        file_make.write("       }\n")
                        item += 1
                elif re.search("\/\*handle\s+for\s+read\s+archive\s+START\*\/", line):
                    file_make.write(line)
                    item = 0
                    for archive in archives:
                        file_make.write("       if(archive_manager_arc_for_read_control[{}]){{\n".format(item))
                        file_make.write("           memset(archive_read_buffer_{},0,"
                                        "*archive_complete_{}.archive_manager_access.body_len->value);"
                                        "\n".format(item, item))
                        file_make.write("           arc_mail_handle.command = ARC_MAIL_GET;\n")
                        file_make.write("           arc_mail_handle.buffer.data = archive_read_buffer_{};"
                                        "\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header = "
                                        "(archive_header_t *)archive_read_buffer_{};\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header->id_number ="
                                        " *archive_complete_{}.archive_manager_access.id_number->value;\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header->body_len ="
                                        " *archive_complete_{}.archive_manager_access.body_len->value;\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header->id_crc ="
                                        " *archive_complete_{}.archive_header_access.id_crc->value;\n".format(item))
                        file_make.write("           arc_mail_handle.buffer.arc_header->number ="
                                        " *archive_complete_{}.archive_manager_access.arc_for_read->value;\n".format(item))
                        file_make.write("           arc_mail_handle.state = ARC_IN_HANDLE;\n")
                        file_make.write("           arc_mail_handle.arc_file_title = &arc_file_title;\n")
                        file_make.write("           p_link_functions->os_signal_set(sofi_arc_thread_id, "
                                        "(int32_t)&arc_mail_handle);\n")
                        file_make.write("           u32 time_temp = p_link_functions->os_kernel_sys_tick();\n"
                                        "           while((arc_mail_handle.state & ARC_IN_HANDLE) && \n"
                                        "               ((p_link_functions->os_kernel_sys_tick()-time_temp)<"
                                        "   ARCHIVE_MANAGER_ADD_GET_TIMEOUT)){\n"
                                        "               p_link_functions->os_delay(3);\n"
                                        "           }\n"
                                        "           if (arc_mail_handle.state & ARC_FLAGS_ERROR_WHILE_HANDLE){\n"
                                        "               p_link_functions->led_error_on(INIT_ERROR_FATAL);\n"
                                        "           }else if(arc_mail_handle.state & ARC_NOT_READED){\n"
                                        "           }else{\n")
                        file_make.write("               archive_header_p = (archive_header_t *)archive_read_buffer_{};\n"
                                        "               *archive_complete_{}.archive_manager_access.unix_time_last_arc"
                                        "->value = arc_file_title.unix_time_last_arc;\n"
                                        "               *archive_complete_{}.archive_manager_access.arcs_number->"
                                        "value = arc_file_title.arcs_number;\n"
                                        
                                        "               *archive_complete_{}.archive_manager_access.last_readed->"
                                        "value = archive_header_p->number;\n"
                                        "               *archive_complete_{}.archive_manager_access.first_available->"
                                        "value = arc_file_title.first_available;\n"
                                        "               archive_manager_arc_for_read_control[{}] = 0;\n"
                                        "           }}\n".format(item, item, item, item, item, item, item))
                        file_make.write("       }\n")
                        item += 1
                elif re.search("\/\*__DECLARE_GLOBAL_LOCATED\s+start\*\/", line):
                    '''__DECLARE_GLOBAL_LOCATED(BOOL,CONFIG,LOCALVAR1)'''
                    file_make.write(line)
                    for archive in archives:
                        for manage_var in archive["arc_manage_vars"]:
                            file_make.write("__DECLARE_GLOBAL_LOCATED({}, CONFIG, {})\n".
                                            format(manage_var["TYPE"][6:-2], manage_var["NAME"][8:]))
                        for header_var in archive["arc_header_vars"]:
                            file_make.write("__DECLARE_GLOBAL_LOCATED({}, CONFIG, {})\n".
                                            format(header_var["TYPE"][6:-2], header_var["NAME"][8:]))
                        for user_var in archive["arc_user_vars"]:
                            file_make.write("__DECLARE_GLOBAL_LOCATED({}, CONFIG, {})\n".
                                            format(user_var["TYPE"][6:-2], user_var["NAME"][8:]))
                elif re.search("\/\*__INIT_GLOBAL_LOCATED\s+start\*\/", line):
                    """  __INIT_GLOBAL_LOCATED(CONFIG,LOCALVAR1,__MX1_3_0,retain)"""
                    file_make.write(line)
                    item = 0
                    for archive in archives:
                        position = 0
                        for manage_var in archive["arc_manage_vars"]:
                            file_make.write("   __INIT_GLOBAL_LOCATED(CONFIG, {},"
                                            " (void*)&archive_manage_buffer_{}[{}], 0)\n".
                                            format(manage_var["NAME"][8:], item, position))
                            position += self.iec_type[manage_var["TYPE"][2:-2]]
                        for header_var in archive["arc_header_vars"]:
                            file_make.write("   __INIT_GLOBAL_LOCATED(CONFIG, {},"
                                            " (void*)&archive_manage_buffer_{}[{}], 0)\n".
                                            format(header_var["NAME"][8:], item, position))
                            position += self.iec_type[header_var["TYPE"][2:-2]]
                        for user_var in archive["arc_user_vars"]:
                            file_make.write("   __INIT_GLOBAL_LOCATED(CONFIG, {},"
                                            " (void*)&archive_manage_buffer_{}[{}], 0)\n".
                                            format(user_var["NAME"][8:], item, position))
                            position += self.iec_type[user_var["TYPE"][2:-2]]
                        item += 1
                else:
                    file_make.write(line)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find ' + path_template)


class IEC104Manager(Source):
    """copy from template"""
    name = "iec104_beremiz.c"

    def make_source(self, iec104s, iec104_ids_table):
        path_template = self.path_template + "freertos/" + self.name
        try:
            file_beremiz = open(path_template, 'r', encoding="utf-8")
            file_make = open(self.path_src + self.name, 'w', encoding="utf-8")
            for line in file_beremiz:
                if re.search("\/\*IEC104\s+VARS\s+START\*\/", line):
                    file_make.write(line)
                    for iec104_item in iec104s:
                        item = [int(s) for s in re.split('[_|.]', iec104_item["location"]) if s.isdigit()][-1]
                        file_make.write("iec104_union_{}_t iec104_union_{} = {{\n".format(item, item))
                        for var_item in iec104_item["vars"]:
                            file_make.write("       .vars.{}={},\n".format(var_item["NAME"],
                                                                           var_item["INIT_VALUE"]))
                        file_make.write("   } ;\n")
                        file_make.write("iec104_group_description_t iec104_group_description_{} = {{\n".format(item))
                        file_make.write("   .originator_address = (void*)&iec104_union_{}.bytes[0],\n".format(item, iec104_item["ORIGINATOR_ADDRESS_NAME"]))
                        file_make.write("   .common_address_asdu = (void*)&iec104_union_{}.bytes[2]\n".format(item, iec104_item["COMMON_ADDRESS_NAME"]))
                        file_make.write(" };\n")
                        for var_item in iec104_item["vars"]:
                            file_make.write("iec104_description_t iec104_description_{}_{} = {{\n".
                                            format(item,var_item["NAME"]))
                            file_make.write("   .iec104_group_description = &iec104_group_description_{},\n".format(item))
                            file_make.write("   .iec104_address = {}\n".format(var_item["ADDRESS"]))
                            file_make.write(" };\n")



                elif re.search("\/\*__DECLARE_GLOBAL_LOCATED\s+start\*\/", line):
                    '''__DECLARE_GLOBAL_LOCATED(BOOL,CONFIG,LOCALVAR1)'''
                    file_make.write(line)
                    for iec104_item in iec104s:
                        for var_item in iec104_item["vars"]:
                            file_make.write("__DECLARE_GLOBAL_LOCATED({}, CONFIG, {})\n".
                                            format(var_item["TYPE"][6:-2], var_item["NAME"][8:]))
                elif re.search("\/\*__INIT_GLOBAL_LOCATED\s+start\*\/", line):
                    """  __INIT_GLOBAL_LOCATED(CONFIG,LOCALVAR1,__MX1_3_0,retain)"""
                    file_make.write(line)
                    for iec104_item in iec104s:
                        position = 0
                        item = [int(s) for s in re.split('[_|.]', iec104_item["location"]) if s.isdigit()][-1]
                        for var_item in iec104_item["vars"]:
                            retain = int("retain" in var_item["OPTS"])
                            file_make.write("   __INIT_GLOBAL_LOCATED(CONFIG, {},"
                                            " (void*)&iec104_union_{}.bytes[{}], {})\n".
                                            format(var_item["NAME"][8:], item, position,retain))
                            position += self.iec_type[var_item["TYPE"][2:-2]]
                else:
                    file_make.write(line)
            file_beremiz.close()
            file_make.close()

        except FileNotFoundError:
            self.print_error('did\'t find ' + path_template)
