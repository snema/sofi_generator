import re
import os
import sys
import logging

STRATIFY_OS = 0x01
FREERTOS_OS = 0x02
MAX_ARCHIVES_MANAGERS = 14
MAX_IEC_GROUPS = 10

# Define your own exception
class Error(Exception):
    pass

class StrClass(object):
    def __init__(self, clear_line, restline):
        self.clear_line = clear_line
        self.restline = restline


class Base(object):
    """
    Attributes:
        path_global - were will be compile projeckt, should have template path
        path_project -
    """
    MIN_CONTROLLER_VERSION = (1, 0, 0, 0)
    error_message = "error handling file"
    deleted_message = "$deleted after handing SNEMA generator"
    progress_message = "P:"
    error_num = 0
    name = ''
    os_type = STRATIFY_OS
    comment = 0
    path_project_src = ''
    path_project_xml = ''
    path_global = '' #where will project lie
    path_lib = ''
    path_project = path_global
    path_src = path_project + 'src/'
    path_log = path_src + 'log.log'
    path_inc = path_project + 'inc/'
    path_template = 'template/'
    path_cmake = ''
    path_gcc = ''
    path_beremiz_app = ''
    iec_type = {"IEC_BOOL": 1, "BOOL": 1,
                "IEC_SINT": 1, "IEC_USINT": 1, "IEC_BYTE": 1,
                "SINT": 1, "USINT": 1, "BYTE": 1,
                "IEC_INT": 2, "IEC_UINT": 2, "IEC_WORD": 2,
                "INT": 2, "UINT": 2, "WORD": 2,
                "IEC_DINT": 4, "IEC_UDINT": 4, "IEC_DWORD": 4,
                "DINT": 4, "UDINT": 4, "DWORD": 4,
                "IEC_LINT": 8, "IEC_ULINT": 8, "IEC_LWORD": 8,
                "LINT": 8, "ULINT": 8, "LWORD": 8,
                "IEC_REAL": 4, "REAL": 4,
                "IEC_LREAL": 8, "LREAL": 8,
                }

    iec_type_to_description = {"IEC_BOOL":"U8_REGS_FLAG","BOOL":"U8_REGS_FLAG",
                "IEC_SINT":"I8_REGS_FLAG","IEC_USINT":"U8_REGS_FLAG","IEC_BYTE":"U8_REGS_FLAG",
                "SINT": "I8_REGS_FLAG", "USINT": "U8_REGS_FLAG", "BYTE": "U8_REGS_FLAG",
                "IEC_INT":"S16_REGS_FLAG","IEC_UINT":"U16_REGS_FLAG","IEC_WORD":"U16_REGS_FLAG",
                "INT": "S16_REGS_FLAG", "UINT": "U16_REGS_FLAG", "WORD": "U16_REGS_FLAG",
                "IEC_DINT": "S32_REGS_FLAG", "IEC_UDINT": "U32_REGS_FLAG", "IEC_DWORD": "U32_REGS_FLAG",
                "DINT": "S32_REGS_FLAG", "UDINT": "U32_REGS_FLAG", "DWORD": "U32_REGS_FLAG",
                "IEC_LINT": "S64_REGS_FLAG", "IEC_ULINT": "U64_REGS_FLAG", "IEC_LWORD": "U64_REGS_FLAG",
                "LINT": "S64_REGS_FLAG", "ULINT": "U64_REGS_FLAG", "LWORD": "U64_REGS_FLAG",
                "IEC_REAL": "FLOAT_REGS_FLAG", "REAL": "FLOAT_REGS_FLAG",
                "IEC_LREAL": "DOUBLE_REGS_FLAG","LREAL": "DOUBLE_REGS_FLAG",
                }

    license_copy = "\
    /*\n\
     * Copyright (c) 2018 Snema Service\n\
     * All rights reserved.\n\
     *\n\
     * Redistribution and use in source and binary forms, with or without modification,\n\
     * are permitted provided that the following conditions are met:\n\
     *\n\
     * 1. Redistributions of source code must retain the above copyright notice,\n\
     *    this list of conditions and the following disclaimer.\n\
     * 2. Redistributions in binary form must reproduce the above copyright notice,\n\
     *    this list of conditions and the following disclaimer in the documentation\n\
     *    and/or other materials provided with the distribution.\n\
     * 3. The name of the author may not be used to endorse or promote products\n\
     *    derived from this software without specific prior written permission.\n\
     *\n\
     * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED\n\
     * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF\n\
     * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT\n\
     * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,\n\
     * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT\n\
     * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS\n\
     * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN\n\
     * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING\n\
     * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY\n\
     * OF SUCH DAMAGE.\n\
     *\n\
     * This file is part of the sofi PLC.\n\
     *\n\
     * Author: Shoma Gane <shomagan@gmail.com>\n\
     *         Ayrat Girfanov <girfanov.ayrat@yandex.ru>\n\
     */\n"
    REGS_ST_TO_CAN_CODE = {
        "USINT": "uint8",
        "UINT": "uint16",
        "UDINT": "uint32",
        "ULINT": "uint64",
        "SINT": "int8",
        "INT": "int16",
        "DINT": "int32",
        "LINT": "int64",
        "REAL": "real32",
        "LREAL": "real64",
        "STRUCT": "visible_string",
    }
    REGS_TYPE_ST_TO_C_CODE = {
        "USINT": "u8",
        "UINT": "u16",
        "UDINT": "u32",
        "ULINT": "u64",
        "SINT": "i8",
        "INT": "i16",
        "DINT": "i32",
        "LINT": "i64",
        "REAL": "float",
        "LREAL": "double",
        "STRUCT": "visible_string",
    }

    REGS_SIZE = {
        "USINT": 1,
        "UINT": 2,
        "UDINT": 4,
        "ULINT": 8,
        "SINT": 1,
        "INT": 2,
        "DINT": 4,
        "LINT": 8,
        "REAL": 4,
        "LREAL": 8,
        "STRUCT": 4,
    }
    BASE_SELF_MDB_ADDRESS = 40000
    BASE_SELF_MDB_ADDRESS_ARRAY = 30000
    custom_crc_table = {}
    first_var_name = "brmz_task_vrsn"

    def __init__(self, os_type):
        # now used for plc_main.c plc_main.h main.cpp kernel.c
        self.os_type = os_type

    def print_error(self, info):
        self.error()
        msg = '\# error {} {} {} : {}\n'.format(self.error_num, self.error_message, self.name, info)
        self.logger.error(msg)        #print("{}:{}:{}\n".format(self.progress_message,self.name,info))
        raise Error(msg)

    def error(self):
        self.error_num += 1

    def print_result(self, info):
        print(self.progress_message, self.name, ":", info, '\n')

    def print_progress(self, info):
        self.error_num = self.error_num
        self.logger.info(info)

    def print_paths(self):
        if self.name:
            print(self.path_project_src + '/' + self.name,
                  self.path_template + '/' + self.name)
        else:
            print(self.path_project_src, self.path_template)

    @staticmethod
    def add_matiec_include(line):
        line = re.sub('^\s*\#include\s+[\<\"]((iec_(std|types)\w{0,9})|(accessor))\.h[\>\"]',
                      '#include \"matiec/\g<1>.h\"', line)
        return line

    def make_comment(self, line):
        line = "//" + line[:-1] + self.deleted_message + '\n'
        return line

    def substitute(self, file_name, str_before, str_to):
        message = ''
        str_before_t = '([\W^])' + str_before + '([\W$])'
        str_to_t = '\g<1>' + str_to + '\g<2>'
        try:
            temp_buff = ''
            file_opened = open(file_name, 'r', encoding="utf-8")
            for line in file_opened:
                line_temp = line
                line = re.sub(str_before_t, str_to_t, line)
                temp_buff += line
                if line_temp != line:
                    message += 'replace ' + str_before + ' in file ' + file_name + '\n'
            file_opened.close()

            file_write = open(file_name, 'w', encoding="utf-8")
            file_write.write(temp_buff)
            file_write.close()
        except FileNotFoundError:
            self.print_error('don\'t find ' + file_name + '\n')
        if len(message):
            self.print_progress(message)

    def find_multi_stroke_comment(self, str_object):
        if self.comment:
            start = str_object.restline.find('*/')
            if start == -1:
                return str_object
            else:
                self.comment = 0
                if start < len(str_object.restline)-3:
                    str_object.restline = str_object.restline[start+2:]
                    self.find_multi_stroke_comment(str_object)
                else:
                    return str_object
        else:
            start = str_object.restline.find('/*')
            if start == -1:
                str_object.clear_line = str_object.clear_line + str_object.restline
                return str_object
            else:
                if start > 0:
                    str_object.clear_line = str_object.clear_line + str_object.restline[:start]
                    str_object.restline = str_object.restline[start+2:]
                self.comment = 1
                start = str_object.restline.find('*/')
                if start == -1:
                    return str_object
                else:
                    self.comment = 0
                    if start < len(str_object.restline)-3:
                        str_object.restline = str_object.restline[start+2:]
                        self.find_multi_stroke_comment(str_object)
                    else:
                        return str_object

    def find_sentence(self, file_name, sentence):
        global comment
        comment = 0
        num_lines = sum(1 for line in open(file_name, encoding="utf-8"))
        num_lines = num_lines-1
        try:
            file_opened = open(file_name, 'r', encoding="utf-8")
            for line in file_opened:
                if '//' in line:
                    start = line.find('//')
                    line_temp = line[:start]
                else:
                    line_temp = line
                clear_line = ''
                str_object = StrClass(clear_line, line_temp)
                self.find_multi_stroke_comment(str_object)
                if len(str_object.clear_line):
                    if sentence in str_object.clear_line:
                        return 1
                if num_lines:
                    num_lines -= 1
            file_opened.close()
        except FileNotFoundError:
            self.print_error('don\'t find ' + file_name + '\n')
            return 0
        return 0

    def iec_type_size(self, iec_type):
        return self.iec_type[iec_type]

    def get_description_type(self, iec_type):
        return self.iec_type_to_description[iec_type]

    def generate_crc32_table(self, _poly=0x04c11db7):
        self.custom_crc_table
        for i in range(256):
            c = i << 24
            for j in range(8):
                c = (c << 1) ^ _poly if (c & 0x80000000) else c << 1
            self.custom_crc_table[i] = c & 0xffffffff

    def crc32_stm(self, bytes_arr, crc=0xffffffff):
        length = len(bytes_arr)
        k = 0
        while length >= 4:
            v = ((bytes_arr[k] << 24) & 0xFF000000) | ((bytes_arr[k+1] << 16) & 0xFF0000) | \
                ((bytes_arr[k+2] << 8) & 0xFF00) | (bytes_arr[k+3] & 0xFF)
            crc = ((crc << 8) & 0xffffffff) ^ self.custom_crc_table[0xFF & ((crc >> 24) ^ v)]
            crc = ((crc << 8) & 0xffffffff) ^ self.custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 8))]
            crc = ((crc << 8) & 0xffffffff) ^ self.custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 16))]
            crc = ((crc << 8) & 0xffffffff) ^ self.custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 24))]
            k += 4
            length -= 4
        if length > 0:
            v = 0
            for i in range(length):
                v |= (bytes_arr[k+i] << 24-i*8)
            if length == 1:
                v &= 0xFF000000
            elif length == 2:
                v &= 0xFFFF0000
            elif length == 3:
                v &= 0xFFFFFF00
            crc = ((crc << 8) & 0xffffffff) ^ self.custom_crc_table[0xFF & ((crc >> 24) ^ v)]
            crc = ((crc << 8) & 0xffffffff) ^ self.custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 8))]
            crc = ((crc << 8) & 0xffffffff) ^ self.custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 16))]
            crc = ((crc << 8) & 0xffffffff) ^ self.custom_crc_table[0xFF & ((crc >> 24) ^ (v >> 24))]
        return crc




