import re
import base_object
from os import walk


class MakeLd(base_object.Base):
    name = ""


class Ld(MakeLd):
    """COPY FROM TEMPLATE LD @todo add handing for internal and external flash"""
    name = "task_internal_flash.ld"

    def make(self):
        if self.os_type == base_object.FREERTOS_OS:
            path_template = self.path_template + "freertos/ldscript/" + self.name
            file_make = open(self.path_project + "ldscript/" + self.name, 'w', encoding="utf-8")
            num_lines_beremiz = sum(1 for line in open(path_template, encoding="utf-8"))
            file_template = open(path_template, 'r', encoding="utf-8")
            lines_template = file_template.readlines()
            for current_line in range(num_lines_beremiz):
                file_make.write(lines_template[current_line])
            file_template.close()
            file_make.close()
