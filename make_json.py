import base_object


class MakeJson(base_object.Base):
    name = ""


class LocalJson(MakeJson):
    def make(self):
        if self.os_type == base_object.STRATIFY_OS:
            self.name = "StratifyLocalSettings.json"
            file_make = open(self.path_project + self.name, 'w', encoding="utf-8")
            file_make.write("{\n"
                            "   \"LocalBuild\": \"build_release_v7em_f4sh\",\n"
                            "   \"LocalInstallPath\": \"/app/.install\",\n"
                            "   \"LocalIsRam\": false,\n"
                            "   \"LocalIsStartup\": false,\n"
                            "   \"LocalProjectId\": \"\n"
                            "}\n")
            file_make.close()


class CommonJson(MakeJson):
    def make(self):
        if self.os_type == base_object.STRATIFY_OS:
            self.name = "StratifySettings.json"
            file_make = open(self.path_project + self.name, 'w', encoding="utf-8")
            file_make.write("{\n"
                            "   \"buildprefix\": \"build_\",\n"
                            "   \"description\": \"start beremiz code.\",\n"
                            "   \"github\": \"\",\n"
                            "   \"hardwareid\": \"\",\n"
                            "   \"name\": \"stratify_beremiz\",\n"
                            "   \"permissions\": \"public\",\n"
                            "   \"publisher\": \"Stratify Labs, Inc\",\n"
                            "   \"ram\": 0,\n"
                            "   \"tags\": \"test\",\n"
                            "   \"type\": \"StratifyApp\",\n"
                            "   \"version\": \"0.1\"\n"
                            "}\n")
            file_make.close()



