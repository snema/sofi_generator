import base_object
import os
import git
try:
    __import__('requests')
except ImportError:
    print("Installing request ")
    from setuptools.command.easy_install import main as install
    install(['requests'])
import requests
import json
import argparse
import time
__description__ = 'script for loading task or os'


def main(ip_address='http://192.168.1.232', load_type="task", bin_path='', os_number='1', min_os_version=(0, 0, 0, 0)):
    ip_address = "" + ip_address
    print(ip_address[0])
    if ip_address.find("http://") != 0:
        ip_address = "http://"+ip_address
    if load_type == "os":
        response = requests.get('{}:80/get_json.cgi'.format(ip_address), headers={'Json': '{"request":"regs_set","reg_num":1,"name":"user_task_config","value":4'})
        json_data = json.loads(response.content);
        if(json_data['status'] == 'ok'):
            print('Task was stopped')
        else:
            return -1
        response = requests.get('{}:80/get_json.cgi'.format(ip_address), headers={'Json': '{"request":"os_param"}'})
        json_data = json.loads(response.content)
        if json_data['current'] and json_data['main'] and json_data['os_ver']:
            print("version - {}".format(json_data['os_ver']))
        else:
            print("content mistaken")
            return -1
        if bin_path == '':
            current_path = os.path.dirname(os.path.abspath(__file__))
            path_bin = current_path + '/../../build/sofi_0_24_0_1_deb_bv_2_os1_crc.bin'
        else:
            path_bin = bin_path
        files = {'upload_file': open(path_bin, 'rb')}
        print("upload os"+os_number)
        r = requests.post('{}/download_os{}.key'.format(ip_address,os_number), files=files)
        print(r)
        print("Starting os"+os_number)
        response = requests.get('{}/command.cgi?os_command=os{}_as_main_start'.format(ip_address,os_number))
        print("Started os"+os_number)
    elif load_type == "task":
        response = requests.get('{}:80/get_json.cgi'.format(ip_address), headers={'Json': '{"request":"regs_set","reg_num":1,"name":"user_task_config","value":4'})
        json_data = json.loads(response.content);
        if(json_data['status'] == 'ok'):
            response = requests.get('{}:80/get_json.cgi'.format(ip_address), headers={'Json': '{"request":"regs","reg_num":1,"get_type":"only_value","name":"user_task_state"}'})
            json_data = json.loads(response.content);
            #print(json_data)
            if((json_data['value'] & 0x03) == 0):
                print('Task was stopped')
            else:
                print('we cant stop task try it manually and repeat')
                return -1
        else:
            print('we cant stop task try it manually and repeat')
            return -1
        response = requests.get('{}:80/get_json.cgi'.format(ip_address), headers={'Json': '{"request":"os_param"}'})
        json_data = json.loads(response.content)
        if json_data['current'] and json_data['main'] and json_data['os_ver']:
            print("os version in plc - {}".format(json_data['os_ver']))
            if check_controller_os_version(json_data['os_ver'], min_os_version) > 0:
                if bin_path != '':
                    if os.path.exists(bin_path):
                        files = {'upload_file': open(bin_path, 'rb')}
                        print("uploading task")
                        response = requests.post('{}/download_internal_flash_task.key'.format(ip_address), files=files)
                        print("Starting task")
                        response = requests.get('{}/get_json.cgi'.format(ip_address), headers={'Json': '{"request":"regs_set","reg_num":1,"name":"user_task_config","value":1'})
                        time.sleep(1.5)
                        print("reading task state")
                        response = requests.get('{}:80/get_json.cgi'.format(ip_address), headers={'Json': '{"request":"regs","reg_num":1,"get_type":"only_value","name":"user_task_state"}'})
                        if len(response.content) > 10:
                            json_data = json.loads(response.content)
                            print(json_data)
                            if (json_data['value'] & 0x01) == 0x01:
                                print("User task started")
                            else:
                                print("User task not launched")
                        else:
                            print("task state unavailable")
                    else:
                        print("bin file not exist")
                else:
                    print("path with loading file is empty")
            else:
                print("please update controller with os version higher then {}".format(min_os_version))
        else:
            print("content mistaken")
            return -1
    elif load_type == "json":
        response = requests.get('{}:80/get_json.cgi'.format(ip_address), headers={'Json': '{"request":"regs","reg_num":"2","get_type":"full","index":"1"}'})
        print(response.content)


def check_controller_os_version(controller_version, min_version=(0, 0, 0, 0)):
    res = 0
    if min_version[0] < controller_version[0]:
        res = 1
    elif min_version[0] == controller_version[0]:
        if min_version[1] < controller_version[1]:
            res = 1
        elif min_version[1] == controller_version[1]:
            if min_version[2] < controller_version[2]:
                res = 1
            elif min_version[2] == controller_version[2]:
                if min_version[3] <= controller_version[3]:
                    res = 1
    return res;




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=__description__)
    parser.add_argument('-p', '--bin_path', type=str, default='',
                        help=('path to pre build bin file'
                              '(default: %(default)s)'))
    parser.add_argument('-i', '--ip', type=str, default='http://192.168.1.232',
                        help=('ip address off plc'
                              '(default: %(default)s)'))
    parser.add_argument('-t', '--type', type=str, default='task',
                        help=('task or os for loading'
                              '(default: %(default)s)'))
    parser.add_argument('-o', '--os', type=str, default='1',
                        help=('os number for loading'
                              '(default: %(default)s)'))
    args = parser.parse_args()

    main(args.ip, args.type, args.bin_path, args.os)
