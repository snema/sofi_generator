import re
import sys
import base_object
from os import walk
import struct
class Analysis(base_object.Base):
    name = ""


class Config(Analysis):
    """ config.c file have two function config_init__() and config_run__
        tick_time - time in ns"""
    name = "config.c"
    init_global_variable = []
    init_prg = []
    num_lines = 0
    tick_time = "1000000ULL"

    def common(self):
        path_beremiz = self.path_project_src + '/' + self.name
        try:
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            self.config_init(file_beremiz)
            self.config_run(file_beremiz)
            self.common_tick_time(path_beremiz)
            file_beremiz.close()
            self.print_progress(self.init_global_variable)
            self.print_progress(self.init_prg)
        except FileNotFoundError:
            self.print_error('did\'t find '+path_beremiz + '\n')

    def config_init(self, file_beremiz):
        begin = 0
        for line in file_beremiz:
            if begin:
                if re.search('^\}', line):
                    return
                else:
                    if re.search('^\s*__INIT_GLOBAL', line):
                        self.init_global_variable.append(line)
                    if re.search('^\s*([\w\d]+)_init__\(\)\;', line):
                        prg_name = re.match('^\s*([\w\d]+)_init__\(\)\;', line)
                        if prg_name.group(1):
                            self.init_prg.append(prg_name.group(1))
            else:
                if re.search('^\s*void\s+config_init__\s*\(\s*void\s*\)', line):
                    begin = 1
                    self.print_progress("handling config init function")

    def config_run(self, file_beremiz):
        begin = 0
        for line in file_beremiz:
            if begin:
                if re.search('^\}', line):
                    return
                else:
                    if re.search('^\s*([\w\d]+)_run__\(\s*\w+\s*\)\;', line):
                        prg_name = re.match('^\s*([\w\d]+)_run__\(\s*\w+\s*\)\;',
                                            line)
                        if prg_name.group(1):
                            if prg_name.group(1) in self.init_prg:
                                self.print_progress("init_ correspond run_ source"
                                                    + prg_name.group(1))
                            else:
                                self.print_error("did't find the same prg task "+
                                                 prg_name.group(1)+\
                                                 "like in init function")
                                self.init_prg.append(prg_name.group(1))
            else:
                if re.search('^\s*void\s+config_run__\s*\(\s*[\s\w]+\s*\)', line):
                    begin = 1
                    self.print_progress("handling config run function")

    def common_tick_time(self, path_beremiz):
        self.num_lines = sum(1 for line in open(path_beremiz, 'r', encoding="utf-8"))
        self.print_progress("line number" + str(self.num_lines))
        file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
        lines = file_beremiz.readlines()
        for current_line in range(self.num_lines):
            line = lines[current_line]
            if re.search('^[\s\w]+common_ticktime__\s*\=\s*(\d+\w*)\s*\;', line):
                tick_time_re = re.match('^[\s\w]+common_ticktime__\s*\=\s*(\d+\w*)\s*\;',
                                        line)
                self.tick_time = tick_time_re.group(1)
                self.print_progress(self.tick_time)


class PlcDebugger(Analysis):
    """ plc_debugger.c file have a lot info about regs"""
    name = "plc_debugger.c"
    regs = []
    config_regs = []
    buffer_size = 0#"#define BUFFER_SIZE 74"
    prog_decl = ""
    vars_decl = ""
    dbg_array_decl = ""

    def find_buffers_size(self):
        """extern __IEC_DINT_t CONFIG__LOCALVAR2;"""
        TEMPLATE_BUFFER_SIZE = re.compile(r"^\s*\#define\s+BUFFER_SIZE\s+(?P<SIZE>[0-9]*)")
        path_beremiz = self.path_project_src + '/' + self.name
        try:
            num_lines_beremiz = sum(1 for line in open(path_beremiz, encoding="utf-8"))
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            for line in range(num_lines_beremiz):
                result = TEMPLATE_BUFFER_SIZE.match(lines[line])
                if result:
                    resdict = result.groupdict()
                    self.buffer_size = int(resdict['SIZE'])
                    break
            file_beremiz.close()
            if self.buffer_size==0:
                self.print_error('did\'t find ' + path_beremiz + '\n')
            return self.buffer_size
        except FileNotFoundError:
            self.print_error('did\'t find '+path_beremiz + '\n')

    def find_prog_declaration(self):
        """extern MODBUS RESOURCE1__INSTANCE0;"""
        TEMPLATE_CONFIG_DECL = re.compile(r"^\s*extern\s+(?P<TYPE>[0-9A-Za-z_.]*)\s+(?P<NAME>[0-9A-Za-z_.]*)\s*\;")
        TEMPLATE_PROG_DECL = re.compile(r"^\s*\*\s*Declare\s+programs\s*")
        TEMPLATE_VARS_DECL = re.compile(r"^\s*\*\s*Declare\s+global\s+variables\s+from\s+resources\s+and\s+conf\s*")
        path_beremiz = self.path_project_src + '/' + self.name
        try:
            num_lines_beremiz = sum(1 for line in open(path_beremiz, encoding="utf-8"))
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            for line in range(num_lines_beremiz):
                result = TEMPLATE_PROG_DECL.match(lines[line])
                if result:
                    line += 1
                    for inline in range(line,num_lines_beremiz):
                        result = TEMPLATE_CONFIG_DECL.match(lines[inline])
                        if result:
                            self.prog_decl += lines[inline]
                        line = inline
                        result = TEMPLATE_VARS_DECL.match(lines[inline])
                        if result:
                            break
                    if line >= num_lines_beremiz-1:
                        self.print_error("end off prog declaration in debug file not found")
                    break
            file_beremiz.close()
            self.print_progress(self.prog_decl)
            return self.prog_decl
        except FileNotFoundError:
            self.print_error('did\'t find '+path_beremiz + '\n')

    def find_vars_declaration(self):
        """extern __IEC_DINT_t CONFIG__LOCALVAR2;"""
        TEMPLATE_CONFIG_DECL = re.compile(r"^\s*extern\s+(?P<TYPE>[0-9A-Za-z_.]*)\s+(?P<NAME>[0-9A-Za-z_.]*)\s*\;")
        TEMPLATE_VARS_DECL = re.compile(r"^\s*\*\s*Declare\s+global\s+variables\s+from\s+resources\s+and\s+conf\s*")
        TEMPLATE_VARS_DECL_STOP = re.compile(r"\s*\}\s*dbgvardsc_t\;")

        path_beremiz = self.path_project_src + '/' + self.name
        try:
            num_lines_beremiz = sum(1 for line in open(path_beremiz, encoding="utf-8"))
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            for line in range(num_lines_beremiz):
                result = TEMPLATE_VARS_DECL.match(lines[line])
                if result:
                    line += 1
                    for inline in range(line,num_lines_beremiz):
                        result = TEMPLATE_CONFIG_DECL.match(lines[inline])
                        if result:
                            self.vars_decl += lines[inline]
                            resdict = result.groupdict()
                            if resdict['NAME'][:6] == "CONFIG":
                                self.config_regs.append({"name": resdict['NAME'], "type": resdict['TYPE']})
                        result = TEMPLATE_VARS_DECL_STOP.match(lines[inline])
                        line = inline
                        if result:
                            break
                    if line >= num_lines_beremiz-1:
                        self.print_error("end off prog declaration in debug file not found")
                    break
            file_beremiz.close()
            self.print_progress(self.vars_decl)
            return self.vars_decl
        except FileNotFoundError:
            self.print_error('did\'t find '+path_beremiz + '\n')


    def find_array(self):
        path_beremiz = self.path_project_src + '/' + self.name
        try:
            num_lines_beremiz = sum(1 for line in open(path_beremiz, encoding="utf-8"))
            file_beremiz = open(path_beremiz, 'r', encoding="utf-8")
            lines = file_beremiz.readlines()
            for line_start in range(num_lines_beremiz):
                if re.search("static\s+dbgvardsc_t\s+dbgvardsc\[\]\s*\=\s*\{",lines[line_start]):
                    line_start+=1
                    TEMPLATE = re.compile(r"^\s*\{\&\s*\(\s*(?P<NAME>[0-9A-Za-z_.]*)\s*\)\s*\,\s*(?P<TYPE>[0-9a-zA-Z_.]*)\}\s*\,?\s*")
                    for line_inside in range(line_start,num_lines_beremiz):
                        if "};" in lines[line_inside]:
                            break
                        result = TEMPLATE.match(lines[line_inside])
                        if result:
                            resdict = result.groupdict()
                            self.dbg_array_decl +="{{&({}), {}, 0}},\n".format(resdict['NAME'],resdict['TYPE'])
                            self.regs.append({"name":resdict['NAME'],"type":resdict['TYPE']})
                    break

            file_beremiz.close()
        except FileNotFoundError:
            self.print_error('did\'t find '+path_beremiz + '\n')


class CanOpenModule(Analysis):
    """ aofile_1_1.c file have a lot info about can regs - pdo receive(writable)
        pdo tranceive(readable) and sdo """
    name = "xxfile_x_x.c"
    files_name = []
    file_type = ["ao", "ai", "di", "do", "bric"]
    modules = []
    pdor_dict = []
    pdot_dict = []

    def module_vars(self):
        self.find_modules_source()
        MODULE_TYPE = re.compile(r"^\s*\/\/\s*module\s+type\s*\-\s*(?P<NAME>\w+)\s*\-\s*(?P<NUMBER>\d+)\s*$")
        VARS_DECL = re.compile(r"^\s*extern\s+(?P<TYPE>[\w\d]+)\s+(?P<NAME>[\w\d]+)\s*\;\/\/\s*name\:(?P<ORIGIN_NAME>[\d\w]+)\s+type\:(?P<ORIGIN_TYPE>[\w\d]+)\s+"\
                                 r"polling\:(?P<POLLING>[\d\w]+)\s+init\:(?P<INIT_VALUE>[\d\.]+)\s+options\:(?P<OPTS>[\w\d]+)\s+address\:(?P<ADDRESS>[\w\d\.]+)\s+description\:(?P<DESC>.*)\s*$")
        for modules in self.files_name:
            num_lines = sum(1 for line in open(self.path_project_src+modules["name"], encoding="utf-8"))
            file_temp = open(self.path_project_src+modules["name"], 'r', encoding="utf-8")
            lines = file_temp.readlines()
            module_dict = {}
            module_vars_dict = []
            module_vars_pdor_dict = []
            module_vars_pdot_dict = []
            module_sdo_polling = []
            pdot_number = 0
            pdor_number = 0
            vars_num = 0
            for line in range(num_lines):
                result = MODULE_TYPE.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    module_dict["module_type"] = res_dict["NAME"]
                    module_dict["module_type_number"] = int(res_dict["NUMBER"])
                result = VARS_DECL.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    if "pdor" in res_dict["OPTS"]:
                        pdor_number += 1
                        module_vars_pdor_dict.append(res_dict)
                    elif "pdot" in res_dict["OPTS"]:
                        pdot_number += 1
                        module_vars_pdot_dict.append(res_dict)

                    if res_dict["OPTS"] == "sdo":
                        if res_dict["POLLING"] == "read" or res_dict["POLLING"] == "write":
                            module_sdo_polling.append(res_dict)
                        elif res_dict["POLLING"] == "0" or res_dict["POLLING"] == "not_used":
                            self.print_progress("sdo {}, not used".format(res_dict["NAME"]))
                        else:
                            self.print_error("sdo polling field should be \"read\" , \"write\" , \"not_used\" or \"0\""
                                             " now it's {}".format(res_dict["POLLING"]))
                    module_vars_dict.append(res_dict)
                    vars_num += 1
            if vars_num:
                module_dict["vars_num"] = vars_num
                module_dict["module_number"] = modules["number"]
                module_dict["module_vars_dict"] = module_vars_dict
                module_dict["module_vars_pdor_dict"] = module_vars_pdor_dict
                module_dict["module_vars_pdot_dict"] = module_vars_pdot_dict
                module_dict["module_sdo_polling"] = module_sdo_polling
                self.modules.append(module_dict)
            file_temp.close()

    def find_modules_source(self):
        """find xxfile_x_x.h files in dir"""
        number = 0
        position = -1
        NAME_FILE_TYPE = re.compile(r"\w{2,8}file_(?P<POSITION>\d+)_(?P<NUMBER>\d+)\.c")
        max_module_number = 0
        numbers = []
        for (dirpath, dirnames, filenames) in walk(self.path_project_src):
            for i in range(len(filenames)):
                res = NAME_FILE_TYPE.match(filenames[i])
                if res:
                    res_dict = res.groupdict()
                    module_number = int(res_dict["NUMBER"])
                    if module_number == 0:
                        self.print_error("module number should more then 0")
                        break
                    else:
                        if position >= 0:
                            if position != int(res_dict["POSITION"]):
                                self.print_error("we can have only one block module")
                                break
                        position = int(res_dict["POSITION"])
                        if len(numbers):
                            for j in numbers:
                                if j == module_number:
                                    self.print_error("module wtih the same number")
                                    break
                            if module_number > max_module_number:
                                max_module_number = module_number
                        else:
                            max_module_number = module_number
                        numbers.append(module_number)
                        self.files_name.append({"name": filenames[i], "number": module_number})
                        number += 1
            if max_module_number != len(numbers):
                self.print_error("some can module missed, you should make it in order ")
        self.print_progress("find can modules"+str(self.files_name))
        return number


class ArchiveModule(Analysis):
    """ archive_3_0.c files has info about archives
        1 manage vars used for user control of archive and check current state
        2 headers vars used for stored system data to archive are filled by system
        3 user vars filled by user program"""
    name = "archive_x_x.c"
    files_name = []
    file_type = ["archive"]
    archives = []

    def module_vars(self):
        self.find_modules_source()
        MODULE_TYPE = re.compile(r"^\s*\/\/\s*module\s+type\s*\-\s*(?P<NAME>\w+)\s*\-\s*(?P<NUMBER>\d+)\s*$")
        VARS_DECL = re.compile(r"^\s*extern\s+(?P<TYPE>[\w\d]+)\s+(?P<NAME>[\w\d]+)\s*\;\/\/\s*name\:(?P<ORIGIN_NAME>[\d\w]+)\s+type\:(?P<ORIGIN_TYPE>[\w\d]+)\s+"\
                                 r"polling\:(?P<POLLING>[\d\w]+)\s+init\:(?P<INIT_VALUE>[\d\.\-]+)\s+options\:(?P<OPTS>[\w\d]+)\s+address\:(?P<ADDRESS>[\w\d\.\-]+)\s+description\:(?P<DESC>.*)\s*$")
        START_BUFFER_TEMPL = re.compile(r"//archive\s+start\s+buffer\s+address\s*-\s*(?P<ADDRESS>[\d]+)")
        SIZE_BUFFER_TEMPL = re.compile(r"//archive\s+size\s+buffer\s*-\s*(?P<SIZE>[\d]+)")
        for modules in self.files_name:
            num_lines = sum(1 for line in open(self.path_project_src+modules["name"], encoding="utf-8"))
            file_temp = open(self.path_project_src+modules["name"], 'r', encoding="utf-8")
            lines = file_temp.readlines()
            file_temp.close()

            archive_dict = {}
            vars_dict = []
            arc_manage_var = []
            arc_header_vars = []
            arc_user_vars = []
            vars_num = 0
            buffers_start_address = -1
            buffers_size = 0
            for line in range(num_lines):
                result = MODULE_TYPE.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    archive_dict["module_type"] = res_dict["NAME"]
                    archive_dict["module_type_number"] = int(res_dict["NUMBER"])
                result = START_BUFFER_TEMPL.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    buffers_start_address = int(res_dict["ADDRESS"])
                result = SIZE_BUFFER_TEMPL.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    buffers_size = int(res_dict["SIZE"])
                result = VARS_DECL.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    if "arc_manage" in res_dict["OPTS"]:
                        arc_manage_var.append(res_dict)
                    elif "arc_header" in res_dict["OPTS"]:
                        arc_header_vars.append(res_dict)
                    elif "arc_user" in res_dict["OPTS"]:
                        arc_user_vars.append(res_dict)
                    vars_dict.append(res_dict)
            if buffers_start_address < 0:
                self.print_error("did't find buffer start address for {}".format(modules["name"]))
            else:
                archive_dict["buffer_start_address"] = buffers_start_address
            if buffers_size > 0:
                archive_dict["buffer_size"] = buffers_size
            else:
                self.print_error("did't find buffer size for arch {}".format(modules["name"]))
            archive_dict["manage_buffer_start_address"] = int(arc_manage_var[0]["ADDRESS"])
            archive_dict["manage_buffer_last_address"] = int(float(arc_user_vars[-1]["ADDRESS"]))
            archive_dict["manage_buffer_regs_num"] = archive_dict["manage_buffer_last_address"] - archive_dict["manage_buffer_start_address"] + self.iec_type[arc_user_vars[-1]["TYPE"][2:-2]]//2 + 1
            archive_dict["location"] = modules["name"]
            if len(vars_dict) > 0:
                archive_dict["vars_num"] = len(vars_dict)
                archive_dict["module_number"] = modules["number"]
                archive_dict["vars"] = vars_dict
                if len(arc_manage_var) == 0:
                    self.print_error("Archive with number {} doesnt have managers vars".format(modules["number"]))
                if len(arc_header_vars) == 0:
                    self.print_error("Archive with number {} doesnt have headers vars".format(modules["number"]))
                if len(arc_user_vars) == 0:
                    self.print_error("Archive with number {} doesnt have users vars".format(modules["number"]))
                archive_dict["arc_manage_vars"] = arc_manage_var
                archive_dict["arc_header_vars"] = arc_header_vars
                archive_dict["arc_user_vars"] = arc_user_vars
                file_temp = open(self.path_project_src+modules["name"], 'rb')
                input_file = file_temp.read()
                file_temp.close()
                length = len(input_file)
                # word allignment
                len_add = 0
                if length % 4 == 1:
                    len_add = 3
                elif length % 4 == 2:
                    len_add = 2
                elif length % 4 == 3:
                    len_add = 1
                # to length add crc and self length
                output = struct.pack('>I', length)
                output += input_file
                if len_add == 3:
                    output += b'\x00\x00\x00'
                elif len_add == 2:
                    output += b'\x00\x00'
                elif len_add == 1:
                    output += b'\x00'
                self.generate_crc32_table()
                buff = list(output)
                crc = self.crc32_stm(buff)
                archive_dict["file_crc"] = crc
                self.archives.append(archive_dict)


    def find_modules_source(self):
        """find archive_x_x.h files in dir"""
        number = 0
        position = -1
        NAME_FILE_TYPE = re.compile(r"archive_(?P<POSITION>\d+)_(?P<NUMBER>\d+)\.c")
        max_archive_number = 0
        numbers = []
        for (dirpath, dirnames, filenames) in walk(self.path_project_src):
            for i in range(len(filenames)):
                res = NAME_FILE_TYPE.match(filenames[i])
                if res:
                    res_dict = res.groupdict()
                    archive_number = int(res_dict["NUMBER"])
                    if position >= 0:
                        if position != int(res_dict["POSITION"]):
                            self.print_error("we can have only one archive block")
                            break
                    position = int(res_dict["POSITION"])
                    if len(numbers):
                        for j in numbers:
                            if j == archive_number:
                                self.print_error("module with the same number")
                                break
                        if archive_number > max_archive_number:
                            max_archive_number = archive_number
                    else:
                        max_archive_number = archive_number
                    numbers.append(archive_number)
                    self.files_name.append({"name": filenames[i], "number": archive_number})
                    number += 1
            if len(numbers) and (max_archive_number+1) != len(numbers):
                self.print_error("some module missed")
            if (max_archive_number+1) > base_object.MAX_ARCHIVES_MANAGERS:
                self.print_error("max archive managers number - {}".format(base_object.MAX_ARCHIVES_MANAGERS))
        self.print_progress("find archive modules"+str(self.files_name))
        return number


class IEC104Module(Analysis):
    """ iec104_3_0.c files has info about iec104 registers
        1 manage vars used for user control of iec104 registers and check current state
        2 headers vars used for stored system data to archive are filled by system
        3 user vars filled by user program"""
    def __init__(self, os_type):
        super().__init__(os_type)
        self.name = "iec104_x_x.c"
        self.files_name = []
        self.file_type = ["iec104"]
        self.iec104s = []

    def module_vars(self):
        self.find_modules_source()
        # description for module type
        MODULE_TYPE = re.compile(r"^\s*\/\/\s*module\s+type\s*\-\s*(?P<NAME>\w+)\s*\-\s*(?P<NUMBER>\d+)\s*$")
        # description for variable
        VARS_DECL = re.compile(r"^\s*extern\s+(?P<TYPE>[\w\d]+)\s+(?P<NAME>[\w\d]+)\s*\;\/\/"
                               r"\s*name\:(?P<ORIGIN_NAME>[\d\w\-]+)\s+type\:(?P<ORIGIN_TYPE>[\w\d]+)\s+"\
                               r"polling\:(?P<POLLING>[\d\w]+)\s+init\:(?P<INIT_VALUE>[\d\.\-]+)\s+"
                               r"options\:(?P<OPTS>[\w\d]+)\s+address\:(?P<ADDRESS>[\w\d\.\-]+)\s+"
                               r"description\:(?P<DESC>.*)\s*$")
        # description where modbus address stored
        START_BUFFER_TEMPL = re.compile(r"//iec104\s+start\s+buffer\s+address\s*-\s*(?P<ADDRESS>[\d]+)")
        # description where buffer size stored
        SIZE_BUFFER_TEMPL = re.compile(r"//iec104\s+size\s+buffer\s*-\s*(?P<SIZE>[\d]+)")
        # description use modbus or not
        USE_MODBUS_TEMPL = re.compile(r"//iec104\s+add\s+to\s+modbus\s+address\s+space\s*-\s*(?P<USE_MODBUS>[\d]+)")
        for modules in self.files_name:
            num_lines = sum(1 for line in open(self.path_project_src+modules["name"], encoding="utf-8"))
            file_temp = open(self.path_project_src+modules["name"], 'r', encoding="utf-8")
            lines = file_temp.readlines()
            file_temp.close()
            iec104_dict = {}
            vars_dict = []
            vars_num = 0
            buffers_start_address = -1
            buffers_size = 0
            use_modbus = -1
            for line in range(num_lines):
                result = MODULE_TYPE.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    iec104_dict["module_type"] = res_dict["NAME"]
                    iec104_dict["module_type_number"] = int(res_dict["NUMBER"])
                result = START_BUFFER_TEMPL.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    buffers_start_address = int(res_dict["ADDRESS"])
                result = SIZE_BUFFER_TEMPL.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    buffers_size = int(res_dict["SIZE"])
                result = USE_MODBUS_TEMPL.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    use_modbus = int(res_dict["USE_MODBUS"])
                result = VARS_DECL.match(lines[line])
                if result:
                    res_dict = result.groupdict()
                    if vars_num == 0:
                        if iec104_dict["module_type"] + "_" + str(
                                iec104_dict["module_type_number"]) + "_originator_address" \
                                != res_dict["ORIGIN_NAME"]:
                            self.print_error("iec104 space should have originator address at first")
                        else:
                            iec104_dict["ORIGINATOR_ADDRESS"] = res_dict["INIT_VALUE"]
                            iec104_dict["ORIGINATOR_ADDRESS_NAME"] = res_dict["NAME"]
                    elif vars_num == 1:
                        if iec104_dict["module_type"] + "_" + str(
                                iec104_dict["module_type_number"]) + "_common_address" \
                                != res_dict["ORIGIN_NAME"]:
                            self.print_error("iec104 space should have common address at second")
                        else:
                            iec104_dict["COMMON_ADDRESS"] = res_dict["INIT_VALUE"]
                            iec104_dict["COMMON_ADDRESS_NAME"] = res_dict["NAME"]
                    vars_dict.append(res_dict)
                    vars_num += 1
            if vars_num < 2:
                self.print_error("iec104 space should have at least 2 vars")
            if buffers_start_address < 0:
                self.print_error("did't find buffer start address for {}".format(modules["name"]))
            else:
                iec104_dict["buffer_start_address"] = buffers_start_address
            if buffers_size > 0:
                iec104_dict["buffer_size"] = buffers_size
            else:
                self.print_error("did't find buffer size for arch {}".format(modules["name"]))
            if use_modbus >= 0:
                iec104_dict["use_modbus"] = use_modbus
            else:
                self.print_error("did't find option use_modbus")
            iec104_dict["location"] = modules["name"]
            if len(vars_dict) > 0:
                iec104_dict["vars_num"] = len(vars_dict)
                iec104_dict["module_number"] = modules["number"]
                iec104_dict["vars"] = vars_dict
                file_temp = open(self.path_project_src+modules["name"], 'rb')
                input_file = file_temp.read()
                file_temp.close()
                self.iec104s.append(iec104_dict)

    def find_modules_source(self):
        """find iec104_x_x.h files in dir"""
        number = 0
        position = -1
        NAME_FILE_TYPE = re.compile(r"iec104_(?P<POSITION>\d+)_(?P<NUMBER>\d+)\.c")
        max_iec104_number = 0
        numbers = []
        for (dirpath, dirnames, filenames) in walk(self.path_project_src):
            for i in range(len(filenames)):
                res = NAME_FILE_TYPE.match(filenames[i])
                if res:
                    res_dict = res.groupdict()
                    iec104_number = int(res_dict["NUMBER"])
                    if position >= 0:
                        if position != int(res_dict["POSITION"]):
                            self.print_error("we can have only one iec104 block")
                            break
                    position = int(res_dict["POSITION"])
                    if len(numbers):
                        for j in numbers:
                            if j == iec104_number:
                                self.print_error("module with the same number")
                                break
                        if iec104_number > max_iec104_number:
                            max_iec104_number = iec104_number
                    else:
                        max_iec104_number = iec104_number
                    numbers.append(iec104_number)
                    self.files_name.append({"name": filenames[i], "number": iec104_number})
                    number += 1
            if len(numbers) and (max_iec104_number+1) != len(numbers):
                self.print_error("some module missed")
            if (max_iec104_number+1) > base_object.MAX_IEC_GROUPS:
                self.print_error("max archive managers number - {}".format(base_object.MAX_IEC_GROUPS))
        self.print_progress("find iec104 modules"+str(self.files_name))
        return number