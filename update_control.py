import os
import base_object
current_path_dir = os.path.dirname(os.path.abspath(__file__))
os.environ["GIT_PYTHON_GIT_EXECUTABLE"] = current_path_dir+"/../../PortableGit/bin/git.exe"
os.environ["GIT_PYTHON_REFRESH"] = "quiet"
from git import Repo
from winreg import *
import admin

class Updater(base_object.Base):
    version_sofi_os = ""
    version_sofi = ""
    version_beremiz = ""
    version_matiec = ""
    version_examples = ""
    tags_sofi = []
    tags_beremiz = []
    tags_matiec = []
    tags_examples = []
    remote_version_sofi = ""
    remote_version_beremiz = ""
    remote_version_matiec = ""
    remote_version_examples = ""
    remote_tags_sofi = []
    remote_tags_beremiz = []
    remote_tags_matiec = []
    remote_tags_examples = []

    #admin.runAsAdmin()

    def print_current_and_last_version(self):
        self.get_current_version()
        self.get_last_version()
        
        #sofi generator
        if(self.version_sofi == self.remote_version_sofi):
            self.print_result("current sofi task generator version is last: {}".format(self.version_sofi))
        else:
            self.print_result("current sofi task generator version: {}".format(self.version_sofi)+" last version: {}".format(self.remote_version_sofi))
        
        #beremiz
        if(self.version_beremiz == self.remote_version_beremiz):
            self.print_result("current beremiz version is last: {}".format(self.version_beremiz))
        else:
            self.print_result("current beremiz version: {}".format(self.version_beremiz)+" last version: {}".format(self.remote_version_beremiz))
        
        #matiec
        if(self.version_matiec == self.remote_version_matiec):
            self.print_result("current matiec version is last: {}".format(self.version_matiec))
        else:
            self.print_result("current matiec version: {}".format(self.version_matiec)+" last version: {}".format(self.remote_version_matiec))
        
        #examples
        if(self.version_examples == self.remote_version_examples):
            self.print_result("current beremiz examples version is last: {}".format(self.version_examples))
        else:
            self.print_result("current beremiz examples version: {}".format(self.version_examples)+" last version: {}".format(self.remote_version_examples))


    def get_current_version(self):
        #self.print_result("Get from git")
        current_path = os.path.dirname(os.path.abspath(__file__))
        #sofi
        git_base_sofi_gen = current_path
        repo = Repo(git_base_sofi_gen)
        assert not repo.bare
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if len(tags) > 0:
            self.tags_sofi = tags
            self.version_sofi = str(tags[-1])
            #self.print_result("current sofi task generator version {}".format(self.version_sofi))
            #SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\sofi_task_generator", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.version_sofi)
            
        #beremiz
        git_base_beremiz = current_path + "/../../beremiz/"
        repo = Repo(git_base_beremiz)
        assert not repo.bare
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if tags:
            self.tags_beremiz = tags
            #self.print_result("current beremiz version {}".format(tags[-1]))
            self.version_beremiz = str(tags[-1])
            #SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\beremiz", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.version_beremiz)
        #matiec
        git_base_matiec = current_path + "/../../matiec/"
        repo = Repo(git_base_matiec)
        assert not repo.bare
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if tags:
            self.tags_matiec = tags
            #self.print_result("current matiec version {}".format(tags[-1]))
            self.version_matiec = str(tags[-1])
            #SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\matiec", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.version_matiec)
        #examples
        git_base_examples = current_path + "/../../bric_examples/"
        repo = Repo(git_base_examples)
        assert not repo.bare
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if tags:
            self.tags_examples = tags
            #self.print_result("current bric examples version {}".format(tags[-1]))
            self.version_examples = str(tags[-1])
            #SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\beremiz_examples", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.version_examples)

    def get_last_version(self):
        current_path = os.path.dirname(os.path.abspath(__file__))
        #sofi
        git_base_sofi = current_path
        repo = Repo(git_base_sofi)
        assert not repo.bare
        origin = repo.remotes.origin
        origin.fetch()
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if len(tags) > 0:
            self.remote_tags_sofi = tags
            #self.print_result("remote sofi task generator version {}".format(tags[-1]))
            self.remote_version_sofi = str(tags[-1])
            self.delete_remote_tags_from_local(repo, self.tags_sofi, self.remote_tags_sofi)
        #beremiz
        git_base_beremiz = current_path + "/../../beremiz/"
        repo = Repo(git_base_beremiz)
        assert not repo.bare
        origin = repo.remotes.origin
        origin.fetch()
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if tags:
            self.remote_tags_beremiz = tags
            #self.print_result("remote beremiz - last version {}".format(tags[-1]))
            self.remote_version_beremiz = str(tags[-1])
            self.delete_remote_tags_from_local(repo, self.tags_beremiz, self.remote_tags_beremiz)
        #matiec
        git_base_matiec = current_path + "/../../matiec/"
        repo = Repo(git_base_matiec)
        assert not repo.bare
        origin = repo.remotes.origin
        origin.fetch()
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if tags:
            self.remote_tags_matiec = tags
            #self.print_result("remote matiec - last version {}".format(tags[-1]))
            self.remote_version_matiec = str(tags[-1])
            self.delete_remote_tags_from_local(repo, self.tags_matiec, self.remote_tags_matiec)
        #exaples
        git_base_examples = current_path + "/../../bric_examples/"
        repo = Repo(git_base_examples)
        assert not repo.bare
        origin = repo.remotes.origin
        origin.fetch()
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if tags:
            self.remote_tags_examples = tags
            #self.print_result("remote bric examples - last version {}".format(tags[-1]))
            self.remote_version_examples = str(tags[-1])
            self.delete_remote_tags_from_local(repo, self.tags_examples, self.remote_tags_examples)

    def update_to_last(self):
        self.get_current_version()
        self.get_last_version()
        #sofi
        current_path = os.path.dirname(os.path.abspath(__file__))
        if self.compare_version(self.version_sofi, self.remote_version_sofi):
            git_base_path = current_path
            repo = Repo(git_base_path)
            origin = repo.remotes.origin
            origin.fetch()
            self.print_result("sofi update to {}".format(self.remote_version_sofi))
            origin.pull()
            #SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\sofi_task_generator", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.remote_version_sofi)
        else:
            self.print_result("current sofi version is last: {}".format(self.version_sofi))
        #beremiz
        if self.compare_version(self.version_beremiz, self.remote_version_beremiz):
            git_base_path = current_path + "/../../beremiz/"
            repo = Repo(git_base_path)
            origin = repo.remotes.origin
            origin.fetch()
            self.print_result("beremiz update to{}".format(self.remote_version_beremiz))
            origin.pull()
            #SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\beremiz", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.remote_version_beremiz)
        else:
            self.print_result("current beremiz version is last: {}".format(self.version_beremiz))
        #matiec
        if self.compare_version(self.version_matiec, self.remote_version_matiec):
            git_base_path = current_path + "/../../matiec/"
            repo = Repo(git_base_path)
            origin = repo.remotes.origin
            origin.fetch()
            self.print_result("matiec update to{}".format(self.remote_version_matiec))
            origin.pull()
            #SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\matiec", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.remote_version_matiec)
        else:
            self.print_result("current matiec version is last: {}".format(self.version_matiec))
        #examples
        if self.compare_version(self.version_matiec, self.remote_version_examples):
            git_base_path = current_path + "/../../bric_examples/"
            repo = Repo(git_base_path)
            origin = repo.remotes.origin
            origin.fetch()
            self.print_result("bric examples update to{}".format(self.remote_version_examples))
            origin.pull()
            #SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\beremiz_examples", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.remote_version_examples)
        else:
            self.print_result("current bric examples version is last: {}".format(self.version_examples))

    def change_os_version(self):
        if self.version_sofi_os:
            current_path = os.path.dirname(os.path.abspath(__file__))
            self.print_result("current os version {}".format(self.version_sofi_os))
            version = str(self.version_sofi_os)
            version = version.replace("os", "")
            version = version.replace(".", ",")
            str_before = "#define\s+OS_VERSION\s+\{[\d\,]+\}"
            str_to = "#define OS_VERSION {" + version + "}"
            print(str_before, str_to)

    def compare_version(self, local, remote):
        """return 1 if last version larger them current or 0"""
        result = 0
        if len(local) and len(remote):
            current = local.replace("v", "")
            current = [int(numeric_string) for numeric_string in current.split(".")]
            last = remote.replace("v", "")
            last = [int(numeric_string) for numeric_string in last.split(".")]
            if len(current) >= 4 and len(last) >= 4:
                if current[0] < last[0]:
                    result = 1
                elif current[0] == last[0]:
                    if current[1] < last[1]:
                        result = 1
                    elif current[1] == last[1]:
                        if current[2] < last[2]:
                            result = 1
                        elif current[2] == last[2]:
                            if current[3] < last[3]:
                                result = 1
            else:
                self.print_error('version len mismatch {} {}'.format(len(current), len(last)))
        else:
            self.print_error('version did not get before')
        return result

    def delete_remote_tags_from_local(self, repo, tags_local, tags_remote):
        if len(tags_local) and len(tags_remote):
            for i in range(1, len(tags_local)):
                if self.compare_version(str(tags_local[-1]), str(tags_remote[-i])):
                    repo.delete_tag(tags_remote[-i])
                else:
                    break

    def write_current_to_winreg(self):
        beremiz_key     = OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\beremiz", 0, KEY_ALL_ACCESS)
        beremiz_ex_key  = OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\beremiz_examples", 0, KEY_ALL_ACCESS)
        matiec_key      = OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\matiec", 0, KEY_ALL_ACCESS)
        sofi_key        = OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\sofi_task_generator", 0, KEY_ALL_ACCESS)
    
        '''beremiz_ver     = QueryValueEx(beremiz_key, "Ver")[0]
        beremiz_ex_ver  = QueryValueEx(beremiz_ex_key, "Ver")[0]
        matiec_ver      = QueryValueEx(matiec_key, "Ver")[0]
        sofi_ver        = QueryValueEx(sofi_key, "Ver")[0]

        beremiz_ver+=".1"
        beremiz_ex_ver+=".1"
        matiec_ver+=".1"
        sofi_ver+=".1"
        
        print("beremiz_ver: "+beremiz_ver)
        print("beremiz_ex_ver: "+beremiz_ex_ver)
        print("matiec_ver: "+matiec_ver)
        print("sofi_ver: "+sofi_ver)'''

        SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\beremiz", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.version_beremiz)
        SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\beremiz_examples", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.version_examples)
        SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\matiec", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.version_matiec)
        SetValueEx(OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE\\Beremiz-BRIC\\sofi_task_generator", 0, KEY_ALL_ACCESS), "Ver", 0, REG_SZ, self.version_sofi)

def main():
    current_path = os.path.dirname(os.path.abspath(__file__))
    git_base_path = current_path
    print(git_base_path)
    repo = Repo(git_base_path)
    assert not repo.bare
    tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
    print("current version {}".format(tags[-1]))

    Updater.write_current_to_winreg(base_object.FREERTOS_OS)

if __name__ == "__main__":
    main()
